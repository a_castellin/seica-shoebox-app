/*
 * app.js
 * Defines the core of the application during initialization.
 *
 * Manuel Minopoli - INTAG S.r.l.
 * Ferdinando Celentano
 */

angular
.module('starter', [
    'ionic',
    'ngCordova',
    'pascalprecht.translate',
    'starter.controllers',
    'starter.services',
    'ui.router',
    'templates'
]).run(['$ionicPlatform', '$translate', 'database', function($ionicPlatform, $translate, database){
    $ionicPlatform.ready(function(){
        // Database
        database.connect();
        database.init();

        // Keyboard
        if(window.cordova && window.cordova.plugins.Keyboard){
            cordova.plugins.Keyboard.disableScroll(true);
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }

        // Language
        if(typeof navigator.globalization !== 'undefined'){
            navigator.globalization.getPreferredLanguage(function(language){
                $translate
                  .use((language.value).split('-')[0])
                  .then(function(data){}, function(error){});
            }, null);
        }

        // Status bar
        if(window.StatusBar){
            StatusBar.styleDefault();
        }
    });
}]).config(['$ionicConfigProvider', '$stateProvider', '$translateProvider', '$urlRouterProvider', function($ionicConfigProvider, $stateProvider, $translateProvider, $urlRouterProvider){
    // Routing
    $stateProvider.state('app', {
        abstract: true,
        templateUrl: 'menu.html',
        url: '/app'
    }).state('app.configuration', {
        url: '/configuration',
        views: {
            'menuContent': {
                templateUrl: 'configuration.html'
            }
        }
    }).state('app.dashboard', {
        url: '/dashboard',
        views: {
            'menuContent': {
                templateUrl: 'dashboard.html'
            }
        }
    }).state('app.initialization', {
        url: '/initialization',
        views: {
            'menuContent': {
                controller: 'InitializationController',
                templateUrl: 'initialization.html'
            }
        }
    }).state('app.scene/active/:id', {
        url: '/scene/active/:id',
        views: {
            'menuContent': {
                templateUrl: 'scene/active.html'
            }
        }
    }).state('app.scene/add/:service/:room', {
        url: '/scene/add/:service/:room',
        views: {
            'menuContent': {
                templateUrl: 'scene/add.html'
            }
        }
    }).state('app.scene/add/detail/:id', {
        url: '/scene/add/detail/:id',
        views: {
            'menuContent': {
                templateUrl: 'scene/addDetail.html'
            }
        }
    }).state('app.sensor', {
        url: '/sensor',
        views: {
            'menuContent': {
                templateUrl: 'sensor/list.html'
            }
        }
    }).state('app.scene/create/:service/:room', {
        url: '/scene/create/:service/:room',
        views: {
            'menuContent': {
                templateUrl: 'scene/create.html'
            }
        }
    }).state('app.service/:service', {
        url: '/service/:service',
        views: {
            'menuContent': {
                templateUrl: 'service/list.html'
            }
        }
    }).state('app.service/detail/:service/:room', {
        url: '/service/detail/:service/:room',
        views: {
            'menuContent': {
                templateUrl: 'service/detail.html'
            }
        }
    }).state('app.service/timer/:service/:room/:element', {
        url: '/service/timer/:service/:room/:element',
        views: {
            'menuContent': {
                templateUrl: 'service/timer.html'
            }
        }
    });

    $urlRouterProvider.otherwise('/app/initialization');

    // Translations
    $translateProvider.translations('en', {
        alert_button: 'OK',
        alert_buttonCancel: 'Cancel',
        alert_buttonNo: 'No',
        alert_buttonSave: 'Save',
        alert_buttonYes: 'Yes',
        alert_configurationError: 'Error during saving data.',
        alert_configurationSuccess: 'Data saved successfully.',
        alert_configurationTitle: 'Configuration',
        alert_connectionError: 'Error during connection: change configuration.',
        alert_connectionTitle: 'Connection',
        alert_noConfigurationError: 'To access the services, set correctly at least one configuration.',
        alert_noConfigurationTitle: 'Configuration',
        alert_sceneActiveConfirm: 'Active the selected scene?',
        alert_sceneActiveTitle: 'Scene',
        alert_sceneActivatedSuccess: 'Scene activated successfully.',
        alert_sceneActivatedTitle: 'Scene',
        alert_sceneAddedSuccess: 'Association added to scene successfully.',
        alert_sceneAddedTitle: 'Scene',
        alert_sceneCreateError: 'Error during saving data.',
        alert_sceneCreateSuccess: 'Data saved successfully.',
        alert_sceneCreateTitle: 'Scene',
        alert_sceneDeactivatedSuccess: 'Scene deactivated successfully.',
        alert_sceneDeactivatedTitle: 'Scene',
        alert_serviceError: 'Service not configured: contact supplier.',
        alert_serviceTitle: 'Service',
        alert_timerMessage: 'Set time in seconds.',
        alert_timerTitle: 'Timer',
        alert_unavailableService: 'Unavailable service.',
        alert_noResponseFromCU: 'No answer received from che control unit. The command may not have reached the system.',
        configuration_api: 'API Key',
        configuration_cloud: 'CLOUD',
        configuration_csid: 'Control System ID',
        configuration_csp: 'Control System Password',
        configuration_disclaimerSummary: 'Put here the text for the disclaimer message, the same for all the configuration typologies.',
        configuration_disclaimerTitle: 'Disclaimer',
        configuration_hostCloud: 'Cloud Server IP address',
        configuration_hostLocal: 'Local IP address',
        configuration_hostRemote: 'Remote IP address',
        configuration_lan: 'LAN',
        configuration_name: 'Name',
        configuration_port: 'Port',
        configuration_save: 'Save',
        configuration_title: 'Configuration',
        configuration_wan: 'WAN',
        dashboard_consumes: 'Consumes',
        dashboard_lights: 'Lights',
        dashboard_rollingShutters: 'Rolling shutters',
        dashboard_scenes: 'SCENES',
        dashboard_sensors: 'Sensors',
        dashboard_services: 'SERVICES',
        dashboard_thermostatus: 'Thermostatus',
        dashboard_title: 'Home',
        initialization_configuration: 'Reading configurations',
        initialization_connection: 'Checking connection',
        initialization_data: 'Connected, reading device data',
        menu_configuration: 'Configuration',
        menu_notification: 'Notifications',
        menu_title: 'Menu',
        network_offline: 'No available connection, active and select one from Settings.',
        scene_active: 'Active/Deactive',
        scene_add: 'Add',
        scene_addTitle: 'Add to scene',
        scene_create: 'New scene',
        scene_createIcon: 'Choose icon',
        scene_createName: 'Name',
        scene_createTitle: 'New scene',
        scene_settings: 'Settings',
        scene_title: 'Scenes',
        sensor_services: 'Services',
        sensor_title: 'Sensors',
        service_consumes: 'Consumes',
        service_light: 'Lights',
        service_rollingShutter: 'Rolling shutters',
        service_rooms: 'Rooms',
        service_sensor: 'Sensors',
        service_thermostatus: 'Thermostatus',
        serviceDetail_general: 'General',
        timer_post: 'Post-Time',
        timer_pre: 'Pre-Time',
        timer_seconds: 'seconds',
        timer_title: 'Timer'
    });

    $translateProvider.translations('it', {
        alert_button: 'OK',
        alert_buttonCancel: 'Annulla',
        alert_buttonNo: 'No',
        alert_buttonSave: 'Salva',
        alert_buttonYes: 'Si',
        alert_configurationError: 'Errore durante il salvataggio.',
        alert_configurationSuccess: 'Dati salvati correttamente.',
        alert_configurationTitle: 'Configurazione',
        alert_connectionError: 'Errore durante la connessione: cambiare la configurazione.',
        alert_connectionTitle: 'Connessione',
        alert_noConfigurationError: 'Per accedere ai servizi, impostare in modo corretto almeno una configurazione.',
        alert_noConfigurationTitle: 'Configurazione',
        alert_sceneActiveConfirm: 'Attivare lo scenario selezionato?',
        alert_sceneActiveTitle: 'Scenario',
        alert_sceneActivatedSuccess: 'Scenario attivato correttamente.',
        alert_sceneActivatedTitle: 'Scenario',
        alert_sceneAddedSuccess: 'Associazione aggiunta allo scenario correttamente.',
        alert_sceneAddedTitle: 'Scenario',
        alert_sceneCreateError: 'Errore durante il salvataggio.',
        alert_sceneCreateSuccess: 'Dati salvati correttamente.',
        alert_sceneCreateTitle: 'Scenario',
        alert_sceneDeactivatedSuccess: 'Scenario disattivato correttamente.',
        alert_sceneDeactivatedTitle: 'Scenario',
        alert_serviceError: 'Servizio non configurato: contattare il fornitore.',
        alert_serviceTitle: 'Servizi',
        alert_timerMessage: 'Imposta il tempo in secondi.',
        alert_timerTitle: 'Timer',
        alert_unavailableService: 'Servizio non disponibile.',
        alert_noResponseFromCU: 'Nessuna risposta dalla centralina. Il comando potrebbe non essere stato recepito.',
        configuration_api: 'API Key',
        configuration_cloud: 'CLOUD',
        configuration_csid: 'Control System ID',
        configuration_csp: 'Control System Password',
        configuration_disclaimerSummary: 'Inserire qui il messaggio per il discalimer, lo stesso per tutte le tipologie di configurazione.',
        configuration_disclaimerTitle: 'Disclaimer',
        configuration_hostCloud: 'Indirizzo IP Cloud Server',
        configuration_hostLocal: 'Indirizzo IP locale',
        configuration_hostRemote: 'Indirizzo IP remoto',
        configuration_lan: 'LAN',
        configuration_name: 'Nome',
        configuration_port: 'Porta',
        configuration_save: 'Salva',
        configuration_title: 'Configurazione',
        configuration_wan: 'WAN',
        dashboard_consumes: 'Consumi',
        dashboard_lights: 'Luci',
        dashboard_rollingShutters: 'Tapparelle',
        dashboard_scenes: 'SCENARI',
        dashboard_sensors: 'Sensori',
        dashboard_services: 'SERVIZI',
        dashboard_thermostatus: 'Termostato',
        dashboard_title: 'Home',
        initialization_configuration: 'Lettura delle configurazioni',
        initialization_connection: 'Verifica della connessione',
        initialization_data: 'Connesso, lettura della centralina',
        menu_configuration: 'Configurazione',
        menu_notification: 'Notifiche',
        menu_title: 'Menu',
        network_offline: 'Nessuna connessione disponibile, attiva e selezionane una da Impostazioni.',
        scene_active: 'Attiva/Disattiva',
        scene_add: 'Aggiungi',
        scene_addTitle: 'Aggiungi a scenario',
        scene_create: 'Nuovo scenario',
        scene_createIcon: 'Scegli icona',
        scene_createName: 'Nome',
        scene_createTitle: 'Nuovo scenario',
        scene_settings: 'Impostazioni',
        scene_title: 'Scenari',
        sensor_services: 'Servizi',
        sensor_title: 'Sensori',
        service_consumes: 'Consumi',
        service_light: 'Luci',
        service_rollingShutter: 'Tapparelle',
        service_rooms: 'Stanze',
        service_sensor: 'Sensori',
        service_thermostatus: 'Termostato',
        serviceDetail_general: 'Generale',
        timer_post: 'Post-Time',
        timer_pre: 'Pre-Time',
        timer_seconds: 'secondi',
        timer_title: 'Timer'
    });

    $translateProvider.preferredLanguage('it');
    
    $translateProvider.fallbackLanguage('en');
    
    // Various
    $ionicConfigProvider.backButton.previousTitleText(false).text('');
    
    $ionicConfigProvider.views.maxCache(0);
}]);