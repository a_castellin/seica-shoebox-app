/*
 * controller.js
 * Defines the controllers for application.
 *
 * Manuel Minopoli - INTAG S.r.l.
 * Ferdinando Celentano
 */

angular
.module('starter.controllers', [])
.controller('InitializationController', ['$scope', '$rootScope', '$q', '$cordovaNetwork', '$cordovaDialogs', '$filter', '$ionicPlatform', '$state', 'database', 'controlSystem', function($scope, $rootScope, $q, $cordovaNetwork, $cordovaDialogs, $filter, $ionicPlatform, $state, database, controlSystem){
    var $translate = $filter('translate'),
        connect = function(connections){
            return $q(function(resolve, reject){
                var connection = connections.shift(),
                    tryNext = function(){
                        console.log('connection error');
                        connect(connections).then(function(data){
                            resolve(data);
                        }).catch(function(err){
                            reject(err);
                        });
                    };

                if(typeof connection !== 'undefined'){
                    $scope.connectionType = connection.type;
                    controlSystem.connectionInit(connection.host, connection.port)
                    .then(function(){
                        controlSystem.connect(connection.type,connection.api, connection.password)
                        .then(function(){
                            resolve("Connected to " + connection.type);
                        })
                        .catch(function(err){
                            tryNext();
                        });
                    })
                    .catch(function(err){
                        tryNext();
                    });
                }else{
                    console.log('No more connections');
                    reject(Error("No more connections"));
                }                           
            });
        };

    $scope.loadingMessage = $translate("initialization_configuration");
    $scope.connectionType = '';

    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
        $state.reload();
    });

    $scope.$on('$ionicView.afterEnter', function(event, data){
        $ionicPlatform.ready(function(){
            var connectionError = function(){
            	$cordovaDialogs.alert(
                	$translate('alert_connectionError'),
                	$translate('alert_connectionTitle'),
                	$translate('alert_button')
                );

                $state.go('app.configuration');
            };
            
            // Checks configuration
            database
            .execute('SELECT * FROM configuration WHERE password IS NOT NULL AND password <> \'\'')
            .then(function(result){
            	var resultLength = result.rows.length;
                if(!resultLength){
                    $state.go('app.configuration');
                    
                    $cordovaDialogs.alert(
                    	$translate('alert_noConfigurationError'),
                    	$translate('alert_noConfigurationTitle'),
                    	$translate('alert_button')
                    );
                }else{
                	var connections = [];
                		
                	for(var i=0;i<resultLength;++i){
                		connections.push(result.rows.item(i));
                	}

                	connections.sort(function(a, b){
						switch(a.type){
							case 'lan': return -1;
							case 'wan': return b.type==='lan' ? 1 : -1;
							case 'cloud': return 1;
						}
					});

                	$scope.loadingMessage = $translate("initialization_connection");
                	connect(connections).then(function(){
                		$scope.loadingMessage = $translate("initialization_data");
                		controlSystem.init().then(function(response){
	                		$state.go('app.dashboard');
	                	})
                		.catch(function(err){
	                		connectionError();
                			controlSystem.disconnect();
	                	});
                	}).catch(function(err){
                		connectionError();
                		controlSystem.disconnect();
                	});
                }
            }, function(error){
                alert(error);
            });
        });
    });
}]).controller('ConfigurationController', ['$scope', '$cordovaDialogs', '$filter', '$state', 'database', function($scope, $cordovaDialogs, $filter, $state, database){
    var $translate = $filter('translate');
    $scope.$on('$ionicView.afterEnter', function(event, data){});
    
    // Defines scopes
    $scope.load = function(type){
        // Manages views
        $('#cloudContainer').hide();
        $('#lanContainer').hide();
        $('#wanContainer').hide();

        $('#cloudLabel').removeClass('active');
        $('#lanLabel').removeClass('active');
        $('#wanLabel').removeClass('active');

        $('#' + type + 'Container').show();
        $('#' + type + 'Label').addClass('active');

        // Loads data
        var query = 'SELECT * FROM configuration WHERE type = ?';
        
        database
        .execute(query, [type])
        .then(function(result){
            if(result.rows.length > 0) {
                // Gets data
                if(type === 'cloud'){
                	$('#' + type + 'Api').val(result.rows.item(0).api);
                }
                $('#' + type + 'Host').val(result.rows.item(0).host);
                $('#' + type + 'Name').val(result.rows.item(0).name);
                $('#' + type + 'Password').val(result.rows.item(0).password);
                $('#' + type + 'Port').val(result.rows.item(0).port);
            }
        }, function(error){
            // Error
            $cordovaDialogs.alert(
            	$translate('alert_configurationError'),
            	$translate('alert_configurationTitle'),
            	$translate('alert_button')
            );
        });
    };

    $scope.save = function(type){
        // Saves data
        var query = 'SELECT id FROM configuration WHERE type = ?';
        
        database.
        execute(query, [type]).
        then(function(result){
            if(result.rows.length > 0){
                // Update
                database
                .execute('UPDATE configuration SET api = ?, host = ?, name = ?, password = ?, port = ?, type = ? WHERE id = ?', [
                	$('#' + type + 'Api').val(),
           			$('#' + type + 'Host').val(),
           			$('#' + type + 'Name').val(),
           			$('#' + type + 'Password').val(),
           			$('#' + type + 'Port').val(),
           			type,
           			result.rows.item(0).id]
           		).then(function(result){
                    // Confirm
                    $cordovaDialogs.alert(
                    	$translate('alert_configurationSuccess'),
                    	$translate('alert_configurationTitle'),
                    	$translate('alert_button')
                    ).then(function(){
                        // Reloads configuration by initialization
                        $state.go('app.initialization');
                    });
                }, function(error){
                    // Error
                    $cordovaDialogs.alert(
                    	$translate('alert_configurationError'),
                    	$translate('alert_configurationTitle'),
                    	$translate('alert_button')
                    );
                });
            }else{
                // Insert                
                database
                .execute('INSERT INTO configuration ( api, host, name, password, port, type ) VALUES  ( ?, ?, ?, ?, ?, ? )', [
                	$('#' + type + 'Api').val(),
           			$('#' + type + 'Host').val(),
           			$('#' + type + 'Name').val(),
                    $('#' + type + 'Password').val(),
                    $('#' + type + 'Port').val(),
                    type]
                ).then(function(result){
                    // Confirm
                    $cordovaDialogs.alert(
                    	$translate('alert_configurationSuccess'),
                    	$translate('alert_configurationTitle'),
                    	$translate('alert_button')
                    ).then(function(){
                        // Reloads configuration by initialization
                        $state.go('app.initialization');
                    });
                }, function(error){
                    // Error
                    $cordovaDialogs.alert(
                    	$translate('alert_configurationError'),
                    	$translate('alert_configurationTitle'),
                    	$translate('alert_button')
                    );
                });
            }
        }, function(error){
            // Error
            $cordovaDialogs.alert(
            	$translate('alert_configurationError'),
            	$translate('alert_configurationTitle'),
            	$translate('alert_button')
            );
        });
    };

    $scope.verify = function(){
        // Checks configuration status
        if(login !== null){
        	$state.go('app.dashboard');
        }
    };
}]).controller('DashboardController', ['$scope', '$cordovaDialogs', '$filter', '$state', 'controlSystem', 'database', function($scope, $cordovaDialogs, $filter, $state, controlSystem, database){
    var $translate = $filter('translate');

    // Defines scopes
    $scope.panelOpen = function(name){
    	//TODO: remove to re-enable Scenes
    	if(name === "scene"){
    		return;
    	}

        // Manages views
        $('#tabScene').removeClass('active');
        $('#tabService').removeClass('active');

        $('#panelScene').addClass('hide');
        $('#panelService').addClass('hide');

        $('#tab' + name.charAt(0).toUpperCase() + name.substr(1)).addClass('active');
        $('#panel' + name.charAt(0).toUpperCase() + name.substr(1)).removeClass('hide');
    };

    $scope.sceneLoad = function(){
    	//TODO: remove to re-enable Scenes
    	if(true){
    		return;
    	}

        $('#scenes').html('');

        var query = 'SELECT * FROM scene';

        database
        .execute(query)
        .then(function(result){
            var content = '',
                i;

            for(i = 0; i < result.rows.length; i++){
                if(i % 2 === 0){
                	content = content + '<div align = \"center\" class = \"container-service-padding-no row\">';
                }

                content = content + '<div align = \"center\" class = \"col-50 sceneItem\" id = \"scene-' + result.rows.item(i).id + '\">' + '<div class = \"card ' + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + '\">' + '<div class = \"item item-height item-text-wrap\">' + '<div class = \"item-icon-container\">' + '<div class = \"row container-thermostatus-data\">' + '<div align = \"left\" class = \"col-50\"></div>' + '<div align = \"right\" class = \"col-50\">' + ((result.rows.item(i).active === 'true') ? '<i class = \"icon ion-ios-checkmark-outline\" style = \"color: #6ebb1f; font-size: 24px !important\"></i>' : '') + '</div>' + '</div>' + '<img height = \"40\" src = \"img/scene/' + result.rows.item(i).icon + '.svg\" />' + '<p class = \"item-icon-font\"><br />' + result.rows.item(i).name + '</p>' + '</div>' + '</div>' + '</div>' + '</div>';

                if(i % 2 !== 0){
                	content = content + '</div>';
                }
            }

            if(i % 2 !== 0){
            	content = content + '</div>';
            }

            $('#scenes').replaceWith(content);
            
            // Defines events
            $('.sceneItem').click(function(){
                var id = $(this).attr('id').replace('scene-', '');
                
                $state.go('app.scene/active/:id', {
                    'id': id
                });
            });
        }, function(error){
            // Error
            $cordovaDialogs.alert(
            	$translate('alert_configurationError'),
            	$translate('alert_configurationTitle'),
            	$translate('alert_button')
            );
        });
    };

    $scope.serviceOpen = function(name){
        if(controlSystem.serviceExist(name)){
        	$state.go('app.service/:service', {
	            'service': name
	        });
        }else{
        	$cordovaDialogs.alert(
        		$translate('alert_serviceError'),
        		$translate('alert_serviceTitle'), 
        		$translate('alert_button')
        	);
        }
    };
}]).controller('SceneActiveController', ['$scope', '$cordovaDialogs', '$filter', '$ionicHistory', '$stateParams', 'database', function($scope, $cordovaDialogs, $filter, $ionicHistory, $stateParams, database){
    var $translate = $filter('translate');

    // Defines title
    database
    .execute('SELECT * FROM scene WHERE id = ?', [$stateParams.id])
    .then(function(result){
        $scope.navigationSubheader = result.rows.item(0).name;
        $('#active').prop('checked', (result.rows.item(0).active === 'true') ? true : false);
    });

    // Defines scopes
    $scope.active = function(){
        if($('#active').is(':checked')){
            // Update all
            database
            .execute('UPDATE scene SET active = ?', ['false'])
            .then(function(result){
                // Update selected scene               
                database
                .execute('UPDATE scene SET active = ? WHERE id = ?', [
                	'true',
           			$stateParams.id]
           		).then(function(result){
                    $cordovaDialogs.alert(
                    	$translate('alert_sceneActivatedSuccess'),
                    	$translate('alert_sceneActivatedTitle'),
                    	$translate('alert_button')
                    ).then(function(){
                        // Reloads scenes
                        $ionicHistory.goBack();
                    });
                });
            });
        }else{
            // Update selected scene
            query = 'UPDATE scene SET active = ? WHERE id = ?';
            
            database
            .execute(query, [
            	'false',
        		$stateParams.id]
        	).then(function(result){
                $cordovaDialogs.alert(
                	$translate('alert_sceneDeactivatedSuccess'),
                	$translate('alert_sceneDeactivatedTitle'),
                	$translate('alert_button')
                ).then(function(){
                    // Reloads scenes
                    $ionicHistory.goBack();
                });
            });
        }
    };
}]).controller('SceneAddController', ['$scope', '$filter', '$state', '$stateParams', 'database', function($scope, $filter, $state, $stateParams, database){
    var $translate = $filter('translate');

    // Defines titles
    $scope.navigationHeader = $filter('capitalize')($stateParams.room);
    
    var query = 'SELECT * FROM scene';
    
    database
    .execute(query)
    .then(function(result){
        var content = '';

        for(var i = 0; i < result.rows.length; i++){
            content = content + '<a class = \"color-gray item sceneAddItem\" id = \"scene-' + result.rows.item(i).id + '\">' + '<div class = \"row\" style = \"padding: 0px !important\">' + '<div class = \"col-10\">' + '<img height = \"20\" src = \"img/scene/' + result.rows.item(i).icon + '.svg\" />' + '</div>' + '<div class = \"col-90 color-gray\">' + result.rows.item(i).name + '</div>' + '</div>' + '</a>';
        }

        $('#scenesAdd').replaceWith(content);
        
        // Defines events
        $('.sceneAddItem').click(function(){
            var id = $(this).attr('id').replace('scene-', '');

            $state.go('app.scene/add/detail/:id', {
                'id': id
            });
        });
    }, function(error){
        // Error
        $cordovaDialogs.alert(
        	$translate('alert_configurationError'),
        	$translate('alert_configurationTitle'),
        	$translate('alert_button')
        );
    });

    // Defines scopes
    $scope.sceneCreate = function(){
        $state.go('app.scene/create/:service/:room', {
            'room': $stateParams.room,
            'service': $stateParams.service
        });
    };
}]).controller('SceneAddDetailController', ['$scope', '$cordovaDialogs', '$filter', '$ionicHistory', '$stateParams', 'database', function($scope, $cordovaDialogs, $filter, $ionicHistory, $stateParams, database){
    var $translate = $filter('translate');

    // Defines title
    var query = 'SELECT * FROM scene WHERE id = ?';

    database
    .execute(query, [$stateParams.id])
    .then(function(result){
        $scope.navigationSubheader = result.rows.item(0).name;
    });

    // Defines scopes
    $scope.add = function(){
        $cordovaDialogs.alert(
        	$translate('alert_sceneAddedSuccess'),
        	$translate('alert_sceneAddedTitle'),
        	$translate('alert_button')
        ).then(function(){
            // Reloads scenes
            $ionicHistory.
            goBack();
        });
    };
}]).controller('SceneCreateController', ['$scope', '$cordovaDialogs', '$filter', '$ionicHistory', '$state', '$stateParams', 'database', function($scope, $cordovaDialogs, $filter, $ionicHistory, $state, $stateParams, database){
    var $translate = $filter('translate');

    // Defines titles
    $scope.navigationHeader = $filter('capitalize')($stateParams.room);
    
    // Defines scopes
    $scope.iconChoose = function(name){
        $('#sceneIcon').
        val(name);
    };
    
    $scope.save = function(){
        // Insert
        var query = 'INSERT INTO scene ( active, icon, name, room, service, state ) VALUES  ( ?, ?, ?, ?, ?, ? )';
        
        database
        .execute(query, [
        	'false',
       		$('#sceneIcon').val(),
       		$('#sceneName').val(),
       		$stateParams.room,
       		$stateParams.service,
       		''
       	]).then(function(result){
            // Confirm
            $cordovaDialogs.alert(
            	$translate('alert_sceneCreateSuccess'),
            	$translate('alert_sceneCreateTitle'),
            	$translate('alert_button')
            ).then(function(){
                // Goes back
                $ionicHistory.goBack();
            });
        }, function(error){
            // Error
            $cordovaDialogs.alert(
            	$translate('alert_sceneCreateError'),
            	$translate('alert_sceneCreateTitle'),
            	$translate('alert_button')
            );
        });
    };
}]).controller('SensorController', ['$scope', '$cordovaDialogs', '$filter', '$state', 'controlSystem', function($scope, $cordovaDialogs, $filter, $state, controlSystem){
    var $translate = $filter('translate');

    // Defines scopes
    $scope.serviceOpen = function(name){
        if(controlSystem.serviceExist(name)){
        	$state.go('app.service/:service', {
	            'service': name
	        });
	    }else{ 
	    	$cordovaDialogs.alert(
	    		$translate('alert_serviceError'),
	    		$translate('alert_serviceTitle'),
	    		$translate('alert_button')
	    	);
        }
    };
}]).controller('ServiceController', ['$scope', '$filter', '$stateParams', 'controlSystem', function($scope, $filter, $stateParams, controlSystem){
	$scope.service = $stateParams.service;

	// Defines title
	$scope.navigationHeader = $filter('translate')('service_'+$scope.service);

    // Shows rooms
    $scope.rooms = controlSystem.roomNames($scope.service);
}]).controller('ServiceDetailController', ['$scope', '$rootScope', '$cordovaDialogs', '$filter', '$state', '$stateParams', 'controlSystem', function($scope, $rootScope, $cordovaDialogs, $filter, $state, $stateParams, controlSystem){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;

    // Defines titles
    $scope.navigationHeader = $filter('capitalize')($scope.room);

    $scope.navigationSubheader = $filter('translate')('service_'+$scope.service);

    $scope.element = controlSystem.element();
	$scope.roomComponents = controlSystem.roomComponents($scope.room).filter(function(comp){
		return comp.service === $scope.service;
	});
    
    $scope.generalSwitchUpdate = function(){
    	if($scope.service === "rollingShutter"){
    		return;
    	}
    	$scope.generalSwitch.value = $scope.roomComponents.some(function(comp){
    		return (comp.toggle && $scope.element[comp.toggle].value !== 0) ||
    			   (comp.dimmer && $scope.element[comp.dimmer].value !== 0);
    	});
    };

    $scope.generalSwitch = { value: false };
    $scope.generalSwitchUpdate();

    var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
		$scope.generalSwitchUpdate();
		$scope.$digest();
	});
  	$scope.$on('$destroy', asyncUpdateListener);

    $scope.update = function(id, value){
        controlSystem.setState(id,value).then(function(){
        	$scope.generalSwitchUpdate();
        }).catch(function(){
        	$cordovaDialogs.alert(
	    		$translate('alert_noResponseFromCU'),
	    		$translate('alert_serviceTitle'),
	    		$translate('alert_button')
	    	);
        });
    };

    $scope.updateAll = function(generalSwitchValue){
    	$scope.roomComponents.forEach(function(comp){
    		if(generalSwitchValue){
    			if(comp.toggle && $scope.element[comp.toggle].value !== 1){
    				$scope.update(comp.toggle,1);
    			}
    			if(comp.dimmer && $scope.element[comp.dimmer].value !== 255){
    				$scope.update(comp.dimmer,255);
    			}
    			if(comp.rollingShutterUp){
    				$scope.update(comp.rollingShutterUp,1);
    			}
    		}else{
    			if(comp.toggle && $scope.element[comp.toggle].value !== 0){
    				$scope.update(comp.toggle,0);
    			}
    			if(comp.dimmer && $scope.element[comp.dimmer].value !== 0){
    				$scope.update(comp.dimmer,0);
    			}
    			if(comp.rollingShutterDown){
    				$scope.update(comp.rollingShutterDown,1);
    			}
    		}
    	});
    };

    // Defines scopes
    $scope.sceneAdd = function(){
    	var $translate = $filter('translate');
    	$cordovaDialogs.alert(
    		$translate('alert_unavailableService'),
    		$translate('scene_title'),
    		$translate('alert_button')
    	);
        /*$state.go('app.scene/add/:service/:room', {
            'room': $stateParams.room,
            'service': $stateParams.service
        });*/
    };
}]).controller('ServiceTimerController', ['$scope', '$cordovaDialogs', '$filter', '$stateParams', 'controlSystem', function($scope, $cordovaDialogs, $filter, $stateParams, controlSystem){
    var $translate = $filter('translate');

    // Defines titles
    $scope.navigationHeader = $filter('capitalize')($stateParams.element);

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
		return comp.name === $stateParams.element;
	})[0] || {};

    if($scope.roomComponent.dimmer){
    	$scope.roomComponent.preTimeId = $scope.roomComponent.dimmer.replace('LO','PR');
		$scope.roomComponent.postTimeId = $scope.roomComponent.dimmer.replace('LO','PS');
    }else if($scope.roomComponent.toggle){
    	$scope.roomComponent.preTimeId = $scope.roomComponent.toggle.replace('TO','PR');
		$scope.roomComponent.postTimeId = $scope.roomComponent.toggle.replace('TO','PS');
    }

	$scope.preTime = $scope.element[$scope.roomComponent.preTimeId].configured;
	$scope.postTime = $scope.element[$scope.roomComponent.postTimeId].configured;

    // Defines scopes
    $scope.timerChange = function(type){
        $cordovaDialogs.prompt(
        	$translate('alert_timerMessage'),
        	$translate('alert_timerTitle'), [
        		$translate('alert_buttonCancel'),
        		$translate('alert_buttonSave')
        	],
        	''
        ).then(function(result){
        	var newValue = result.input1;

            if(result.buttonIndex === 2 && newValue !== $scope[type + 'Time']){
            	$scope[type + 'Time'] = newValue;
            	controlSystem.setState($scope.roomComponent[type + 'TimeId'],newValue);
            }
        });
    };
}]).controller('UtilityController', ['$scope', '$cordovaNetwork', '$ionicHistory', '$state', '$rootScope', 'controlSystem', function($scope, $cordovaNetwork, $ionicHistory, $state, $rootScope, controlSystem){
    // Network status
    $rootScope.isOnline = true;
    // listen for Online event
    var gotOnlineListener = $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
        console.log("got online");
        $rootScope.isOnline = true;
    });
    $scope.$on('$destroy', gotOnlineListener);
    // listen for Offline event
    var gotOfflineListener = $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
        console.log("got offline");
        $rootScope.isOnline = false;
    });
    $scope.$on('$destroy', gotOfflineListener);

    var asyncUpdateListener = $rootScope.$on('socketDisconnected', function (event, data){
		$scope.configuration();
	});
  	$scope.$on('$destroy', asyncUpdateListener);

    // Defines scopes
    $scope.back = function(){
        $ionicHistory.goBack();
    };
    $scope.dashboard = function(){
        $state.go('app.dashboard');
    };
    $scope.configuration = function(){
        if(controlSystem.isOnline()){
            controlSystem.disconnect();
        }
        $state.go('app.configuration');
    };
    $scope.notification = function(){
        $state.go('app.notfication');
    };
}]);