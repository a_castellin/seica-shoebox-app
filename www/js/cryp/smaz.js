/*
 * Function taken from the smaz compression library ported into javascript, strings are used
 * instead of arrays (when possible) and the Seica defined codebooks are used (smaz_cbs.js)
 */

angular.module("cryp.smaz",[])
.service("smaz", function(){

  /* Our compression codebook, used for compression */
  var smaz_cb = {
    "0" : 0,"1" : 1,"2" : 2,"3" : 3,"4" : 4,"5" : 5,"6" : 6,"7" : 7,"8" : 8,"9" : 9,"01" : 11,"02" : 12,
    "03" : 14,"04" : 15,"05" : 16,"06" : 17,"07" : 18,"08" : 19,"09" : 20,"10" : 21,"11" : 22,"12" : 23,
    "20" : 24,"30" : 25,"40" : 26,"50" : 27,"60" : 28,"70" : 29,"80" : 30,"90" : 31,"00" : 32,"0." : 33,
    "0.0" : 34,"get" : 35,"set" : 36,"state" : 37,"State" : 38,"sync" : 39,"Async" : 40,"config" : 41,
    "cfg" : 42,"login" : 43,"logout" : 44,"pwd" : 45,"time" : 46,"msgtype" : 47,"address" : 48,"object" : 49,
    "value" : 50,"last" : 51,"update" : 52,"bias" : 53,"weekday" : 54,"id" : 55,"apikey" : 56,"prior" : 57,
    "alarm" : 58,"tauto" : 59,"tmanu" : 60,"tinte" : 61,"texte" : 62,"green" : 63,"shutu" : 64,"shutd" : 65,
    "sqkey" : 66,"solar" : 67,"echooff" : 68,"service" : 69,"enable" : 70,"virtual" : 71,"Virtual" : 72,
    "device" : 73,"#" : 74,"Modbus" : 75,"seica" : 76,"RFS" : 77,"GVS" : 78,"TI" : 79,"TO" : 80,"LO" : 81,
    "/" : 82,"-" : 83,"+" : 84,"_" : 85,"@" : 86,"}" : 87,"}}" : 88,"}\"}" : 89,"{{" : 90,"\"}" : 91,"{\"" : 92,
    ":\"" : 93,"\":" : 94,"\",\"" : 95,"\":\"" : 96,":" : 97,"," : 98,"." : 99,"\"" : 100,"'" : 101,"<" : 102,
    ">" : 103,"<>" : 104,"</" : 105,"//" : 106,"//:" : 107,"a" : 108,"b" : 109,"c" : 110,"d" : 111,"e" : 112,
    "f" : 113,"g" : 114,"h" : 115,"i" : 116,"j" : 117,"k" : 118,"l" : 119,"m" : 120,"n" : 121,"o" : 122,"p" : 123,
    "q" : 124,"r" : 125,"s" : 126,"t" : 127,"u" : 128,"v" : 129,"w" : 130,"x" : 131,"y" : 132,"z" : 133,"A" : 134,
    "B" : 135,"C" : 136,"D" : 137,"E" : 138,"F" : 139,"G" : 140,"H" : 141,"I" : 142,"J" : 143,"K" : 144,"L" : 145,
    "M" : 146,"N" : 147,"O" : 148,"P" : 149,"Q" : 150,"R" : 151,"S" : 152,"T" : 153,"U" : 154,"V" : 155,"X" : 156,
    "Y" : 157,"Z" : 158,"of" : 159,"on" : 160,"and" : 161,"th" : 162,"in" : 163,"an" : 164,"er" : 165,"re" : 166,
    "is" : 167,"at" : 168,"or" : 169,"en" : 170,"es" : 171,"an" : 172,"nd" : 173,"ed" : 174,"for" : 175,"te" : 176,
    "ing" : 177,"ti" : 178,"st" : 179,"in" : 180,"ar" : 181,"nt" : 182,"to" : 183,"ng" : 184,"le" : 185,"al" : 186,
    "ou" : 187,"be" : 188,"se" : 189,"ha" : 190,"hi" : 191,"de" : 192,"me" : 193,"ve" : 194,"all" : 195,"ri" : 196,
    "ro" : 197,"co" : 198,"ea" : 199,"by" : 200,"di" : 201,"ra" : 202,"ic" : 203,"not" : 204,"at" : 205,"ce" : 206,
    "la" : 207,"ne" : 208,"as" : 209,"tio" : 210,"io" : 211,"we" : 212,"om" : 213,"ur" : 214,"li" : 215,"ll" : 216,
    "ch" : 217,"ere" : 218,"us" : 219,"ss" : 220,"ma" : 221,"one" : 222,"but" : 223,"el" : 224,"so" : 225,"no" : 226,
    "iv" : 227,"ho" : 228,"hat" : 229,"ns" : 230,"wh" : 231,"tr" : 232,"ut" : 233,"ly" : 234,"ta" : 235,"pe" : 236,
    "ass" : 237,"si" : 238,"wa" : 239,"fo" : 240,"rs" : 241,"domo" : 242,"DOMO" : 243,"SIG" : 244,"SENSOR" : 245,
    "PLUG" : 246,"V/I" : 247,"SHOEBOX" : 248,"CARRIER" : 249,"\n" : 250,"\r" : 251," " : 252,"  " : 253
  };

  /* Reverse compression codebook, used for decompression */
  var smaz_rcb = [
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "", "01", "02", 
    "", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "20", 
    "30", "40", "50", "60", "70", "80", "90", "00", "0.", "0.0", "get", "set", 
    "state", "State", "sync", "Async", "config", "cfg", "login", "logout", "pwd", "time", "msgtype", "address", 
    "object", "value", "last", "update", "bias", "weekday", "id", "apikey", "prior", "alarm", "tauto", "tmanu", 
    "tinte", "texte", "green", "shutu", "shutd", "sqkey", "solar", "echooff", "service", "enable", "virtual", "Virtual", 
    "device", "#", "Modbus", "seica", "RFS", "GVS", "TI", "TO", "LO", "/", "-", "+", 
    "_", "@", "}", "}}", "}\"}", "{{", "\"}", "{\"", ":\"", "\":", "\",\"", "\":\"", 
    ":", ",", ".", "\"", "'", "<", ">", "<>", "</", "//", "//:", "a", "b", "c", "d", "e", "f", "g", 
    "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", 
    "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", 
    "R", "S", "T", "U", "V", "X", "Y", "Z", "of", "on", "and", "th", "in", "an", "er", "re", "is", "at", 
    "or", "en", "es", "an", "nd", "ed", "for", "te", "ing", "ti", "st", "in", "ar", "nt", "to", "ng", "le", "al", 
    "ou", "be", "se", "ha", "hi", "de", "me", "ve", "all", "ri", "ro", "co", "ea", "by", "di", "ra", "ic", "not", 
    "at", "ce", "la", "ne", "as", "tio", "io", "we", "om", "ur", "li", "ll", "ch", "ere", "us", "ss", "ma", "one", 
    "but", "el", "so", "no", "iv", "ho", "hat", "ns", "wh", "tr", "ut", "ly", "ta", "pe", "ass", "si", "wa", "fo", 
    "rs", "domo", "DOMO", "SIG", "SENSOR", "PLUG", "V/I", "SHOEBOX", "CARRIER", "\n", "\r", " ", "  "
  ];

  function flush_verbatim(verbatim) {
    var k, output, _i, _len;
    output = [];
    if (verbatim.length > 1) {
      output.push(String.fromCharCode(255));
      output.push(String.fromCharCode(verbatim.length - 1));
    } else {
      output.push(String.fromCharCode(254));
    }
    for (_i = 0, _len = verbatim.length; _i < _len; _i++) {
      k = verbatim[_i];
      output.push(k);
    }
    return output;
  };

  this.compress = function(input){
    var code, encoded, input_index, j, output, verbatim, _i;
    verbatim = '';
    output = [];
    input_index = 0;
    while (input_index < input.length) {
      encoded = false;
      j = 7;
      if (input.length - input_index < 7) {
	j = input.length - input_index;
      }
      for (j = _i = j; j <= 0 ? _i < 0 : _i > 0; j = j <= 0 ? ++_i : --_i) {
	//code = smaz.codebook[input.substr(input_index, j)];
	code = smaz_cb[input.substr(input_index, j)];
	if (code != null) {
	  if (verbatim) {
	    output = output.concat(flush_verbatim(verbatim));
	    verbatim = '';
	  }
	  output.push(String.fromCharCode(code));
	  input_index += j;
	  encoded = true;
	  break;
	}
      }
      if (!encoded) {
	verbatim += input[input_index];
	input_index++;
	if (verbatim.length === 256) {
	  output = output.concat(flush_verbatim(verbatim));
	  verbatim = '';
	}
      }
    }
    if (verbatim) {
      output = output.concat(flush_verbatim(verbatim));
    }
    return output.join('');
  };

  this.decompress = function(str_input){
    var i, input, j, output, _i, _ref;
    output = '';
    input = (function() {
      var _i, _ref, _results;
      _results = [];
      for (i = _i = 0, _ref = str_input.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
	_results.push(str_input.charCodeAt(i));
      }
      return _results;
    })();
    i = 0;
    while (i < input.length) {
      if (input[i] === 254) {
	if (i + 1 > input.length) {
	  throw 'Malformed smaz';
	}
	output += str_input[i + 1];
	i += 2;
      } else if (input[i] === 255) {
	if (i + input[i + 1] + 2 >= input.length) {
	  throw 'Malformed smaz';
	}
	for (j = _i = 0, _ref = input[i + 1] + 1; 0 <= _ref ? _i < _ref : _i > _ref; j = 0 <= _ref ? ++_i : --_i) {
	  output += str_input[i + 2 + j];
	}
	i += 3 + input[i + 1];
      } else {
	//output += smaz.reverse_codebook[input[i]];
	output += smaz_rcb[input[i]];
	i++;
      }
    }
    return output;
  };
});
