/*
 * SeicaCypher.js : Defines the exported functions for the JS application. 
 */

angular.module("cryp.cipher",["cryp.smaz"])
.service("cipher", ["smaz", function(smaz){

  /*
   * This instance variable will just be created once and used as global variable
   * try to use it as a singleton
   */
  //this.newInstance = function(){
    this.pInstance = {
		KeyA               : 0x33,
		KeyB               : 0x44,
		FireTicket         : 0x00,
		PreviousFireTicket : 0x00,
		CriptoLevelSB      : 0x01,
		FireWall           : 0x00,
		CriptoLevel6000    : 0x01
	};
    //return pInstance;
  //};


  /*
  * Just used to generate a random number
  */
  function ReadCoreTimer(){
    //return GetTickCount();
    return Math.round((Math.random() * 0xff));
  }

  // ----------------------------------------------------------------------------//
  // Procedure Name   : cripto_compress                                          //
  // Description      : This function can compress and encript a string          //
  //                    optimized for JSON commands, There are 2 BYTE (KeyA KeyB)//
  //                    that permit the encription ( 16bit ) of the message      //
  //                    when Criptolevel is set to 1 ( true ).                   //
  // ----------------------------------------------------------------------------//
  //                                                                             //
  // Input       :      inStr        = input string to compress and encript      //
  // All other arguments are part of the Instance object (to allow modifications)//
  // here as CriptoLevel the CriptoLevel6000 field is used                       //
  // Input       :      Criptolevel  = 0 no encription , 1 encription performed  //
  //                                   ALL Devices use Criptolevel 1 when the    //
  //                                   KeyA and KeyB are know                    //
  //                                 - CLOUD when start comunication with        //
  //                                   CARRIER (port 6000)can't send message in  //
  //                                   criptolevel 1 and the CARRIER must accept //
  //                                   message in criptolevel 0 for 'getapikey'  //
  //                                   and 'gettime' request , when there is a   //
  //                                   login request CLOUD must send message     //
  //                                   in criptolevel 1 mode, coming from APP    //
  //                                   (port 8081).                              //
  // Input       :      KeyA         = first 8 bit of KEY                        //
  // Input       :      KeyB         = second 8 bit of KEY                       //
  // Input       :      FireWall     = 1 active, 0 inactive                      //
  //                                 - CARRIER use FireWall = 1 on port 8081     //
  //                                 - CARRIER use FireWall = 1 on port 6000     //
  //                                   exept for 'getapikey'                     //
  //                                   and 'gettime' request                     //
  //                                 - APP use FireWall = 0 on port 8081         //
  //                                 - CLOUD use FireWall = 0 on port 6000       //
  //                                 - CLOUD use FireWall = 1 on port 8081       //
  // Input/Output:      *FireTicket  = Global Variable used for encription       //
  //                                   and decription                            //
  //                                 - Input when FireWall is OFF                //
  //                                 - Output when FireWall is ON                //
  // Input/Output:*PreviousFireTicket= Global Variable used for encription       //
  //                                   and decription                            //
  //                                 - Input when FireWall is OFF                //
  //                                 - Output when FireWall is ON                //
  // Output      :                   = outStr encripted string                   //
  //                                                                             //
  // --------------------------------------------------------------------------- //
  //let CRC_smaz,RotoEncryptor,RotoSmaz, CypherSmaz,LoginRequest; //don't need to be static
  this.CriptoCompress = function(inStr){
      //static BYTE CRC_smaz,RotoEncryptor,RotoSmaz, CypherSmaz,LoginRequest;
      var CRC_smaz=0,RotoEncryptor,RotoSmaz, CypherSmaz,LoginRequest=0;
      var CriptoLevel = this.pInstance.CriptoLevel6000; //compress is using this level by default

      RotoEncryptor = ReadCoreTimer();  // generazione rotazione casuale

      //if ( RotoEncryptor == '\n' ||  RotoEncryptor == '\r' )
      if(RotoEncryptor == 10 ||  RotoEncryptor == 13)
	  RotoEncryptor++; // mai i caratteri terminatori (\n e \r)

	// rimozione /n/r al fondo
      //for (index_cripto=inlen-1;
      //    ((index_cripto > 0) && (inStr[index_cripto] == '\n' ||
      //	inStr[index_cripto] == '\r'));
      //    index_cripto--){
      //  inlen--;
      //  inStr[index_cripto]='\0'; // termina
      //}
      inStr=inStr.replace(/[\r\n]+$/,""); //trim all CR and LF in the end

      // calcolo CRC
      //CRC_smaz = 0;
      //for (index_cripto=0; index_cripto<inlen; index_cripto++)
      for(var i in inStr) //cycle all string
	CRC_smaz^=inStr.charCodeAt(i);

      //if ( inStr[0] == 'g' ){
	 //if( memcmp(&in[1],"etapikey",8 ) == 0 )
      // se con crc corretto arriva un comando di login allora e' valido altimenti no
      if(inStr.slice(0.9) == "getapikey") {
	 LoginRequest = 1;
	 CriptoLevel = 0;
      }
      //}

      if( CriptoLevel == 1){
	CypherSmaz = RotoEncryptor ^ this.pInstance.KeyA;
	CRC_smaz = CRC_smaz ^ this.pInstance.KeyB;
      } else {
	CypherSmaz = RotoEncryptor;
      }

      if ( !LoginRequest )                      // se non e' una login request
      {
	  if( this.pInstance.FireWall ) // se firewall attivo ( in RX ) genera codice di riconoscimento FireTicket
	  {
	    if ( this.pInstance.PreviousFireTicket == 0 )  // se in ricezione e' arrivato il FireTicket mandato in trasmissione ne genera uno nuovo e invalida quello vecchio
	    {
	      this.pInstance.PreviousFireTicket = this.pInstance.FireTicket; // memorizza il precedente prima di crearne uno nuovo
	      this.pInstance.FireTicket = ReadCoreTimer();  // generazione Ticket casuale se invalidato il precedente
	      if (this.pInstance.FireTicket == 0 ) // non deve mai essere 0
		  (this.pInstance.FireTicket)++; this.pInstance.FireTicket &= 0xff;
	      if (this.pInstance.FireTicket == this.pInstance.KeyB ) // non deve mai essere KeyB
		  (this.pInstance.FireTicket)++; this.pInstance.FireTicket &= 0xff;
	      if (this.pInstance.FireTicket == 0 ) // non deve mai essere 0
		  (this.pInstance.FireTicket)++; this.pInstance.FireTicket &= 0xff;
	    }
	  }
	  // altrimenti usa codice di riconoscimento FireTicket precedentemente letto in RX
	  CRC_smaz = CRC_smaz ^ this.pInstance.FireTicket ;
      }

      //inStr[index_cripto]= CRC_smaz;
      inStr=inStr.concat(String.fromCharCode(CRC_smaz));
      //inlen++; // aggiunta CRC

      //ReturnSmaz = smaz_compress(inStr,inlen,outStr,outlen ) ;
      outStr = smaz.compress(inStr);
      outArr = [];

      for (var ind=0 in outStr)  // ruota tutto
      {
	RotoSmaz = (outStr.charCodeAt(ind) + CypherSmaz) & 0xff;
	if ( RotoSmaz == 10 || RotoSmaz == 13 ) // carattere vietato
	  RotoSmaz += CypherSmaz; RotoSmaz &= 0xff;
	if ( RotoSmaz == 10 || RotoSmaz == 13 ) // carattere vietato  max 2 volte
	  RotoSmaz += CypherSmaz; RotoSmaz &= 0xff;
	if ( RotoSmaz == 10 || RotoSmaz == 13 ) // carattere vietato  max 2 volte
	  RotoSmaz += CypherSmaz; RotoSmaz &= 0xff;

	outArr[ind] = String.fromCharCode(RotoSmaz);
      }

      outArr.push(String.fromCharCode(RotoEncryptor)); //roto cripto value
      //ReturnSmaz++;

      if  ( CriptoLevel ){
	//outStr[ReturnSmaz] = '\r'; //cripto livello 1
	outArr.push('\r'); //cripto livello 1
	//ReturnSmaz++;
      }

      //in[inlen - 1]= '\0';
      //inlen--; // aggiunta CRC

      outArr.push('\n'); //terminatore
      //ReturnSmaz++;
      //outStr[ReturnSmaz] = '\0'; //terminatore

      //return ReturnSmaz;
      return outArr.join('');
  };

  // ----------------------------------------------------------------------------//
  // Procedure Name   : cripto_decompress                                        //
  // Description      : This function can decompress a encripted string          //
  //                    optimized for JSON commands, There are 2 BYTE (KeyA KeyB)//
  //                    that permit the decription ( 16bit ) of the message      //
  //                    and return Criptolevel to 1 when the message is encripted.//
  // ----------------------------------------------------------------------------//
  //                                                                             //
  // Input       :      inStr        = input string to decompress and decript    //
  // All other arguments are part of the Instance object (to allow modifications)//
  // here as CriptoLevel the CriptoLevelSB field is used                         //
  // Output      :      Criptolevel  = return 0 no encripted, 1 encripted string //
  //                                   CARRIER (on port 6000)can accept message  //
  //                                   in criptolevel 0 just for 'getapikey'     //
  //                                   and 'gettime' request.                    //
  // Input       :      KeyA         = first 8 bit of KEY                        //
  // Input       :      KeyB         = second 8 bit of KEY                       //
  // Input       :      FireWall     = 1 active, 0 inactive                      //
  //                                 - CARRIER use FireWall = 1 on port 8081     //
  //                                 - CARRIER use FireWall = 1 on port 6000     //
  //                                   exept for 'getapikey'                     //
  //                                   and 'gettime' request                     //
  //                                 - APP use FireWall = 0 on port 8081         //
  //                                 - CLOUD use FireWall = 0 on port 6000       //
  //                                 - CLOUD use FireWall = 1 on port 8081       //
  // Input/Output:      *FireTicket  = Global Variable used for encription       //
  //                                   and decription                            //
  //                                 - Input when FireWall is OFF                //
  //                                 - Output when FireWall is ON                //
  // Input/Output:*PreviousFireTicket= Global Variable used for encription       //
  //                                   and decription                            //
  //                                 - Input when FireWall is OFF                //
  //                                 - Output when FireWall is ON                //
  // Output      :                   = outStr dencripted string                  //
  //                                                                             //
  // --------------------------------------------------------------------------- //
  //let CRC_smaz,RotoDecriptor,RotoSmaz,CypherSmaz,FireTicketReceived; //FIXME do they really need to be static?
  this.CriptoDecompress = function(inStr){
      var CRC_smaz=0,RotoDecriptor,RotoSmaz,CypherSmaz,FireTicketReceived;
      //*CriptoLevel = 0;
      this.pInstance.CriptoLevelSB = 0; //use this here
	// check /n/r al fondo
      //for ( index_cripto=inlen-1;
      //    ((index_cripto > 0) && (inStr[index_cripto] == '\n' ||  inStr[index_cripto] == '\r'));
      //    index_cripto-- ){
      //  inlen--;
      //  if(inStr[index_cripto] == '\r' )  // *CriptoLevel
      //    pInstance.CriptoLevelSB = 1;

      //  //in[index_cripto]='\0'; // termina
      //}
      var init_len = inStr.length;
      if(inStr.match(/\r[\n]*$/)) //fires if CR in ending sequence (regardless of LF)
	  this.pInstance.CriptoLevelSB = 1;
      inStr=inStr.replace(/[\r\n]+$/,"");

      //if( index_cripto==0 )
      if( init_len==inStr.length ) //if it didn't trim any eol
	return null; // errore stringa non valida

      RotoDecriptor = inStr.charCodeAt(inStr.length-1);  // estrtazione chiave di derotazione
      //inlen--;
      //in[index_cripto] = '\0'; // trenima stringa
      inStr=inStr.slice(0,inStr.length-1);

      if( this.pInstance.CriptoLevelSB == 1)
	CypherSmaz = RotoDecriptor ^ this.pInstance.KeyA;
      else
	CypherSmaz = RotoDecriptor;

      inArr = [];

      for (var ind in inStr)  // anti ruota tutto
      {
	RotoSmaz = ( inStr.charCodeAt(ind) - CypherSmaz ) & 0xff;
	if ( RotoSmaz == 10 || RotoSmaz == 13 ) // carattere vietato
	  RotoSmaz -= CypherSmaz; RotoSmaz &= 0xff;
	if ( RotoSmaz == 10 || RotoSmaz == 13 ) // carattere vietato  max 2 volte
	  RotoSmaz -= CypherSmaz; RotoSmaz &= 0xff;
	if ( RotoSmaz == 10 || RotoSmaz == 13 ) // carattere vietato  max 2 volte
	  RotoSmaz -= CypherSmaz; RotoSmaz &= 0xff;

	inArr[ind] = String.fromCharCode(RotoSmaz);
      }

      //ReturnSmaz=smaz_decompress(inStr,inlen, outStr, outlen);
      outStr=smaz.decompress(inArr.join(''));

      //CRC_smaz = 0;
      for (var i=0;i<outStr.length-1;i++)
	  CRC_smaz^=outStr.charCodeAt(i);

      if( this.pInstance.CriptoLevelSB == 1)
      {
	CRC_smaz = CRC_smaz ^ this.pInstance.KeyB;
      }
  // la LOGIN se e' giusta passa il FIREWALL
      if ( this.pInstance.FireWall )
      {
	  if( CRC_smaz == outStr.charCodeAt(outStr.length-1))
	  {
	      if ( outStr[0] == 'g' )
	      {
		//if( memcmp(&out[1],"etapikey",8 ) == 0 )
		if(outStr.slice(1,9) == "etapikey") // se con crc corretto arriva un comando di login allora e' valido altimenti no
		{
		    //ReturnSmaz--; // stringa ok
		    //out[ReturnSmaz] ='\0';  // terminatore
		    return outStr.slice(0,outStr.length-1);
		    //return ReturnSmaz;
		}
		else
		{
		    //ReturnSmaz = 0; // invalida la stringa
		    //out[ReturnSmaz] ='\0';  // terminatore
		    return null;
		}
	      }
	      else
	      {
		  //ReturnSmaz = 0; // invalida la stringa
		  //out[ReturnSmaz] ='\0';  // terminatore
		  return null;
	      }
	  }

	  //FireTicketReceived = CRC_smaz ^ outStr.charCodeAt(index_cripto);
	  FireTicketReceived = CRC_smaz ^ outStr.charCodeAt(outStr.length-1);

	  if ( FireTicketReceived == this.pInstance.FireTicket )  // Risposta con il nuovo FireTicket Invalida il Vecchio  (se il FireTicket mandato e ricevuto sono UGUALI invalide quello precedente)
	  {
	      this.pInstance.PreviousFireTicket = 0;
	      //ReturnSmaz--; // stringa ok
	      //out[ReturnSmaz] ='\0';  // terminatore
	      //return ReturnSmaz;
	      return outStr.slice(0,outStr.length-1);
	  }
	  else if (  FireTicketReceived == this.pInstance.PreviousFireTicket && this.pInstance.PreviousFireTicket != 0 )  // Risposta con Vecchio FireTichet
	  {
	      //ReturnSmaz--; // stringa ok
	      //out[ReturnSmaz] ='\0';  // terminatore
	      //return ReturnSmaz;
	      return outStr.slice(0,outStr.length-1);
	  }
	  else
	  {
	      //ReturnSmaz = 0; // invalida la stringa
	      //out[ReturnSmaz] ='\0';  // terminatore
	      return null;
	  }
      }
      else
      {
	  this.pInstance.FireTicket = CRC_smaz ^ outStr.charCodeAt(outStr.length-1); // estrazione FireTicket da usare per i prossimi TX packet
	  //ReturnSmaz--; // stringa ok
	  //out[ReturnSmaz] ='\0';  // terminatore
	  //return ReturnSmaz;
	  return outStr.slice(0,outStr.length-1);
      }
  };

  // ------------------------------------------------------------------------- //
  // Procedure Name   : setKey                                                 //
  // Description      :                                                        //
  // ------------------------------------------------------------------------- //
  //                                                                           //
  // Invocation       : setKey();                                              //
  // Input            : ApyKey                                                 //
  // The following are modified as fields of pInstance                         //
  // Output           : KeyA                                                   //
  // Output           : KeyB                                                   //
  //                                                                           //
  // ------------------------------------------------------------------------- //


  //const unsigned int FLAC__crc16_table[256] = {
  const FLAC__crc16_table = [
      0x0000,  0x8005,  0x800f,  0x000a,  0x801b,  0x001e,  0x0014,  0x8011,
      0x8033,  0x0036,  0x003c,  0x8039,  0x0028,  0x802d,  0x8027,  0x0022,
      0x8063,  0x0066,  0x006c,  0x8069,  0x0078,  0x807d,  0x8077,  0x0072,
      0x0050,  0x8055,  0x805f,  0x005a,  0x804b,  0x004e,  0x0044,  0x8041,
      0x80c3,  0x00c6,  0x00cc,  0x80c9,  0x00d8,  0x80dd,  0x80d7,  0x00d2,
      0x00f0,  0x80f5,  0x80ff,  0x00fa,  0x80eb,  0x00ee,  0x00e4,  0x80e1,
      0x00a0,  0x80a5,  0x80af,  0x00aa,  0x80bb,  0x00be,  0x00b4,  0x80b1,
      0x8093,  0x0096,  0x009c,  0x8099,  0x0088,  0x808d,  0x8087,  0x0082,
      0x8183,  0x0186,  0x018c,  0x8189,  0x0198,  0x819d,  0x8197,  0x0192,
      0x01b0,  0x81b5,  0x81bf,  0x01ba,  0x81ab,  0x01ae,  0x01a4,  0x81a1,
      0x01e0,  0x81e5,  0x81ef,  0x01ea,  0x81fb,  0x01fe,  0x01f4,  0x81f1,
      0x81d3,  0x01d6,  0x01dc,  0x81d9,  0x01c8,  0x81cd,  0x81c7,  0x01c2,
      0x0140,  0x8145,  0x814f,  0x014a,  0x815b,  0x015e,  0x0154,  0x8151,
      0x8173,  0x0176,  0x017c,  0x8179,  0x0168,  0x816d,  0x8167,  0x0162,
      0x8123,  0x0126,  0x012c,  0x8129,  0x0138,  0x813d,  0x8137,  0x0132,
      0x0110,  0x8115,  0x811f,  0x011a,  0x810b,  0x010e,  0x0104,  0x8101,
      0x8303,  0x0306,  0x030c,  0x8309,  0x0318,  0x831d,  0x8317,  0x0312,
      0x0330,  0x8335,  0x833f,  0x033a,  0x832b,  0x032e,  0x0324,  0x8321,
      0x0360,  0x8365,  0x836f,  0x036a,  0x837b,  0x037e,  0x0374,  0x8371,
      0x8353,  0x0356,  0x035c,  0x8359,  0x0348,  0x834d,  0x8347,  0x0342,
      0x03c0,  0x83c5,  0x83cf,  0x03ca,  0x83db,  0x03de,  0x03d4,  0x83d1,
      0x83f3,  0x03f6,  0x03fc,  0x83f9,  0x03e8,  0x83ed,  0x83e7,  0x03e2,
      0x83a3,  0x03a6,  0x03ac,  0x83a9,  0x03b8,  0x83bd,  0x83b7,  0x03b2,
      0x0390,  0x8395,  0x839f,  0x039a,  0x838b,  0x038e,  0x0384,  0x8381,
      0x0280,  0x8285,  0x828f,  0x028a,  0x829b,  0x029e,  0x0294,  0x8291,
      0x82b3,  0x02b6,  0x02bc,  0x82b9,  0x02a8,  0x82ad,  0x82a7,  0x02a2,
      0x82e3,  0x02e6,  0x02ec,  0x82e9,  0x02f8,  0x82fd,  0x82f7,  0x02f2,
      0x02d0,  0x82d5,  0x82df,  0x02da,  0x82cb,  0x02ce,  0x02c4,  0x82c1,
      0x8243,  0x0246,  0x024c,  0x8249,  0x0258,  0x825d,  0x8257,  0x0252,
      0x0270,  0x8275,  0x827f,  0x027a,  0x826b,  0x026e,  0x0264,  0x8261,
      0x0220,  0x8225,  0x822f,  0x022a,  0x823b,  0x023e,  0x0234,  0x8231,
      0x8213,  0x0216,  0x021c,  0x8219,  0x0208,  0x820d,  0x8207,  0x0202
  ];

  //void setKey(char *ApiKey, BYTE *KeyA,BYTE *KeyB )  // LOGIN_String.apiKey
  this.SetKey = function(ApiKey){  // LOGIN_String.apiKey
    //unsigned int crc16 = 0;
    var crc16 = 0;

    for(var i in ApiKey){
      //crc16 = ((crc16<<8) ^ FLAC__crc16_table[(crc16>>8) ^ *ApiKey++]) & 0xffff;
      crc16 = ((crc16<<8) ^ FLAC__crc16_table[(crc16>>8) ^ ApiKey.charCodeAt(i)]) & 0xffff;
    }
    
    this.pInstance.KeyA = crc16 & 0xff; //needs to stay in a byte
    this.pInstance.KeyB = (crc16 >> 8) & 0xff; //likely to be useless
  };

}]);
