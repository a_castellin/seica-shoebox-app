/*
 * service.js
 * Defines the services needed by the application
 *
 * Ferdinando Celentano
 */

angular
.module('starter.services', [])
.filter('capitalize', function(){
    return function(s){
      return (angular.isString(s) && s.length > 0) ? s[0].toUpperCase() + s.substr(1).toLowerCase() : s;
    };
})
.factory("socketMessageEncode", function(){ 
    return function(data){
	    var buffer = '',
	        encode = new Uint8Array(data);

	    for(var i = 0; i < encode.length; i++){
	      buffer = buffer + String.fromCharCode(encode[i]);
	    }
	    
	    return buffer;
	};
})
.factory("socketMessagePrepare", function(){ 
    return function(data){
	    // Converts string to array
	    var buffer = new ArrayBuffer(data.length),
	        view = new Uint8Array(buffer);

	    for(var i = 0; i < data.length; i++){
	      view[i] = data.charCodeAt(i);
	    }

	    return buffer;
	};
})
.factory('semaphoreFactory', function($q){
	var Semaphore = function(val){
		var id = Date.now(),
			init = val && val>0 ? val : 1,
			value = init;
			queue = [];

		return {
			wait: function(){
				var defer = $q.defer();

				if(--value < 0){
					queue.push(defer);
				}else{
					defer.resolve(value);
				}

				console.log("Semaphore " + id + " -> wait (current value: " + value + ")");

			  	return defer.promise;
			},
			signal: function(){
				var defer = queue.shift();
				
				++value;

				if(value > init){
					value = init;
				}
					
				if(typeof defer !== "undefined"){
					defer.resolve(value);
				}

				console.log("Semaphore " + id + " -> signal (current value: " + value + ")");
			}
		};
	};

	return {
		getInstance: function(value){
			return new Semaphore(value);
		}
	};
})
.service("socket", function(socketMessagePrepare, $q){
	var that = this,
		socket = null,
		connectionTimeout = 10000;

	this.id = function(){
		return socket;
	};

    this.open = function(host, port, onReceiveListener, onReceiveErrorListener){
    	if(typeof onReceiveListener !== "function"){
    		onReceiveListener = function(){};
    	}
    	if(typeof onReceiveListener !== "function"){
    		onReceiveErrorListener = function(){};
    	}

    	return $q(function(resolve, reject){
    		var connectionError = function(){
    			that.close();
    			reject(Error("Error opening socket"));
    		};

			// Connects socket to host
		    try{
		    	port = parseInt(port);
		        // Establishes the connection
		        chrome.sockets.tcp.create({}, function(dataCreate){
		            socket = dataCreate.socketId;
		            var timeout = setTimeout(function(){
		            	connectionError();
		            },connectionTimeout);
		            chrome.sockets.tcp.connect(socket, host, port, function(result){
		                // Checks if connections done
		                if(result === 0){
		                	clearTimeout(timeout);
			                resolve("Socket opened");
			                chrome.sockets.tcp.onReceive.addListener(onReceiveListener);
		                    chrome.sockets.tcp.onReceiveError.addListener(onReceiveErrorListener);
			        	}else{
			        		connectionError();
			        	}		                
		            });
		        });
		    }catch(error){
		      connectionError();
		    }
		});
	};

	this.close = function(){
	    // Disconnects from socket
	    try {
	        chrome.sockets.tcp.close(socket);
	        socket = null;
	    }catch(error){
	        // Error
	    }
	};

	this.send = function(message, callback){
		return $q(function(resolve, reject){
		    try{
		        chrome.sockets.tcp.send(socket, socketMessagePrepare(message), function(sendInfo){
		        	if(sendInfo.resultCode === 0){
		                resolve("Message sent on the socket");
		                if(typeof callback === 'function'){
		                	callback(sendInfo);
		                }
		        	}else{
		        		reject(Error("Error sending message on the socket"));
		        	}
		        });
		    }catch(error){
				reject(Error("Error sending message on the socket"));
		    }
		});
	};
})
.service("controlSystem", function(socket, socketMessageEncode, semaphoreFactory, $q, $rootScope){
	var that=this,
		sendTimeout = 10000,
		eventManager = $({}),
		semaphore = semaphoreFactory.getInstance(3),	//used to have max 3 concurrent message
		connected = false,
		initialized = false,
		element = {},
		room = {},
		association = {},
	    service = [];

	var _code2numLookupTable = {
			"LO1": { num: 0, type: "dimmer" },
			"LO2": { num: 1, type: "dimmer" },
			"LO3": { num: 2, type: "dimmer" },
			"LO4": { num: 3, type: "dimmer" },
			"LO5": { num: 4, type: "dimmer" },
			"TO1": { num: 5, type: "toggle" },
			"TO2": { num: 6, type: "toggle" },
			"TO3": { num: 7, type: "toggle" },
			"TO4": { num: 8, type: "toggle" },
			"TO5": { num: 9, type: "toggle" },
			"TI1": { num: 10, type: "input" },
			"TI2": { num: 11, type: "input" },
			"TI3": { num: 12, type: "input" },
			"TI4": { num: 13, type: "input" },
			"TI5": { num: 14, type: "input" },
			"TI6": { num: 15, type: "input" },
			"TI7": { num: 16, type: "input" },
			"TI8": { num: 17, type: "input" },
			"S01": { num: 18, type: "sensor" },
			"S02": { num: 19, type: "sensor" },
			"S03": { num: 20, type: "sensor" },
			"S04": { num: 21, type: "sensor" },
			"S05": { num: 22, type: "sensor" },
			"S06": { num: 23, type: "sensor" },
			"S07": { num: 24, type: "sensor" },
			"S08": { num: 25, type: "sensor" },
			"S09": { num: 26, type: "sensor" },
			"S10": { num: 27, type: "sensor" },
			"S11": { num: 28, type: "sensor" },
			"S12": { num: 29, type: "sensor" },
			"S13": { num: 30, type: "sensor" },
			"PR1": { num: 40, type: "pre-time"},
			"PR2": { num: 41, type: "pre-time"},
			"PR3": { num: 42, type: "pre-time"},
			"PR4": { num: 43, type: "pre-time"},
			"PR5": { num: 44, type: "pre-time"},
			"PS1": { num: 45, type: "post-time"},
			"PS2": { num: 46, type: "post-time"},
			"PS3": { num: 47, type: "post-time"},
			"PS4": { num: 48, type: "post-time"},
			"PS5": { num: 49, type: "post-time"}
		},
		_num2codeLookupTable = {},
		_id2numId = function(id){
			var tmp = id.split(':');
			return tmp[0] + ':' + _code2numLookupTable[tmp[1]].num;
		};

	Object.keys(_code2numLookupTable).forEach(function(code){
		var tmp = _code2numLookupTable[code];
		_num2codeLookupTable[tmp.num] = { code: code, type: tmp.type };
	});

	//Connection flow functions
    var _init = function(host, port){
    	eventManager.on("AsyncState",function(event,response){
    		var CU = response.address + ',' + response.object,
    			id;

    		Object.keys(response.value).forEach(function(code){
    			id = CU + ':' + code;
    			if(element[id]){
    				if(id.indexOf('PR')>-1 || id.indexOf('PS')>-1){
						element[id].configured = response.value[code];
					}else{
						element[id].value = response.value[code];
					}

    				if(code !== "GVS" && code !== "RFS"){
    					$rootScope.$emit('asyncUpdate');
    				}
    			}
    		});
    	});

    	return socket.open(
    		host,
    		port,
    		function(info){
    			if(info.socketId === socket.id()){
	    			// Parses received socket's data
	                var response = socketMessageEncode(info.data);
	                
	                if(response.substr(response.length - 1).charCodeAt(0)=== 10) {
	                	if(!connected){
					    	connected = true;
					    }

					    response.split('\n').filter(function(x){return x !== '';})
					    .forEach(function(x){
					    	try{
						    	var buffer = JSON.parse(x);

						    	if(buffer.msgtype !== "AsyncState"){
						    		semaphore.signal();
						    	}

						    	console.log("RECEIVED " + buffer.msgtype,buffer);
						    	eventManager.trigger(buffer.msgtype,buffer);
					    	}catch(error){
					    		console.log("Error parsing json: ", x);
					    		console.log(error);
					    	}
					    });
	                }
    			}
    		},
    		function(info){
    			console.log('Socket error.');
    			that.disconnect();
    		}
    	);
    },
    _send = function(message, event, success, error, resolveCheck){
    	console.log('WAITING TO SEND -> ' + message);

    	if(typeof resolveCheck !== "function"){
    		resolveCheck = function(){return true; };
    	}

    	return semaphore.wait().then(function(){
    		return $q(function(resolve, reject){
    			var errorHandler = function(message, err){
    				eventManager.off(event);
    				semaphore.signal();
	    			console.log("Timeout sending message on socket");
	    			if(err){
	    				console.log(err);
	    			}
	    			error();
	    			reject(Error("Timeout sending message on socket"));
    			},
    			timeout = setTimeout(function(){
    				errorHandler("Timeout sending message on socket");
				},sendTimeout);

	    		eventManager.on(event, function(ev, response){
	    			if(resolveCheck(event, response)){
	    				clearTimeout(timeout);
		    			eventManager.off(event);
		    			success(event, response, resolve, reject);
	    			}
	    		});

	    		console.log('SENT -> ' + message);
	    		socket.send(message + '\r\n')
				.catch(function(err){
					errorHandler("ERROR -> " + event, err);
				});
			});
    	});
    },
    _centralId = function(apiKey, password){
    	return _send(
    		'{ "msgtype" : "centralId", "value" : "' + apiKey + '"}',
    		'centralId.' + apiKey,
    		function(event, response, resolve, reject){
	    		switch(response.value){
			    	case 0:	//success
			    		_login(password)
			    		.then(function(data){
			    			resolve(data);
			    		});
			    		break;
			    	//case 2:	//fail
			    	default:
			    		that.disconnect();
			    		reject(Error("centralId error"));
			    }
	    	},
	    	function(){
				that.disconnect();
			}
    	);
	},
	_login = function(password){
		return _send(
    		'login:' + password + ':',
    		'login.' + password,
    		function(event, response, resolve, reject){
	    		switch(response.value){
			    	case 1:		//success
				        // Gets services
				        // TODO: dynamic?
				        service.push('light');
				        service.push('rollingShutter');
						//service.push('thermostatus');

				        resolve("Connected to controlSystem");
			    		break;
			    	default:	//fail
			    		that.disconnect();
			    		reject(Error("login error"));
			    }
	    	},
	    	function(){
				that.disconnect();
			}
    	);	           
	},
	_connect = function(type, apiKey, password){
    	switch(type){
    		case 'cloud':
    			return _centralId(apiKey, password);
    		case 'lan':
    		case 'wan':
    			return _login(password);   		
    		default:
    			throw 'Unsupported connection type';
    	}	           
	};

	//Init flow functions
    var _getLastUpdate = function(rqsttype){
    	return _send(
    		'getlastupdate:' + rqsttype + ':',
    		'getlastupdate.' + rqsttype,
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.rqsttype == rqsttype;
			}
    	);           
	},
	_getControlUnitNumber = function(){
    	return _getLastUpdate(2).catch(function(err){
    		console.log('getControlUnitNumber error',err);
    	});	           
	},
	_getConfig = function(address, object){
		return _send(
    		'getconfig:' + address + ',' + object +  ':',
    		'getconfig.' + address + ',' + object,
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.address == address && response.object == object;
			}
    	);        
	},
	_getState = function(address, object){
		return _send(
    		'getstate:' + address + ',' + object +  ':',
    		'getstate.' + address + ',' + object,
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.address == address && response.object == object;
			}
    	);	           
	},
	_getConfigAndState = function(address, object){
		return $q(function(resolve, reject){
				_getConfig(address, object).then(function(response){
					var respAddr = response.address,
						respObj = response.object,
						id;

        			for(var elem in response.value){
        				id = respAddr + ',' + respObj + ':' + elem;
			        	element[id] = element[id] || {};
			        	element[id].id = id;
			        	element[id].CU = respAddr + ',' + respObj;
			        	element[id].code = elem;
			        	element[id].configured = response.value[elem];
			        }

			        _getState(respAddr, respObj).then(function(response){
			        	var respAddr = response.address,
    						respObj = response.object,
    						id;

	        			for(var elem in response.value){
	        				id = respAddr + ',' + respObj + ':' + elem;

				        	if(!element[id]){
				        		element[respAddr + ',' + respObj + ':' + elem.split('_')[0]].unit = response.value[elem];
				        	}else{
			        			element[id].value = response.value[elem];
				        	}
				        }
				        resolve('Config and state get for ' + respAddr + ', ' + respObj);
        			}).catch(function(){
			    		reject('Error getting config and state for ' + respAddr + ', ' + respObj);
			    	});
        		});
    		});
	},
	_getAllConfigsAndStates = function(controlUnitNumber){
		element = {};	

    	return $q(function(resolveAll, rejectAll){
    		var promises = [],
    			rejAll = function(){
    				rejectAll('Error getting config and state');
    			};

    		for(var i=0; i<controlUnitNumber; ++i){
    			var address = parseInt(i / 8);
        		var object = parseInt(i % 8);
        		
        		promises.push(_getConfigAndState(address,object).catch(rejAll));
    		}

    		$q.all(promises).then(function(values){
    			resolveAll(values);
    		});
		});	           
	},
	_getAssociationNumber = function(){
    	return _getLastUpdate(1).catch(function(err){
    		console.log('getAssociationNumber error',err);
    	});	           
	},
	_getCfgState = function(i){
		return _send(
    		'getcfgstate:' + i +  ':',
    		'getcfgstate.' + i,
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.number == i;
			}
    	);           
	},
	_checkAssociation = function(response){
		var state = response.state.split(':'),
			value = response.value.split(':'),
			roomName = value[0],
			componentName = value[1],
			component;

		room[roomName] = room[roomName] || {};	//adds room object
		room[roomName][componentName] = room[roomName][componentName] || {	//adds component object
			name: componentName
		};
		component = room[roomName][componentName];

		switch(state[0]){
	        case 'cfgprior':
	        	component.service = "thermostatus";
	            break;
	        case 'cfgshutd':
	        case 'cfgshutu':
	        	component.service = "rollingShutter";
	            break;
	        case 'cfgstate':
	        	component.service = "light";
	            break;
        }

        component.groups =[];
        for(var i=1, n=state.length-1; i<n; i+=2){
        	component.groups.push(state[i].split(',').concat(state[i+1].split(',')));
		}

		component.groups.forEach(function(group){
			var obj = _num2codeLookupTable[group[2]] || {},
				CU = group[0] + ',' + group[1],
				code = obj.code,
				id = CU + ':' + code;

			if(code){
				if(element[id].configured !== 0 && obj.type !== 'input'){	        				
    				switch(state[0]){
				        case 'cfgshutd':
				        	component.rollingShutterDown = id;
				            break;
				        case 'cfgshutu':
				        	component.rollingShutterUp = id;
				            break;
				        case 'cfgprior':
				        	component[obj.type] = id;
				            break;
				        case 'cfgstate':
				        	component[obj.type] = id;
				        	
				        	if(obj.type === 'toggle'){
				        		id = id.replace('TO','LO');
				        		if(element[id] && element[id].configured !== 0){
				        			component.dimmer = id;
				        			delete component.toggle;
				        		}
				        	}
				            break;
			        }
				}
			}else{
				component.timer = true;
			}
		});
	},
	_getAllCfgStates = function(associationNumber){
		room = {};

		semaphore.wait();
		semaphore.wait();

    	return $q(function(resolve, reject){
    		var promises = [];

    		for(var i=0; i<associationNumber; ++i){      		
        		promises.push(_getCfgState(i).then(_checkAssociation));
    		}

    		$q.all(promises).then(function(values){
    			semaphore.signal();
				semaphore.signal();
    			resolve(values);
    		});
		});	           
	};

	this.connectionInit = function(host, port){
		return _init(host,port);
	};

	this.connect = function(type, apiKey, password){
		return _connect(type, apiKey, password);
	};

    this.disconnect = function(){
	    socket.close();
	    connected = false;
	    $rootScope.$emit('socketDisconnected');
	};

	this.isOnline = function(){
	    return connected;
	};

	this.init = function(){
    	return $q(function(resolve, reject){
    		_getControlUnitNumber().then(function(response){
	    		_getAllConfigsAndStates(response.value).then(function(response){
		    		_getAssociationNumber().then(function(response){
			    		_getAllCfgStates(response.value).then(function(response){
				    		// Initialization completed
			                initialized = true;
			                resolve('Init completed successfully');
				    	});
			    	});  
		    	});
	    	});
	    });
	};

	this.isInitialized = function(){
	    return initialized;
	};

	this.service = function(name){
	    return name ? service[name] : service;
	};

	this.serviceExist = function(name){
	    return (service.indexOf(name) !== -1) ? true : false;
	};

	this.roomComponents = function(name){
		return Object.values(room[name]);
	};

	this.element = function(){
		return element;
	};

	this.roomNames = function(service){
		if(service){
	    	return Object.keys(room).filter(function(x){return Object.values(room[x]).filter(function(y){return y.service === service;}).length > 0; });
		}else{
			return Object.keys(room);
		}
	};

	this.setState = function(id,value){
		if(id.indexOf('PR')>-1 || id.indexOf('PS')>-1){
			element[id].configured = value;
		}else{
			element[id].value = value;
		}

		return _send(
    		'setstate:' + _id2numId(id) + ',' + value + ':',
    		'setstate',
    		function(event, response, resolve, reject){
    			if(response.value === 1){
    				resolve('SetState OK');
    			}else{
    				reject(Error('SetState Error'));
    			}
	    	},
	    	function(){
				reject(Error('SetState Error'));
			}
    	);
	};
})	
.service("database", function($cordovaSQLite){
	var database = null;

    this.connect = function(){
	    try{
            // Establishes the connection
            database = $cordovaSQLite.openDB({
                name: 'seica-shoebox.db',
                location: 'default'
            });
        }catch(error){
            database = null;
        }
	};

	this.execute = function(query, params){
		return $cordovaSQLite.execute(database, query, params);
	};

	this.init = function(){
		this.execute('CREATE TABLE IF NOT EXISTS configuration ( id integer primary key autoincrement, api text, host text, name text, password text, port text, type integer )');
        this.execute('CREATE TABLE IF NOT EXISTS scene ( id integer primary key autoincrement, active text, icon text, name text, room text, service text, state text )');
	};
});