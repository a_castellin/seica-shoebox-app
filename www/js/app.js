/*
 * app.js
 * Defines the core of the application during initialization.
 *
 * Manuel Minopoli - INTAG S.r.l.
 * Ferdinando Celentano
 */

angular
.module('starter', [
    'ionic',
    'ngCordova',
    'pascalprecht.translate',
    'starter.controllers',
    'starter.services',
    'ui.router',
    //'templates'
]).run(function($ionicPlatform,$ionicHistory, $translate, database, $state,fcm){
    $ionicPlatform.ready(function(){
        window.MyDatabase = database;
        window.MyFCM = fcm;
        // Database
        database.connect();
        database.init();

        // Keyboard
        if(window.cordova && window.cordova.plugins.Keyboard){
            cordova.plugins.Keyboard.disableScroll(true);
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }

        // Language
        if(typeof navigator.globalization !== 'undefined'){
            navigator.globalization.getPreferredLanguage(function(language){
                $translate
                  .use((language.value).split('-')[0])
                  .then(function(data){}, function(error){});
            }, null);
        }

        // Status bar
        if(window.StatusBar){
            StatusBar.styleDefault();
        }

        fcm.init();

        $state.go('app.connection_list');

    });

}).config(function($ionicConfigProvider, $stateProvider, $translateProvider, $urlRouterProvider){

    $ionicConfigProvider.navBar.alignTitle('center');
    
    // Routing
    $stateProvider.state('app', {
        abstract: true,
        templateUrl: 'templates/menu.html',
        url: '/app'
    }).state('app.configuration', {
        url: '/configuration',
        views: {
            'menuContent': {
                templateUrl: 'templates/configuration.html'
            }
        }
    }).state('app.dashboard', {
        url: '/dashboard',
        views: {
            'menuContent': {
                templateUrl: 'templates/dashboard.html'
            }
        }
    }).state('app.initialization', {
        url: '/initialization',
        views: {
            'menuContent': {
                controller: 'InitializationController',
                templateUrl: 'templates/initialization.html'
            }
        }
    }).state('app.scene/active/:id', {
        url: '/scene/active/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/scene/active.html'
            }
        }
    }).state('app.scene/add/:service/:room', {
        url: '/scene/add/:service/:room',
        views: {
            'menuContent': {
                templateUrl: 'templates/scene/add.html'
            }
        }
    }).state('app.scene/add/detail/:id', {
        url: '/scene/add/detail/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/scene/addDetail.html'
            }
        }
    }).state('app.sensor', {
        url: '/sensor',
        views: {
            'menuContent': {
                templateUrl: 'templates/sensor/list.html'
            }
        }
    }).state('app.sensor/detail/:room', {
        url: '/sensor/detail/:room',
        views: {
            'menuContent': {
                templateUrl: 'templates/sensor/detail.html'
            }
        }
    }).state('app.sensor/detail/:room/:component', { //AC_20180514
        url: '/sensor/detail/:room/:component',
        views: {
            'menuContent': {
                templateUrl: 'templates/sensor/detail.html'
            }
        }
    }).state('app.scene/create/:service/:room', {
        url: '/scene/create/:service/:room',
        views: {
            'menuContent': {
                templateUrl: 'templates/scene/create.html'
            }
        }
    }).state('app.service/:service', {
        url: '/service/:service',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/list.html'
            }
        }
    }).state('app.service/detail/:service/:room', {
        url: '/service/detail/:service/:room',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/detail.html'
            }
        }
    }).state('app.service/detail/:service/:room/:fromRoom', { //AC_20171023
        url: '/service/detail/:service/:room/:fromRoom',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/detail.html'
            }
        }
    }).state('app.service/timer/:service/:room/:element', {
        url: '/service/timer/:service/:room/:element',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer.html'
            }
        }
    }).state('app.service/timer_detail/:service/:room/:element/:edited', {
        url: '/service/timer_detail/:service/:room/:element/:edited',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_detail.html'
            }
        }
    }).state('app.service/edit_timer/:service/:room/:element/:edited/:timer', {
        url: '/service/edit_timer/:service/:room/:element/:edited/:timer',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer.html'
            }
        }
    }).state('app.service/edit_timer/:service/:room/:element/:edited', {
        url: '/service/edit_timer/:service/:room/:element/:edited',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer.html'
            }
        }
    }).state('app.service/timer_therm_detail/:service/:room/:element/:useMode/:edited', {
        url: '/service/timer_therm_detail/:service/:room/:element/:useMode/:edited',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_therm_detail.html'
            }
        }
    }).state('app.service/edit_timer_therm/:service/:room/:useMode/:element/:edited/:timer', {
        url: '/service/edit_timer_therm/:service/:room/:useMode/:element/:edited/:timer',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer_therm.html'
            }
        }
    }).state('app.service/edit_timer_therm/:service/:room/:useMode/:element/:edited', {
        url: '/service/edit_timer_therm/:service/:room/:useMode/:element/:edited',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer_therm.html'
            }
        }
    }).state('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData', {
        url: '/service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_spinner.html'
            }
        }
    }).state('app.service/edit_order_consumi/:service/:room/:element', {
        url: '/service/edit_order_consumi/:service/:room/:element',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_order_consumi.html'
            }
        }
    }).state('app.connection_list', {
        url: '/connection_list',
        views: {
            'menuContent': {
                templateUrl: 'templates/connection_list.html'
            }
        }
    }).state('app.service/room/:room',{
        url: '/service/room/:room',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/room_list.html'
            }
        }
    }).state('app.service/room/detail/:room',{
        url: '/service/room/detail/:room',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/room_detail.html'
            }
        }
    }).state('app.service/room/detail/:room/:component',{ //AC_20180514
        url: '/service/room/detail/:room/:component',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/room_detail.html'
            }
        }
    }).state('app.service/timer_rs/:service/:room/:element', {//AC_20171204
        url: '/service/timer_rs/:service/:room/:element',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_rs.html'
            }
        }
    }).state('app.service/timer_detail_rs/:service/:room/:element/:edited', {
        url: '/service/timer_detail_rs/:service/:room/:element/:edited',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_detail_rs.html'
            }
        }
    }).state('app.service/edit_timer_rs/:service/:room/:element/:edited/:timer', {
        url: '/service/edit_timer_rs/:service/:room/:element/:edited/:timer',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer_rs.html'
            }
        }
    }).state('app.service/edit_timer_rs/:service/:room/:element/:edited', {
        url: '/service/edit_timer_rs/:service/:room/:element/:edited',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer_rs.html'
            }
        }
    }).state('app.service/timer_spinner_rs/:newIndexU/:oldIndexU/:messageU/:newIndexD/:oldIndexD/:messageD/:nextPage/:nextPageData', {
        url: '/service/timer_spinner/:newIndexU/:oldIndexU/:messageU/:newIndexD/:oldIndexD/:messageD/:nextPage/:nextPageData',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_spinner_rs.html'
            }
        }
    }).state('app.info', {
        url: '/info',
        views: {
            'menuContent': {
                templateUrl: 'templates/info.html'
            }
        }
    }).state('app.service/timer_enable/:service/:room/:element/:type', {//AC_20180319
        url: '/service/timer_enable/:service/:room/:element/:type',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_enable.html'
            }
        }
    }).state('app.service/rs_app_list/:service/:room/:element/:component', {//AC_20180319
        url: '/service/rs_app_list/:service/:room/:element/:component',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/rs_association_list.html'
            }
        }
    }).state('app.service/timer_detail_en/:service/:room/:element/:edited/:type', {//AC_20180319
        url: '/service/timer_detail_en/:service/:room/:element/:edited/:type',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/timer_detail_en.html'
            }
        }
    }).state('app.service/edit_timer_en/:service/:room/:element/:edited/:type/:timer', {//AC_20180319
        url: '/service/edit_timer_en/:service/:room/:element/:edited/:type/:timer',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer_en.html'
            }
        }
    }).state('app.service/edit_timer_en/:service/:room/:element/:edited/:type', {//AC_20180319
        url: '/service/edit_timer_en/:service/:room/:element/:edited/:type',
        views: {
            'menuContent': {
                templateUrl: 'templates/service/edit_timer_en.html'
            }
        }
    });

    //$urlRouterProvider.otherwise('/app/connection_list');

    // Translations
    $translateProvider.translations('en', {
        add_timer_title: 'Add Timer',
        alert_button: 'OK',
        alert_buttonCancel: 'Cancel',
        alert_buttonNo: 'No',
        alert_buttonSave: 'Save',
        alert_buttonYes: 'Yes',
        alert_configurationError: 'Error during saving data.',
        alert_configurationSuccess: 'Data saved successfully.',
        alert_configurationTitle: 'Configuration',
        alert_connectionError: 'Error during connection: change configuration.',
        alert_connectionTitle: 'Connection',
        alert_noConfigurationError: 'To access the services, set correctly at least one configuration.',
        alert_noConfigurationTitle: 'Configuration',
        alert_sceneActiveConfirm: 'Active the selected scene?',
        alert_sceneActiveTitle: 'Scene',
        alert_sceneActivatedSuccess: 'Scene activated successfully.',
        alert_sceneActivatedTitle: 'Scene',
        alert_sceneAddedSuccess: 'Association added to scene successfully.',
        alert_sceneAddedTitle: 'Scene',
        alert_sceneCreateError: 'Error during saving data.',
        alert_sceneCreateSuccess: 'Data saved successfully.',
        alert_sceneCreateTitle: 'Scene',
        alert_sceneDeactivatedSuccess: 'Scene deactivated successfully.',
        alert_sceneDeactivatedTitle: 'Scene',
        alert_serviceError: 'Service not configured: contact supplier.',
        alert_serviceTitle: 'Service',
        alert_timerMessage: 'Set time in seconds.',
        alert_timerTitle: 'Timer',
        alert_unavailableService: 'Unavailable service.',
        alert_noResponseFromCU: 'No answer received from che control unit. The command may not have reached the system.',
        alert_cfgStateTitle:"Association",
        alert_cfgStateDeleteError:'There was an error while deleting the configuration',
        alert_cfgStateSetError:'There was an error while saving the configuration',
        alert_loginError:'Wrong Password',
        alert_loginErrorTitle:'Login',
        alert_centralDisconnectedTitle:'Connection',
        alert_centralDisconnected:'The plant is not currently available.',
        all: 'all',
        cond_auto: 'Auto',
        cond_manual: 'Manual',
        cond_int: 'Int',
        configuration_api: 'API Key',
        configuration_cloud: 'CLOUD',
        configuration_csid: 'Control System ID',
        configuration_csp: 'Control System Password',
        configuration_disclaimerSummary: 'Put here the text for the disclaimer message, the same for all the configuration typologies.',
        configuration_disclaimerTitle: 'Disclaimer',
        configuration_hostCloud: 'Cloud Server IP address',
        configuration_hostLocal: 'Local IP address',
        configuration_hostRemote: 'Remote IP address',
        configuration_lan: 'LAN',
        configuration_name: 'Name',
        configuration_port: 'Port',
        configuration_save: 'Save',
        configuration_title: 'Configuration',
        configuration_wan: 'WAN',
        connection_list_header:'Connections List',
        connection_list_new_title:'Add new connection',
        connection_list_new_cancel:'Cancel',
        connection_list_new_save:'Save',
        connection_list_new_error:'The inserted name is not valid',
        connection_list_delete_title:'Remove connection',
        connection_list_delete_message:'Do you realy want to remove this connection?',
        connection_list_delete_cancel:'No',
        connection_list_delete_ok:'Ok',
        dashboard_consumes: 'Energy use',
        dashboard_lights: 'Lights',
        dashboard_rollingShutters: 'Shutters',
        dashboard_scenes: 'SCENES',
        dashboard_sensors: 'Sensors',
        dashboard_services: 'SERVICES',
        dashboard_thermostatus: 'Thermostat',
        dashboard_title: 'Home',
        dashboard_rooms: 'Rooms',
        edit_timer_title:'Edit Timer',
        fw_updateTitle:'Firmware update',
        fw_updateBody:'In order to be able to use all functions, it is necessary to update the control unit.',
        fw_updateBodyCurrent:'Current Firmware Version:',
        fw_updateBodyRequired:'Required Firmware Version:',
        fw_update_button: 'OK',
        fw_updateDontShow:"Don't show this again!",
        initialization_configuration: 'Reading configurations',
        initialization_connection: 'Checking connection',
        initialization_data: 'Connected, reading device data',
        info_version: 'version',
        info_dati_centrale: 'Control unit data',
        loads_title: 'Loads',
        loads_generic: 'Load',
        menu_configuration: 'Configuration',
        menu_notification: 'Notifications',
        menu_refresh: 'Reload plant',
        menu_title: 'Menu',
        network_offline: 'No available connection, active and select one from Settings.',
        not_configured: 'Not configured',
        remove_timer: 'Remove Timer',
        remove_manual: 'Remove',
        room_services: 'Services',
        rs_Up: 'Up',
        rs_Down: 'Down',
        saving_config:'Saving configuration',
        scene_active: 'Active/Deactive',
        scene_add: 'Add',
        scene_addTitle: 'Add to scene',
        scene_create: 'New scene',
        scene_createIcon: 'Choose icon',
        scene_createName: 'Name',
        scene_createTitle: 'New scene',
        scene_settings: 'Settings',
        scene_title: 'Scenes',
        sensor_services: 'Services',
        sensor_title: 'Sensors',
        service_consume: 'Energy Use',
        service_light: 'Lights',
        service_rollingShutter: 'Shutters',
        service_rooms: 'Rooms',
        service_sensor: 'Sensors',
        service_thermostatus: 'Thermostat',
        serviceDetail_general: 'General',
        serviceDetail_condition: 'Condition',
        serviceDetail_loads: 'Loads',
        therm_auto: 'Auto',
        therm_manual: 'Manual',
        therm_int: 'Int',
        timer_post: 'Post-Time',
        timer_pre: 'Pre-Time',
        timer_seconds: 'seconds',
        timer_title: 'Timer',
        timer_only_once: 'Only once',
        timer_delay_hour: 'hours',
        timer_delay_min: 'min',
        timer_calendar: 'calendar',
        timer_delay: 'delay',
        week_1: 'mo',
        week_2: 'tu',
        week_3: 'we',
        week_4: 'th',
        week_5: 'fr',
        week_6: 'sa',
        week_0: 'su',
        auto:'Auto',
        manual:'Manual',
        info_text:'ShoeBox (Smart Home Electronic Box) identify a set of control units that, using the Power Line Communication technology, allows you to create a simple and economic home automation system adaptable to all homes, new or existent.\n\nThrough the App it is possible to remotely control the system. This function allows you to manage and monitor all the devices connected to the system, at any time from anywhere.',
        enable_disable:'Disable / Enable',
        rs_associations: 'Select assocation',
        en_label_output: 'Output value',
        en_label_enable: 'Enabling',
        push_notification_alternative_title: 'Notification',
        push_notification_alternative_msg: 'Notification riceived',
        push_notification_central_id: 'Central ID',
        push_notification_show: 'Display',
        push_notification_hide: 'Ignore',
        push_notification_body_msg: 'Text'
    });

    $translateProvider.translations('it', {
        add_timer_title: 'Aggiungi Timer',
        alert_button: 'OK',
        alert_buttonCancel: 'Annulla',
        alert_buttonNo: 'No',
        alert_buttonSave: 'Salva',
        alert_buttonYes: 'Si',
        alert_configurationError: 'Errore durante il salvataggio.',
        alert_configurationSuccess: 'Dati salvati correttamente.',
        alert_configurationTitle: 'Configurazione',
        alert_connectionError: 'Errore durante la connessione: cambiare la configurazione.',
        alert_connectionTitle: 'Connessione',
        alert_noConfigurationError: 'Per accedere ai servizi, impostare in modo corretto almeno una configurazione.',
        alert_noConfigurationTitle: 'Configurazione',
        alert_sceneActiveConfirm: 'Attivare lo scenario selezionato?',
        alert_sceneActiveTitle: 'Scenario',
        alert_sceneActivatedSuccess: 'Scenario attivato correttamente.',
        alert_sceneActivatedTitle: 'Scenario',
        alert_sceneAddedSuccess: 'Associazione aggiunta allo scenario correttamente.',
        alert_sceneAddedTitle: 'Scenario',
        alert_sceneCreateError: 'Errore durante il salvataggio.',
        alert_sceneCreateSuccess: 'Dati salvati correttamente.',
        alert_sceneCreateTitle: 'Scenario',
        alert_sceneDeactivatedSuccess: 'Scenario disattivato correttamente.',
        alert_sceneDeactivatedTitle: 'Scenario',
        alert_serviceError: 'Servizio non configurato: contattare il fornitore.',
        alert_serviceTitle: 'Servizi',
        alert_timerMessage: 'Imposta il tempo in secondi.',
        alert_timerTitle: 'Timer',
        alert_unavailableService: 'Servizio non disponibile.',
        alert_noResponseFromCU: 'Nessuna risposta dalla centralina. Il comando potrebbe non essere stato recepito.',
        alert_cfgStateTitle:"Associazione",
        alert_cfgStateDeleteError:'Si è verificato un errore durante la cancellazione della configurazione',
        alert_cfgStateSetError:'Si è verificato un errore durante il salvataggio della configurazione',
        alert_loginError:'Password Errata',
        alert_loginErrorTitle:'Login',
        alert_centralDisconnectedTitle:'Connessione',
        alert_centralDisconnected:'La centrale non è al momento disponibile.',
        all: 'all',
        cond_auto: 'Auto',
        cond_manual: 'Manual',
        cond_int: 'Int',
        configuration_api: 'API Key',
        configuration_cloud: 'CLOUD',
        configuration_csid: 'Control System ID',
        configuration_csp: 'Control System Password',
        configuration_disclaimerSummary: 'Inserire qui il messaggio per il discalimer, lo stesso per tutte le tipologie di configurazione.',
        configuration_disclaimerTitle: 'Disclaimer',
        configuration_hostCloud: 'Indirizzo IP Cloud Server',
        configuration_hostLocal: 'Indirizzo IP locale',
        configuration_hostRemote: 'Indirizzo IP remoto',
        configuration_lan: 'LAN',
        configuration_name: 'Nome',
        configuration_port: 'Porta',
        configuration_save: 'Salva',
        configuration_title: 'Configurazione',
        configuration_wan: 'WAN',
        connection_list_header:'Elenco Connessioni',
        connection_list_new_title:'Inserisci nuova centrale',
        connection_list_new_cancel:'Cancella',
        connection_list_new_save:'Salva',
        connection_list_new_error:'La centrale inserita non è valida',
        connection_list_delete_title:'Rimuovi centrale',
        connection_list_delete_message:'Desideri veramente rimuovere questa centrale?',
        connection_list_delete_cancel:'No',
        connection_list_delete_ok:'Sì',
        dashboard_consumes: 'Consumi',
        dashboard_lights: 'Luci',
        dashboard_rollingShutters: 'Tapparelle',
        dashboard_scenes: 'SCENARI',
        dashboard_sensors: 'Sensori',
        dashboard_services: 'SERVIZI',
        dashboard_thermostatus: 'Termostato',
        dashboard_title: 'Home',
        dashboard_rooms: 'Stanze',
        edit_timer_title:'Modifica Timer',
        fw_updateTitle:'Aggiornamento Firmware',
        fw_updateBody:'Per poter utilizzare tutte le funzionalità è necessario aggiornare la centralina.',
        fw_updateBodyCurrent:'Versione Firmware Corrente:',
        fw_updateBodyRequired:'Versione Firmware Richiesta:',
        fw_update_button: 'OK',
        fw_updateDontShow:"Non mostrare più!",
        initialization_configuration: 'Lettura delle configurazioni',
        initialization_connection: 'Verifica della connessione',
        initialization_data: 'Connesso, lettura della centralina',
        info_version: 'versione',
        info_dati_centrale: 'Dati centrale',
        loads_title: 'Carichi',
        loads_generic: 'Carico',
        menu_configuration: 'Configurazione',
        menu_notification: 'Notifiche',
        menu_refresh: 'Ricarica impianto',
        menu_title: 'Menu',
        network_offline: 'Nessuna connessione disponibile, attiva e selezionane una da Impostazioni.',
        not_configured: 'Non configurato',
        remove_timer: 'Elimina Timer',
        remove_manual: 'Elimina',
        room_services: 'Servizi',
        rs_Up: 'Su',
        rs_Down: 'Giù',
        saving_config:'Salvataggio configurazione',
        scene_active: 'Attiva/Disattiva',
        scene_add: 'Aggiungi',
        scene_addTitle: 'Aggiungi a scenario',
        scene_create: 'Nuovo scenario',
        scene_createIcon: 'Scegli icona',
        scene_createName: 'Nome',
        scene_createTitle: 'Nuovo scenario',
        scene_settings: 'Impostazioni',
        scene_title: 'Scenari',
        sensor_services: 'Servizi',
        sensor_title: 'Sensori',
        service_consume: 'Consumi',
        service_light: 'Luci',
        service_rollingShutter: 'Tapparelle',
        service_rooms: 'Stanze',
        service_sensor: 'Sensori',
        service_thermostatus: 'Termostato',
        serviceDetail_general: 'Generale',
        serviceDetail_condition: 'Condizione',
        serviceDetail_loads: 'Carichi',
        therm_auto: 'Auto',
        therm_manual: 'Manual',
        therm_int: 'Int',
        timer_post: 'Post-Time',
        timer_pre: 'Pre-Time',
        timer_seconds: 'secondi',
        timer_title: 'Timer',
        timer_only_once: 'Solo una volta',
        timer_delay_hour: 'Ore',
        timer_delay_min: 'minuti',
        timer_calendar: 'Calendario',
        timer_delay: 'Ritardo',
        week_1: 'lu',
        week_2: 'ma',
        week_3: 'me',
        week_4: 'gi',
        week_5: 've',
        week_6: 'sa',
        week_0: 'do',
        auto:'Auto',
        manual:'Manuale',
        info_text:'ShoeBox (Smart Home Electronic Box) identifica un insieme di centraline attraverso le quali è possibile connettere tra loro tutti i dispositivi di sistema attraverso la stessa linea di potenza, sfruttando la tecnologia delle onde convogliate in ridondanza con la tecnologia wireless.\n\nTramite l’App è possibile controllare da remoto l’impianto. Questa funzione permette di gestire e monitorare tutti i dispositivi collegati all’impianto, in qualsiasi momento a da qualsiasi luogo.',
        enable_disable:'Disabilita / Abilita',
        rs_associations: 'Seleziona associazione',
        en_label_output: 'Valore uscite',
        en_label_enable: 'Abilitazione',
        push_notification_alternative_title: 'Notifica',
        push_notification_alternative_msg: 'Notifica ricevuta',
        push_notification_central_id: 'ID Centrale',
        push_notification_show: 'Visualizza',
        push_notification_hide: 'Ignora',
        push_notification_body_msg: 'Testo'        
    });

    $translateProvider.preferredLanguage('it');
    
    $translateProvider.fallbackLanguage('en');
    
    // Various
    $ionicConfigProvider.backButton.previousTitleText(false).text('');
    
    $ionicConfigProvider.views.maxCache(0);
});