/*
 * service.js
 * Defines the services needed by the application
 *
 * Ferdinando Celentano
 */
 var head = "";
 var last = "";
 var msgCount = 0;
 var arGetStates = {};

 String.prototype.padZero= function(len, c){
    var s= '', c= c || '0', len= (len || 2)-this.length;
    while(s.length<len) s+= c;
    return s+this;
}

Number.prototype.padZero = function(len, c){
    return String(this).padZero(len,c);
}

if (typeof String.prototype.startsWith != 'function') {
	// see below for better implementation!
	String.prototype.startsWith = function (str){
		return this.indexOf(str) === 0;
	};
}

if (typeof String.prototype.endsWith != 'function') {
	// see below for better implementation!
	String.prototype.endsWith = function (str){
		return this.indexOf(str) === (this.length - 1);
	};
}

var Version = "1.2.10";
var _CENTRAL_ID_LENGTH = 50;

angular
// .module('starter.services', ['cryp.cipher'])//CYPHER
.module('starter.services', [])
.filter('capitalize', function(){
    return function(s){
      return (angular.isString(s) && s.length > 0) ? s[0].toUpperCase() + s.substr(1).toLowerCase() : s;
    };
})

.filter('padNumber', function(){
    return function(number,length,padChar){
      if(isNaN(number))
      	return number;

      var stringNumber = number.toString();

      if(stringNumber.length >= length)
      	return stringNumber;

      var padding = '';

      for(var i=0; i<(length - stringNumber.length); i++)
      {
      	padding += padChar;
      }

      return padding + stringNumber;
    };
})
.factory("socketMessageEncode", function(){ 
    return function(data){
	    var buffer = '',
	        encode = new Uint8Array(data);

	    for(var i = 0; i < encode.length; i++){
	      buffer = buffer + String.fromCharCode(encode[i]);
	    }
	    
	    return buffer;
	};
})
.factory("socketMessagePrepare", function(){ 
    return function(data){
	    // Converts string to array
	    ////console.log("@@SendData:\n"+data);
	    var buffer = new ArrayBuffer(data.length),
	        view = new Uint8Array(buffer);

	    for(var i = 0; i < data.length; i++){
	      view[i] = data.charCodeAt(i);
	    }

	    return buffer;
	};
})
.factory('semaphoreFactory', function($q){
	var Semaphore = function(val){
		var id = Date.now(),
			init = val && val>0 ? val : 1,
			value = init;
			queue = [];

		return {
			wait: function(){
				var defer = $q.defer();

				if(--value < 0){
					queue.push(defer);
				}else{
					defer.resolve(value);
				}

				//console.log("Semaphore " + id + " -> wait (current value: " + value + ")");

			  	return defer.promise;
			},
			signal: function(){
				var defer = queue.shift();
				////console.log("###QUEUE LENGTH: "+queue.length);
				
				++value;

				if(value > init){
					value = init;
				}
					
				if(typeof defer !== "undefined"){
					defer.resolve(value);
				}

				//console.log("Semaphore " + id + " -> signal (current value: " + value + ")");
			},
			reset: function(){
			    while(queue.length > 0 )
			        queue.pop();

			     value = init;
			}
		};
	};

	return {
		getInstance: function(value){
			return new Semaphore(value);
		}
	};
})
.factory('SecuredPopups', [
	'$ionicPopup',
	'$q',
	function ($ionicPopup, $q) {

		var firstDeferred = $q.defer();
		firstDeferred.resolve();

		var lastPopupPromise = firstDeferred.promise;

		return {
			'show': function (method, object) {
				var deferred = $q.defer();

				lastPopupPromise.then(function () {
					$ionicPopup[method](object).then(function (res) {
						deferred.resolve(res);
					});
				});

				lastPopupPromise = deferred.promise;

				return deferred.promise;
			}
		};
	}
])
.service("socket", function(socketMessagePrepare, $q){
	var that = this,
		socket = null,
		connectionTimeout = 10000;
		connected = false;

	this.id = function(){
		return socket;
	};

    this.open = function(host, port, onReceiveListener, onReceiveErrorListener){
    	if(typeof onReceiveListener !== "function"){
    		onReceiveListener = function(){};
    	}
    	if(typeof onReceiveErrorListener !== "function"){
    		onReceiveErrorListener = function(){};
    	}

    	return $q(function(resolve, reject){
    		var connectionError = function(){
    			that.close();
    			reject(Error("Error opening socket"));
    		};

			// Connects socket to host
		    try{
		    	port = parseInt(port);
		        // Establishes the connection
		        chrome.sockets.tcp.create({}, function(dataCreate){
		            socket = dataCreate.socketId;
		            var timeout = setTimeout(function(){
		            	connectionError();
		            },connectionTimeout);
		            chrome.sockets.tcp.connect(socket, host, port, function(result){
						// Checks if connections done
						clearTimeout(timeout);						
		                if(result === 0){
							that.connected = true;
							resolve("Socket opened");
							if(chrome.sockets.tcp.onReceive.listeners && chrome.sockets.tcp.onReceive.listeners.length > 0 )
							{
								chrome.sockets.tcp.onReceive.listeners[0] = onReceiveListener;
							}
							else
								chrome.sockets.tcp.onReceive.addListener(onReceiveListener);

							if(chrome.sockets.tcp.onReceiveError.listeners && chrome.sockets.tcp.onReceiveError.listeners.length > 0 )
							{
								chrome.sockets.tcp.onReceiveError.listeners[0] = onReceiveErrorListener;
							}
							else	
		                    	chrome.sockets.tcp.onReceiveError.addListener(onReceiveErrorListener);
			        	}else{
			        		connectionError();
			        	}		                
		            });
		        });
		    }catch(error){
		      connectionError();
		    }
		});
	};

	this.close = function(){
	    // Disconnects from socket
	    try {
	        chrome.sockets.tcp.close(socket);
			socket = null;
			that.connected = false;
	    }catch(error){
	        // Error
	    }
	};

	this.send = function(message, callback){
		return $q(function(resolve, reject){
		    try{
		        chrome.sockets.tcp.send(socket, socketMessagePrepare(message), function(sendInfo){
		        	if(sendInfo.resultCode === 0){
		                resolve("Message sent on the socket");
		                if(typeof callback === 'function'){
		                	callback(sendInfo);
		                }
		        	}else{
		        		reject(Error("Error sending message on the socket"));
		        	}
		        });
		    }catch(error){
				reject(Error("Error sending message on the socket"));
		    }
		});
	};

	this.isConnected = function()
	{
		return that.connected;
	}
})	
.service("database", function($cordovaSQLite){
	var database = null;

    this.connect = function(){
	    try{
            // Establishes the connection
            database = $cordovaSQLite.openDB({
                name: 'seica-shoebox.db',
                location: 'default'
            });
        }catch(error){
            database = null;
        }
	};

	this.execute = function(query, params){
		return $cordovaSQLite.execute(database, query, params);
	};

	this.init = function(){
		this.execute('CREATE TABLE IF NOT EXISTS configuration ( id integer primary key autoincrement, api text, host text, name text, password text, port text, type integer )');
        this.execute('CREATE TABLE IF NOT EXISTS scene ( id integer primary key autoincrement, active text, icon text, name text, room text, service text, state text )');
		this.execute('CREATE TABLE IF NOT EXISTS configuration_alert ( id integer primary key autoincrement, name text, show_fw_alert integer )');
		//AC_20180424
		this.execute('CREATE TABLE IF NOT EXISTS device_info ( key text primary key , value integer )')
		//AC_20180528
		this.execute('CREATE TABLE IF NOT EXISTS plant_info ( plant_id text primary key , last_update text )')
		this.execute('CREATE TABLE IF NOT EXISTS plant_data ( plant_id text primary key , element_value text , association_value text , control_unit_number integer)')
	};
})
.service("fcm", function($rootScope, database){
	var _currentToken = "";
	var _oldToken = "";
	var _notificationList;

	var _setCurrentToken = function(sToken)
	{
		_currentToken = sToken;
		console.log("Current Token:" + sToken);
		//AC_20180424
		//Scrittura su DB del nuovo token
		database.
		execute('SELECT value FROM device_info WHERE key="notification_token"').
		then(function(result){
			if(result != undefined && result.rows.length > 0)
			{
				database
				.execute('UPDATE device_info SET value = ? WHERE key="notification_token"', [
					sToken]);
			}else{
				database
				.execute('INSERT INTO device_info ( key, value) VALUES  ( "notification_token", ?)', [
					sToken]);
			}
		});
	};

	var _refreshToken = function()
	{
		//FireBase
        //FCMPlugin.getToken( successCallback(token), errorCallback(err) );
        //Keep in mind the function will return null if the token has not been established yet.
		FCMPlugin.getToken(
            function (token) {
				if(token != null)
				{
					_setCurrentToken(token);
				}
				else
				{
					_refreshToken();
				}
            },
            function (err) {
                console.log('error retrieving token: ' + err);
            }
        );
	};

	var _addNotification = function(sIdCentrale, pData)
	{
		if(_notificationList == undefined && _notificationList ==null)
			_notificationList = [];
		if(sIdCentrale != "" && sIdCentrale.length == _CENTRAL_ID_LENGTH)
		{
			var sTemp = sIdCentrale.substring(18)//Estraggo dall'id centrale la parte necessaria all'app per la connessione, cioè gli ultimi 32 caratteri di 50
			sIdCentrale = sTemp;
		}
		_notificationList.push({"central_id": sIdCentrale, "values": pData});
		$rootScope.$emit('pushNotification');
	}

	var _init = function()
	{
		database.
		execute('SELECT value FROM device_info WHERE key="notification_token"').
		then(function(result){
			if(result != undefined && result.rows.length > 0)
			{
				_oldToken = result.rows.item(0).value;
			}
		})

		//FCMPlugin.onTokenRefresh( onTokenRefreshCallback(token) );
		//Note that this callback will be fired everytime a new token is generated, including the first time.
		FCMPlugin.onTokenRefresh(function(token){
			if(token != null)
			{
				_setCurrentToken(token)
			}
		});

		FCMPlugin.onNotification(
			function(data){
				if(data)
				{
					_addNotification((data.central_id != undefined) ? data.central_id : "" ,data)
				}
		// 		if(data.wasTapped){
		// //Notification was received on device tray and tapped by the user.
		// 			console.log("Tapped: " +  JSON.stringify(data) );
		// 		}else{
		// //Notification was received in foreground. Maybe the user needs to be notified.
		// 			console.log("Not tapped: " + JSON.stringify(data) );
		// 		}
			},
			function(msg){
				console.log('onNotification callback successfully registered: ' + msg);
			},
			function(err){
				console.log('Error registering onNotification callback: ' + err);
			}
		);

		_refreshToken();
	};

	this.init = function()
	{
		return _init();
	};

	this.getCurrentToken = function()
	{
		return _currentToken != "" ? _currentToken : _oldToken;
	};

	this.getOldToken = function()
	{
		return _oldToken;
	}

	this.getNotificationArray = function()
	{
		if(_notificationList == undefined && _notificationList == null)
			return [];
		
		return _notificationList;
	}

	this.getLastNotification = function()
	{
		if(_notificationList == undefined && _notificationList == null)
			return null;
		
		return _notificationList.pop();
	}

	this.refreshToken = function()
	{
		return _refreshToken()
	}
})
.service("controlSystem", function(socket, socketMessageEncode, semaphoreFactory, $q, $rootScope, fcm, database){
// .service("controlSystem", function(socket, socketMessageEncode, semaphoreFactory, $q, $rootScope, fcm, database, cipher){//CYPHER
	var that=this,
		sendTimeout = 10000,
		retryTimeout = 1000,
		eventManager = $({}),
		semaphore = semaphoreFactory.getInstance(3),	//used to have max 3 concurrent message
		connected = false,
		initialized = false,
		element = {},
		room = {},
		association = [],
		associationNumber = -1,//Numero di associazioni
		externalSensors = [],
	    service = [],
		currentFWVersion = "000000",
		//AC_20171113
		messageIndex = 0,
		messageIndexMaxValue = 999999,
		enableSendId = true,
		//AC_20180123
		controlUnitNumber=-1,
		//AC_20180528
		currentCentralID = "",
		// readFromDB = false,//CYPHER
		// enCypher = false;
		readFromDB = false;

	that.appVersion = Version;

	var _addId = function()
	{
		if(enableSendId)
		{
			messageIndex = (messageIndex + 1) % messageIndexMaxValue;
			return 'id:' + messageIndex.padZero(6) + ':';
		}
		else
		{
			return '';
		}
	};

	var EnableLvs = //Versione minima di FW della centrale per ciascun livello di abilitazione
	    {
	    	"L1":{"enableLevel":1, "minFWVersion":'020400'},
			"L2":{"enableLevel":2, "minFWVersion":'030100'},
			"L3":{"enableLevel":3, "minFWVersion":'030200'}
	    };

	var _code2numLookupTable = {
			"LO1": { num: 0, type: "dimmer" },
			"LO2": { num: 1, type: "dimmer" },
			"LO3": { num: 2, type: "dimmer" },
			"LO4": { num: 3, type: "dimmer" },
			"LO5": { num: 4, type: "dimmer" },
			"TO1": { num: 5, type: "toggle" },
			"TO2": { num: 6, type: "toggle" },
			"TO3": { num: 7, type: "toggle" },
			"TO4": { num: 8, type: "toggle" },
			"TO5": { num: 9, type: "toggle" },
			"TI1": { num: 10, type: "input" },
			"TI2": { num: 11, type: "input" },
			"TI3": { num: 12, type: "input" },
			"TI4": { num: 13, type: "input" },
			"TI5": { num: 14, type: "input" },
			"TI6": { num: 15, type: "input" },
			"TI7": { num: 16, type: "input" },
			"TI8": { num: 17, type: "input" },
			"S01": { num: 18, type: "sensor" },
			"S02": { num: 19, type: "sensor" },
			"S03": { num: 20, type: "sensor" },
			"S04": { num: 21, type: "sensor" },
			"S05": { num: 22, type: "sensor" },
			"S06": { num: 23, type: "sensor" },
			"S07": { num: 24, type: "sensor" },
			"S08": { num: 25, type: "sensor" },
			"S09": { num: 26, type: "sensor" },
			"S10": { num: 27, type: "sensor" },
			"S11": { num: 28, type: "sensor" },
			"S12": { num: 29, type: "sensor" },
			"S13": { num: 30, type: "sensor" },
			"S14": { num: 31, type: "sensor" },
			"S21": { num: 101, type: "sensor" },//Sensore MOME
			"S22": { num: 102, type: "sensor" },//Sensore MOME
			"S23": { num: 103, type: "sensor" },//Sensore MOME
			"S24": { num: 104, type: "sensor" },//Sensore MOME
			"S25": { num: 105, type: "sensor" },//Sensore MOME
			"S26": { num: 106, type: "sensor" },//Sensore MOME
			"S27": { num: 107, type: "sensor" },//Sensore MOME
			"S28": { num: 108, type: "sensor" },//Sensore MOME
			"S29": { num: 109, type: "sensor" },//Sensore MOME
			"S30": { num: 110, type: "sensor" },//Sensore MOME
			"S31": { num: 111, type: "sensor" },//Sensore MOME
			"S32": { num: 112, type: "sensor" },//Sensore MOME
			"S33": { num: 113, type: "sensor" },//Sensore MOME
			"S34": { num: 114, type: "sensor" },//Sensore MOME
			"PR1": { num: 40, type: "pre-time"},
			"PR2": { num: 41, type: "pre-time"},
			"PR3": { num: 42, type: "pre-time"},
			"PR4": { num: 43, type: "pre-time"},
			"PR5": { num: 44, type: "pre-time"},
			"PS1": { num: 45, type: "post-time"},
			"PS2": { num: 46, type: "post-time"},
			"PS3": { num: 47, type: "post-time"},
			"PS4": { num: 48, type: "post-time"},
			"PS5": { num: 49, type: "post-time"}
		},
		_num2codeLookupTable = {},
		_id2numId = function(id){
			var tmp = id.split(':');
			return tmp[0] + ':' + _code2numLookupTable[tmp[1]].num;
		};

	Object.keys(_code2numLookupTable).forEach(function(code){
		var tmp = _code2numLookupTable[code];
		_num2codeLookupTable[tmp.num] = { code: code, type: tmp.type };
	});

	//Connection flow functions
    var _init = function(host, port){
    	eventManager.off("AsyncState").on("AsyncState",function(event,response){
    		var CU = response.address + ',' + response.object,
    			id;

    		Object.keys(response.value).forEach(function(code){
    			id = CU + ':' + code;
    			if(element[id]){
    				if(id.indexOf('PR')>-1 || id.indexOf('PS')>-1){
						element[id].configured = response.value[code];
					}else{
						element[id].value = response.value[code];
					}

    				if(code !== "GVS" && code !== "RFS"){
    					$rootScope.$emit('asyncUpdate', id);
    				}
    			}
    		});
		});

		//CYPHER
		// if(port == "8082")
		// {
		// 	that.enCypher = true;
		// }
		// else
		// {
		// 	that.enCypher = false;
		// }

    	return socket.open(
    		host,
    		port,
    		function(info){
    			if(info.socketId === socket.id()){
	    			// Parses received socket's data
	                var response = socketMessageEncode(info.data);
					console.log("@@:"+response);
					
					// //var responseJSON;
					
					// if(that.enCypher && response.indexOf("\"msgtype\":\"centralId\"") == -1)//CYPHER
					// {
					// 	//decifro messaggio ricevuto
					// 	response = cipher.CriptoDecompress(response);
					// }

					var arr = response.split('\n');
	                response = "";
	                for(var i=0;i<arr.length; i++)
	                {

	                    if(arr[i]=="")
	                    {
	                        continue;
	                    }

	                    if(arr[i].substr(arr[i].length - 1) != "}")
	                    {
	                        if(arr[i].indexOf("{")!=-1) {
	                            head = arr[i];
	                        }
                            else if(head.indexOf(arr[i])==-1) {
                                head += arr[i]
                            }
                            continue;
	                    }
	                    else if(arr[i].indexOf("{")==-1 )
	                    {
	                        if(head=="")
	                            continue;
	                        arr[i] = head+arr[i];
	                        if(arr[i].indexOf("AsyncState")==-1)
	                            //console.log("%<Reconstructed message");
	                        //console.log("@##@FULL MSG:\n"+arr[i]);

	                        head = "";
	                    }
	                    response += arr[i]+'\n';
	                }

	                
	                if(response.substr(response.length - 1).charCodeAt(0)=== 10) {
	                	if(!connected){
					    	connected = true;
					    }

					    response.split('\n').filter(function(x){return x !== '';})
					    .forEach(function(x){
					    	try{
						    	var buffer = JSON.parse(x);

						    	var s = buffer.msgtype;

						    	if(s !== "AsyncState"){
						    		semaphore.signal();
						    	}

						    	if(s === "getconfig"){
                                    s = buffer.msgtype+"."+buffer.address+","+buffer.object;
                                }
                                else if(s === "getlastupdate"){
                                    s = buffer.msgtype+"."+buffer.rqsttype;
                                }
                                else if(s === "getcfgstate"){
                                    s = buffer.msgtype+"."+buffer.number;
                                }
                                else if(s === "getstate"){
                                    // s = buffer.msgtype+"."+buffer.address+","+buffer.object;
                                    s = buffer.msgtype+"."+buffer.id;
                                }
                                else if(s === "cfgsetup"){
                                    s = buffer.msgtype+"."+buffer.number+"."+buffer.rqsttype;
								}
								else if(s == "setfcmtoken"){
									s = buffer.msgtype + ".1";
								}
								//AC_20180108 - Start Async
								// else if(s === "startasync"){
                                //     s = buffer.msgtype+"."+buffer.number+"."+buffer.rqsttype;
                                // }

                                if(s !== "AsyncState"){
                                    //console.log("@##@Received: "+s);
                                }
						    	////console.log("RECEIVED " + buffer.msgtype + "\n<<" + x+">>",buffer);
						    	eventManager.trigger(s,buffer);
					    	}catch(error){
					    		//console.log("Error parsing json: \n"+x, x);
					    		//console.log(error);
					    	}
					    });
	                }
    			}
    		},
    		function(info){
    			console.log('Socket error.');
    			that.disconnect();
    		}
    	);
	},
    _inner_send = function(message, event, success, error, resolveCheck, numtry, once, enable_cypher){

		console.log('WAITING TO SEND -> '+ message);
		
		//CYPHER
		// if(enable_cypher && that.enCypher)
		// {
		// 	//Cifro messaggio da inviare alla centrale
		// 	message = cipher.CriptoCompress(message)
		// 	console.log('Messaggio decriptato -> '+ cipher.CriptoDecompress(message));
		// }

    	if(typeof resolveCheck !== "function"){
    		resolveCheck = function(){return true; };
    	}

    	numtry--;

    	return semaphore.wait().then(function(){
    		return $q(function(resolve, reject){
    			var errorHandler = function(message, err){
    				eventManager.off(event);
    				semaphore.signal();
	    			//console.log("Timeout sending message on socket\n"+message);
	    			if(err){
	    				console.log(err);
	    			}
	    			error();
	    			reject(Error("Timeout sending message on socket\n"+message));
    			},
    			timeout;

    			if(numtry>0)
    			{
    			    timeout = setTimeout(function(){
                        _inner_send(message, event, success, error, resolveCheck, numtry, once, enable_cypher);
                    },retryTimeout);
    			}
    			else
    			{
                    timeout = setTimeout(function(){
                        errorHandler("Timeout sending message on socket\n"+message);
                    },sendTimeout);
    			}

                if(once)
                {
                    eventManager.one(event, function(ev, response){
                        ////console.log("@@Event triggered: "+event);
                        if(resolveCheck(event, response))
                        {
                            //console.log("@@Received:"+event);
                            clearTimeout(timeout);
                            eventManager.off(event);
                            success(event, response, resolve, reject);
                        }
                    });
                }
                else
                {
                    eventManager.on(event, function(ev, response){
                        ////console.log("@@Event triggered: "+event);
                        if(resolveCheck(event, response))
                        {
                            //console.log("@@Received:"+event);
                            clearTimeout(timeout);
                            eventManager.off(event);
                            success(event, response, resolve, reject);
                        }
                    });
	    		}
			   	if(socket.isConnected()){
					console.log('@@SENT -> ' + message );

					socket.send(message + '\r\n')
					.catch(function(err){
						errorHandler("ERROR -> " + event, err);
					});
				}else{
					console.log('@@NOT_SENT -> ' + message );
					semaphore.reset();					
				}

			});
    	});
    },
	_send = function(message, event, success, error, resolveCheck, numtry, once){
		return _inner_send(message, event, success, error, resolveCheck, numtry, once, true)
    },
    _centralId = function(apiKey, password){
        //semaphore.reset();
    	return _inner_send(
    		'{ "msgtype" : "centralId", "value" : "' + apiKey + '"}',
    		'centralId',//'centralId.' + apiKey,
    		function(event, response, resolve, reject){
	    		switch(response.value){
			    	case 0:	//success
			    		_login(password)
			    		.then(function(data){
			    			resolve(data);
			    		});
			    		break;
					case 2:	//fail
			    	default:
						$rootScope.$emit('central_disconnected');
			    		that.disconnect();
			    		reject(Error("centralId error"));
			    }
	    	},
	    	function(){
				that.disconnect();
			},
			null,
			1,
			false,
			false
    	);
	},
	_login = function(password){
	    semaphore.reset();
	    head = "";
		return _send(
    		_addId() + 'login:' + password + ':',
    		'login.' + password,
    		function(event, response, resolve, reject){
	    		switch(response.value){
			    	case 1:		//success
				        // Gets services
				        // TODO: dynamic?
				        service.push('light');
				        service.push('rollingShutter');
						service.push('thermostatus');
						//AC_20170725
						service.push('consume');
				        resolve("Connected to controlSystem");
			    		break;
			    	default:	//fail
						$rootScope.$emit('login_failed');
			    		that.disconnect();
			    		reject(Error("login error"));
			    }
	    	},
	    	function(){
				that.disconnect();
			},
			1
    	);
	},
	_getApiKey = function() {
		
	    return _send(
            _addId() + 'getapikey:',
            'getapikey',
            function(event, response, resolve, reject){
                if(response.value && response.value.length>18){
                    //salvare API key
                    //console.log("@@APIKEY::"+response.value);
                    //$('#cloudApi').val(response.value);
                    resolve(response.value.substring(18));
                }
                else
                    reject(Error("Empty API key"));

            },
            function(){
                return true;
            },
            1,
            true
        );
	},
	_connect = function(type, apiKey, password){

		// //CYPHER
		// if(apiKey != null && apiKey != undefined && apiKey!="")
		// {
		// 	cipher.SetKey(apiKey)
		// }
		// else
		// {
		// 	apiKey = that.currentCentralID;
		// 	if(apiKey != null && apiKey != undefined && apiKey!="")
		// 	{
		// 		cipher.SetKey(apiKey)
		// 	}
		// 	else
		// 	{
		// 		that.enCypher = false;
		// 	}
		// }

    	switch(type){
    		case 'cloud':
    			return _centralId(apiKey, password);
    		case 'lan':
    		case 'wan':
    			return _login(password);
    		default:
    			throw 'Unsupported connection type';
    	}
	};

	//Init flow functions
    var _getLastUpdate = function(rqsttype){
    	return _send(
    		_addId() + 'getlastupdate:' + rqsttype + ':',
    		'getlastupdate.' + rqsttype,
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.rqsttype == rqsttype;
			},
			1
    	);
	},
	_getControlUnitNumber = function(){
    	return _getLastUpdate(2).catch(function(err){
    		//console.log('getControlUnitNumber error',err);
    	});
	},
	_getFWVersion = function(){
    	return _getLastUpdate(3).catch(function(err){
    		//console.log('getControlUnitNumber error',err);
    	});
	},
	_getConfig = function(address, object){
		return _send(
    		_addId() + 'getconfig:' + address + ',' + object +  ':',
    		'getconfig.' + address + ',' + object,
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.address == address && response.object == object;
			},
			1,
			true

    	);        
	},
	_getState = function(address, object){
		var id = messageIndex = (messageIndex + 1) % messageIndexMaxValue;
		arGetStates[id.padZero(6)]={"address":address, "object":object};
		return _send(
    		'id:' + id.padZero(6) + ':getstate:' + address + ',' + object +  ':',
			//'getstate.' + address + ',' + object,
			'getstate.' + id.padZero(6),
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.address == address && response.object == object;
			},
			1,
			true
    	);	           
	},
	_getConfigAndState = function(address, object){
		return $q(function(resolve, reject){
				_getConfig(address, object).then(function(response){
					var respAddr = response.address,
						respObj = response.object,
						id;

        			for(var elem in response.value){
        				id = respAddr + ',' + respObj + ':' + elem;
			        	element[id] = element[id] || {};
			        	element[id].id = id;
			        	element[id].CU = respAddr + ',' + respObj;
			        	element[id].code = elem;
			        	element[id].configured = response.value[elem];
			        }

			        _getState(respAddr, respObj).then(function(response){
						if(response != undefined && response.value != undefined){
							try{
								
								if(response.value == 19 || response.value == 20)
								{
									var currentRespAddr = arGetStates[response.id].address,
										currentRespObj = arGetStates[response.id].object;
									
									delete arGetStates[response.id.padZero(6)];
								}
								else{
									delete arGetStates[response.id.padZero(6)];									
									
									var currentRespAddr = response.address,
										currentRespObj = response.object,
										id;
									
									for(var elem in response.value){
										id = currentRespAddr + ',' + currentRespObj + ':' + elem;

										if(!element[id]){
											if(element[currentRespAddr + ',' + currentRespObj + ':' + elem.split('_')[0]]!=undefined)
												element[currentRespAddr + ',' + currentRespObj + ':' + elem.split('_')[0]].unit = response.value[elem].replace(/ï¿½/g," ");
										}else{
											element[id].value = response.value[elem];
										}
									}
									resolve('Config and state get for ' + currentRespAddr + ', ' + currentRespObj);
								}
							}
							catch(exception)
							{
								reject('Error getting config and state for ' + respAddr + ', ' + respObj);
							}
						}
						else{
							reject('Error getting config and state for ' + respAddr + ', ' + respObj);
						}
        			}).catch(function(){
			    		reject('Error getting config and state for ' + respAddr + ', ' + respObj);
			    	});
        		});
    		});
	},
	_getAllConfigsAndStates = function(controlUnitNumber){
		element = {};	

    	return $q(function(resolveAll, rejectAll){

    		var promises = [],
    			rejAll = function(){
    				rejectAll('Error getting config and state');
    			};


    		for(var i=0; i<controlUnitNumber; ++i){
    			var address = parseInt(i / 8);
        		var object = parseInt(i % 8);

        		promises.push(_getConfigAndState(address,object).catch(rejAll));

    		}

    		$q.all(promises).then(function(values){
    			resolveAll(values);
    		});
		});	           
	},
	_getAssociationNumber = function(){
    	return _getLastUpdate(1).catch(function(err){
    		//console.log('getAssociationNumber error',err);
    	});	           
	},
	_getCfgState = function(i){
	    //semaphore.reset();
		return _send(
    		_addId() + 'getcfgstate:' + i +  ':',
    		'getcfgstate.' + i,
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.number == i;
			},
			1,
			true
    	);           
	},
	_checkAssociation = function(response){
		if(response.state && response.value){
			var state = response.state.split(':'),
				value = response.value.split(':'),
				number = response.number,
				roomName = value[0],
				realComponentName = value[1],
				componentName = "",
				component;

			roomName = roomName.toString().replace(/\//g, "_252F").replace(/#/g,"_2523");
			componentName = realComponentName.toString().replace(/\//g, "_252F").replace(/#/g,"_2523");
			room[roomName] = room[roomName] || {};	//adds room object

			association[number] = association[number] || {};
			association[number].service = state[0];
			association[number].groups =[];
	        for(var i=1, n=state.length-1; i<n; i+=2){
	        	association[number].groups.push({"id":state[i],"data":state[i+1]})
			}
			association[number].roomName = roomName;
			association[number].componentName = componentName;

			var newComp = true;
			if(room[roomName][componentName]){
				if((state[0]=='cfgshutd' || state[0]=='cfgshutu') && room[roomName][componentName].service == "rollingShutter")
				{
					newComp = false;
				}

				if((state[0]=='cfgtmanu' || state[0]=='cfgtauto' || state[0]=='cfgtinte' ||
					state[0]=='cfgcmanu' || state[0]=='cfgcauto' || state[0]=='cfgcinte') && 
					room[roomName][componentName].service == "thermostatus")
				{
					newComp = false;
				}
			}

			room[roomName][componentName] = room[roomName][componentName] || {	//adds component object
				name: componentName,
				showName : realComponentName.toString()
			};
			component = room[roomName][componentName];
			component.number = number;

			switch(state[0]){
		        case 'cfgtmanu':
		        case 'cfgtauto':
		        case 'cfgtinte':
		        	component.service = "thermostatus";
		            break;
		        case 'cfgtexte':
		        	component.service = "thermostatus-external"
		        	break;
		        case 'cfgshutd':
		        case 'cfgshutu':
		        	component.service = "rollingShutter";
		            break;
		        case 'cfgstate':
		        	component.service = "light";
		            break;
		        case 'cfgprior':
		        	component.service = "consume";
	        }

			
	        component.groups =[];
	        for(var i=1, n=state.length-1; i<n; i+=2){
	        	component.groups.push(state[i].split(',').concat(state[i+1].split(',')));
			}

	        if(newComp){
	            component.toggleAssoc = "";
	            component.rollDownAssoc = "";
				component.rollUpAssoc = "";
				//AC_20171205;
				component.rollDownComp = {};
				component.rollUpComp = {};

	            component.dimmerAssoc = "";
	            component.assocOutputType = "";
	            component.assocInputType = "";
	            //_AC_20170717
	            component.sensorList = {};
	            component.toggleList = {};
	            component.dimmerList = {};
	            component.inputList = {};
	            component.thermAuto = {};//oggetto per la gestione dell'associazione AUTOMATIC del termostato
	            component.thermInte = {};//oggetto per la gestione dell'associazione INTELLIGENT del termostato
	            component.thermManu = {};//oggetto per la gestione dell'associazione MANUAL del termostato
	            component.condAuto = {};//oggetto per la gestione dell'associazione AUTOMATIC del termostato
	            component.condInte = {};//oggetto per la gestione dell'associazione INTELLIGENT del termostato
	            component.condManu = {};//oggetto per la gestione dell'associazione MANUAL del termostato
	            component.timer = [];
	            component.hasTimer = false;
	        }


	        var currentSensorList = [];
	        var currentToggleList = [];
	        var currentDimmerList = [];
	        var currentInputList = [];
	        var currentTimerList = [];

	        component.groups.forEach(function(group){
				var obj = _num2codeLookupTable[group[2]] || {},
					CU = group[0] + ',' + group[1],
					code = obj.code,
					id = CU + ':' + code;

			    /*if(code)
			    {
			        //var tokens = code.split(',');
			        ////console.log("@@CODE "+tokens[0]+","+tokens[1]);
			        if(code.indexOf("TI")==-1)
			        {
	                    if(assoc !== "")
	                        assoc += ";";
	                    assoc += id;
	                }
			    }*/

				////console.log("##Element "+id+" undefined!");

				if(code && element[id]!=undefined){
					if(element[id].configured !== 0){

						if(obj.type == 'sensor')
						{
							if(component.sensorList[id]==undefined)
							{
								component.sensorList[id] = {'element': element[id], 'condition': group[3]!=undefined ? group[3] : '' , 'value': group[3]!=undefined ? group[3] : '' };
							}
							currentSensorList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
						}

						if(obj.type == 'toggle')
						{
							if(component.toggleList[id]==undefined)
							{
								component.toggleList[id] = {'element': element[id], 'value': group[3]!=undefined ? group[3] : '' };
							}
							currentToggleList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
						}	

						if(obj.type == 'dimmer')
						{
							if(component.dimmerList[id]==undefined)
							{
								component.dimmerList[id] = {'element': element[id], 'value': group[3]!=undefined ? group[3] : '' };
							}
							currentDimmerList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
						}

						if(obj.type == 'input')
						{
							if(component.inputList[id]==undefined)
							{
								component.inputList[id] = {'element': element[id], 'value': group[3]!=undefined ? group[3] : '' };
							}
							currentInputList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
						}

	    				switch(state[0]){
					        case 'cfgshutd':
					        	if( obj.type !== 'input'){
						        	component.rollingShutterDown = id;
						        	if(obj.type === 'toggle'){
							        	if(component.rollDownAssoc && component.rollDownAssoc !== "")
			                                component.rollDownAssoc += ";";
			                            component.rollDownAssoc += id;
			                            //console.log("@@ROLLDOWN:"+roomName+" : "+component.name+" : "+component.rollDownAssoc);
			                        }
								}
					            break;
					        case 'cfgshutu':
					        	if( obj.type !== 'input'){
					        		component.rollingShutterUp = id;
						        	if(obj.type === 'toggle'){
						        		if(component.rollUpAssoc && component.rollUpAssoc !== "")
		                            	    component.rollUpAssoc += ";";
		                            	component.rollUpAssoc += id;
		                            }
	                            }

					            break;
					        case 'cfgprior':
					        	if( obj.type !== 'input'){
					        		component[obj.type] = id;

					        		if(obj.type === 'toggle'){
	                            	    if(component.outputList == undefined)
	                            	        component.outputList = [];
	                            	    if(component.outputList.filter(function(out){return out.element && out.element.id && out.element.id === id;}).length<=0)
	                            	    	component.outputList.push({	'element': element[id],
	                            	    								'priority': component.outputList.length,
	                            	    								'value': group[3]!=undefined ? group[3] : '' });
					        		}

					        	}
					            break;
					        case 'cfgstate':
					        	if( obj.type !== 'input'){
					        		component[obj.type] = id;
					        		
					        		if(obj.type === 'toggle'){
		
	                            	    if(component.toggleAssoc && component.toggleAssoc !== "")
	                            	        component.toggleAssoc += ";";
	                            	    component.toggleAssoc += id;
		
					        			id = id.replace('TO','LO');
					        			if(element[id] && element[id].configured !== 0){
	                            	        if(component.dimmerAssoc && component.dimmerAssoc !== "")
	                            	            component.dimmerAssoc += ";";

	                            	        component.dimmerAssoc += id;
					        				component.dimmer = id;
					        				//delete component.toggle;
					        			}
					        		}
					        	}
					            break;
				        }
					}
				}else{
					//_AC_20170711 - se non è né un sensore né un ingresso né un uscita
					switch(group[2])
		    		{
		    			case '4096':
		    				component.assocOutputType = 'off';
		    				break;
		    			case '8192':
		    				component.assocOutputType = 'on';
		    				break;
		    			case '16384':
		    				component.assocOutputType = 'toggle';
		    				break;
		    			case '32768':
		    				component.assocInputType = 'toggle';
		    				break;
		    			case '32769':
		    				component.assocInputType = 'and';
		    				break;
		    			default:
		    				//Se group[2] è compreso tra 0x100 e 0x300 è un timer
		    				if(group[2] >= 256 && group[2] < 768)
		    				{
								component.hasTimer = false;

		    					var timer = {};

		    					timer.disabledDays = [0,0,0,0,0,0,0];
		    					if( group[0] > 0)
		    					{
		    						var val = group[0];
		    						for(var i=0; i < timer.disabledDays.length; i++)
		    						{
		    							var bit = val%2;
		    							val = (val -bit)/2;
		    							timer.disabledDays[i]=bit;
		    						}
		    					}
		    					timer.disabledMonth = [0,0,0,0,0,0,0,0,0,0,0,0];
		    					if( group[1] > 0)
		    					{
		    						var val = group[1];
		    						for(var i=0; i < timer.disabledMonth.length; i++)
		    						{
		    							var bit = val%2;
		    							val = (val -bit)/2;
		    							timer.disabledMonth[i]=bit;
		    						}
		    					}
		    					
		    					//Se group[2] è compreso tra 0x100 e 0x200 è un timer OFF
		    					if(group[2] >= 256 && group[2] < 512)
			    				{
			    					//timer.index = group[2]-256;
			    					timer.type = "off";
			    				}
			    				//Se group[2] è compreso tra 0x200 e 0x300 è un timer ON
			    				else if(group[2] >= 512 && group[2] < 768)
			    				{
			    					//timer.index = group[2]-512;
			    					timer.type = "on";
			    				}

			    				//Estraggo Indice del primo caratte non numerico
			    				var indexFirstNotNum = group[3].indexOf(group[3].match(/\D/));
			    				if(indexFirstNotNum == -1) indexFirstNotNum = group[3].length;

		    					timer.minutesFromMidnight = parseInt(group[3].substring(0, indexFirstNotNum));
		    					
								timer.condition = group[3].substring(indexFirstNotNum).replace(/\+/g,"");
								
								//AC_20180305
								//Check OnlyOnce condition
								var CurrentCondition = timer.condition;
								//AC_20180219 - Inserisco parametro onlyOnce in attesa di sapere come leggerlo
								timer.onlyOnce = 0;
								if(CurrentCondition.endsWith("u"))
								{
									timer.onlyOnce = 1;
									timer.condition = CurrentCondition.slice(0,CurrentCondition.length-1);
								}

		    					//Se termostato separo verso della condizione dalla soglia
		    					if(component.service == "thermostatus"){
			    					var indexSoglia = timer.condition.indexOf(timer.condition.match(/\d/));
				    				if(indexSoglia == -1)
				    				{ 
				    					timer.soglia = 0;
				    					timer.side = timer.condition;
				    				}
				    				else
				    				{
										if(indexSoglia > 0 && timer.condition[indexSoglia-1]=='-')
											indexSoglia--;
				    					timer.soglia = parseFloat(timer.condition.substring(indexSoglia));
				    					timer.side = timer.condition.substring(0, indexSoglia);
				    				}
								}
								
								//AC_20171205
								//Se Tapparelle separo separo timer su da timer giù
		    					if(component.service == "rollingShutter"){
			    					switch(state[0]){
										case 'cfgshutd':
											timer.direction = 'Down';
											break;
										case 'cfgshutu':
											timer.direction = 'Up';
										break;
									}
								}

		    					timer.formatTime = function()
		    					{
		    						var minutes = this.minutesFromMidnight % 60;
		    						var hours = (this.minutesFromMidnight - minutes) / 60;

		    						return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes );
		    					};
		    					
								timer.associationNumber = number;
								
								timer.index = currentTimerList.length;

		    					//if(component.timer.filter(function(MyTimer){return MyTimer.associationNumber == timer.associationNumber && MyTimer.index == timer.index;}).length <= 0){
		    						component.timer.push(timer);
		    					//}
		    					//if(currentTimerList.filter(function(MyTimer){return MyTimer.associationNumber == timer.associationNumber && MyTimer.index == timer.index;}).length <= 0){
		    						currentTimerList.push(timer);
								//}
		    				}
		    			break;
		    		}
				}
	            //020317fg per pilotare tutte le uscite incluse nell'associazione
				//component.toggleAssoc = toggleAssoc;
				//component.dimmerAssoc = dimmerAssoc;
			});

			if(component.service == "thermostatus")
			{
				if(currentSensorList.length > 0){
					var segno = '';
					var soglia = 0;
					if(currentSensorList[0].value != undefined && currentSensorList[0].element){//condizione del sensore [cond (<,>,etc...)][soglia]
	                    var indexFirstNum = currentSensorList[0].value.indexOf(currentSensorList[0].value.match(/\d/));//estraggo il primo caratte numerico
	                    if(indexFirstNum != -1){
							if(indexFirstNum > 0 && currentSensorList[0].value[indexFirstNum-1] == '-')
								indexFirstNum--;
							soglia = currentSensorList[0].value.substring(indexFirstNum);
	                        segno = currentSensorList[0].value.substring(0,indexFirstNum);
	                    }
	                }

					switch(state[0])
					{
				        case 'cfgtmanu':
				        {
				        	if(segno == '<' || segno == '<=')//Termico
				        	{
				        		component.thermManu.number = number;

				        		if(component.thermManu.inputList == undefined) 
								{
									component.thermManu.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.thermManu.inputList[id]==undefined)
									{
										component.thermManu.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.thermManu.toggleList == undefined) 
								{
									component.thermManu.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.thermManu.toggleList[id]==undefined)
									{
										component.thermManu.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.thermManu.dimmerList == undefined) 
								{
									component.thermManu.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.thermManu.dimmerList[id]==undefined)
									{
										component.thermManu.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.thermManu.sensorList == undefined) 
								{
									component.thermManu.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.thermManu.sensorList[id]==undefined)
									{
										component.thermManu.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.thermManu.timer == undefined)
								{
									component.thermManu.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.thermManu.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.thermManu.timer.push(timer);
								});

								component.thermManu.assocOutputType = component.assocOutputType;
								component.thermManu.assocInputType = component.assocInputType;
				        	}
				        	else if(segno == '>' || segno == '>=')//Condizionamento
				        	{
				        		component.condManu.number = number;

				        		if(component.condManu.inputList == undefined) 
								{
									component.condManu.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.condManu.inputList[id]==undefined)
									{
										component.condManu.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.condManu.toggleList == undefined) 
								{
									component.condManu.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.condManu.toggleList[id]==undefined)
									{
										component.condManu.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.condManu.dimmerList == undefined) 
								{
									component.condManu.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.condManu.dimmerList[id]==undefined)
									{
										component.condManu.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.condManu.sensorList == undefined) 
								{
									component.condManu.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.condManu.sensorList[id]==undefined)
									{
										component.condManu.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.condManu.timer == undefined)
								{
									component.condManu.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.condManu.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.condManu.timer.push(timer);
								});

								component.condManu.assocOutputType = component.assocOutputType;
								component.condManu.assocInputType = component.assocInputType;
				        	}
				        }break;
				        case 'cfgtauto':
				        {
				        	if(segno == '<' || segno =='<=')//Termico
				        	{
				        		component.thermAuto.number = number;

				        		if(component.thermAuto.inputList == undefined) 
								{
									component.thermAuto.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.thermAuto.inputList[id]==undefined)
									{
										component.thermAuto.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.thermAuto.toggleList == undefined) 
								{
									component.thermAuto.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.thermAuto.toggleList[id]==undefined)
									{
										component.thermAuto.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.thermAuto.dimmerList == undefined) 
								{
									component.thermAuto.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.thermAuto.dimmerList[id]==undefined)
									{
										component.thermAuto.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.thermAuto.sensorList == undefined) 
								{
									component.thermAuto.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.thermAuto.sensorList[id]==undefined)
									{
										component.thermAuto.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.thermAuto.timer == undefined)
								{
									component.thermAuto.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.thermAuto.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.thermAuto.timer.push(timer);
								});

								component.thermAuto.assocOutputType = component.assocOutputType;
								component.thermAuto.assocInputType = component.assocInputType;
				        	}
				        	else if(segno == '>' || segno == '>=')//Condizionamento
				        	{
				        		component.condAuto.number = number;

				        		if(component.condAuto.inputList == undefined) 
								{
									component.condAuto.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.condAuto.inputList[id]==undefined)
									{
										component.condAuto.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.condAuto.toggleList == undefined) 
								{
									component.condAuto.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.condAuto.toggleList[id]==undefined)
									{
										component.condAuto.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.condAuto.dimmerList == undefined) 
								{
									component.condAuto.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.condAuto.dimmerList[id]==undefined)
									{
										component.condAuto.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.condAuto.sensorList == undefined) 
								{
									component.condAuto.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.condAuto.sensorList[id]==undefined)
									{
										component.condAuto.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.condAuto.timer == undefined)
								{
									component.condAuto.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.condAuto.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.condAuto.timer.push(timer);
								});

								component.condAuto.assocOutputType = component.assocOutputType;
								component.condAuto.assocInputType = component.assocInputType;
				        	}
				        }break;
				        case 'cfgtinte':
				        {
				        	if(segno == '<' || segno == '<=')//Termico
				        	{
				        		component.thermInte.number = number;

				        		if(component.thermInte.inputList == undefined) 
								{
									component.thermInte.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.thermInte.inputList[id]==undefined)
									{
										component.thermInte.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.thermInte.toggleList == undefined) 
								{
									component.thermInte.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.thermInte.toggleList[id]==undefined)
									{
										component.thermInte.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.thermInte.dimmerList == undefined) 
								{
									component.thermInte.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.thermInte.dimmerList[id]==undefined)
									{
										component.thermInte.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.thermInte.sensorList == undefined) 
								{
									component.thermInte.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.thermInte.sensorList[id]==undefined)
									{
										component.thermInte.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.thermInte.timer == undefined)
								{
									component.thermInte.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.thermInte.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.thermInte.timer.push(timer);
								});

								component.thermInte.assocOutputType = component.assocOutputType;
								component.thermInte.assocInputType = component.assocInputType;
				        	}
				        	else if(segno == '>' || segno == '>=')//Condizionamento
				        	{
				        		component.condInte.number = number;

				        		if(component.condInte.inputList == undefined) 
								{
									component.condInte.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.condInte.inputList[id]==undefined)
									{
										component.condInte.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.condInte.toggleList == undefined) 
								{
									component.condInte.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.condInte.toggleList[id]==undefined)
									{
										component.condInte.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.condInte.dimmerList == undefined) 
								{
									component.condInte.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.condInte.dimmerList[id]==undefined)
									{
										component.condInte.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.condInte.sensorList == undefined) 
								{
									component.condInte.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.condInte.sensorList[id]==undefined)
									{
										component.condInte.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.condInte.timer == undefined)
								{
									component.condInte.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.condInte.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.condInte.timer.push(timer);
								});

								component.condInte.assocOutputType = component.assocOutputType;
								component.condInte.assocInputType = component.assocInputType;
				        	}
				        }break;
					}
				}
			}

			if(component.service == "thermostatus-external")
				externalSensors.push(component);

			if(component.service == "rollingShutter")
			{
				switch(state[0])
				{
					case 'cfgshutd':
					{
						component.rollDownNumber = number;
						component.rollDownComp.number = number;

						if(component.rollDownComp.inputList == undefined) 
						{
							component.rollDownComp.inputList={};
						}
						currentInputList.forEach(function(input){
							var id = input.element.id;
							if(component.rollDownComp.inputList[id]==undefined)
							{
								component.rollDownComp.inputList[id] = {'element': input.element, 'value': input.value };
							}
						});
						if(component.rollDownComp.toggleList == undefined) 
						{
							component.rollDownComp.toggleList={};
						}
						currentToggleList.forEach(function(toggle){
							var id = toggle.element.id;
							if(component.rollDownComp.toggleList[id]==undefined)
							{
								component.rollDownComp.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
							}
						});
						if(component.rollDownComp.dimmerList == undefined) 
						{
							component.rollDownComp.dimmerList={};
						}
						currentDimmerList.forEach(function(dimmer){
							var id = dimmer.element.id;
							if(component.rollDownComp.dimmerList[id]==undefined)
							{
								component.rollDownComp.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
							}
						});
						if(component.rollDownComp.sensorList == undefined) 
						{
							component.rollDownComp.sensorList={};
						}
						currentSensorList.forEach(function(sensor){
							var id = sensor.element.id;
							if(component.rollDownComp.sensorList[id]==undefined)
							{
								component.rollDownComp.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
							}
						});
						if(component.rollDownComp.timer == undefined)
						{
							component.rollDownComp.timer = [];
						}
						currentTimerList.forEach(function(timer){
							if(component.rollDownComp.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
								component.rollDownComp.timer.push(timer);
						});

						component.rollDownComp.assocOutputType = component.assocOutputType;
						component.rollDownComp.assocInputType = component.assocInputType;
					}break;
					case 'cfgshutu':
					{
						component.rollUpNumber = number;
						component.rollUpComp.number = number;

						if(component.rollUpComp.inputList == undefined) 
						{
							component.rollUpComp.inputList={};
						}
						currentInputList.forEach(function(input){
							var id = input.element.id;
							if(component.rollUpComp.inputList[id]==undefined)
							{
								component.rollUpComp.inputList[id] = {'element': input.element, 'value': input.value };
							}
						});
						if(component.rollUpComp.toggleList == undefined) 
						{
							component.rollUpComp.toggleList={};
						}
						currentToggleList.forEach(function(toggle){
							var id = toggle.element.id;
							if(component.rollUpComp.toggleList[id]==undefined)
							{
								component.rollUpComp.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
							}
						});
						if(component.rollUpComp.dimmerList == undefined) 
						{
							component.rollUpComp.dimmerList={};
						}
						currentDimmerList.forEach(function(dimmer){
							var id = dimmer.element.id;
							if(component.rollUpComp.dimmerList[id]==undefined)
							{
								component.rollUpComp.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
							}
						});
						if(component.rollUpComp.sensorList == undefined) 
						{
							component.rollUpComp.sensorList={};
						}
						currentSensorList.forEach(function(sensor){
							var id = sensor.element.id;
							if(component.rollUpComp.sensorList[id]==undefined)
							{
								component.rollUpComp.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
							}
						});
						if(component.rollUpComp.timer == undefined)
						{
							component.rollUpComp.timer = [];
						}
						currentTimerList.forEach(function(timer){
							if(component.rollUpComp.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
								component.rollUpComp.timer.push(timer);
						});

						component.rollUpComp.assocOutputType = component.assocOutputType;
						component.rollUpComp.assocInputType = component.assocInputType;
					}break;
				}
			}

			component.checkValue = function(bVal)
	        {
	            var checked=0;
	            if(arguments.length){
	                var assoc = component.toggleAssoc.split(';');
	                for(var i=0; i<assoc.length; i++)
	                {
	                    element[assoc[i]].value = bVal;
	                    checked = bVal;
	                }
	            }
	            else
	            {
	                var assoc = component.toggleAssoc.split(';');
	                for(var i=0; i<assoc.length; i++)
	                {
	                	if(element[assoc[i]] &&  element[assoc[i]].value)
	                    	checked += element[assoc[i]].value;
	                }
	            }

	            return checked > 0 ? 1 : 0;
	        };

	        component.rollUpValue = function(bVal)
	        {
	            var checked=0;
	            if(arguments.length){
	                var assoc = component.rollUpAssoc.split(';');
	                for(var i=0; i<assoc.length; i++)
	                {
	                    element[assoc[i]].value = bVal;
	                    checked = bVal;
	                }
	            }
	            else
	            {
	                var assoc = component.rollUpAssoc.split(';');
	                for(var i=0; i<assoc.length; i++)
	                {
	                	if(element[assoc[i]] &&  element[assoc[i]].value)
	                    	checked += element[assoc[i]].value;
	                }
	            }

	            return checked > 0 ? 1 : 0;
	        };

	        component.rollDownValue = function(bVal)
	        {
	            var checked=0;
	            if(arguments.length){
	                var assoc = component.rollDownAssoc.split(';');
	                for(var i=0; i<assoc.length; i++)
	                {
	                    element[assoc[i]].value = bVal;
	                    checked = bVal;
	                }
	            }
	            else
	            {
	                var assoc = component.rollDownAssoc.split(';');
	                for(var i=0; i<assoc.length; i++)
	                {
	                	if(element[assoc[i]] &&  element[assoc[i]].value)
	                    	checked += element[assoc[i]].value;
	                }
	            }

	            return checked > 0 ? 1 : 0;
	        };

	        
	        if(Object.keys(component.sensorList).length == 1)
	        {
	        	if(Object.keys(component.toggleList).length <= 0 && Object.keys(component.dimmerList).length <= 0)
	        	{
	        		component.service = "sensor";
	        		Object.keys(component.sensorList).forEach(function(key){
		        		component.sensorList[key].element.name = componentName;
		        		component.sensorList[key].element.showName = realComponentName;
		        	});
	        	}
	        }

	        if(Object.keys(component.toggleList).length == 1)
	        {
	        	Object.keys(component.toggleList).forEach(function(key){
	        		component.toggleList[key].element.name = componentName;
	        		component.toggleList[key].element.showName = realComponentName;
	        	});
	        }
		}
	},
	_getAllCfgStates = function(associationNumber){
		room = {};
		association = [];

		semaphore.wait();
		semaphore.wait();

    	return $q(function(resolve, reject){
    		var promises = [];

    		for(var i=0; i<associationNumber; ++i){      		
        		promises.push(_getCfgState(i).then(_checkAssociation));
    		}

    		$q.all(promises).then(function(values){
    			semaphore.signal();
				semaphore.signal();
    			resolve(values);
    		});
		});	           
	},
	_getCfgEnable = function(i){
		//Richiesta di stato per l'i-esima associazione
	    //semaphore.reset();
		return _send(
    		_addId() + 'cfgsetup:' + i +  ':2:',//utilizzando 2 
    		'cfgsetup.' + i + '.2',
    		function(event, response, resolve, reject){
	    		resolve(response);
	    	},
	    	function(){
				that.disconnect();
			},
	    	function(event, response){
				return response.number == i;
			},
			1,
			true
    	);
	},
	_evaluateEnable = function(response){
		//Definizione Abilitazione delle diverse stringhe di associazione
		//Struttura della risposta {msgtype:"cfgsetup", number:"idx", rqsttype:"0/1/2", value:"{EN:1, PR:... , PS:..."}

		var value = response.value,
			number = response.number,
			rqstType = response.rqsttype;

		var currentAssociation = association[number];

		if(currentAssociation != undefined && rqstType == 2)
		{
			currentRoom = room[currentAssociation.roomName][currentAssociation.componentName];
			if(currentRoom != undefined)
			{
				association[number].enabled = value.EN;
				if(currentRoom.number == number)
					currentRoom.enabled = value.EN;
				switch(currentRoom.service)
				{
					case "thermostatus":
					{
						if(currentRoom.thermManu && currentRoom.thermManu.number == number)
							currentRoom.thermManu.enabled = value.EN;
						else if(currentRoom.thermAuto && currentRoom.thermAuto.number == number)
							currentRoom.thermAuto.enabled = value.EN;
						else if(currentRoom.thermInte && currentRoom.thermInte.number == number)
							currentRoom.thermInte.enabled = value.EN;
						else if(currentRoom.condManu && currentRoom.condManu.number == number)
							currentRoom.condManu.enabled = value.EN;
						else if(currentRoom.condAuto && currentRoom.condAuto.number == number)
							currentRoom.condAuto.enabled = value.EN;
						else if(currentRoom.condInte && currentRoom.condInte.number == number)
							currentRoom.condInte.enabled = value.EN;
					}break;

				}
			}
		}
	},
	_getAllCfgEnables = function(associationNumber){
		semaphore.wait();
		semaphore.wait();

    	return $q(function(resolve, reject){
    		var promises = [];

    		for(var i=0; i<associationNumber; ++i){      		
        		promises.push(_getCfgEnable(i).then(_evaluateEnable));
    		}

    		$q.all(promises).then(function(values){
    			semaphore.signal();
				semaphore.signal();
    			resolve(values);
    		});
		});	           
	},
	_getEnableLevel = function()
	{
		var enLevel = 0;
		Object.keys(EnableLvs).forEach(function(key){
			if(currentFWVersion >= EnableLvs[key].minFWVersion)
				if(EnableLvs[key].enableLevel > enLevel)
					enLevel = EnableLvs[key].enableLevel;
		});
		return enLevel;
	},
	_getSogliaLivello = function(levelName)
	{
		if(EnableLvs[levelName])
			return EnableLvs[levelName].enableLevel;
		return Number.MAX_VALUE;
	},
	_setFCMToken = function()
	{
		//Richiesta di stato per l'i-esima associazione
		//semaphore.reset();
		var sCurrentToken = fcm.getCurrentToken();
		var sOldToken = fcm.getOldToken();
		
		sOldToken = (sOldToken != null && sOldToken != undefined) ? sOldToken : '';
		if(sCurrentToken != null && sCurrentToken != undefined && sCurrentToken != '')
		{
			//AC_20180518 - Siccome i Token possono contenere caratteri non alfanumerici che possono generare problemi 
			// si è deciso di codificarli in base64 per l'invio alla centrale
			var bCT = btoa(sCurrentToken);
			var bOT = '';
			if(sOldToken != null && sOldToken != undefined && sOldToken != '')
				bOT = btoa(sOldToken);
				
			return _send(
				_addId() + 'setfcmtoken:1:' + bOT + ':' + bCT,
				'setfcmtoken.1',
				function(event, response, resolve, reject){
					resolve(response);
				},
				function(){
					that.disconnect();
				},
				1,
				true
			);
		}
	},
	//AC_20180528
	_getLastUpdateDate = function(){
    	return _getLastUpdate(0).catch(function(err){
    		//console.log('getAssociationNumber error',err);
    	});	;
	},
	_checkDBLastUpdate = function(item)
	{
		readFromDB = false;
		if(item != null && item != undefined && item.value != null && item.value != undefined && item.value != '')
		{
			if(lastUpdate == item.value)
				readFromDB = true;
			lastUpdate = item.value;
		}
		return readFromDB;
	},
	_updateDBLastUpdate = function() //Aggiornamento LastUpdate dell'impianto a DB
	{
		var query = 'SELECT * FROM plant_info WHERE plant_id = ?';

		database.
		execute(query,[currentCentralID]).
		then(function(result){
			if(result.rows.length > 0){
				// Update
				database
				.execute('UPDATE plant_info SET last_update = ? WHERE plant_id = ?', [
					lastUpdate,
					currentCentralID]
				).then(function(result){

				}, function(error){
					// // Error
					// $cordovaDialogs.alert(
					// 	$translate('alert_configurationError'),
					// 	$translate('alert_configurationTitle'),
					// 	$translate('alert_button')
					// );
				});
			}else{
				// Insert
				database
				.execute('INSERT INTO plant_info ( last_update, plant_id) VALUES  ( ?, ? )', [
					lastUpdate,
					currentCentralID]
				).then(function(result){

				}, function(error){
					// // Error
					// $cordovaDialogs.alert(
					// 	$translate('alert_configurationError'),
					// 	$translate('alert_configurationTitle'),
					// 	$translate('alert_button')
					// );
				});
			}
		}, function(error){
			// Error
			// $cordovaDialogs.alert(
			// 	$translate('alert_configurationError'),
			// 	$translate('alert_configurationTitle'),
			// 	$translate('alert_button')
			// );
		});
	};

	_updateDBPlantData = function() //Aggiornamento dati impianto su DB
	{
		if(element != undefined && element != null && association != undefined && association != null )
		{
			if(element != {} && association != {})
			{

				var query = 'SELECT * FROM plant_data WHERE plant_id = ?';

				database.
				execute(query,[currentCentralID]).
				then(function(result){
					if(result.rows.length > 0){
						// Update
						database
						.execute('UPDATE plant_data SET element_value = ?, association_value = ?, control_unit_number = ? WHERE plant_id = ?',[
							JSON.stringify(element),
							JSON.stringify(association),
							controlUnitNumber,
							currentCentralID]
						).then(function(result){

						}, function(error){
							// // Error
							// $cordovaDialogs.alert(
							// 	$translate('alert_configurationError'),
							// 	$translate('alert_configurationTitle'),
							// 	$translate('alert_button')
							// );
						});
					}else{
						// Insert
						database
						.execute("INSERT INTO plant_data ( plant_id, element_value, association_value, control_unit_number ) VALUES ( ?, ?, ?, ? )",[
							currentCentralID,
							JSON.stringify(element),
							JSON.stringify(association),
							controlUnitNumber]
						).then(function(result){

						}, function(error){
							// // Error
							// $cordovaDialogs.alert(
							// 	$translate('alert_configurationError'),
							// 	$translate('alert_configurationTitle'),
							// 	$translate('alert_button')
							// );
						});
					}
				}, function(error){
					// Error
					// $cordovaDialogs.alert(
					// 	$translate('alert_configurationError'),
					// 	$translate('alert_configurationTitle'),
					// 	$translate('alert_button')
					// );
				});

				// database
				// .execute("INSERT INTO plant_data ( plant_id, element_value, association_value, control_unit_number ) VALUES ( ?, ?, ?, ? )", [currentCentralID,JSON.stringify(element),JSON.stringify(association),controlUnitNumber]);
			}
		}
	},
	//20180604 - Crazione Stanza a partire dall'elenco delle associazioni letto da DB
	_readAssociationfromDB = function(result){
		if(result != undefined && result.rows.length > 0)
		{
			var response = result.rows.item(0);
			if(response.element_value && response.association_value && response.control_unit_number){
				element = JSON.parse(response.element_value);
				association = JSON.parse(response.association_value);
				controlUnitNumber = response.control_unit_number;
				associationNumber = association.length;
				association.forEach(function(item,index){

					var roomName,
						component,
						componentName,
						realComponentName;

					roomName = item.roomName
					componentName = item.componentName
					realComponentName = componentName.toString().replace(/_252F/g, "/").replace(/_2523/g,"#");
					room[roomName] = room[roomName] || {};	//adds room object

					// association[number] = association[number] || {};
					// association[number].service = state[0];
					// association[number].groups =[];
					// for(var i=1, n=state.length-1; i<n; i+=2){
					// 	association[number].groups.push({"id":state[i],"data":state[i+1]})
					// }
					// association[number].roomName = roomName;
					// association[number].componentName = componentName;

					var newComp = true;
					if(room[roomName][componentName]){
						if((item.service=='cfgshutd' || item.service=='cfgshutu') && room[roomName][componentName].service == "rollingShutter")
						{
							newComp = false;
						}

						if((item.service=='cfgtmanu' || item.service=='cfgtauto' || item.service=='cfgtinte' ||
							item.service=='cfgcmanu' || item.service=='cfgcauto' || item.service=='cfgcinte') && 
							room[roomName][componentName].service == "thermostatus")
						{
							newComp = false;
						}
					}

					room[roomName][componentName] = room[roomName][componentName] || {	//adds component object
						name: componentName,
						showName : realComponentName.toString()
					};
					component = room[roomName][componentName];
					component.number = index;

					switch(item.service){
						case 'cfgtmanu':
						case 'cfgtauto':
						case 'cfgtinte':
							component.service = "thermostatus";
							break;
						case 'cfgtexte':
							component.service = "thermostatus-external"
							break;
						case 'cfgshutd':
						case 'cfgshutu':
							component.service = "rollingShutter";
							break;
						case 'cfgstate':
							component.service = "light";
							break;
						case 'cfgprior':
							component.service = "consume";
					}

					
					component.groups =[];
					for(var i=0; i<item.groups.length; i++){
						component.groups.push(item.groups[i].id.split(',').concat(item.groups[i].data.split(',')));
					}

					if(newComp){
						component.toggleAssoc = "";
						component.rollDownAssoc = "";
						component.rollUpAssoc = "";
						//AC_20171205;
						component.rollDownComp = {};
						component.rollUpComp = {};

						component.dimmerAssoc = "";
						component.assocOutputType = "";
						component.assocInputType = "";
						//_AC_20170717
						component.sensorList = {};
						component.toggleList = {};
						component.dimmerList = {};
						component.inputList = {};
						component.thermAuto = {};//oggetto per la gestione dell'associazione AUTOMATIC del termostato
						component.thermInte = {};//oggetto per la gestione dell'associazione INTELLIGENT del termostato
						component.thermManu = {};//oggetto per la gestione dell'associazione MANUAL del termostato
						component.condAuto = {};//oggetto per la gestione dell'associazione AUTOMATIC del termostato
						component.condInte = {};//oggetto per la gestione dell'associazione INTELLIGENT del termostato
						component.condManu = {};//oggetto per la gestione dell'associazione MANUAL del termostato
						component.timer = [];
						component.hasTimer = false;
					}


					var currentSensorList = [];
					var currentToggleList = [];
					var currentDimmerList = [];
					var currentInputList = [];
					var currentTimerList = [];

					component.groups.forEach(function(group){
						var obj = _num2codeLookupTable[group[2]] || {},
							CU = group[0] + ',' + group[1],
							code = obj.code,
							id = CU + ':' + code;

						/*if(code)
						{
							//var tokens = code.split(',');
							////console.log("@@CODE "+tokens[0]+","+tokens[1]);
							if(code.indexOf("TI")==-1)
							{
								if(assoc !== "")
									assoc += ";";
								assoc += id;
							}
						}*/

						////console.log("##Element "+id+" undefined!");

						if(code && element[id]!=undefined){
							if(element[id].configured !== 0){

								if(obj.type == 'sensor')
								{
									if(component.sensorList[id]==undefined)
									{
										component.sensorList[id] = {'element': element[id], 'condition': group[3]!=undefined ? group[3] : '' , 'value': group[3]!=undefined ? group[3] : '' };
									}
									currentSensorList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
								}

								if(obj.type == 'toggle')
								{
									if(component.toggleList[id]==undefined)
									{
										component.toggleList[id] = {'element': element[id], 'value': group[3]!=undefined ? group[3] : '' };
									}
									currentToggleList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
								}	

								if(obj.type == 'dimmer')
								{
									if(component.dimmerList[id]==undefined)
									{
										component.dimmerList[id] = {'element': element[id], 'value': group[3]!=undefined ? group[3] : '' };
									}
									currentDimmerList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
								}

								if(obj.type == 'input')
								{
									if(component.inputList[id]==undefined)
									{
										component.inputList[id] = {'element': element[id], 'value': group[3]!=undefined ? group[3] : '' };
									}
									currentInputList.push({'element': element[id], 'value': group[3]!=undefined ? group[3] : '' });
								}

								switch(item.service){
									case 'cfgshutd':
										if( obj.type !== 'input'){
											component.rollingShutterDown = id;
											if(obj.type === 'toggle'){
												if(component.rollDownAssoc && component.rollDownAssoc !== "")
													component.rollDownAssoc += ";";
												component.rollDownAssoc += id;
												//console.log("@@ROLLDOWN:"+roomName+" : "+component.name+" : "+component.rollDownAssoc);
											}
										}
										break;
									case 'cfgshutu':
										if( obj.type !== 'input'){
											component.rollingShutterUp = id;
											if(obj.type === 'toggle'){
												if(component.rollUpAssoc && component.rollUpAssoc !== "")
													component.rollUpAssoc += ";";
												component.rollUpAssoc += id;
											}
										}

										break;
									case 'cfgprior':
										if( obj.type !== 'input'){
											component[obj.type] = id;

											if(obj.type === 'toggle'){
												if(component.outputList == undefined)
													component.outputList = [];
												if(component.outputList.filter(function(out){return out.element && out.element.id && out.element.id === id;}).length<=0)
													component.outputList.push({	'element': element[id],
																				'priority': component.outputList.length,
																				'value': group[3]!=undefined ? group[3] : '' });
											}

										}
										break;
									case 'cfgstate':
										if( obj.type !== 'input'){
											component[obj.type] = id;
											
											if(obj.type === 'toggle'){
				
												if(component.toggleAssoc && component.toggleAssoc !== "")
													component.toggleAssoc += ";";
												component.toggleAssoc += id;
				
												id = id.replace('TO','LO');
												if(element[id] && element[id].configured !== 0){
													if(component.dimmerAssoc && component.dimmerAssoc !== "")
														component.dimmerAssoc += ";";

													component.dimmerAssoc += id;
													component.dimmer = id;
													//delete component.toggle;
												}
											}
										}
										break;
								}
							}
						}else{
							//_AC_20170711 - se non è né un sensore né un ingresso né un uscita
							switch(group[2])
							{
								case '4096':
									component.assocOutputType = 'off';
									break;
								case '8192':
									component.assocOutputType = 'on';
									break;
								case '16384':
									component.assocOutputType = 'toggle';
									break;
								case '32768':
									component.assocInputType = 'toggle';
									break;
								case '32769':
									component.assocInputType = 'and';
									break;
								default:
									//Se group[2] è compreso tra 0x100 e 0x300 è un timer
									if(group[2] >= 256 && group[2] < 768)
									{
										component.hasTimer = false;

										var timer = {};

										timer.disabledDays = [0,0,0,0,0,0,0];
										if( group[0] > 0)
										{
											var val = group[0];
											for(var i=0; i < timer.disabledDays.length; i++)
											{
												var bit = val%2;
												val = (val -bit)/2;
												timer.disabledDays[i]=bit;
											}
										}
										timer.disabledMonth = [0,0,0,0,0,0,0,0,0,0,0,0];
										if( group[1] > 0)
										{
											var val = group[1];
											for(var i=0; i < timer.disabledMonth.length; i++)
											{
												var bit = val%2;
												val = (val -bit)/2;
												timer.disabledMonth[i]=bit;
											}
										}
										
										//Se group[2] è compreso tra 0x100 e 0x200 è un timer OFF
										if(group[2] >= 256 && group[2] < 512)
										{
											//timer.index = group[2]-256;
											timer.type = "off";
										}
										//Se group[2] è compreso tra 0x200 e 0x300 è un timer ON
										else if(group[2] >= 512 && group[2] < 768)
										{
											//timer.index = group[2]-512;
											timer.type = "on";
										}

										//Estraggo Indice del primo caratte non numerico
										var indexFirstNotNum = group[3].indexOf(group[3].match(/\D/));
										if(indexFirstNotNum == -1) indexFirstNotNum = group[3].length;

										timer.minutesFromMidnight = parseInt(group[3].substring(0, indexFirstNotNum));
										
										timer.condition = group[3].substring(indexFirstNotNum).replace(/\+/g,"");
										
										//AC_20180305
										//Check OnlyOnce condition
										var CurrentCondition = timer.condition;
										//AC_20180219 - Inserisco parametro onlyOnce in attesa di sapere come leggerlo
										timer.onlyOnce = 0;
										if(CurrentCondition.endsWith("u"))
										{
											timer.onlyOnce = 1;
											timer.condition = CurrentCondition.slice(0,CurrentCondition.length-1);
										}

										//Se termostato separo verso della condizione dalla soglia
										if(component.service == "thermostatus"){
											var indexSoglia = timer.condition.indexOf(timer.condition.match(/\d/));
											if(indexSoglia == -1)
											{ 
												timer.soglia = 0;
												timer.side = timer.condition;
											}
											else
											{
												if(indexSoglia > 0 && timer.condition[indexSoglia-1]=='-')
													indexSoglia--;
												timer.soglia = parseFloat(timer.condition.substring(indexSoglia));
												timer.side = timer.condition.substring(0, indexSoglia);
											}
										}
										
										//AC_20171205
										//Se Tapparelle separo separo timer su da timer giù
										if(component.service == "rollingShutter"){
											switch(item.service){
												case 'cfgshutd':
													timer.direction = 'Down';
													break;
												case 'cfgshutu':
													timer.direction = 'Up';
												break;
											}
										}

										timer.formatTime = function()
										{
											var minutes = this.minutesFromMidnight % 60;
											var hours = (this.minutesFromMidnight - minutes) / 60;

											return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes );
										};
										
										timer.associationNumber = index;
										
										timer.index = currentTimerList.length;

										//if(component.timer.filter(function(MyTimer){return MyTimer.associationNumber == timer.associationNumber && MyTimer.index == timer.index;}).length <= 0){
											component.timer.push(timer);
										//}
										//if(currentTimerList.filter(function(MyTimer){return MyTimer.associationNumber == timer.associationNumber && MyTimer.index == timer.index;}).length <= 0){
											currentTimerList.push(timer);
										//}
									}
								break;
							}
						}
						//020317fg per pilotare tutte le uscite incluse nell'associazione
						//component.toggleAssoc = toggleAssoc;
						//component.dimmerAssoc = dimmerAssoc;
					});

					if(component.service == "thermostatus")
					{
						if(currentSensorList.length > 0){
							var segno = '';
							var soglia = 0;
							if(currentSensorList[0].value != undefined && currentSensorList[0].element){//condizione del sensore [cond (<,>,etc...)][soglia]
								var indexFirstNum = currentSensorList[0].value.indexOf(currentSensorList[0].value.match(/\d/));//estraggo il primo caratte numerico
								if(indexFirstNum != -1){
									if(indexFirstNum > 0 && currentSensorList[0].value[indexFirstNum-1] == '-')
										indexFirstNum--;
									soglia = currentSensorList[0].value.substring(indexFirstNum);
									segno = currentSensorList[0].value.substring(0,indexFirstNum);
								}
							}

							switch(item.service)
							{
								case 'cfgtmanu':
								{
									if(segno == '<' || segno == '<=')//Termico
									{
										component.thermManu.number = index;

										if(component.thermManu.inputList == undefined) 
										{
											component.thermManu.inputList={};
										}
										currentInputList.forEach(function(input){
											var id = input.element.id;
											if(component.thermManu.inputList[id]==undefined)
											{
												component.thermManu.inputList[id] = {'element': input.element, 'value': input.value };
											}
										});
										if(component.thermManu.toggleList == undefined) 
										{
											component.thermManu.toggleList={};
										}
										currentToggleList.forEach(function(toggle){
											var id = toggle.element.id;
											if(component.thermManu.toggleList[id]==undefined)
											{
												component.thermManu.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
											}
										});
										if(component.thermManu.dimmerList == undefined) 
										{
											component.thermManu.dimmerList={};
										}
										currentDimmerList.forEach(function(dimmer){
											var id = dimmer.element.id;
											if(component.thermManu.dimmerList[id]==undefined)
											{
												component.thermManu.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
											}
										});
										if(component.thermManu.sensorList == undefined) 
										{
											component.thermManu.sensorList={};
										}
										currentSensorList.forEach(function(sensor){
											var id = sensor.element.id;
											if(component.thermManu.sensorList[id]==undefined)
											{
												component.thermManu.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
											}
										});
										if(component.thermManu.timer == undefined)
										{
											component.thermManu.timer = [];
										}
										currentTimerList.forEach(function(timer){
											if(component.thermManu.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
												component.thermManu.timer.push(timer);
										});

										component.thermManu.assocOutputType = component.assocOutputType;
										component.thermManu.assocInputType = component.assocInputType;
									}
									else if(segno == '>' || segno == '>=')//Condizionamento
									{
										component.condManu.number = index;

										if(component.condManu.inputList == undefined) 
										{
											component.condManu.inputList={};
										}
										currentInputList.forEach(function(input){
											var id = input.element.id;
											if(component.condManu.inputList[id]==undefined)
											{
												component.condManu.inputList[id] = {'element': input.element, 'value': input.value };
											}
										});
										if(component.condManu.toggleList == undefined) 
										{
											component.condManu.toggleList={};
										}
										currentToggleList.forEach(function(toggle){
											var id = toggle.element.id;
											if(component.condManu.toggleList[id]==undefined)
											{
												component.condManu.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
											}
										});
										if(component.condManu.dimmerList == undefined) 
										{
											component.condManu.dimmerList={};
										}
										currentDimmerList.forEach(function(dimmer){
											var id = dimmer.element.id;
											if(component.condManu.dimmerList[id]==undefined)
											{
												component.condManu.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
											}
										});
										if(component.condManu.sensorList == undefined) 
										{
											component.condManu.sensorList={};
										}
										currentSensorList.forEach(function(sensor){
											var id = sensor.element.id;
											if(component.condManu.sensorList[id]==undefined)
											{
												component.condManu.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
											}
										});
										if(component.condManu.timer == undefined)
										{
											component.condManu.timer = [];
										}
										currentTimerList.forEach(function(timer){
											if(component.condManu.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
												component.condManu.timer.push(timer);
										});

										component.condManu.assocOutputType = component.assocOutputType;
										component.condManu.assocInputType = component.assocInputType;
									}
								}break;
								case 'cfgtauto':
								{
									if(segno == '<' || segno =='<=')//Termico
									{
										component.thermAuto.number = index;

										if(component.thermAuto.inputList == undefined) 
										{
											component.thermAuto.inputList={};
										}
										currentInputList.forEach(function(input){
											var id = input.element.id;
											if(component.thermAuto.inputList[id]==undefined)
											{
												component.thermAuto.inputList[id] = {'element': input.element, 'value': input.value };
											}
										});
										if(component.thermAuto.toggleList == undefined) 
										{
											component.thermAuto.toggleList={};
										}
										currentToggleList.forEach(function(toggle){
											var id = toggle.element.id;
											if(component.thermAuto.toggleList[id]==undefined)
											{
												component.thermAuto.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
											}
										});
										if(component.thermAuto.dimmerList == undefined) 
										{
											component.thermAuto.dimmerList={};
										}
										currentDimmerList.forEach(function(dimmer){
											var id = dimmer.element.id;
											if(component.thermAuto.dimmerList[id]==undefined)
											{
												component.thermAuto.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
											}
										});
										if(component.thermAuto.sensorList == undefined) 
										{
											component.thermAuto.sensorList={};
										}
										currentSensorList.forEach(function(sensor){
											var id = sensor.element.id;
											if(component.thermAuto.sensorList[id]==undefined)
											{
												component.thermAuto.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
											}
										});
										if(component.thermAuto.timer == undefined)
										{
											component.thermAuto.timer = [];
										}
										currentTimerList.forEach(function(timer){
											if(component.thermAuto.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
												component.thermAuto.timer.push(timer);
										});

										component.thermAuto.assocOutputType = component.assocOutputType;
										component.thermAuto.assocInputType = component.assocInputType;
									}
									else if(segno == '>' || segno == '>=')//Condizionamento
									{
										component.condAuto.number = index;

										if(component.condAuto.inputList == undefined) 
										{
											component.condAuto.inputList={};
										}
										currentInputList.forEach(function(input){
											var id = input.element.id;
											if(component.condAuto.inputList[id]==undefined)
											{
												component.condAuto.inputList[id] = {'element': input.element, 'value': input.value };
											}
										});
										if(component.condAuto.toggleList == undefined) 
										{
											component.condAuto.toggleList={};
										}
										currentToggleList.forEach(function(toggle){
											var id = toggle.element.id;
											if(component.condAuto.toggleList[id]==undefined)
											{
												component.condAuto.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
											}
										});
										if(component.condAuto.dimmerList == undefined) 
										{
											component.condAuto.dimmerList={};
										}
										currentDimmerList.forEach(function(dimmer){
											var id = dimmer.element.id;
											if(component.condAuto.dimmerList[id]==undefined)
											{
												component.condAuto.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
											}
										});
										if(component.condAuto.sensorList == undefined) 
										{
											component.condAuto.sensorList={};
										}
										currentSensorList.forEach(function(sensor){
											var id = sensor.element.id;
											if(component.condAuto.sensorList[id]==undefined)
											{
												component.condAuto.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
											}
										});
										if(component.condAuto.timer == undefined)
										{
											component.condAuto.timer = [];
										}
										currentTimerList.forEach(function(timer){
											if(component.condAuto.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
												component.condAuto.timer.push(timer);
										});

										component.condAuto.assocOutputType = component.assocOutputType;
										component.condAuto.assocInputType = component.assocInputType;
									}
								}break;
								case 'cfgtinte':
								{
									if(segno == '<' || segno == '<=')//Termico
									{
										component.thermInte.number = index;

										if(component.thermInte.inputList == undefined) 
										{
											component.thermInte.inputList={};
										}
										currentInputList.forEach(function(input){
											var id = input.element.id;
											if(component.thermInte.inputList[id]==undefined)
											{
												component.thermInte.inputList[id] = {'element': input.element, 'value': input.value };
											}
										});
										if(component.thermInte.toggleList == undefined) 
										{
											component.thermInte.toggleList={};
										}
										currentToggleList.forEach(function(toggle){
											var id = toggle.element.id;
											if(component.thermInte.toggleList[id]==undefined)
											{
												component.thermInte.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
											}
										});
										if(component.thermInte.dimmerList == undefined) 
										{
											component.thermInte.dimmerList={};
										}
										currentDimmerList.forEach(function(dimmer){
											var id = dimmer.element.id;
											if(component.thermInte.dimmerList[id]==undefined)
											{
												component.thermInte.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
											}
										});
										if(component.thermInte.sensorList == undefined) 
										{
											component.thermInte.sensorList={};
										}
										currentSensorList.forEach(function(sensor){
											var id = sensor.element.id;
											if(component.thermInte.sensorList[id]==undefined)
											{
												component.thermInte.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
											}
										});
										if(component.thermInte.timer == undefined)
										{
											component.thermInte.timer = [];
										}
										currentTimerList.forEach(function(timer){
											if(component.thermInte.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
												component.thermInte.timer.push(timer);
										});

										component.thermInte.assocOutputType = component.assocOutputType;
										component.thermInte.assocInputType = component.assocInputType;
									}
									else if(segno == '>' || segno == '>=')//Condizionamento
									{
										component.condInte.number = index;

										if(component.condInte.inputList == undefined) 
										{
											component.condInte.inputList={};
										}
										currentInputList.forEach(function(input){
											var id = input.element.id;
											if(component.condInte.inputList[id]==undefined)
											{
												component.condInte.inputList[id] = {'element': input.element, 'value': input.value };
											}
										});
										if(component.condInte.toggleList == undefined) 
										{
											component.condInte.toggleList={};
										}
										currentToggleList.forEach(function(toggle){
											var id = toggle.element.id;
											if(component.condInte.toggleList[id]==undefined)
											{
												component.condInte.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
											}
										});
										if(component.condInte.dimmerList == undefined) 
										{
											component.condInte.dimmerList={};
										}
										currentDimmerList.forEach(function(dimmer){
											var id = dimmer.element.id;
											if(component.condInte.dimmerList[id]==undefined)
											{
												component.condInte.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
											}
										});
										if(component.condInte.sensorList == undefined) 
										{
											component.condInte.sensorList={};
										}
										currentSensorList.forEach(function(sensor){
											var id = sensor.element.id;
											if(component.condInte.sensorList[id]==undefined)
											{
												component.condInte.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
											}
										});
										if(component.condInte.timer == undefined)
										{
											component.condInte.timer = [];
										}
										currentTimerList.forEach(function(timer){
											if(component.condInte.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
												component.condInte.timer.push(timer);
										});

										component.condInte.assocOutputType = component.assocOutputType;
										component.condInte.assocInputType = component.assocInputType;
									}
								}break;
							}
						}
					}

					if(component.service == "thermostatus-external")
						externalSensors.push(component);

					if(component.service == "rollingShutter")
					{
						switch(item.service)
						{
							case 'cfgshutd':
							{
								component.rollDownNumber = index;
								component.rollDownComp.number = index;

								if(component.rollDownComp.inputList == undefined) 
								{
									component.rollDownComp.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.rollDownComp.inputList[id]==undefined)
									{
										component.rollDownComp.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.rollDownComp.toggleList == undefined) 
								{
									component.rollDownComp.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.rollDownComp.toggleList[id]==undefined)
									{
										component.rollDownComp.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.rollDownComp.dimmerList == undefined) 
								{
									component.rollDownComp.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.rollDownComp.dimmerList[id]==undefined)
									{
										component.rollDownComp.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.rollDownComp.sensorList == undefined) 
								{
									component.rollDownComp.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.rollDownComp.sensorList[id]==undefined)
									{
										component.rollDownComp.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.rollDownComp.timer == undefined)
								{
									component.rollDownComp.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.rollDownComp.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.rollDownComp.timer.push(timer);
								});

								component.rollDownComp.assocOutputType = component.assocOutputType;
								component.rollDownComp.assocInputType = component.assocInputType;
							}break;
							case 'cfgshutu':
							{
								component.rollUpNumber = index;
								component.rollUpComp.number = index;

								if(component.rollUpComp.inputList == undefined) 
								{
									component.rollUpComp.inputList={};
								}
								currentInputList.forEach(function(input){
									var id = input.element.id;
									if(component.rollUpComp.inputList[id]==undefined)
									{
										component.rollUpComp.inputList[id] = {'element': input.element, 'value': input.value };
									}
								});
								if(component.rollUpComp.toggleList == undefined) 
								{
									component.rollUpComp.toggleList={};
								}
								currentToggleList.forEach(function(toggle){
									var id = toggle.element.id;
									if(component.rollUpComp.toggleList[id]==undefined)
									{
										component.rollUpComp.toggleList[id] = {'element': toggle.element, 'value': toggle.value };
									}
								});
								if(component.rollUpComp.dimmerList == undefined) 
								{
									component.rollUpComp.dimmerList={};
								}
								currentDimmerList.forEach(function(dimmer){
									var id = dimmer.element.id;
									if(component.rollUpComp.dimmerList[id]==undefined)
									{
										component.rollUpComp.dimmerList[id] = {'element': dimmer.element, 'value': dimmer.value };
									}
								});
								if(component.rollUpComp.sensorList == undefined) 
								{
									component.rollUpComp.sensorList={};
								}
								currentSensorList.forEach(function(sensor){
									var id = sensor.element.id;
									if(component.rollUpComp.sensorList[id]==undefined)
									{
										component.rollUpComp.sensorList[id] = {'element': sensor.element, 'value': sensor.value };
									}
								});
								if(component.rollUpComp.timer == undefined)
								{
									component.rollUpComp.timer = [];
								}
								currentTimerList.forEach(function(timer){
									if(component.rollUpComp.timer.filter(function(item){ return item.index == timer.index && item.type == timer.type; }).length <=0)
										component.rollUpComp.timer.push(timer);
								});

								component.rollUpComp.assocOutputType = component.assocOutputType;
								component.rollUpComp.assocInputType = component.assocInputType;
							}break;
						}
					}

					component.checkValue = function(bVal)
					{
						var checked=0;
						if(arguments.length){
							var assoc = component.toggleAssoc.split(';');
							for(var i=0; i<assoc.length; i++)
							{
								element[assoc[i]].value = bVal;
								checked = bVal;
							}
						}
						else
						{
							var assoc = component.toggleAssoc.split(';');
							for(var i=0; i<assoc.length; i++)
							{
								if(element[assoc[i]] &&  element[assoc[i]].value)
									checked += element[assoc[i]].value;
							}
						}

						return checked > 0 ? 1 : 0;
					};

					component.rollUpValue = function(bVal)
					{
						var checked=0;
						if(arguments.length){
							var assoc = component.rollUpAssoc.split(';');
							for(var i=0; i<assoc.length; i++)
							{
								element[assoc[i]].value = bVal;
								checked = bVal;
							}
						}
						else
						{
							var assoc = component.rollUpAssoc.split(';');
							for(var i=0; i<assoc.length; i++)
							{
								if(element[assoc[i]] &&  element[assoc[i]].value)
									checked += element[assoc[i]].value;
							}
						}

						return checked > 0 ? 1 : 0;
					};

					component.rollDownValue = function(bVal)
					{
						var checked=0;
						if(arguments.length){
							var assoc = component.rollDownAssoc.split(';');
							for(var i=0; i<assoc.length; i++)
							{
								element[assoc[i]].value = bVal;
								checked = bVal;
							}
						}
						else
						{
							var assoc = component.rollDownAssoc.split(';');
							for(var i=0; i<assoc.length; i++)
							{
								if(element[assoc[i]] &&  element[assoc[i]].value)
									checked += element[assoc[i]].value;
							}
						}

						return checked > 0 ? 1 : 0;
					};

					
					if(Object.keys(component.sensorList).length == 1)
					{
						if(Object.keys(component.toggleList).length <= 0 && Object.keys(component.dimmerList).length <= 0)
						{
							component.service = "sensor";
							Object.keys(component.sensorList).forEach(function(key){
								component.sensorList[key].element.name = componentName;
								component.sensorList[key].element.showName = realComponentName;
							});
						}
					}

					if(Object.keys(component.toggleList).length == 1)
					{
						Object.keys(component.toggleList).forEach(function(key){
							component.toggleList[key].element.name = componentName;
							component.toggleList[key].element.showName = realComponentName;
						});
					}
				})
			}
		}
	},
	_evaluateEnableFromDB = function() //Lettura abilitazioni da DB
	{
		for(var i=0; i < association.length; i++)
		{
			var currentAssociation = association[i];

			if(currentAssociation != undefined)
			{
				currentRoom = room[currentAssociation.roomName][currentAssociation.componentName];
				if(currentRoom != undefined)
				{
					if(currentRoom.number == i)
						currentRoom.enabled = association[i].enabled;
					switch(currentRoom.service)
					{
						case "thermostatus":
						{
							if(currentRoom.thermManu && currentRoom.thermManu.number == i)
								currentRoom.thermManu.enabled = association[i].enabled;
							else if(currentRoom.thermAuto && currentRoom.thermAuto.number == i)
								currentRoom.thermAuto.enabled = association[i].enabled;
							else if(currentRoom.thermInte && currentRoom.thermInte.number == i)
								currentRoom.thermInte.enabled = association[i].enabled;
							else if(currentRoom.condManu && currentRoom.condManu.number == i)
								currentRoom.condManu.enabled = association[i].enabled;
							else if(currentRoom.condAuto && currentRoom.condAuto.number == i)
								currentRoom.condAuto.enabled = association[i].enabled;
							else if(currentRoom.condInte && currentRoom.condInte.number == i)
								currentRoom.condInte.enabled = association[i].enabled;
						}break;
					}
				}
			}
		}
	},
	_readPlantFromDB =  function()//Lettura Dati Impianto da DB
	{
		room = {};
		association = [];
		element = {};

		semaphore.wait();
		semaphore.wait();
		
    	return $q(function(resolve, reject){
    		var promises = [];

    		promises.push(database.execute('SELECT * FROM plant_data WHERE plant_id=?',[currentCentralID]).then(_readAssociationfromDB));

    		$q.all(promises).then(function(values){
    			semaphore.signal();
				semaphore.signal();
    			resolve(values);
    		});
		});	    
	},
	_clearArGetStates = function()
	{
		arGetStates = {};
	}

	this.connectionInit = function(host, port){
		socket.close();
	    connected = false;
		return _init(host,port);
	};

	this.connect = function(type, apiKey, password){
		return _connect(type, apiKey, password);
	};

	this.getApiKey = function(){
	    return _getApiKey();
	}

    this.disconnect = function(){
        //console.log("@##@DISCONNECT-------------");
	    socket.close();
		connected = false;
	    $rootScope.$emit('socketDisconnected');
	};

	this.isOnline = function(){
	    return connected;
	};

	this.init = function(){

    	return $q(function(resolve, reject){
			_clearArGetStates();
			_getFWVersion().then(function(response){
				if(response.rqsttype != undefined && response.rqsttype == 3 && response.value != undefined && response.value.toString().length == 6)
				{
					currentFWVersion = response.value;
				}
				_getLastUpdateDate().then(function(response){
					if(_checkDBLastUpdate(response)) //Se la data di LastUdate letta da centrale e quella letta da DB per la centrale corrente coincido leggo le info da DB
					{
						_readPlantFromDB().then(function(){
							_evaluateEnableFromDB();
							initialized = true;
							resolve('Init completed successfully');
						})
					}
					else{
						_getControlUnitNumber().then(function(response){
							controlUnitNumber = response.value;
							_getAllConfigsAndStates(response.value).then(function(response){
								_getAssociationNumber().then(function(response){
									associationNumber = response.value;
									_getAllCfgStates(response.value).then(function(response){
										if(_getEnableLevel() < _getSogliaLivello("L1"))
										{
											initialized = true;
											resolve('Init completed successfully');
										}
										else
										{
											_getAllCfgEnables(associationNumber).then(function(response){

												_updateDBLastUpdate();
												_updateDBPlantData();

												// Initialization completed
												initialized = true;
												resolve('Init completed successfully');
											});
										}
									});
								});  
							});
						});
					}
				});
			});
		});
	};

	this.isInitialized = function(){
	    return initialized;
	};

	this.service = function(name){
	    return name ? service[name] : service;
	};

	this.serviceExist = function(name){

	    return (service.indexOf(name) !== -1) ? true : false;
	};

	//AC_20171023
	this.roomExist = function(name){

	    return (room[name] !== undefined) ? true : false;
	};

	this.roomServices = function(name){
		
		var services = [];

		if(this.roomExist(name))
		{
			var components = this.roomComponents(name).forEach(function(comp)
			{
				if(services.indexOf(comp.service) == -1)
					services.push(comp.service);
			})
		}

		return services;
	}


	this.roomComponents = function(name){
		return Object.values(room[name]);
	};

	this.element = function(){
		return element;
	};

	this.roomNames = function(service, excludeService){

		var arRooms = [];

		if(service){
			if(excludeService != undefined && excludeService == true)
				arRooms = Object.keys(room).filter(function(x){return Object.values(room[x]).filter(function(y){return y.service !== service;}).length > 0; });			
			else
	    		arRooms = Object.keys(room).filter(function(x){return Object.values(room[x]).filter(function(y){return y.service === service;}).length > 0; });
		}else{
			arRooms = Object.keys(room);
		}

		return arRooms.filter(function(x){return !( x.startsWith("_2523") && x.endsWith("_2523") ); });
	};

	this.setState = function(id,value){
		if(id.indexOf('PR')>-1 || id.indexOf('PS')>-1){
			element[id].configured = value;
		}else{
			element[id].value = value;
		}

		return _send(
    		_addId() + 'setstate:' + _id2numId(id) + ',' + value + ':',
    		'setstate',
    		function(event, response, resolve, reject){
    			if(response.value === 1){
    				resolve('SetState OK');
    			}else{
    				reject(Error('SetState Error'));
    			}
	    	},
	    	function(){
				Error('SetState Error');
			},
			1
    	);
	};

	this.setCfgState = function(id,value){

		var index = parseInt(id);
		if(index != NaN && index != undefined)//Indice valido per l'invio della configurazione
		{
			return _send(
	    		_addId() + 'setcfgstate:' + index + ':' + value,
	    		'setcfgstate',
	    		function(event, response, resolve, reject){
	    			if(response.value === 1){
	    				resolve('SetCfgState OK');
	    			}else{
	    				reject(Error('SetCfgState Error'));
	    			}
		    	},
		    	function(){
					Error('SetCfgState Error');
				},
				1
    		);
		}
		else
		{
			Error('SetCfgState Error');
		}

	};

	this.deleteCfgState = function(id){

		var index = parseInt(id);
		if(index != NaN && index != undefined && index < association.length)//Indice valido per l'invio della configurazione
		{
			return _send(
	    		_addId() + 'delcfgstate:' + index + ':',
	    		'delcfgstate',
	    		function(event, response, resolve, reject){
	    			if(response.value === 1){
	    				resolve('DeleteCfgState OK');
	    			}else if(response.value === 6){
	    				reject(Error('DeleteCfgState Error 6 - Invalid index ' + response.number));
	    			}else{
	    				reject(Error('DeleteCfgState Error'));
	    			}
		    	},
		    	function(){
					Error('DeleteCfgState Error');
				},
				1
    		);
		}
		else
		{
			Error('DeleteCfgState Error');
		}

	};

	this.enableCfgState = function(id,forceUpdate){

		var index = parseInt(id);
		if(index != NaN && index != undefined && (index < association.length || (forceUpdate !=undefined && forceUpdate == true)))//Indice valido per l'invio della configurazione
		{
			return _send(
	    		_addId() + 'cfgsetup:' + index + ':1:',
	    		'cfgsetup.' + index + '.1',
	    		function(event, response, resolve, reject){
	    			if(response.rqsttype === 1 && response.number === index.toString()){
	    				resolve('cfgsetup OK');
	    				association[index].enabled = 1;
	    			}else{
	    				reject(Error('cfgsetup Error'));
	    			}
		    	},
		    	function(){
					Error('cfgsetup Error');
				},
				1
    		);
		}
		else
		{
			Error('cfgsetup Error');
		}

	};

	this.disableCfgState = function(id,forceUpdate){

		var index = parseInt(id);
		if(index != NaN && index != undefined && (index < association.length || (forceUpdate !=undefined && forceUpdate == true)))//Indice valido per l'invio della configurazione
		{
			return _send(
	    		_addId() + 'cfgsetup:' + index + ':0:',
	    		'cfgsetup.' + index + '.0',
	    		function(event, response, resolve, reject){
	    			if(response.rqsttype === 0 && response.number === index.toString()){
	    				resolve('cfgsetup OK');
	    				association[index].enabled = 0;
	    			}else{
	    				reject(Error('cfgsetup Error'));
	    			}
		    	},
		    	function(){
					Error('cfgsetup Error');
				},
				1
    		);
		}
		else
		{
			Error('cfgsetup Error');
		}

	};

	//AC_20171205
	this.setCfgEnable = function(id,value,forceUpdate){
		var intValue = (value == true)?1:0;
		var index = parseInt(id);
		if(index != NaN && index != undefined && (index < association.length || (forceUpdate !=undefined && forceUpdate == true)))//Indice valido per l'invio della configurazione
		{
			return _send(
	    		_addId() + 'cfgsetup:' + index + ':'+ intValue + ':',
	    		'cfgsetup.' + index + '.' + intValue,
	    		function(event, response, resolve, reject){
	    			if(response.rqsttype === intValue && response.number === index.toString()){
	    				resolve('cfgsetup OK');
	    				association[index].enabled = intValue;
	    			}else{
	    				reject(Error('cfgsetup Error'));
	    			}
		    	},
		    	function(){
					Error('cfgsetup Error');
				},
				1
    		);
		}
		else
		{
			Error('cfgsetup Error');
		}
	};
	
	//AC_20180108
	this.startAsync = function(){
		console.log('startAsync');
		return _send(
			_addId() + 'asyncenable:1:',
			'asyncenable.1',
			function(event, response, resolve, reject){
				if(response.rqsttype === 1 && response.value === 1){
					resolve('asyncenable OK');
				}else{
					reject(Error('asyncenable Error'));
				}
			},
			function(){
				Error('asyncenable Error');
			},
			1
		);
	};

	//AC_20180108
	this.stopAsync = function(){
		console.log('stopAsync');
		return _send(
			_addId() + 'asyncenable:0:',
			'asyncenable.0',
			function(event, response, resolve, reject){
				if(response.rqsttype === 0 && response.value === 1){
					resolve('asyncenable OK');
				}else{
					reject(Error('asyncenable Error'));
				}
			},
			function(){
				Error('asyncenable Error');
			},
			1
		);
	};

	this.associations = function(){
		return association;
	};

	this.getAssociationNumber = function(){
		return associationNumber;
	};

	this.externalSensors = function(){
		return externalSensors;
	};

	this.id2numId = function(value){
		return _id2numId(value);
	}

	this.getEnableLevel = function()
	{
		return _getEnableLevel();
	}

	//AC_2080320
	this.getAllCfgEnables = function()
	{
		return _getAllCfgEnables(associationNumber);
	}

	this.getRequiredFWVersion = function()
	{
		var enLevel = 0;
		var requiredFWVersion = '000000';
		Object.keys(EnableLvs).forEach(function(key){
			if(requiredFWVersion < EnableLvs[key].minFWVersion)
				requiredFWVersion = EnableLvs[key].minFWVersion;
		});
		return requiredFWVersion;
	}

	this.CurrentFWVersion = function()
	{
		return currentFWVersion;
	}

	this.getSogliaLivello=function(levelName)
	{
		return _getSogliaLivello(levelName);
	}

	//AC_20112017
	this.customServices = function(){
	    	return Object.keys(room).filter(function(x){return ( x.startsWith("_2523") && x.endsWith("_2523") ); });
	};

	//AC_20180123
	this.updateState = function()
	{
		return $q(function(resolveAll, rejectAll){

    		var promises = [],
    			rejAll = function(){
    				rejectAll('Error getting config and state');
    			};


    		for(var i=0; i<controlUnitNumber; ++i){
    			var address = parseInt(i / 8);
        		var object = parseInt(i % 8);

        		promises.push(_getState(address, object).then(function(response){
					if(response != undefined && response.value != undefined){
						try{
									
							if(response.value == 19 || response.value == 20)
							{
								var currentRespAddr = arGetStates[response.id].address,
									currentRespObj = arGetStates[response.id].object;
								
								delete arGetStates[response.id.padZero(6)];
							}
							else{
								delete arGetStates[response.id.padZero(6)];									
								
								var currentRespAddr = response.address,
									currentRespObj = response.object,
									id;
								
								for(var elem in response.value){
									id = currentRespAddr + ',' + currentRespObj + ':' + elem;

									if(!element[id]){
										if(element[currentRespAddr + ',' + currentRespObj + ':' + elem.split('_')[0]]!=undefined)
											element[currentRespAddr + ',' + currentRespObj + ':' + elem.split('_')[0]].unit = response.value[elem].replace(/ï¿½/g," ");
									}else{
										element[id].value = response.value[elem];
									}
								}
							}
						}
						catch(exception)
						{
							rejAll();
						}
					}
					else{
						rejAll();
					}
				}).catch(rejAll));
    		}

    		$q.all(promises).then(function(values){
    			resolveAll(values);
    		});
		});	       		
	}

	this.setFCMToken = function()
	{
		return _setFCMToken();
	}

	//AC_20180528
	this.setCurrentCentralID = function(value)
	{
		if(value != undefined && value!= null)
		{
			currentCentralID = value;
		}
	}
	this.getCurrentCentralID = function()
	{
		return currentCentralID;
	}
	//AC_20180529
	this.setDBLastUpdate = function(value)
	{
		if(value != undefined && value!= null)
		{
			lastUpdate = value;
		}
	}
	this.getDBLastUpdate = function()
	{
		return lastUpdate;
	}

	this.semaphoreWait = function(){
		return semaphore.wait();
	}

	this.semaphoreSignal = function(){
		return semaphore.signal();
	}

	this.semaphoreReset = function(){
		return semaphore.reset();
	}

	this.clearPlantData = function(centralID)
	{
		return $q(function(resolve, reject){
			
			if(centralID == undefined)
				centralID = currentCentralID;

			lastUpdate = "";

			var promises = [];

    		promises.push(database.execute("DELETE FROM plant_info WHERE plant_id = ?",[centralID]));
    		promises.push(database.execute("DELETE FROM plant_data WHERE plant_id = ?",[centralID]));
    		

    		$q.all(promises).then(function(values){
    			resolve(values);
    		});
		});	
	}

	// this.EnableCypher=function()//CYPHER
	// {
	// 	that.enCypher = true;
	// }

	// this.DisableCypher=function()
	// {
	// 	that.enCypher = false;
	// }

    var clearGetStateArray = function () {
		_clearArGetStates();
	};
});