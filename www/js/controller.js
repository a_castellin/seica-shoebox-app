/*
 * controller.js
 * Defines the controllers for application.
 *
 * Manuel Minopoli - INTAG S.r.l.
 * Ferdinando Celentano
 */

 var _default_port = "8081";
 var _default_server = "domo.seica.com";
 var _STATE_UPDATE_TIMEOUT = 60 * 1000;
 var _ENABLE_UPDATE_TIMEOUT = 30 * 1000;

 function fromTimerToCalendar(timer){};
 function fromCalendarToTimer(Calendar){};

angular
.module('starter.controllers', [])
.controller('InitializationController', function($scope, $rootScope, $q, $cordovaNetwork, $cordovaDialogs, $filter, $ionicPlatform, $state, database, controlSystem, $ionicHistory){

    $rootScope.connectionErrorDialogOpen = false;
    //AC_20180123
    $scope.stopUpdateState();
    //AC_20180320
    $scope.stopUpdateEnable();
    //AC_20112017
    $rootScope.navigateByRoom = false;
    $rootScope.navigateByCustomService = false;

    var $translate = $filter('translate'),
        connect = function(connections){
            return $q(function(resolve, reject){
                var connection = connections.shift(),
                    tryNext = function(){
                        //console.log('connection error');
                        connect(connections).then(function(data){
                            resolve(data);
                        }).catch(function(err){
                            reject(err);
                        });
                    };

                if(typeof connection !== 'undefined'){
                    $scope.connectionType = connection.type;
                    //_AC_20170730
                    if($cordovaNetwork.getNetwork() !== 'wifi' && $cordovaNetwork.getNetwork() !== 'ethernet' && connection.type=="lan")
                    {
                        tryNext();
                    }
                    else
                    {
                        controlSystem.connectionInit(connection.host, connection.port)
                        .then(function(){
                            controlSystem.connect(connection.type,connection.api, connection.password)
                            .then(function(){
                                resolve("Connected to " + connection.type);
                                

                                //210317fg quando mi collego la prima volta in locale ad una carrier, configuro automaticamente la connessione cloud
                                if(connection.type=="lan")
                                {
                                    controlSystem.getApiKey()
                                    .then(function(val){
                                        //AC_20180528
                                        controlSystem.setCurrentCentralID(val);
                                        
                                        ////console.log("@@Get APIKEY DONE:"+val);
                                        database.
                                        execute('SELECT password FROM configuration WHERE type="lan" AND name=?',[$rootScope.selectedDevice] ).
                                        then(function(result){
                                            var password = result.rows.item(0).password;

                                            var query = 'SELECT * FROM configuration WHERE type = "cloud" AND name=?';

                                            database.
                                            execute(query,[$rootScope.selectedDevice]).
                                            then(function(result){
                                                if(result.rows.length > 0){
                                                    if(result.rows.item(0).api != val)
                                                        controlSystem.clearPlantData(result.rows.item(0).api);
                                                    // Update
                                                    database
                                                    .execute('UPDATE configuration SET api = ?, host = ?, password = ?, port = ? WHERE id = ?', [
                                                        val,
                                                        (result.rows.item(0).host != undefined && result.rows.item(0).host != '')? result.rows.item(0).host : _default_server,
                                                        password,
                                                        (result.rows.item(0).port != undefined && result.rows.item(0).port != '')? result.rows.item(0).port :_default_port,
                                                        result.rows.item(0).id]
                                                    ).then(function(result){

                                                    }, function(error){
                                                        // Error
                                                        $cordovaDialogs.alert(
                                                            $translate('alert_configurationError'),
                                                            $translate('alert_configurationTitle'),
                                                            $translate('alert_button')
                                                        );
                                                    });
                                                }else{
                                                    // Insert
                                                    database
                                                    .execute('INSERT INTO configuration ( api, host, name, password, port, type ) VALUES  ( ?, ?, ?, ?, ?, ? )', [
                                                        val,
                                                        _default_server,
                                                        $rootScope.selectedDevice,
                                                        password,
                                                        _default_port,
                                                        "cloud"]
                                                    ).then(function(result){

                                                    }, function(error){
                                                        // Error
                                                        $cordovaDialogs.alert(
                                                            $translate('alert_configurationError'),
                                                            $translate('alert_configurationTitle'),
                                                            $translate('alert_button')
                                                        );
                                                    });
                                                }
                                            }, function(error){
                                                // Error
                                                $cordovaDialogs.alert(
                                                    $translate('alert_configurationError'),
                                                    $translate('alert_configurationTitle'),
                                                    $translate('alert_button')
                                                );
                                            });
                                        });

                                    });
                                }
                                //AC_20180528
                                else
                                {
                                    controlSystem.getApiKey()
                                    .then(function(val){
                                        //AC_20180528
                                        controlSystem.setCurrentCentralID(val);
                                    });
                                }
                            })
                            .catch(function(err){
                                tryNext();
                            });
                        })
                        .catch(function(err){
                            tryNext();
                        });
                    }
                }else{
                    //console.log('No more connections');
                    reject(Error("No more connections"));
                }                           
            });
        };

    $scope.loadingMessage = $translate("initialization_configuration");
    $scope.connectionType = '';

    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
        //$state.reload();
    });

    $scope.$on('$ionicView.afterEnter', function(event, data){
        $ionicPlatform.ready(function(){
            var connectionError = function(){
                if(!$rootScope.connectionErrorDialogOpen)
                {
                    $rootScope.connectionErrorDialogOpen = true;
                    $cordovaDialogs.alert(
                        $translate('alert_connectionError'),
                        $translate('alert_connectionTitle'),
                        $translate('alert_button')
                    );
                }

                $state.go('app.configuration');
            };
            
            // Checks configuration
            database
            .execute('SELECT * FROM configuration WHERE password IS NOT NULL AND password <> \'\' AND name = ?', [$rootScope.selectedDevice])
            .then(function(result){
            	var resultLength = result.rows.length;
                if(!resultLength){
                    $state.go('app.configuration');
                    
                    $cordovaDialogs.alert(
                    	$translate('alert_noConfigurationError'),
                    	$translate('alert_noConfigurationTitle'),
                    	$translate('alert_button')
                    );
                }else{
                	var connections = [];
                		
                	for(var i=0;i<resultLength;++i){
                		connections.push(result.rows.item(i));
                	}

                	connections.sort(function(a, b){
						switch(a.type){
							case 'lan': return -1;
							case 'wan': return b.type==='lan' ? 1 : -1;
							case 'cloud': return 1;
						}
					});

                	$scope.loadingMessage = $translate("initialization_connection");
                	connect(connections).then(function(){
                	    //console.log("@##@INIT--------");
                        $scope.loadingMessage = $translate("initialization_data");
                        //controlSystem.stopAsync();
                		controlSystem.init().then(function(response){
                            //AC_20180108 - Abilita invio asincroni da parte della centrale
                            if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L2'))
                            { 
                                controlSystem.startAsync();
                            }
                            //AC_20180123
                            $scope.startUpdateState();
                            //AC_20180320
                            if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L1'))
                            { 
                                $scope.startUpdateEnable();
                            }
                            //AC_20180508
                            if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L3'))
                            { 
                                controlSystem.setFCMToken();
                            }
                            
                            
                            $state.go('app.dashboard');
	                	})
                		.catch(function(err){
                            console.dir(err);
	                		connectionError();
                			controlSystem.disconnect();
	                	});
                	}).catch(function(err){
                        console.dir(err);                        
                		connectionError();
                		controlSystem.disconnect();
                	});
                }
            }, function(error){
                alert(error);
            });
        });
    });

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault(); 
        navigator.app.exitApp();
    },1000);
}).controller('ConfigurationController', function($scope, $rootScope, $cordovaDialogs, $filter, $state, database, controlSystem, $ionicHistory, $ionicPlatform){
    
    //AC_20180518 - azzero la notifica selezionata perchè se arrivo qua dopo un visualizza vuol dire che la connessione con la centrale è fallita
    $rootScope.selectedNotification = undefined; 
    //AC_20180123
    $scope.stopUpdateState();
    //AC_20180320
    $scope.stopUpdateEnable();
    if(controlSystem.isOnline()){
        controlSystem.disconnect();
    }

    var $translate = $filter('translate');
    $scope.$on('$ionicView.afterEnter', function(event, data){});

    // Defines scopes
    $scope.load = function(type){
        // Manages views
        $('#cloudContainer').hide();
        $('#lanContainer').hide();
        $('#wanContainer').hide();

        $('#cloudLabel').removeClass('active');
        $('#lanLabel').removeClass('active');
        $('#wanLabel').removeClass('active');

        $('#' + type + 'Container').show();
        $('#' + type + 'Label').addClass('active');

        // Loads data
        var query = 'SELECT * FROM configuration WHERE type = ? AND name = ?';
        
        database
        .execute(query, [type,$rootScope.selectedDevice])
        .then(function(result){
            if(result.rows.length > 0) {
                // Gets data
                if(type === 'cloud'){
                	$('#' + type + 'Api').val(result.rows.item(0).api);
                }
                $('#' + type + 'Host').val(result.rows.item(0).host);
                $('#' + type + 'Name').val(result.rows.item(0).name);
                $('#' + type + 'Password').val(result.rows.item(0).password);
                $('#' + type + 'Port').val(result.rows.item(0).port);
            }
            else{
                $('#' + type + 'Name').val($('#lanName').val());
            }

        }, function(error){
            // Error
            $cordovaDialogs.alert(
            	$translate('alert_configurationError'),
            	$translate('alert_configurationTitle'),
            	$translate('alert_button')
            );
        });
    };


    $scope.save = function(type){

        //AC_20180529 Pulizia impianto memorizzato
        // Saves data
        var query = 'SELECT * FROM configuration WHERE type = ? AND name = ?';
        
        database.
        execute(query, ['cloud',$rootScope.selectedDevice]).
        then(function(result){
            if(result.rows.length > 0){
                controlSystem.clearPlantData(result.rows.item(0).api)
            }

            // Saves data
            var query = 'SELECT * FROM configuration WHERE type = ? AND name = ?';
            
            database.
            execute(query, [type,$rootScope.selectedDevice]).
            then(function(result){
                if(result.rows.length > 0){
                    // Update
                    database
                    .execute('UPDATE configuration SET api = ?, host = ?, password = ?, port = ?, type = ? WHERE id = ?', [
                        $('#' + type + 'Api').val(),
                        $('#' + type + 'Host').val(),
                        $('#' + type + 'Password').val(),
                        $('#' + type + 'Port').val(),
                        type,
                        result.rows.item(0).id]
                    ).then(function(result){

                        var newName = $('#' + type + 'Name').val();
                        if(newName != $rootScope.selectedDevice){
                            database
                            .execute('UPDATE configuration SET name = ? WHERE name = ?', [
                                newName,
                                $rootScope.selectedDevice]
                            ).then(function(result){
                                database
                                .execute('UPDATE configuration_alert SET name = ? WHERE name = ?', [
                                    newName,
                                    $rootScope.selectedDevice]
                                ).then(function(result){
                                    // Confirm
                                    $cordovaDialogs.alert(
                                        $translate('alert_configurationSuccess'),
                                        $translate('alert_configurationTitle'),
                                        $translate('alert_button')
                                    ).then(function(){
                                        $rootScope.selectedDevice = newName;
                                    });
                                }, function(error){
                                    // Error
                                    $cordovaDialogs.alert(
                                        $translate('alert_configurationError'),
                                        $translate('alert_configurationTitle'),
                                        $translate('alert_button')
                                    );
                                });
                            }, function(error){
                                // Error
                                $cordovaDialogs.alert(
                                    $translate('alert_configurationError'),
                                    $translate('alert_configurationTitle'),
                                    $translate('alert_button')
                                );
                            });
                        }else
                        {
                            // Confirm
                            $cordovaDialogs.alert(
                                $translate('alert_configurationSuccess'),
                                $translate('alert_configurationTitle'),
                                $translate('alert_button')
                            ).then(function(){
                                // Reloads configuration by initialization
                                //$state.go('app.initialization');
                            });
                        }
                    }, function(error){
                        // Error
                        $cordovaDialogs.alert(
                            $translate('alert_configurationError'),
                            $translate('alert_configurationTitle'),
                            $translate('alert_button')
                        );
                    });
                }else{
                    // Insert                
                    database
                    .execute('INSERT INTO configuration ( api, host, name, password, port, type ) VALUES  ( ?, ?, ?, ?, ?, ? )', [
                        $('#' + type + 'Api').val(),
                        $('#' + type + 'Host').val(),
                        $('#' + type + 'Name').val(),
                        $('#' + type + 'Password').val(),
                        $('#' + type + 'Port').val(),
                        type]
                    ).then(function(result){
                        //Confirm
                        var newName = $('#' + type + 'Name').val();
                        if(newName != $rootScope.selectedDevice){
                            database
                            .execute('UPDATE configuration SET name = ? WHERE name = ?', [
                                newName,
                                $rootScope.selectedDevice]
                            ).then(function(result){
                                database
                                .execute('UPDATE configuration_alert SET name = ? WHERE name = ?', [
                                    newName,
                                    $rootScope.selectedDevice]
                                ).then(function(result){
                                    // Confirm
                                    $cordovaDialogs.alert(
                                        $translate('alert_configurationSuccess'),
                                        $translate('alert_configurationTitle'),
                                        $translate('alert_button')
                                    ).then(function(){
                                        $rootScope.selectedDevice = newName;
                                    });
                                }, function(error){
                                    // Error
                                    $cordovaDialogs.alert(
                                        $translate('alert_configurationError'),
                                        $translate('alert_configurationTitle'),
                                        $translate('alert_button')
                                    );
                                });
                            }, function(error){
                                // Error
                                $cordovaDialogs.alert(
                                    $translate('alert_configurationError'),
                                    $translate('alert_configurationTitle'),
                                    $translate('alert_button')
                                );
                            });
                        }else
                        {
                            // Confirm
                            $cordovaDialogs.alert(
                                $translate('alert_configurationSuccess'),
                                $translate('alert_configurationTitle'),
                                $translate('alert_button')
                            ).then(function(){
                                // Reloads configuration by initialization
                                //$state.go('app.initialization');
                            });
                        }
                    }, function(error){
                        // Error
                        $cordovaDialogs.alert(
                            $translate('alert_configurationError'),
                            $translate('alert_configurationTitle'),
                            $translate('alert_button')
                        );
                    });
                }
            }, function(error){
                // Error
                $cordovaDialogs.alert(
                    $translate('alert_configurationError'),
                    $translate('alert_configurationTitle'),
                    $translate('alert_button')
                );
            });
        });
    };

    $scope.verify = function(){
        // Checks configuration status
        if(login !== null){
        	$state.go('app.dashboard');
        }
    };

    $scope.connect = function(){
        
        $scope.initialization();
        //$state.go('app.initialization');
    };

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();  
        navigator.app.exitApp();
    },1000);    

}).controller('DashboardController', function($scope, $cordovaDialogs, $rootScope, $filter, $state, controlSystem, database, $ionicHistory,$ionicPopup, SecuredPopups, $ionicPlatform){
    var $translate = $filter('translate');

    $scope.currentNavigateByRoom = $rootScope.navigateByRoom;

    $rootScope.navigateByRoom = false; //azzeramento flag di navigazione per stanza
    //AC_21112017
    $rootScope.navigateByCustomService = false;

    $scope.hideDialog = $rootScope.hideDialog;
    $scope.changeHideValue = function()
    {
        $scope.hideDialog = !$scope.hideDialog;
    }

    $scope.$on('$ionicView.afterEnter', function(event, data){

        if($scope.previousPage == 'app.initialization'){ //se dopo scaricamento dati        
            var currentFW = controlSystem.CurrentFWVersion();
            var requiredFW = controlSystem.getRequiredFWVersion();

            if(currentFW < requiredFW && !$scope.hideDialog)
            {
                // $cordovaDialogs.alert(
                //     $translate('fw_updateBody') + 
                //     '\r\n\r\n' + $translate('fw_updateBodyRequired') + '\r\n' + requiredFW +
                //     '\r\n\r\n' + $translate('fw_updateBodyCurrent') + '\r\n' + currentFW,
                //     $translate('fw_updateTitle'),
                //     $translate('fw_update_button')
                // );

                // custom popup
                // var updFWPopup = $ionicPopup.show({
                var updFWPopup = SecuredPopups.show('show',{
                    template:   '<div>'+
                                $translate('fw_updateBody') + 
                                '<br><br>' + $translate('fw_updateBodyRequired') + '<br>' + requiredFW +
                                '<br><br>' + $translate('fw_updateBodyCurrent') + '<br>' + currentFW+
                                '<br><br><ion-checkbox type="checkbox" ng-model="hideDialog" ng-change="changeHideValue()">' + $translate('fw_updateDontShow') + '</ion-checkbox>',
                    title: $translate('fw_updateTitle'),
                    cssClass: 'updFWPopup',
                    //scope: $scope,
                    buttons: [
                    {   text: $translate('fw_update_button'),
                        type: 'button-positive',
                        onTap: function(e) {
                            return $scope.hideDialog;
                        }
                    }]
                });

                updFWPopup.then(function(res) {
                    if(res != undefined){

                        var query = 'SELECT * FROM configuration_alert WHERE name = ?';
                
                        database
                        .execute(query, [$rootScope.selectedDevice])
                        .then(function(result){
                            if(result.rows.length == 0) {
                                // Insert                
                                database
                                .execute('INSERT INTO configuration_alert ( name, show_fw_alert ) VALUES  ( ?, ? )', [
                                    $rootScope.selectedDevice,
                                    ($scope.hideDialog)?0:1
                                    ]
                                );
                            }
                            else
                            {
                                // update                
                                database
                                .execute('UPDATE configuration_alert SET show_fw_alert = ? WHERE name = ?', [
                                    ($scope.hideDialog)?0:1,
                                    $rootScope.selectedDevice
                                    ]
                                );
                            }

                        });
                    }
                });
            }
        }

        $scope.serviceLoad();    

        if($scope.currentNavigateByRoom)
        {
            angular.element(document.querySelector('#tabRooms')).triggerHandler('click');
        }

    });

    // Defines scopes
    $scope.panelOpen = function(name){
    	//TODO: remove to re-enable Scenes
    	if(name === "scene"){
    		return;
    	}

        // Manages views
        $('#tabScene').removeClass('active');
        $('#tabService').removeClass('active');
        $('#tabRooms').removeClass('active');

        $('#panelScene').addClass('hide');
        $('#panelService').addClass('hide');
        $('#panelRooms').addClass('hide');

        $('#tab' + name.charAt(0).toUpperCase() + name.substr(1)).addClass('active');
        $('#panel' + name.charAt(0).toUpperCase() + name.substr(1)).removeClass('hide');
    };

    $scope.sceneLoad = function(){
    	//TODO: remove to re-enable Scenes
    	if(true){
    		return;
    	}

        $('#scenes').html('');

        var query = 'SELECT * FROM scene';

        database
        .execute(query)
        .then(function(result){
            var content = '',
                i;

            for(i = 0; i < result.rows.length; i++){
                if(i % 2 === 0){
                	content = content + '<div align = \"center\" class = \"container-service-padding-no row\">';
                }

                content = content + '<div align = \"center\" class = \"col-50 sceneItem\" id = \"scene-' + result.rows.item(i).id + '\">' + '<div class = \"card ' + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + '\">' + '<div class = \"item item-height item-text-wrap\">' + '<div class = \"item-icon-container\">' + '<div class = \"row container-thermostatus-data\">' + '<div align = \"left\" class = \"col-50\"></div>' + '<div align = \"right\" class = \"col-50\">' + ((result.rows.item(i).active === 'true') ? '<i class = \"icon ion-ios-checkmark-outline\" style = \"color: #6ebb1f; font-size: 24px !important\"></i>' : '') + '</div>' + '</div>' + '<img height = \"40\" src = \"img/scene/' + result.rows.item(i).icon + '.svg\" />' + '<p class = \"item-icon-font\"><br />' + result.rows.item(i).name + '</p>' + '</div>' + '</div>' + '</div>' + '</div>';

                if(i % 2 !== 0){
                	content = content + '</div>';
                }
            }

            if(i % 2 !== 0){
            	content = content + '</div>';
            }

            $('#scenes').replaceWith(content);
            
            // Defines events
            $('.sceneItem').click(function(){
                var id = $(this).attr('id').replace('scene-', '');
                
                $state.go('app.scene/active/:id', {
                    'id': id
                });
            });
        }, function(error){
            // Error
            $cordovaDialogs.alert(
            	$translate('alert_configurationError'),
            	$translate('alert_configurationTitle'),
            	$translate('alert_button')
            );
        });
    };

    $scope.roomsLoad = function(){

        $scope.rooms = controlSystem.roomNames('sensor', true).map(function(item){

            var showItem = item.toString().replace(/_252F/g, "/").replace(/_2523/g,"#");
            return {name: item, showName: showItem};      
        });

        $scope.rooms.sort(function(a,b){return a.showName.localeCompare(b.showName);});

        $('#rooms').html('');

        var content = '';

        for(var i = 0; i < $scope.rooms.length; i++){
            if(i % 2 === 0){
                content = content + '<div align = \"center\" class = \"container-service-padding-no row\">';
            }

            content = content + 
            '<div align = \"center\" class = \"col-50 roomItem\" id = \"room-' + i + '\">' + 
                '<div class = \"card ' + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + '\">' + 
                    '<div class = \"item item-height item-text-wrap\">' + 
                        '<div class = \"item-icon-container\">' + 
                            '<img height = \"40\" src = \"img/icon/home_dashboard.svg\" />' + 
                            //'<p class = \"item-icon-font\"><br />' + $filter('capitalize')($scope.rooms[i].showName) + '</p>' + 
                            '<p class = \"item-icon-font\"><br />' + $scope.rooms[i].showName + '</p>' + 
                        '</div>' + 
                    '</div>' + 
                '</div>' + 
            '</div>';

            if(i % 2 !== 0){
                content = content + '</div>';
            }
        }

        $('#rooms').replaceWith(content);
        
        // Defines events
        $('.roomItem').click(function(){
            $rootScope.navigateByRoom = true;
            var id = $(this).attr('id').replace('room-', '');
            $scope.roomOpen($scope.rooms[parseInt(id)].name)
        });
    };

    $scope.serviceLoad = function(){
        $('#services').html('');
        var content = '';
        var baseServices = 5;

        $scope.customServices = controlSystem.customServices().map(function(item){
            
            var showItem = item.toString().replace(/_252F/g, "/").replace(/_2523/g, "#");

            if(showItem.startsWith("#") && showItem.endsWith("#"))
            {
                showItem = showItem.replace("#","");
                showItem = showItem.slice(0,showItem.length-1);
            }

            return {name: item, showName: showItem};      
        });

        $scope.customServices.sort(function(a,b){return a.showName.localeCompare(b.showName);});

        var serviceNumber = baseServices + $scope.customServices.length;

        //Inizializzazione componenti fisse
        for(var i = 0; i < serviceNumber; i++){
            if(i % 2 === 0){
                content = content + '<div align = \"center\" class = \"container-service-padding-no row\">';
            }

            switch(i)
            {
                case 0:
                {
                    content = content + 
                    "<div class=\"col-50 lightItem\">" +
                    "   <div class=\"card " + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + "\">" +
                    "       <div class=\"item item-height item-text-wrap\">" +
                    "           <div class=\"item-icon-container\">" +
                    "               <img height=\"40\" src=\"img/service/light.svg\" />" +
                    "               <p class=\"item-icon-font\"><br />" + $translate("dashboard_lights") + "</p>" +
                    "           </div>" +
                    "       </div>" +
                    "   </div>" +
                    "</div>";
                }break;
                case 1:
                {
                    content = content + 
                    "<div class=\"col-50 rollingItem\">" +
                    "   <div class=\"card " + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + "\">" +
                    "       <div class=\"item item-height item-text-wrap\">" +
                    "           <div class=\"item-icon-container\">" +
                    "               <img height=\"40\" src=\"img/service/rolling-shutter.svg\" />" +
                    "               <p class=\"item-icon-font\"><br />" + $translate("dashboard_rollingShutters") + "</p>" +
                    "           </div>" +
                    "       </div>" +
                    "   </div>" +
                    "</div>";
                }break;
                case 2:
                {
                    content = content + 
                    "<div class=\"col-50 thermItem\">" +
                    "   <div class=\"card " + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + "\">" +
                    "       <div class=\"item item-height item-text-wrap\">" +
                    "           <div class=\"item-icon-container\">" +
                    "               <img height=\"40\" src=\"img/service/thermostatus.svg\" />" +
                    "               <p class=\"item-icon-font\"><br />" + $translate("dashboard_thermostatus") + "</p>" +
                    "           </div>" +
                    "       </div>" +
                    "   </div>" +
                    "</div>";
                }break;
                case 3:
                {
                    content = content + 
                    "<div class=\"col-50 sensorItem\">" +
                    "   <div class=\"card " + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + "\">" +
                    "       <div class=\"item item-height item-text-wrap\">" +
                    "           <div class=\"item-icon-container\">" +
                    "               <img height=\"40\" src=\"img/service/sensor.svg\" />" +
                    "               <p class=\"item-icon-font\"><br />" + $translate("dashboard_sensors") + "</p>" +
                    "           </div>" +
                    "       </div>" +
                    "   </div>" +
                    "</div>";
                }break;
                case 4:
                {
                    content = content + 
                    "<div class=\"col-50 consumeItem\">" +
                    "   <div class=\"card " + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + "\">" +
                    "       <div class=\"item item-height item-text-wrap\">" +
                    "           <div class=\"item-icon-container\">" +
                    "               <img height=\"40\" src=\"img/service/consume.svg\" />" +
                    "               <p class=\"item-icon-font\"><br />" + $translate("dashboard_consumes") + "</p>" +
                    "           </div>" +
                    "       </div>" +
                    "   </div>" +
                    "</div>";
                }break;
                default:
                {
                    var currentIndex = i - baseServices;
                    content = content + 
                    "<div class=\"col-50 serviceItem\" id = \"serv-" + currentIndex + "\">" +
                    "   <div class=\"card " + ((i % 2 === 0) ? 'card-icon-left' : 'card-icon-right') + "\">" +
                    "       <div class=\"item item-height item-text-wrap\">" +
                    "           <div class=\"item-icon-container\">" +
                    "               <img height=\"40\" src=\"img/service/serv_gen.svg\" />" +
                    //"               <p class=\"item-icon-font\"><br />" + $filter('capitalize')($scope.customServices[currentIndex].showName) + "</p>" +
                    "               <p class=\"item-icon-font\"><br />" + $scope.customServices[currentIndex].showName + "</p>" +
                    "           </div>" +
                    "       </div>" +
                    "   </div>" +
                    "</div>";
                }break;

            }

            if(i % 2 !== 0){
                content = content + '</div>';
            }

        }

        $('#services').replaceWith(content);

        // Defines events
        $('.lightItem').click(function(){
            $scope.serviceOpen('light');
        });
        $('.rollingItem').click(function(){
            $scope.serviceOpen('rollingShutter');
        });
        $('.thermItem').click(function(){
            $scope.serviceOpen('thermostatus');
        });
        $('.sensorItem').click(function(){
            $scope.serviceOpen('sensor');
        });
        $('.consumeItem').click(function(){
            $scope.serviceOpen('consume');
        });

        $('.serviceItem').click(function(){
            $rootScope.navigateByCustomService = true;
            var id = $(this).attr('id').replace('serv-', '');
            $scope.roomOpen($scope.customServices[parseInt(id)].name);
        });

        
    };

    $scope.SelectedTab = function(sTab)
    {
        switch(sTab)
        {
            case 'service':{
                return !bNavigateByRoom ? 'active':'tab-item-disabled';
            }break;
            case 'rooms':{
                return bNavigateByRoom ? 'active':'tab-item-disabled';
            }break;
            case 'scene':{
                return 'tab-item-disabled';
            }break;
        }
    };

    if($rootScope.selectedNotification != undefined)
    {
        var currentNotification = JSON.parse(JSON.stringify($rootScope.selectedNotification));
        $rootScope.selectedNotification = undefined;
        if(currentNotification.values.id != undefined)
        {
            var association = controlSystem.associations()[currentNotification.values.id];

            if(association != undefined)
            {
                var roomComponents = controlSystem.roomComponents(association.roomName).filter(function(comp){
                    return comp.name != undefined && comp.name === association.componentName;
                });
                if(roomComponents && roomComponents[0].service === "sensor")
                {
                    $scope.sensorOpen(association.roomName,association.componentName)
                }
                else
                {
                    $scope.roomOpen(association.roomName,association.componentName);
                }
            }
        }
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault(); 
        navigator.app.exitApp();
    },1000);    

// }).controller('SceneActiveController', function($scope, $cordovaDialogs, $filter, $ionicHistory, $stateParams, database, $ionicHistory){
//     var $translate = $filter('translate');

//     // Defines title
//     database
//     .execute('SELECT * FROM scene WHERE id = ?', [$stateParams.id])
//     .then(function(result){
//         $scope.navigationSubheader = result.rows.item(0).name;
//         $('#active').prop('checked', (result.rows.item(0).active === 'true') ? true : false);
//     });

//     // Defines scopes
//     $scope.active = function(){
//         if($('#active').is(':checked')){
//             // Update all
//             database
//             .execute('UPDATE scene SET active = ?', ['false'])
//             .then(function(result){
//                 // Update selected scene               
//                 database
//                 .execute('UPDATE scene SET active = ? WHERE id = ?', [
//                 	'true',
//            			$stateParams.id]
//            		).then(function(result){
//                     $cordovaDialogs.alert(
//                     	$translate('alert_sceneActivatedSuccess'),
//                     	$translate('alert_sceneActivatedTitle'),
//                     	$translate('alert_button')
//                     ).then(function(){
//                         // Reloads scenes
//                         $ionicHistory.goBack();
//                     });
//                 });
//             });
//         }else{
//             // Update selected scene
//             query = 'UPDATE scene SET active = ? WHERE id = ?';
            
//             database
//             .execute(query, [
//             	'false',
//         		$stateParams.id]
//         	).then(function(result){
//                 $cordovaDialogs.alert(
//                 	$translate('alert_sceneDeactivatedSuccess'),
//                 	$translate('alert_sceneDeactivatedTitle'),
//                 	$translate('alert_button')
//                 ).then(function(){
//                     // Reloads scenes
//                     $ionicHistory.goBack();
//                 });
//             });
//         }
//     };
// }).controller('SceneAddController', function($scope, $filter, $state, $stateParams, database){
//     var $translate = $filter('translate');

//     // Defines titles
//     //$scope.navigationHeader = $filter('capitalize')($stateParams.room);
//     $scope.navigationHeader =$stateParams.room;
    
//     var query = 'SELECT * FROM scene';
    
//     database
//     .execute(query)
//     .then(function(result){
//         var content = '';

//         for(var i = 0; i < result.rows.length; i++){
//             content = content + '<a class = \"color-gray item sceneAddItem\" id = \"scene-' + result.rows.item(i).id + '\">' + '<div class = \"row\" style = \"padding: 0px !important\">' + '<div class = \"col-10\">' + '<img height = \"20\" src = \"img/scene/' + result.rows.item(i).icon + '.svg\" />' + '</div>' + '<div class = \"col-90 color-gray\">' + result.rows.item(i).name + '</div>' + '</div>' + '</a>';
//         }

//         $('#scenesAdd').replaceWith(content);
        
//         // Defines events
//         $('.sceneAddItem').click(function(){
//             var id = $(this).attr('id').replace('scene-', '');

//             $state.go('app.scene/add/detail/:id', {
//                 'id': id
//             });
//         });
//     }, function(error){
//         // Error
//         $cordovaDialogs.alert(
//         	$translate('alert_configurationError'),
//         	$translate('alert_configurationTitle'),
//         	$translate('alert_button')
//         );
//     });

//     // Defines scopes
//     $scope.sceneCreate = function(){
//         $state.go('app.scene/create/:service/:room', {
//             'room': $stateParams.room,
//             'service': $stateParams.service
//         });
//     };
// }).controller('SceneAddDetailController', function($scope, $cordovaDialogs, $filter, $ionicHistory, $stateParams, database){
//     var $translate = $filter('translate');

//     // Defines title
//     var query = 'SELECT * FROM scene WHERE id = ?';

//     database
//     .execute(query, [$stateParams.id])
//     .then(function(result){
//         $scope.navigationSubheader = result.rows.item(0).name;
//     });

//     // Defines scopes
//     $scope.add = function(){
//         $cordovaDialogs.alert(
//         	$translate('alert_sceneAddedSuccess'),
//         	$translate('alert_sceneAddedTitle'),
//         	$translate('alert_button')
//         ).then(function(){
//             // Reloads scenes
//             $ionicHistory.goBack();
//         });
//     };
// }).controller('SceneCreateController', function($scope, $cordovaDialogs, $filter, $ionicHistory, $state, $stateParams, database){
    // var $translate = $filter('translate');

    // // Defines titles
    // //$scope.navigationHeader = $filter('capitalize')($stateParams.room);
    // $scope.navigationHeader = $stateParams.room;
    
    // // Defines scopes
    // $scope.iconChoose = function(name){
    //     $('#sceneIcon').
    //     val(name);
    // };
    
    // $scope.save = function(){
    //     // Insert
    //     var query = 'INSERT INTO scene ( active, icon, name, room, service, state ) VALUES  ( ?, ?, ?, ?, ?, ? )';
        
    //     database
    //     .execute(query, [
    //     	'false',
    //    		$('#sceneIcon').val(),
    //    		$('#sceneName').val(),
    //    		$stateParams.room,
    //    		$stateParams.service,
    //    		''
    //    	]).then(function(result){
    //         // Confirm
    //         $cordovaDialogs.alert(
    //         	$translate('alert_sceneCreateSuccess'),
    //         	$translate('alert_sceneCreateTitle'),
    //         	$translate('alert_button')
    //         ).then(function(){
    //             // Goes back
    //             $ionicHistory.goBack();
    //         });
    //     }, function(error){
    //         // Error
    //         $cordovaDialogs.alert(
    //         	$translate('alert_sceneCreateError'),
    //         	$translate('alert_sceneCreateTitle'),
    //         	$translate('alert_button')
    //         );
    //     });
    // };
}).controller('SensorController', function($scope, $rootScope, $filter, $state, controlSystem, $ionicHistory,$ionicPlatform){
    var $translate = $filter('translate');

    $scope.rooms = controlSystem.roomNames('sensor').filter(function(room){
        var roomComponents = controlSystem.roomComponents(room).filter(function(comp){
            return comp.sensorList != undefined && Object.keys(comp.sensorList).length > 0 && comp.service === 'sensor';
        });
        return roomComponents != undefined && roomComponents.length > 0;
    }).map(function(item){
        var showItem = item.toString().replace(/_252F/g, "/").replace(/_2523/g,"#");
        return {name: item, showName: showItem};
    });

    var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
        $scope.$digest();
    });
    $scope.$on('$destroy', asyncUpdateListener);

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.dashboard()
    },1000)
}).controller('SensorDetailController', function($scope, $rootScope, $cordovaDialogs, $filter, $state, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.component = $stateParams.component;
    var $translate = $filter('translate');

    $scope.service = 'sensor';
    
    // $scope.navigationHeader = $filter('capitalize')($scope.room.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($scope.room.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    var roomComponents = controlSystem.roomComponents($scope.room).filter(function(comp){
        return comp.service === 'sensor';
    })
    .filter(function(comp){
        return comp.sensorList != undefined && Object.keys(comp.sensorList).length > 0;
    });

    $scope.sensorList = [];
    roomComponents.forEach(function(comp){
        var sensors = [];
        for(var key in comp.sensorList)
        {
            if(comp.sensorList.hasOwnProperty(key) && $scope.sensorList.filter(function(sensor){return sensor.element && sensor.element.id && sensor.element.id === key;}).length <= 0)
                $scope.sensorList.push(comp.sensorList[key]);
        }
    });

    var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
        $scope.$digest();
    });
    $scope.$on('$destroy', asyncUpdateListener);

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.serviceOpen($scope.service)
    },1000)
    
}).controller('ServiceController', function($scope, $state, $rootScope, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
	$scope.service = $stateParams.service;
    var $translate = $filter('translate');

	// Defines title
	$scope.navigationHeader = $filter('translate')('service_'+$scope.service);

    // Shows rooms - 170303fg also show status of lights in the room
    $scope.rooms = controlSystem.roomNames($scope.service).map(function(item){

        var showItem = item.toString().replace(/_252F/g, "/").replace(/_2523/g,"#");
        if($scope.service === "light")
        {
            var lightsOn = 0;
            var lights = 0;
            $scope.element = controlSystem.element();
            controlSystem.roomComponents(item).forEach(function(comp){
                if(comp.service === "light")
                {
                    if(comp.checkValue() !== 0)
                        lightsOn++;
                    //console.log("@@"+item+" : "+comp.toggle);
                    lights++;
                }
            });
            var status = lightsOn+"/"+lights;
            var statIcon = (lightsOn>0)? "icon ion-ios-sunny" : "icon ion-ios-sunny-outline";

            return {name: item, showName: showItem, stat: status, icon: statIcon};
        }
        else if($scope.service === "rollingShutter")
        {
            return {name: item, showName: showItem, stat: "", icon: ""};
        }
        else if($scope.service === "thermostatus")
        {
            return {name: item, showName: showItem, stat: "", icon: ""};
        }
        else if($scope.service === "consume")
        {
            return {name: item, showName: showItem, stat: "", icon: ""};
        }
    });

    //AC_20170711
    $scope.lightsStatusUpdate = function(id){
        if($scope.service === "light" && id.indexOf('TO')>-1){
            $scope.rooms.forEach(function(room){
                var lightsOn = 0;
                var lights = 0;
                $scope.element = controlSystem.element();
                controlSystem.roomComponents(room.name).forEach(function(comp){
                    if(comp.service === "light")
                    {
                        if(comp.checkValue() !== 0)
                            lightsOn++;
                        lights++;
                    }
                });
                var status = lightsOn+"/"+lights;
                var statIcon = (lightsOn>0)? "icon ion-ios-sunny" : "icon ion-ios-sunny-outline";

                room.stat= status;
                room.icon = statIcon;

            });
        }
    };
    var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
        $scope.lightsStatusUpdate(data);
        $scope.$digest();
    });

    $scope.$on('$destroy', asyncUpdateListener);

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.dashboard()
    },1000)

}).controller('RoomController', function($scope, $state, $rootScope, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    var $translate = $filter('translate');

    $scope.fromRoom = true;

    // Defines title
    // $scope.navigationHeader = $filter('capitalize')($scope.room.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($scope.room.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    // Shows services
    $scope.services = controlSystem.roomServices($scope.room).map(function(item){

        if(item === "light")
        {
            return {name: item, icon: "img/service/light.svg"};
        }
        else if(item === "rollingShutter")
        {
            return {name: item, icon: "img/service/rolling-shutter.svg"};
        }
        else if(item === "thermostatus")
        {
            return {name: item, icon: "img/service/thermostatus.svg"};
        }
        else if(item === "consume")
        {
            return {name: item, icon: "img/service/consume.svg"};
        }
        else if(item === "sensor")
        {
            return {name: item, icon: "img/service/sensor.svg"};
        }
    });

    $ionicPlatform.registerBackButtonAction(function(event){ 
        event.preventDefault();
        $scope.dashboard()
    },1000)

}).controller('ServiceDetailController', function($scope, $state, $rootScope, $cordovaDialogs, $filter, $state, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');
    $scope.translate = $filter('translate');

    //AC_20171023
    $scope.fromRooms = ($stateParams.fromRoom != undefined) ? $stateParams.fromRoom : false;
    //console.log("@@#@@ServiceDetailController");
    // Defines titles

    //210317fg non mi serve interruttore generale su tapparelle
    if($scope.service=="rollingShutter" || $scope.service == "thermostatus" || $scope.service == "consume")
        $('#generalSwitch_list').hide();

    // $scope.navigationHeader = $filter('capitalize')($scope.room.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($scope.room.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.navigationSubheader = $filter('translate')('service_'+$scope.service);

    $scope.element = controlSystem.element();
	$scope.roomComponents = controlSystem.roomComponents($scope.room).filter(function(comp){
		return comp.service === $scope.service;
    });
    
    $scope.associations = controlSystem.associations();

    switch($scope.service)
    {
        case "light":
        {
            $scope.generalSwitchUpdate = function(){
                $scope.generalSwitch.value = $scope.roomComponents.some(function(comp){
                    //return (comp.toggle && $scope.element[comp.toggle].value !== 0) ||
                    //     (comp.dimmer && $scope.element[comp.dimmer].value !== 0);

                    return (comp.checkValue() !== 0);
                });
            };

            $scope.generalSwitch = { value: false };
            $scope.generalSwitchUpdate();

            var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
                $scope.generalSwitchUpdate();
                $scope.$digest();
            });
            $scope.$on('$destroy', asyncUpdateListener);

            $scope.update = function(id, value){
                //console.log("@@#@@update:"+id+" value:"+value);
                var assoc = id.split(';');
                for(var i=0; i<assoc.length; i++)
                {
                    controlSystem.setState(assoc[i],value).then(function(){
                        $scope.generalSwitchUpdate();
                    }).catch(function(){
                        $cordovaDialogs.alert(
                            $translate('alert_noResponseFromCU'),
                            $translate('alert_serviceTitle'),
                            $translate('alert_button')
                        );
                    });
                }
            };

            $scope.updateAll = function(generalSwitchValue){
                $scope.roomComponents.forEach(function(comp){
                    if(generalSwitchValue){
                        if(comp.toggleAssoc){
                            $scope.update(comp.toggleAssoc,1);
                        }
                        if(comp.dimmerAssoc){
                            //$scope.update(comp.dimmerAssoc,255);
                        }
                        if(comp.rollUpAssoc){
                            $scope.update(comp.rollUpAssoc,1);
                        }
                    }else{
                        if(comp.toggleAssoc){
                            $scope.update(comp.toggleAssoc,0);
                        }
                        if(comp.dimmerAssoc){
                            //$scope.update(comp.dimmerAssoc,0);
                        }
                        if(comp.rollUpAssoc){
                            $scope.update(comp.rollDownAssoc,1);
                        }
                    }
                });
            };

            $scope.editTimerLight = function(comp)
            {
                $state.go('app.service/timer/:service/:room/:element',
                {"service": comp.service,
                    "room": $scope.room,
                    "element": comp.name});
            };

            //AC_20180319
            $scope.roomComponents.forEach(function(Item){
                Item.enabled = $scope.associations[Item.number].enabled;
            })
        }
        break;

        case "rollingShutter":
        {   
            $scope.generalSwitch = { value: false };

            var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
                $scope.$digest();
            });
            $scope.$on('$destroy', asyncUpdateListener);

            $scope.update = function(id, value){
                //console.log("@@#@@update:"+id+" value:"+value);
                var assoc = id.split(';');
                for(var i=0; i<assoc.length; i++)
                {
                    controlSystem.setState(assoc[i],value).then(function(){
                    }).catch(function(){
                        $cordovaDialogs.alert(
                            $translate('alert_noResponseFromCU'),
                            $translate('alert_serviceTitle'),
                            $translate('alert_button')
                        );
                    });
                }
            };

            $scope.updateRollingShutter = function(id){
                //console.log("@@#@@update:"+id+" value:"+value);
                var assoc = id.split(';');
                var newValue=0;
                
                for(var i=0; i<assoc.length; i++)
                {
                    var currentEl = $scope.element[assoc[i]];
                    if(currentEl != undefined && currentEl != null)
                        newValue += currentEl.value;
                }

                newValue = newValue > 0 ? 0 : 1;

                for(var i=0; i<assoc.length; i++)
                {
                    controlSystem.setState(assoc[i],newValue).catch(function(){
                        $cordovaDialogs.alert(
                            $translate('alert_noResponseFromCU'),
                            $translate('alert_serviceTitle'),
                            $translate('alert_button')
                        );
                    });
                }
            };

            $scope.updateAll = function(generalSwitchValue){
                $scope.roomComponents.forEach(function(comp){
                    if(generalSwitchValue){
                        if(comp.rollUpAssoc){
                            $scope.update(comp.rollUpAssoc,1);
                        }
                    }else{
                        if(comp.rollUpAssoc){
                            $scope.update(comp.rollDownAssoc,1);
                        }
                    }
                });
            };

            $scope.editTimer = function(comp)
            {
                $state.go('app.service/timer_rs/:service/:room/:element',
                {"service": $scope.service,
                 "room": $scope.room,
                 "element": comp.name});
            };

            $scope.checkEnabledU = function(comp){
                var nAssIndex = 0;
                if (comp.rollUpComp)
                {
                    nAssIndex = comp.rollUpComp.number;
                    var nEnable = controlSystem.associations()[nAssIndex].enabled
                    if (nEnable == 0)
                        return true;
                    else
                        return false;
                }
                return false;
            };

            $scope.checkEnabledD = function(comp){
                var nAssIndex = 0;
                if (comp.rollDownComp)
                {
                    nAssIndex = comp.rollDownComp.number;
                    var nEnable = controlSystem.associations()[nAssIndex].enabled
                    if (nEnable == 0)
                        return true;
                    else
                        return false;
                }
                return false;                
            };


        }
        break;

        case "thermostatus":
        {
            $scope.generalSwitch = { value: false };
            
            $scope.externalSensors = controlSystem.externalSensors();

            var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
                $scope.$digest();
            });
            $scope.$on('$destroy', asyncUpdateListener);

            $scope.changeMode = function(comp){
                switch(comp.useMode)
                {
                    case 'therm_auto':
                    {
                        switch(comp.thermType)
                        {
                            case 'cond':
                            {
                                comp.useMode='cond_auto';
                            }break;
                            case 'therm':
                            {
                                comp.useMode='therm_manual';
                            }break;
                        }
                    }break;
                    case 'therm_manual':
                    {
                        switch(comp.thermType)
                        {
                            case 'cond':
                            {
                                comp.useMode='cond_manual';
                            }break;
                            case 'therm':
                            {
                                comp.useMode='therm_int';
                            }break;
                        }
                    }break;
                    case 'therm_int':
                    {
                        switch(comp.thermType)
                        {
                            case 'cond':
                            {
                                comp.useMode='cond_int';
                            }break;
                            case 'therm':
                            {
                                comp.useMode='therm_auto';
                            }break;
                        }
                    }break;
                    case 'cond_auto':
                    {
                        switch(comp.thermType)
                        {
                            case 'therm':
                            {
                                comp.useMode='therm_auto';
                            }break;
                            case 'cond':
                            {
                                comp.useMode='cond_manual';
                            }break;
                        }
                    }break;
                    case 'cond_manual':
                    {
                        switch(comp.thermType)
                        {
                            case 'therm':
                            {
                                comp.useMode='therm_manual';
                            }break;
                            case 'cond':
                            {
                                comp.useMode='cond_int';
                            }break;
                        }
                    }break;
                    case 'cond_int':
                    {
                        switch(comp.thermType)
                        {
                            case 'therm':
                            {
                                comp.useMode='therm_int';
                            }break;
                            case 'cond':
                            {
                                comp.useMode='cond_auto';
                            }break;
                        }
                    }break;
                }
            };

            $scope.changeType = function(comp){
                switch(comp.thermType)
                {
                    case 'therm':
                    {
                        comp.thermType='cond';
                    }break;
                    case 'cond':
                    {
                        comp.thermType='therm';
                    }break;
                }
                $scope.changeMode(comp);
            };

            $scope.getSoglia = function(comp){
                var soglia = "";
                var sensorList;
                switch(comp.useMode)
                {
                    case 'therm_auto':
                    {
                        sensorList = (comp.thermAuto) ? comp.thermAuto.sensorList : undefined;
                    }break;
                    case 'therm_manual':
                    {
                        sensorList = (comp.thermManu) ? comp.thermManu.sensorList : undefined;
                    }break;
                    case 'therm_int':
                    {
                        sensorList = (comp.thermInte) ? comp.thermInte.sensorList : undefined;
                    }break;
                    case 'cond_auto':
                    {
                        sensorList = (comp.condAuto) ? comp.condAuto.sensorList : undefined;
                    }break;
                    case 'cond_manual':
                    {
                        sensorList = (comp.condManu) ? comp.condManu.sensorList : undefined;
                    }break;
                    case 'cond_int':
                    {
                        sensorList = (comp.condInte) ? comp.condInte.sensorList : undefined;
                    }break;
                }

                if(sensorList != undefined)
                {
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                if(indexFirstNum != -1){
                                    if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                        indexFirstNum--;
                                    soglia = sensorList[key].value.substring(indexFirstNum) + ( sensorList[key].element.unit ? sensorList[key].element.unit : '' );
                                    return;
                                }
                            }
                        }
                    });
                }
                return (soglia == '') ? '--' : soglia ;
            };

            $scope.getValue = function(comp){
                var valore = -1000;
                var unit = "";
                var sensorList;
                switch(comp.useMode)
                {
                    case 'therm_auto':
                    {
                        sensorList = (comp.thermAuto) ? comp.thermAuto.sensorList : undefined;
                    }break;
                    case 'therm_manual':
                    {
                        sensorList = (comp.thermManu) ? comp.thermManu.sensorList : undefined;
                    }break;
                    case 'therm_int':
                    {
                        sensorList = (comp.thermInte) ? comp.thermInte.sensorList : undefined;
                    }break;
                    case 'cond_auto':
                    {
                        sensorList = (comp.condAuto) ? comp.condAuto.sensorList : undefined;
                    }break;
                    case 'cond_manual':
                    {
                        sensorList = (comp.condManu) ? comp.condManu.sensorList : undefined;
                    }break;
                    case 'cond_int':
                    {
                        sensorList = (comp.condInte) ? comp.condInte.sensorList : undefined;
                    }break;
                }
                if(sensorList != undefined)
                {
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                if(sensorList[key].element.value > valore)
                                {
                                    valore = sensorList[key].element.value;
                                    unit = ( sensorList[key].element.unit ? sensorList[key].element.unit : '' )
                                }
                            }
                        }
                    });
                    return valore + unit;
                }

                return'--';
            };

            $scope.getValueExt = function(comp){
                var valore = -1000;
                var unit = "";
                var sensorList = ($scope.externalSensors[0]) ? $scope.externalSensors[0].sensorList : undefined; 
                if(sensorList != undefined)
                {
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                if(sensorList[key].element.value > valore)
                                {
                                    valore = sensorList[key].element.value;
                                    unit = ( sensorList[key].element.unit ? sensorList[key].element.unit : '' )
                                }
                            }
                        }
                    });
                    return valore + unit;
                }

                return'--';
            };

            $scope.getOnOffValue = function(comp){
                var valore = 0;
                var toggleList;
                switch(comp.useMode)
                {
                    case 'therm_auto':
                    {
                        toggleList = (comp.thermAuto) ? comp.thermAuto.toggleList : undefined;
                    }break;
                    case 'therm_manual':
                    {
                        toggleList = (comp.thermManu) ? comp.thermManu.toggleList : undefined;
                    }break;
                    case 'therm_int':
                    {
                        toggleList = (comp.thermInte) ? comp.thermInte.toggleList : undefined;
                    }break;
                    case 'cond_auto':
                    {
                        toggleList = (comp.condAuto) ? comp.condAuto.toggleList : undefined;
                    }break;
                    case 'cond_manual':
                    {
                        toggleList = (comp.condManu) ? comp.condManu.toggleList : undefined;
                    }break;
                    case 'cond_int':
                    {
                        toggleList = (comp.condInte) ? comp.condInte.toggleList : undefined;
                    }break;
                }

                if(toggleList != undefined)
                {
                    Object.keys(toggleList).forEach(function (key) {
                        if(toggleList[key] !== 'function'){
                            if(toggleList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                valore += parseInt(toggleList[key].element.value);
                            }
                        }
                    });
                }

                if(valore > 0)
                {
                    if(comp.thermType == 'therm')
                        return 'ion-fireball color-red';
                    else
                        return 'ion-ios-snowy color-blue';
                }
                else
                {
                    if(comp.thermType == 'therm')
                        return 'ion-fireball color-gray';
                    else
                        return 'ion-ios-snowy color-gray';

                }
            };

            $scope.roomComponents.forEach(function(comp){

                comp.timer.forEach(function(timer){
                    timer.showTimer = function(weekDay)
                        {
                            if(weekDay >=0 && weekDay <7)
                                if(this.disabledDays[weekDay] == 1)
                                    return false;
                            return true;
                        };  

                    timer.formatTime = function()
                    {
                        var minutes = this.minutesFromMidnight % 60;
                        var hours = (this.minutesFromMidnight - minutes) / 60;

                        return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                    }

                });

                if(comp.thermAuto != undefined && comp.thermAuto.timer != undefined){
                    comp.thermAuto.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            }; 

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(comp.thermManu != undefined && comp.thermManu.timer != undefined){
                    comp.thermManu.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(comp.thermInte != undefined && comp.thermInte.timer != undefined){
                    comp.thermInte.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(comp.condAuto != undefined && comp.condAuto.timer != undefined){
                    comp.condAuto.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(comp.condManu != undefined && comp.condManu.timer != undefined){
                    comp.condManu.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(comp.condInte != undefined && comp.condInte.timer != undefined){
                    comp.condInte.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }
            });

            $scope.roomComponents.forEach(function(comp){
                comp.thermType = 'therm';
                comp.useMode = 'therm_auto';

                comp.today = new Date();

                //definizione funzione per gestione manuale termico
                if(comp.thermManu.number != undefined)
                {
                    if(comp.thermManu.sensorList != undefined)
                    {
                        var soglia=0;
                        var sensorList = comp.thermManu.sensorList;
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        soglia = parseFloat(sensorList[key].value.substring(indexFirstNum));
                                        return;
                                    }
                                }
                            }
                        });

                        comp.thermManu.soglia = soglia;
                        comp.thermManu.interval = 2;

                        comp.resetSoglia = function(){
                            this.thermManu.sogliaTemp = this.thermManu.soglia;
                            this.thermManu.intervalTemp = this.thermManu.interval;
                        };

                        comp.upSoglia = function(){
                            this.thermManu.sogliaTemp += parseFloat(comp.thermManu.multiplier.toFixed(1));
                        };

                        comp.downSoglia = function(){
                            this.thermManu.sogliaTemp -= parseFloat(comp.thermManu.multiplier.toFixed(1));
                        };

                        comp.upInterval = function(){

                            this.thermManu.intervalTemp += 1;

                            if(this.thermManu.intervalTemp>=24)
                                this.thermManu.intervalTemp = 24;
                        };
                        comp.downInterval = function(){
                            
                            this.thermManu.intervalTemp -= 1;

                            if(this.thermManu.intervalTemp<1)
                                this.thermManu.intervalTemp = 1;
                        };

                        comp.thermManu.multiplier = 1;

                        comp.currentMultiplier = function(value)
                        {
                            return (comp.thermManu.multiplier == value)?'selected':''
                        }

                        comp.changeMultiplier = function(value)
                        {
                            comp.thermManu.multiplier = value;
                        }

                        comp.resetSoglia();
                    }
                }

                //definizione funzione per gestione manuale condizionamento
                if(comp.condManu.number != undefined)
                {
                    if(comp.condManu.sensorList != undefined)
                    {
                        var soglia=0;
                        var sensorList = comp.condManu.sensorList;
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        soglia = parseFloat(sensorList[key].value.substring(indexFirstNum));
                                        return;
                                    }
                                }
                            }
                        });

                        comp.condManu.soglia = soglia;
                        comp.condManu.interval = 2;

                        comp.resetSogliaC = function(){
                            this.condManu.sogliaTemp = this.condManu.soglia;
                            this.condManu.intervalTemp = this.condManu.interval;
                        };

                        comp.upSogliaC = function(){
                            this.condManu.sogliaTemp += comp.condManu.multiplier;
                        };

                        comp.downSogliaC = function(){
                            this.condManu.sogliaTemp -= comp.condManu.multiplier;
                        };

                        comp.upIntervalC = function(){

                            this.condManu.intervalTemp += 1;

                            if(this.condManu.intervalTemp>=24)
                                this.condManu.intervalTemp = 24;
                        };
                        comp.downIntervalC = function(){
                            
                            this.condManu.intervalTemp -= 1;

                            if(this.condManu.intervalTemp<1)
                                this.condManu.intervalTemp = 1;
                        };

                        comp.condManu.multiplier = 1;

                        comp.currentMultiplier = function(value)
                        {
                            return (comp.condManu.multiplier == value)?'selected':''
                        }

                        comp.changeMultiplier = function(value)
                        {
                            comp.condManu.multiplier = value;
                        }

                        comp.resetSogliaC();
                    }
                }

                comp.selectedDay = comp.today.getDay();

                //Individuazione modo d'uso abilitato
                if(comp.thermManu && comp.thermManu.enabled == 1)
                {
                    comp.thermType = 'therm';
                    comp.useMode = 'therm_manual';
                }
                if(comp.thermAuto && comp.thermAuto.enabled == 1)
                {
                    comp.thermType = 'therm';
                    comp.useMode = 'therm_auto';
                }
                if(comp.thermInte && comp.thermInte.enabled == 1)
                {
                    comp.thermType = 'therm';
                    comp.useMode = 'therm_int';
                }
                if(comp.condManu && comp.condManu.enabled == 1)
                {
                    comp.thermType = 'cond';
                    comp.useMode = 'cond_manual';
                }
                if(comp.condAuto && comp.condAuto.enabled == 1)
                {
                    comp.thermType = 'cond';
                    comp.useMode = 'cond_auto';
                }
                if(comp.condInte && comp.condInte.enabled ==1)
                {
                    comp.thermType = 'cond';
                    comp.useMode = 'cond_int';
                }

                comp.isSelected = function(dayNumber)
                {
                    if(comp.selectedDay == dayNumber)
                        return "selected";
                    return "";
                }

                comp.setSelectedDay = function(dayNumber)
                {
                    comp.selectedDay = dayNumber;
                }
            });

            //AC_20170925
            $scope.roomComponents.forEach(function(comp){
                
                switch(comp.useMode)
                {
                    case 'therm_auto':
                    {
                        if(comp.thermAuto == undefined)
                        {
                            comp.thermAuto = {'enabled':0};
                        }
                        else
                        {
                            if(comp.thermAuto.enabled == undefined)
                                comp.thermAuto.enabled = 0;
                        }
                    }break;
                    case 'therm_manual':
                    {
                        if(comp.thermManu == undefined)
                        {
                            comp.thermManu = {'enabled':0};
                        }
                        else
                        {
                            if(comp.thermManu.enabled == undefined)
                                comp.thermManu.enabled = 0;
                        }
                    }break;
                    case 'therm_int':
                    {
                        if(comp.thermInte == undefined)
                        {
                            comp.thermInte = {'enabled':0};
                        }
                        else
                        {
                            if(comp.thermInte.enabled == undefined)
                                comp.thermInte.enabled = 0;
                        }
                    }break;
                    case 'cond_auto':
                    {
                        if(comp.condAuto == undefined)
                        {
                            comp.condAuto = {'enabled':0};
                        }
                        else
                        {
                            if(comp.condAuto.enabled == undefined)
                                comp.condAuto.enabled = 0;
                        }
                    }break;
                    case 'cond_manual':
                    {
                        if(comp.condManu == undefined)
                        {
                            comp.condManu = {'enabled':0};
                        }
                        else
                        {
                            if(comp.condManu.enabled == undefined)
                                comp.condManu.enabled = 0;
                        }
                    }break;
                    case 'cond_int':
                    {
                        if(comp.condInte == undefined)
                        {
                            comp.condInte = {'enabled':0};
                        }
                        else
                        {
                            if(comp.condInte.enabled == undefined)
                                comp.condInte.enabled = 0;
                        }
                    }break;
                }
            });

            $scope.HideEnableSwitch = function(comp)
            {
                switch(comp.useMode)
                {
                    case 'therm_auto':
                    {
                        if(comp.thermAuto.number == undefined)
                        {
                            return true;
                        }
                    }break;
                    case 'therm_manual':
                    {
                        if(comp.thermManu.number == undefined)
                        {
                            return true;
                        }
                    }break;
                    case 'therm_int':
                    {
                        if(comp.thermInte.number == undefined)
                        {
                            return true;
                        }
                    }break;
                    case 'cond_auto':
                    {
                        if(comp.condAuto.number == undefined)
                        {
                            return true;
                        }
                    }break;
                    case 'cond_manual':
                    {
                        if(comp.condManu.number == undefined)
                        {
                            return true;
                        }
                    }break;
                    case 'cond_int':
                    {
                        if(comp.condInte.number == undefined)
                        {
                            return true;
                        }
                    }break;
                }
                return false;
            };

            $scope.toggleEnable = function(comp,value)
            {
                switch(comp.useMode)
                {
                    case 'therm_auto':
                    {
                        if(value)
                        {
                            comp.thermManu.enabled = 0;
                            if(comp.thermManu.number)
                                controlSystem.disableCfgState(comp.thermManu.number);

                            comp.thermInte.enabled = 0;
                            if(comp.thermInte.number)
                                controlSystem.disableCfgState(comp.thermInte.number);

                            comp.condAuto.enabled = 0;
                            if(comp.condAuto.number)
                                controlSystem.disableCfgState(comp.condAuto.number);
                                
                            comp.condManu.enabled = 0;
                            if(comp.condManu.number)
                                controlSystem.disableCfgState(comp.condManu.number);
                                
                            comp.condInte.enabled = 0;
                            if(comp.condInte.number)
                                controlSystem.disableCfgState(comp.condInte.number);
                                

                            if(comp.thermAuto.number)
                                controlSystem.enableCfgState(comp.thermAuto.number);
                                
                        }
                        else
                        {
                            if(comp.thermAuto.number)
                                controlSystem.disableCfgState(comp.thermAuto.number);
                        }
                    }break;
                    case 'therm_manual':
                    {
                        if(value)
                        {
                            comp.thermAuto.enabled = 0;
                            if(comp.thermAuto.number)
                                controlSystem.disableCfgState(comp.thermAuto.number);

                            comp.thermInte.enabled = 0;
                            if(comp.thermInte.number)
                                controlSystem.disableCfgState(comp.thermInte.number);

                            comp.condAuto.enabled = 0;
                            if(comp.condAuto.number)
                                controlSystem.disableCfgState(comp.condAuto.number);
                                
                            comp.condManu.enabled = 0;
                            if(comp.condManu.number)
                                controlSystem.disableCfgState(comp.condManu.number);
                                
                            comp.condInte.enabled = 0;
                            if(comp.condInte.number)
                                controlSystem.disableCfgState(comp.condInte.number);
                                

                            if(comp.thermManu.number)
                                controlSystem.enableCfgState(comp.thermManu.number);
                                
                        }
                        else
                        {
                            if(comp.thermManu.number)
                                controlSystem.disableCfgState(comp.thermManu.number);
                        }
                    }break;
                    case 'therm_int':
                    {
                        if(value)
                        {
                            comp.thermManu.enabled = 0;
                            if(comp.thermManu.number)
                                controlSystem.disableCfgState(comp.thermManu.number);

                            comp.thermAuto.enabled = 0;
                            if(comp.thermAuto.number)
                                controlSystem.disableCfgState(comp.thermAuto.number);

                            comp.condAuto.enabled = 0;
                            if(comp.condAuto.number)
                                controlSystem.disableCfgState(comp.condAuto.number);
                                
                            comp.condManu.enabled = 0;
                            if(comp.condManu.number)
                                controlSystem.disableCfgState(comp.condManu.number);
                                
                            comp.condInte.enabled = 0;
                            if(comp.condInte.number)
                                controlSystem.disableCfgState(comp.condInte.number);
                                

                            if(comp.thermInte.number)
                                controlSystem.enableCfgState(comp.thermInte.number);
                                
                        }
                        else
                        {
                            if(comp.thermInte.number)
                                controlSystem.disableCfgState(comp.thermInte.number);
                        }
                    }break;
                    case 'cond_auto':
                    {
                        if(value)
                        {
                            comp.thermManu.enabled = 0;
                            if(comp.thermManu.number)
                                controlSystem.disableCfgState(comp.thermManu.number);

                            comp.thermInte.enabled = 0;
                            if(comp.thermInte.number)
                                controlSystem.disableCfgState(comp.thermInte.number);

                            comp.thermAuto.enabled = 0;
                            if(comp.thermAuto.number)
                                controlSystem.disableCfgState(comp.thermAuto.number);
                                
                            comp.condManu.enabled = 0;
                            if(comp.condManu.number)
                                controlSystem.disableCfgState(comp.condManu.number);
                                
                            comp.condInte.enabled = 0;
                            if(comp.condInte.number)
                                controlSystem.disableCfgState(comp.condInte.number);
                                

                            if(comp.condAuto.number)
                                controlSystem.enableCfgState(comp.condAuto.number);
                                
                        }
                        else
                        {
                            if(comp.condAuto.number)
                                controlSystem.disableCfgState(comp.condAuto.number);
                        }
                    }break;
                    case 'cond_manual':
                    {
                        if(value)
                        {
                            comp.thermManu.enabled = 0;
                            if(comp.thermManu.number)
                                controlSystem.disableCfgState(comp.thermManu.number);

                            comp.thermInte.enabled = 0;
                            if(comp.thermInte.number)
                                controlSystem.disableCfgState(comp.thermInte.number);

                            comp.condAuto.enabled = 0;
                            if(comp.condAuto.number)
                                controlSystem.disableCfgState(comp.condAuto.number);
                                
                            comp.thermAuto.enabled = 0;
                            if(comp.thermAuto.number)
                                controlSystem.disableCfgState(comp.thermAuto.number);
                                
                            comp.condInte.enabled = 0;
                            if(comp.condInte.number)
                                controlSystem.disableCfgState(comp.condInte.number);
                                

                            if(comp.condManu.number)
                                controlSystem.enableCfgState(comp.condManu.number);
                                
                        }
                        else
                        {
                            if(comp.condManu.number)
                                controlSystem.disableCfgState(comp.condManu.number);
                        }
                    }break;
                    case 'cond_int':
                    {
                        if(value)
                        {
                            comp.thermManu.enabled = 0;
                            if(comp.thermManu.number)
                                controlSystem.disableCfgState(comp.thermManu.number);

                            comp.thermInte.enabled = 0;
                            if(comp.thermInte.number)
                                controlSystem.disableCfgState(comp.thermInte.number);

                            comp.condAuto.enabled = 0;
                            if(comp.condAuto.number)
                                controlSystem.disableCfgState(comp.condAuto.number);
                                
                            comp.condManu.enabled = 0;
                            if(comp.condManu.number)
                                controlSystem.disableCfgState(comp.condManu.number);
                                
                            comp.thermAuto.enabled = 0;
                            if(comp.thermAuto.number)
                                controlSystem.disableCfgState(comp.thermAuto.number);
                                

                            if(comp.condInte.number)
                                controlSystem.enableCfgState(comp.condInte.number);
                                
                        }
                        else
                        {
                            if(comp.condInte.number)
                                controlSystem.disableCfgState(comp.condInte.number);
                        }
                    }break;
                }
            };

            $scope.showAddBtn = function(comp)
            {
                switch(comp.thermType)
                {
                    case "therm":
                    {
                        return comp.thermAuto.number != undefined || comp.thermInte.number != undefined || comp.thermManu.number != undefined;
                    }break;

                    case "cond":
                    {
                        return comp.condAuto.number != undefined || comp.condInte.number != undefined || comp.condManu.number != undefined;                            
                    }break;
                }
            }

            $scope.SaveManualConfig = function(comp)//Salvataggio della configurazione appena terminata
            {
                var stringSetCfgState = "";
                var useModeCode = "";
                var currentComponent = {};
                
                if(comp.useMode == "therm_manual" || comp.useMode == "cond_manual")
                {
                    switch(comp.useMode)
                    {
                        case 'therm_manual':
                        {
                            currentComponent = comp.thermManu;
                        }break;
                        case 'cond_manual':
                        {
                            currentComponent = comp.condManu;
                        }break;
                    }
                

                    stringSetCfgState = "";
                    useModeCode = "cfgtmanu";
                    
                    stringSetCfgState = useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + comp.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

                    switch(currentComponent.assocOutputType)
                    {
                        case 'off':
                        {
                            stringSetCfgState += '0,0:4096,0:'
                        }break;
                        case 'on':
                        {
                            stringSetCfgState += '0,0:8192,0:'
                        }break;
                        case 'toggle':
                        {
                            stringSetCfgState += '0,0:16384,0:'
                        }break;
                    }

                    switch(currentComponent.assocInputType)
                    {
                        case 'and':
                        {
                            stringSetCfgState += '0,0:32769,0:'
                        }break;
                        case 'toggle':
                        {
                            stringSetCfgState += '0,0:32768,0:'
                        }break;
                    }

                    //Caricamento Timer
                    currentComponent.timer.forEach(function(timer){
                        
                        var valDisabledDays = 0;
                        if( timer.disabledDays )
                        {
                            for(var i=0; i < timer.disabledDays.length; i++)
                            {
                                valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                            }
                        }

                        var valDisabledMonth = 0;
                        if( timer.disabledMonth )
                        {
                            for(var i=0; i < timer.disabledMonth.length; i++)
                            {
                                valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                            }
                        }

                        stringSetCfgState += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                        switch(timer.type.toLowerCase())
                        {
                            case 'off':
                            {
                                var index = 256 + parseInt(timer.index);
                                stringSetCfgState += index.toString() + ',';
                            }break;
                            case 'on':
                            {
                                var index = 512 + parseInt(timer.index);
                                stringSetCfgState += index.toString() + ',';
                            }break;
                        }

                        stringSetCfgState += timer.minutesFromMidnight.toString();


                        stringSetCfgState += timer.side;

                        stringSetCfgState += timer.soglia.toString() + ":";
                    });

                    var inputList = currentComponent.inputList;
                    Object.keys(inputList).forEach(function (key) {
                        if(inputList[key] !== 'function'){
                            if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                            }
                        }
                    });
                    var sensorList = currentComponent.sensorList;
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + ",";
                                var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                if(indexFirstNum != -1){
                                    if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                        indexFirstNum--;
                                    var condizione = sensorList[key].value.substring(0,indexFirstNum);

                                    stringSetCfgState = stringSetCfgState + condizione + currentComponent.sogliaTemp.toFixed(1) + ':';

                                }
                            }
                        }
                    });
                    var dimmerList = currentComponent.dimmerList;
                    Object.keys(dimmerList).forEach(function (key) {
                        if(dimmerList[key] !== 'function'){
                            if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                            }
                        }
                    });
                    var toggleList = currentComponent.toggleList;
                    Object.keys(toggleList).forEach(function (key) {
                        if(toggleList[key] !== 'function'){
                            if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                            }
                        }
                    });

                    $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                    {"newIndex": controlSystem.getAssociationNumber(),
                     "oldIndex": currentComponent.number,
                     "message": stringSetCfgState,
                     "nextPage": 'app.service/detail/:service/:room',
                     "nextPageData" : JSON.stringify({"service":$scope.service, "room":$scope.room})
                    });
                    
                    //$state.go('app.service/detail/:service/:room', {"room":$scope.room, "service":$scope.service});
                }
            };

            $scope.DeleteManualConfig = function(comp)//Cancellazione della configurazione appena terminata
            {
                var stringSetCfgState = "";
                var currentComponent = {};
                
                if(comp.useMode == "therm_manual" || comp.useMode == "cond_manual")
                {
                    switch(comp.useMode)
                    {
                        case 'therm_manual':
                        {
                            currentComponent = comp.thermManu;
                        }break;
                        case 'cond_manual':
                        {
                            currentComponent = comp.condManu;
                        }break;
                    }

                    $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                    {"newIndex": -1,
                     "oldIndex": currentComponent.number,
                     "message": stringSetCfgState,
                     "nextPage": 'app.service/detail/:service/:room',
                     "nextPageData" : JSON.stringify({"service":$scope.service, "room":$scope.room})
                    });
                    
                    //$state.go('app.service/detail/:service/:room', {"room":$scope.room, "service":$scope.service});
                }
            };

            $scope.cloneConfig = function(comp)//funzione per aggiungere una configurazione assente clonandone una dello stesso tipo
            {
                var stringSetCfgState = "";
                var useModeCode = "";
                var currentComponent;

                switch(comp.thermType)
                {
                    case "therm":
                    {
                        if(comp.thermAuto.number != undefined)
                            currentComponent = comp.thermAuto;
                        else if (comp.thermInte.number != undefined)
                            currentComponent = comp.thermInte;
                        else if (comp.thermManu.number != undefined)
                            currentComponent = comp.thermManu;
                    }break;

                    case "therm":
                    {
                        if(comp.condAuto.number != undefined)
                            currentComponent = comp.condAuto;
                        else if (comp.condInte.number != undefined)
                            currentComponent = comp.condInte;
                        else if (comp.condManu.number != undefined)
                            currentComponent = comp.condManu;                           
                    }break;
                }
                
                if(currentComponent != undefined)
                {
                    switch(comp.useMode)
                    {
                        case "therm_manual":
                        case "cond_manual":
                        {
                            useModeCode = "cfgtmanu";
                        }break;

                        case "therm_auto":
                        case "cond_auto":
                        {
                            useModeCode = "cfgtauto";
                        }break;

                        case "therm_int":
                        case "cond_int":
                        {
                            useModeCode = "cfgtinte";
                        }break;
                    }

                    stringSetCfgState = useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + comp.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

                    switch(currentComponent.assocOutputType)
                    {
                        case 'off':
                        {
                            stringSetCfgState += '0,0:4096,0:';
                        }break;
                        case 'on':
                        {
                            stringSetCfgState += '0,0:8192,0:';
                        }break;
                        case 'toggle':
                        {
                            stringSetCfgState += '0,0:16384,0:';
                        }break;
                    }

                    switch(currentComponent.assocInputType)
                    {
                        case 'and':
                        {
                            stringSetCfgState += '0,0:32769,0:';
                        }break;
                        case 'toggle':
                        {
                            stringSetCfgState += '0,0:32768,0:';
                        }break;
                    }

                    var inputList = currentComponent.inputList;
                    Object.keys(inputList).forEach(function (key) {
                        if(inputList[key] !== 'function'){
                            if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                            }
                        }
                    });
                    var sensorList = currentComponent.sensorList;
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                            }
                        }
                    });
                    var dimmerList = currentComponent.dimmerList;
                    Object.keys(dimmerList).forEach(function (key) {
                        if(dimmerList[key] !== 'function'){
                            if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                            }
                        }
                    });
                    var toggleList = currentComponent.toggleList;
                    Object.keys(toggleList).forEach(function (key) {
                        if(toggleList[key] !== 'function'){
                            if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                stringSetCfgState += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                            }
                        }
                    });

                    $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                    {"newIndex": controlSystem.getAssociationNumber(),
                     "oldIndex": -1,
                     "message": stringSetCfgState,
                     "nextPage": 'app.service/detail/:service/:room',
                     "nextPageData" : JSON.stringify({"service":$scope.service, "room":$scope.room})
                    });
                }
            };
        }
        break;

        case "consume":
        {
            $scope.generalSwitch = { value: false };
            
            $scope.generalSwitchUpdate = function(comp){
                comp.generalSwitchValue = comp.outputList.some(function(load){
                    //return (comp.toggle && $scope.element[comp.toggle].value !== 0) ||
                    //     (comp.dimmer && $scope.element[comp.dimmer].value !== 0);

                    return (load.element.value !== 0);
                });
            };

            var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
                $scope.roomComponents.forEach(function(comp){
                    $scope.generalSwitchUpdate(comp);
                });
                $scope.$digest();
            });

            $scope.roomComponents.forEach(function(comp){
                if(comp.enabled == undefined)
                {
                    comp.enabled = 1;
                }
                $scope.generalSwitchUpdate(comp);
            });

            $scope.$on('$destroy', asyncUpdateListener);

            $scope.changeMode = function(comp){
                comp.enabled = (comp.enabled + 1)%2;
                if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello("L1"))
                {
                    if(comp.number != undefined)
                    {
                        if(comp.enabled == 0)
                        {
                            controlSystem.disableCfgState(comp.number);
                        }
                        else
                        {
                            controlSystem.enableCfgState(comp.number);
                        }
                    }
                }
            };

            $scope.getSoglia = function(comp){
                var soglia = "";
                var sensorList = comp.sensorList;

                if(sensorList != undefined)
                {
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].condition != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                var indexFirstNum = sensorList[key].condition.indexOf(sensorList[key].condition.match(/\d/));//estraggo il primo caratte numerico
                                if(indexFirstNum != -1){
                                    if(indexFirstNum > 0 && sensorList[key].condition[indexFirstNum-1] == '-')
                                        indexFirstNum--;
                                    soglia = sensorList[key].condition.substring(indexFirstNum) + ' ' + ( sensorList[key].element.unit ? sensorList[key].element.unit : '' );
                                    return;
                                }
                            }
                        }
                    });
                }
                return (soglia == '') ? '--' : soglia ;
            };

            $scope.getCondizione = function(comp){
                var condizione = "";
                var sensorList = comp.sensorList;

                if(sensorList != undefined)
                {
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].condition != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                var indexFirstNum = sensorList[key].condition.indexOf(sensorList[key].condition.match(/\d/));//estraggo il primo caratte numerico
                                if(indexFirstNum != -1){
                                    if(indexFirstNum > 0 && sensorList[key].condition[indexFirstNum-1] == '-')
                                        indexFirstNum--;
                                    condizione = sensorList[key].condition.substring(0,indexFirstNum);
                                    return;
                                }
                            }
                        }
                    });
                }
                return (condizione == '') ? '--' : condizione ;
            };

            $scope.getValue = function(comp){
                var valore = -1000;
                var unit = "";
                var sensorList = comp.sensorList;
                
                if(sensorList != undefined)
                {
                    Object.keys(sensorList).forEach(function (key) {
                        if(sensorList[key] !== 'function'){
                            if(sensorList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                if(sensorList[key].element.value > valore)
                                {
                                    valore = sensorList[key].element.value;
                                    unit = ( sensorList[key].element.unit ? sensorList[key].element.unit : '' );
                                }
                            }
                        }
                    });
                    return valore + ' ' + unit;
                }

                return'--';
            };
            
            $scope.hideSensor = function(comp){
                return comp.sensorList == undefined || Object.keys(comp.sensorList).length == 0;
            };

            $scope.disableSwitch = function(comp){
                if(this.hideSensor(comp))
                    return false;
                else if (comp.enabled == 0)
                    return false;
                else
                    return true;
            };

            $scope.update = function(comp, id, value){
                var assoc = id.split(';');
                for(var i=0; i<assoc.length; i++)
                {
                    controlSystem.setState(assoc[i],value).then(function(){
                        $scope.generalSwitchUpdate(comp);
                    }).catch(function(){
                        $cordovaDialogs.alert(
                            $translate('alert_noResponseFromCU'),
                            $translate('alert_serviceTitle'),
                            $translate('alert_button')
                        );
                    });
                }
            };

            $scope.updateAll = function(comp, generalSwitchValue){
                comp.outputList.forEach(function(load){
                    if(generalSwitchValue){
                        $scope.update(comp, load.element.id, 1);
                    }else{
                        $scope.update(comp, load.element.id, 0);
                    }
                });
            };

            $scope.showEdit = function(comp){
                if(controlSystem.getEnableLevel() < controlSystem.getSogliaLivello("L1") || comp.outputList.length <= 1)
                    return false;
                else
                    return true;
            };

        }break;
    }
    
    

    // Defines scopes
    $scope.sceneAdd = function(){
    	var $translate = $filter('translate');
    	$cordovaDialogs.alert(
    		$translate('alert_unavailableService'),
    		$translate('scene_title'),
    		$translate('alert_button')
    	);
        /*$state.go('app.scene/add/:service/:room', {
            'room': $stateParams.room,
            'service': $stateParams.service
        });*/
    };

    $scope.goBack = function()
    {
        if($scope.fromRooms)
        {
            $scope.roomOpen($scope.room);
        }
        else
        {
            $scope.serviceOpen($scope.service);
        }
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.goBack();
    },1000);

    $scope.checkEnabled = function(comp){
        if (comp.enabled == 0)
            return true;
        else
            return false;
    };

    $scope.getEnabledIcon = function(comp)
    {
        if(comp.enabled && comp.enabled == 1)
        {
            return "ion-ios-unlocked-outline";
        }
        else
        {
            return "ion-ios-locked-outline";
        }
    }

    $scope.editEnableTimer = function(comp,type)
    {
        $state.go('app.service/timer_enable/:service/:room/:element/:type',
        {"service": comp.service,
            "room": $scope.room,
            "element": comp.name,
            "type":type});
    };

    $scope.getEnabledIconRS = function(comp)
    {
        var upEnabled = (comp.rollUpComp && comp.rollUpComp.number && $scope.associations[comp.rollUpComp.number].enabled == 1)
        var downEnabled = (comp.rollDownComp && comp.rollDownComp.number && $scope.associations[comp.rollDownComp.number].enabled == 1)

        if(upEnabled || downEnabled)
        {
            return "ion-ios-unlocked-outline";
        }
        else
        {
            return "ion-ios-locked-outline";
        }
    };

    $scope.editEnableTimerRS = function(comp)
    {
        $state.go('app.service/rs_app_list/:service/:room/:element/:component',
        {"service": 'rollingShutter',
            "room": $scope.room,
            "element": comp.name,
            "component": JSON.stringify(comp)});
    };

}).controller('RoomDetailController', function($scope, $state, $rootScope, $cordovaDialogs, $filter, $state, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    var $translate = $filter('translate');
    $scope.translate = $filter('translate');
    
    $scope.component = $stateParams.component;

    //AC_20180319
    $scope.associations = controlSystem.associations();

    // $scope.navigationHeader = $filter('capitalize')($scope.room.toString().replace(/_252F/g, "/")).replace(/_2523/g,"#");
    $scope.navigationHeader = ($scope.room.toString().replace(/_252F/g, "/")).replace(/_2523/g,"#");
    if($scope.navigationHeader.startsWith("#") && $scope.navigationHeader.endsWith("#"))
    {
        var navHeader = $scope.navigationHeader.replace("#","");
        // $scope.navigationHeader = $filter('capitalize')(navHeader.slice(0,navHeader.length-1));
        $scope.navigationHeader = (navHeader.slice(0,navHeader.length-1));
    }

    $scope.element = controlSystem.element();
    $scope.roomComponents = controlSystem.roomComponents($scope.room).filter(function(elem){
        if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello("L1"))
        {
            return elem.service == 'light' || elem.service == 'rollingShutter' || elem.service == 'thermostatus' || elem.service == 'consume';
        }else
        {
            return elem.service == 'light' || elem.service == 'rollingShutter' || elem.service == 'consume';
        }
    });

    var asyncUpdateListener = $rootScope.$on('asyncUpdate', function (event, data){
        $scope.$digest();
    });
    $scope.$on('$destroy', asyncUpdateListener);

    $scope.roomComponents.sort(function(a, b){
        var valA = 0;
        var valB = 0;
        switch(a.service)
        {
          case 'light': valA = 0; break;
          case 'rollingShutter': valA = 1; break;
          case 'thermostatus': valA = 2; break;
          case 'consume': valA = 3; break;
          default: valA = 5; break;
        }
        switch(b.service)
        {
          case 'light': valB = 0; break;
          case 'rollingShutter': valB = 1; break;
          case 'thermostatus': valB = 2; break;
          case 'consume': valB = 3; break;
          default: valB = 5; break;
        }
        return valA - valB;
    });

    $scope.roomComponents.forEach(function(roomComp){
        switch(roomComp.service)
        {
            case "light":
            {
                
                $scope.updateLight = function(id, value){
                    //console.log("@@#@@update:"+id+" value:"+value);
                    var assoc = id.split(';');
                    for(var i=0; i<assoc.length; i++)
                    {
                        controlSystem.setState(assoc[i],value).then(function(){
                        }).catch(function(){
                            $cordovaDialogs.alert(
                                $translate('alert_noResponseFromCU'),
                                $translate('alert_serviceTitle'),
                                $translate('alert_button')
                            );
                        });
                    }
                };

                $scope.editTimerLight = function(comp)
                {
                    $state.go('app.service/timer/:service/:room/:element',
                    {"service": comp.service,
                     "room": $scope.room,
                     "element": comp.name});
                };

                //AC_20180319
                roomComp.enabled = controlSystem.associations()[roomComp.number].enabled;
            }
            break;

            case "rollingShutter":
            {   
                
                $scope.updateRS = function(id, value){
                    //console.log("@@#@@update:"+id+" value:"+value);
                    var assoc = id.split(';');
                    for(var i=0; i<assoc.length; i++)
                    {
                        controlSystem.setState(assoc[i],value).then(function(){
                        }).catch(function(){
                            $cordovaDialogs.alert(
                                $translate('alert_noResponseFromCU'),
                                $translate('alert_serviceTitle'),
                                $translate('alert_button')
                            );
                        });
                    }
                };

                $scope.updateRollingShutter = function(id){
                    //console.log("@@#@@update:"+id+" value:"+value);
                    var assoc = id.split(';');
                    var newValue=0;
                    
                    for(var i=0; i<assoc.length; i++)
                    {
                        var currentEl = $scope.element[assoc[i]];
                        if(currentEl != undefined && currentEl != null)
                            newValue += currentEl.value;
                    }

                    newValue = newValue > 0 ? 0 : 1;

                    for(var i=0; i<assoc.length; i++)
                    {
                        controlSystem.setState(assoc[i],newValue).catch(function(){
                            $cordovaDialogs.alert(
                                $translate('alert_noResponseFromCU'),
                                $translate('alert_serviceTitle'),
                                $translate('alert_button')
                            );
                        });
                    }
                };

                $scope.editTimerRS = function(comp)
                {
                    $state.go('app.service/timer_rs/:service/:room/:element',
                    {"service": comp.service,
                     "room": $scope.room,
                     "element": comp.name});
                };

                $scope.checkEnabledU = function(comp){
                    var nAssIndex = 0;
                    if (comp.rollUpComp)
                    {
                        nAssIndex = comp.rollUpComp.number;
                        var nEnable = controlSystem.associations()[nAssIndex].enabled
                        if (nEnable == 0)
                            return true;
                        else
                            return false;
                    }
                    return false;
                };

                $scope.checkEnabledD = function(comp){
                    var nAssIndex = 0;
                    if (comp.rollDownComp)
                    {
                        nAssIndex = comp.rollDownComp.number;
                        var nEnable = controlSystem.associations()[nAssIndex].enabled
                        if (nEnable == 0)
                            return true;
                        else
                            return false;
                    }
                    return false;
                };
            }
            break;

            case "thermostatus":
            {
                $scope.externalSensors = controlSystem.externalSensors();


                $scope.changeModeTherm = function(comp){
                    switch(comp.useMode)
                    {
                        case 'therm_auto':
                        {
                            switch(comp.thermType)
                            {
                                case 'cond':
                                {
                                    comp.useMode='cond_auto';
                                }break;
                                case 'therm':
                                {
                                    comp.useMode='therm_manual';
                                }break;
                            }
                        }break;
                        case 'therm_manual':
                        {
                            switch(comp.thermType)
                            {
                                case 'cond':
                                {
                                    comp.useMode='cond_manual';
                                }break;
                                case 'therm':
                                {
                                    comp.useMode='therm_int';
                                }break;
                            }
                        }break;
                        case 'therm_int':
                        {
                            switch(comp.thermType)
                            {
                                case 'cond':
                                {
                                    comp.useMode='cond_int';
                                }break;
                                case 'therm':
                                {
                                    comp.useMode='therm_auto';
                                }break;
                            }
                        }break;
                        case 'cond_auto':
                        {
                            switch(comp.thermType)
                            {
                                case 'therm':
                                {
                                    comp.useMode='therm_auto';
                                }break;
                                case 'cond':
                                {
                                    comp.useMode='cond_manual';
                                }break;
                            }
                        }break;
                        case 'cond_manual':
                        {
                            switch(comp.thermType)
                            {
                                case 'therm':
                                {
                                    comp.useMode='therm_manual';
                                }break;
                                case 'cond':
                                {
                                    comp.useMode='cond_int';
                                }break;
                            }
                        }break;
                        case 'cond_int':
                        {
                            switch(comp.thermType)
                            {
                                case 'therm':
                                {
                                    comp.useMode='therm_int';
                                }break;
                                case 'cond':
                                {
                                    comp.useMode='cond_auto';
                                }break;
                            }
                        }break;
                    }
                };

                $scope.changeTypeTherm = function(comp){
                    switch(comp.thermType)
                    {
                        case 'therm':
                        {
                            comp.thermType='cond';
                        }break;
                        case 'cond':
                        {
                            comp.thermType='therm';
                        }break;
                    }
                    $scope.changeModeTherm(comp);
                };

                $scope.getSogliaTherm = function(comp){
                    var soglia = "";
                    var sensorList;
                    switch(comp.useMode)
                    {
                        case 'therm_auto':
                        {
                            sensorList = (comp.thermAuto) ? comp.thermAuto.sensorList : undefined;
                        }break;
                        case 'therm_manual':
                        {
                            sensorList = (comp.thermManu) ? comp.thermManu.sensorList : undefined;
                        }break;
                        case 'therm_int':
                        {
                            sensorList = (comp.thermInte) ? comp.thermInte.sensorList : undefined;
                        }break;
                        case 'cond_auto':
                        {
                            sensorList = (comp.condAuto) ? comp.condAuto.sensorList : undefined;
                        }break;
                        case 'cond_manual':
                        {
                            sensorList = (comp.condManu) ? comp.condManu.sensorList : undefined;
                        }break;
                        case 'cond_int':
                        {
                            sensorList = (comp.condInte) ? comp.condInte.sensorList : undefined;
                        }break;
                    }

                    if(sensorList != undefined)
                    {
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        soglia = sensorList[key].value.substring(indexFirstNum) + ( sensorList[key].element.unit ? sensorList[key].element.unit : '' );
                                        return;
                                    }
                                }
                            }
                        });
                    }
                    return (soglia == '') ? '--' : soglia ;
                };

                $scope.getValueTherm = function(comp){
                    var valore = -1000;
                    var unit = "";
                    var sensorList;
                    switch(comp.useMode)
                    {
                        case 'therm_auto':
                        {
                            sensorList = (comp.thermAuto) ? comp.thermAuto.sensorList : undefined;
                        }break;
                        case 'therm_manual':
                        {
                            sensorList = (comp.thermManu) ? comp.thermManu.sensorList : undefined;
                        }break;
                        case 'therm_int':
                        {
                            sensorList = (comp.thermInte) ? comp.thermInte.sensorList : undefined;
                        }break;
                        case 'cond_auto':
                        {
                            sensorList = (comp.condAuto) ? comp.condAuto.sensorList : undefined;
                        }break;
                        case 'cond_manual':
                        {
                            sensorList = (comp.condManu) ? comp.condManu.sensorList : undefined;
                        }break;
                        case 'cond_int':
                        {
                            sensorList = (comp.condInte) ? comp.condInte.sensorList : undefined;
                        }break;
                    }
                    if(sensorList != undefined)
                    {
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    if(sensorList[key].element.value > valore)
                                    {
                                        valore = sensorList[key].element.value;
                                        unit = ( sensorList[key].element.unit ? sensorList[key].element.unit : '' )
                                    }
                                }
                            }
                        });
                        return valore + unit;
                    }

                    return'--';
                };

                $scope.getValueExtTherm = function(comp){
                    var valore = -1000;
                    var unit = "";
                    var sensorList = ($scope.externalSensors[0]) ? $scope.externalSensors[0].sensorList : undefined; 
                    if(sensorList != undefined)
                    {
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    if(sensorList[key].element.value > valore)
                                    {
                                        valore = sensorList[key].element.value;
                                        unit = ( sensorList[key].element.unit ? sensorList[key].element.unit : '' )
                                    }
                                }
                            }
                        });
                        return valore + unit;
                    }

                    return'--';
                };

                $scope.getOnOffValueTherm = function(comp){
                    var valore = 0;
                    var toggleList;
                    switch(comp.useMode)
                    {
                        case 'therm_auto':
                        {
                            toggleList = (comp.thermAuto) ? comp.thermAuto.toggleList : undefined;
                        }break;
                        case 'therm_manual':
                        {
                            toggleList = (comp.thermManu) ? comp.thermManu.toggleList : undefined;
                        }break;
                        case 'therm_int':
                        {
                            toggleList = (comp.thermInte) ? comp.thermInte.toggleList : undefined;
                        }break;
                        case 'cond_auto':
                        {
                            toggleList = (comp.condAuto) ? comp.condAuto.toggleList : undefined;
                        }break;
                        case 'cond_manual':
                        {
                            toggleList = (comp.condManu) ? comp.condManu.toggleList : undefined;
                        }break;
                        case 'cond_int':
                        {
                            toggleList = (comp.condInte) ? comp.condInte.toggleList : undefined;
                        }break;
                    }

                    if(toggleList != undefined)
                    {
                        Object.keys(toggleList).forEach(function (key) {
                            if(toggleList[key] !== 'function'){
                                if(toggleList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    valore += parseInt(toggleList[key].element.value);
                                }
                            }
                        });
                    }

                    if(valore > 0)
                    {
                        if(comp.thermType == 'therm')
                            return 'ion-fireball color-red';
                        else
                            return 'ion-ios-snowy color-blue';
                    }
                    else
                    {
                        if(comp.thermType == 'therm')
                            return 'ion-fireball color-gray';
                        else
                            return 'ion-ios-snowy color-gray';

                    }
                };

                roomComp.timer.forEach(function(timer){
                    timer.showTimer = function(weekDay)
                        {
                            if(weekDay >=0 && weekDay <7)
                                if(this.disabledDays[weekDay] == 1)
                                    return false;
                            return true;
                        };  

                    timer.formatTime = function()
                    {
                        var minutes = this.minutesFromMidnight % 60;
                        var hours = (this.minutesFromMidnight - minutes) / 60;

                        return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                    }

                });

                if(roomComp.thermAuto != undefined && roomComp.thermAuto.timer != undefined){
                    roomComp.thermAuto.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            }; 

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(roomComp.thermManu != undefined && roomComp.thermManu.timer != undefined){
                    roomComp.thermManu.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(roomComp.thermInte != undefined && roomComp.thermInte.timer != undefined){
                    roomComp.thermInte.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(roomComp.condAuto != undefined && roomComp.condAuto.timer != undefined){
                    roomComp.condAuto.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(roomComp.condManu != undefined && roomComp.condManu.timer != undefined){
                    roomComp.condManu.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                if(roomComp.condInte != undefined && roomComp.condInte.timer != undefined){
                    roomComp.condInte.timer.forEach(function(timer){
                        timer.showTimer = function(weekDay)
                            {
                                if(weekDay >=0 && weekDay <7)
                                    if(this.disabledDays[weekDay] == 1)
                                        return false;
                                return true;
                            };

                        timer.formatTime = function()
                        {
                            var minutes = this.minutesFromMidnight % 60;
                            var hours = (this.minutesFromMidnight - minutes) / 60;

                            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
                        }
                    });
                }

                roomComp.thermType = 'therm';
                roomComp.useMode = 'therm_auto';

                roomComp.today = new Date();

                //definizione funzione per gestione manuale termico
                if(roomComp.thermManu.number != undefined)
                {
                    if(roomComp.thermManu.sensorList != undefined)
                    {
                        var soglia=0;
                        var sensorList = roomComp.thermManu.sensorList;
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        soglia = parseFloat(sensorList[key].value.substring(indexFirstNum));
                                        return;
                                    }
                                }
                            }
                        });

                        roomComp.thermManu.soglia = soglia;
                        roomComp.thermManu.interval = 2;

                        roomComp.resetSoglia = function(){
                            this.thermManu.sogliaTemp = this.thermManu.soglia;
                            this.thermManu.intervalTemp = this.thermManu.interval;
                        };

                        roomComp.upSoglia = function(){
                            this.thermManu.sogliaTemp += parseFloat(roomComp.thermManu.multiplier.toFixed(1));
                        };

                        roomComp.downSoglia = function(){
                            this.thermManu.sogliaTemp -= parseFloat(roomComp.thermManu.multiplier.toFixed(1));
                        };

                        roomComp.upInterval = function(){

                            this.thermManu.intervalTemp += 1;

                            if(this.thermManu.intervalTemp>=24)
                                this.thermManu.intervalTemp = 24;
                        };
                        roomComp.downInterval = function(){
                            
                            this.thermManu.intervalTemp -= 1;

                            if(this.thermManu.intervalTemp<1)
                                this.thermManu.intervalTemp = 1;
                        };

                        roomComp.thermManu.multiplier = 1;

                        roomComp.currentMultiplier = function(value)
                        {
                            return (roomComp.thermManu.multiplier == value)?'selected':''
                        }

                        roomComp.changeMultiplier = function(value)
                        {
                            roomComp.thermManu.multiplier = value;
                        }

                        roomComp.resetSoglia();
                    }
                }

                //definizione funzione per gestione manuale condizionamento
                if(roomComp.condManu.number != undefined)
                {
                    if(roomComp.condManu.sensorList != undefined)
                    {
                        var soglia=0;
                        var sensorList = roomComp.condManu.sensorList;
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        soglia = parseFloat(sensorList[key].value.substring(indexFirstNum));
                                        return;
                                    }
                                }
                            }
                        });

                        roomComp.condManu.soglia = soglia;
                        roomComp.condManu.interval = 2;

                        roomComp.resetSogliaC = function(){
                            this.condManu.sogliaTemp = this.condManu.soglia;
                            this.condManu.intervalTemp = this.condManu.interval;
                        };

                        roomComp.upSogliaC = function(){
                            this.condManu.sogliaTemp += roomComp.condManu.multiplier;
                        };

                        roomComp.downSogliaC = function(){
                            this.condManu.sogliaTemp -= roomComp.condManu.multiplier;
                        };

                        roomComp.upIntervalC = function(){

                            this.condManu.intervalTemp += 1;

                            if(this.condManu.intervalTemp>=24)
                                this.condManu.intervalTemp = 24;
                        };
                        roomComp.downIntervalC = function(){
                            
                            this.condManu.intervalTemp -= 1;

                            if(this.condManu.intervalTemp<1)
                                this.condManu.intervalTemp = 1;
                        };

                        roomComp.condManu.multiplier = 1;

                        roomComp.currentMultiplier = function(value)
                        {
                            return (roomComp.condManu.multiplier == value)?'selected':''
                        }

                        roomComp.changeMultiplier = function(value)
                        {
                            roomComp.condManu.multiplier = value;
                        }

                        roomComp.resetSogliaC();
                    }
                }

                roomComp.selectedDay = roomComp.today.getDay();

                //Individuazione modo d'uso abilitato
                if(roomComp.thermManu && roomComp.thermManu.enabled == 1)
                {
                    roomComp.thermType = 'therm';
                    roomComp.useMode = 'therm_manual';
                }
                if(roomComp.thermAuto && roomComp.thermAuto.enabled == 1)
                {
                    roomComp.thermType = 'therm';
                    roomComp.useMode = 'therm_auto';
                }
                if(roomComp.thermInte && roomComp.thermInte.enabled == 1)
                {
                    roomComp.thermType = 'therm';
                    roomComp.useMode = 'therm_int';
                }
                if(roomComp.condManu && roomComp.condManu.enabled == 1)
                {
                    roomComp.thermType = 'cond';
                    roomComp.useMode = 'cond_manual';
                }
                if(roomComp.condAuto && roomComp.condAuto.enabled == 1)
                {
                    roomComp.thermType = 'cond';
                    roomComp.useMode = 'cond_auto';
                }
                if(roomComp.condInte && roomComp.condInte.enabled ==1)
                {
                    roomComp.thermType = 'cond';
                    roomComp.useMode = 'cond_int';
                }

                roomComp.isSelected = function(dayNumber)
                {
                    if(roomComp.selectedDay == dayNumber)
                        return "selected";
                    return "";
                }

                roomComp.setSelectedDay = function(dayNumber)
                {
                    roomComp.selectedDay = dayNumber;
                }
   
                switch(roomComp.useMode)
                {
                    case 'therm_auto':
                    {
                        if(roomComp.thermAuto == undefined)
                        {
                            roomComp.thermAuto = {'enabled':0};
                        }
                        else
                        {
                            if(roomComp.thermAuto.enabled == undefined)
                                roomComp.thermAuto.enabled = 0;
                        }
                    }break;
                    case 'therm_manual':
                    {
                        if(roomComp.thermManu == undefined)
                        {
                            roomComp.thermManu = {'enabled':0};
                        }
                        else
                        {
                            if(roomComp.thermManu.enabled == undefined)
                                roomComp.thermManu.enabled = 0;
                        }
                    }break;
                    case 'therm_int':
                    {
                        if(roomComp.thermInte == undefined)
                        {
                            roomComp.thermInte = {'enabled':0};
                        }
                        else
                        {
                            if(roomComp.thermInte.enabled == undefined)
                                roomComp.thermInte.enabled = 0;
                        }
                    }break;
                    case 'cond_auto':
                    {
                        if(roomComp.condAuto == undefined)
                        {
                            roomComp.condAuto = {'enabled':0};
                        }
                        else
                        {
                            if(roomComp.condAuto.enabled == undefined)
                                roomComp.condAuto.enabled = 0;
                        }
                    }break;
                    case 'cond_manual':
                    {
                        if(roomComp.condManu == undefined)
                        {
                            roomComp.condManu = {'enabled':0};
                        }
                        else
                        {
                            if(roomComp.condManu.enabled == undefined)
                                roomComp.condManu.enabled = 0;
                        }
                    }break;
                    case 'cond_int':
                    {
                        if(roomComp.condInte == undefined)
                        {
                            roomComp.condInte = {'enabled':0};
                        }
                        else
                        {
                            if(roomComp.condInte.enabled == undefined)
                                roomComp.condInte.enabled = 0;
                        }
                    }break;
                }

                $scope.HideEnableSwitchTherm = function(comp)
                {
                    switch(comp.useMode)
                    {
                        case 'therm_auto':
                        {
                            if(comp.thermAuto.number == undefined)
                            {
                                return true;
                            }
                        }break;
                        case 'therm_manual':
                        {
                            if(comp.thermManu.number == undefined)
                            {
                                return true;
                            }
                        }break;
                        case 'therm_int':
                        {
                            if(comp.thermInte.number == undefined)
                            {
                                return true;
                            }
                        }break;
                        case 'cond_auto':
                        {
                            if(comp.condAuto.number == undefined)
                            {
                                return true;
                            }
                        }break;
                        case 'cond_manual':
                        {
                            if(comp.condManu.number == undefined)
                            {
                                return true;
                            }
                        }break;
                        case 'cond_int':
                        {
                            if(comp.condInte.number == undefined)
                            {
                                return true;
                            }
                        }break;
                    }
                    return false;
                };

                $scope.toggleEnableTherm = function(comp,value)
                {
                    switch(comp.useMode)
                    {
                        case 'therm_auto':
                        {
                            if(value)
                            {
                                comp.thermManu.enabled = 0;
                                if(comp.thermManu.number)
                                    controlSystem.disableCfgState(comp.thermManu.number)

                                comp.thermInte.enabled = 0;
                                if(comp.thermInte.number)
                                    controlSystem.disableCfgState(comp.thermInte.number)

                                comp.condAuto.enabled = 0;
                                if(comp.condAuto.number)
                                    controlSystem.disableCfgState(comp.condAuto.number)
                                    
                                comp.condManu.enabled = 0;
                                if(comp.condManu.number)
                                    controlSystem.disableCfgState(comp.condManu.number)
                                    
                                comp.condInte.enabled = 0;
                                if(comp.condInte.number)
                                    controlSystem.disableCfgState(comp.condInte.number)
                                    

                                if(comp.thermAuto.number)
                                    controlSystem.enableCfgState(comp.thermAuto.number)
                                    
                            }
                            else
                            {
                                if(comp.thermAuto.number)
                                    controlSystem.disableCfgState(comp.thermAuto.number)
                            }
                        }break;
                        case 'therm_manual':
                        {
                            if(value)
                            {
                                comp.thermAuto.enabled = 0;
                                if(comp.thermAuto.number)
                                    controlSystem.disableCfgState(comp.thermAuto.number)

                                comp.thermInte.enabled = 0;
                                if(comp.thermInte.number)
                                    controlSystem.disableCfgState(comp.thermInte.number)

                                comp.condAuto.enabled = 0;
                                if(comp.condAuto.number)
                                    controlSystem.disableCfgState(comp.condAuto.number)
                                    
                                comp.condManu.enabled = 0;
                                if(comp.condManu.number)
                                    controlSystem.disableCfgState(comp.condManu.number)
                                    
                                comp.condInte.enabled = 0;
                                if(comp.condInte.number)
                                    controlSystem.disableCfgState(comp.condInte.number)
                                    

                                if(comp.thermManu.number)
                                    controlSystem.enableCfgState(comp.thermManu.number)
                                    
                            }
                            else
                            {
                                if(comp.thermManu.number)
                                    controlSystem.disableCfgState(comp.thermManu.number)
                            }
                        }break;
                        case 'therm_int':
                        {
                            if(value)
                            {
                                comp.thermManu.enabled = 0;
                                if(comp.thermManu.number)
                                    controlSystem.disableCfgState(comp.thermManu.number)

                                comp.thermAuto.enabled = 0;
                                if(comp.thermAuto.number)
                                    controlSystem.disableCfgState(comp.thermAuto.number)

                                comp.condAuto.enabled = 0;
                                if(comp.condAuto.number)
                                    controlSystem.disableCfgState(comp.condAuto.number)
                                    
                                comp.condManu.enabled = 0;
                                if(comp.condManu.number)
                                    controlSystem.disableCfgState(comp.condManu.number)
                                    
                                comp.condInte.enabled = 0;
                                if(comp.condInte.number)
                                    controlSystem.disableCfgState(comp.condInte.number)
                                    

                                if(comp.thermInte.number)
                                    controlSystem.enableCfgState(comp.thermInte.number)
                                    
                            }
                            else
                            {
                                if(comp.thermInte.number)
                                    controlSystem.disableCfgState(comp.thermInte.number)
                            }
                        }break;
                        case 'cond_auto':
                        {
                            if(value)
                            {
                                comp.thermManu.enabled = 0;
                                if(comp.thermManu.number)
                                    controlSystem.disableCfgState(comp.thermManu.number)

                                comp.thermInte.enabled = 0;
                                if(comp.thermInte.number)
                                    controlSystem.disableCfgState(comp.thermInte.number)

                                comp.thermAuto.enabled = 0;
                                if(comp.thermAuto.number)
                                    controlSystem.disableCfgState(comp.thermAuto.number)
                                    
                                comp.condManu.enabled = 0;
                                if(comp.condManu.number)
                                    controlSystem.disableCfgState(comp.condManu.number)
                                    
                                comp.condInte.enabled = 0;
                                if(comp.condInte.number)
                                    controlSystem.disableCfgState(comp.condInte.number)
                                    

                                if(comp.condAuto.number)
                                    controlSystem.enableCfgState(comp.condAuto.number)
                                    
                            }
                            else
                            {
                                if(comp.condAuto.number)
                                    controlSystem.disableCfgState(comp.condAuto.number)
                            }
                        }break;
                        case 'cond_manual':
                        {
                            if(value)
                            {
                                comp.thermManu.enabled = 0;
                                if(comp.thermManu.number)
                                    controlSystem.disableCfgState(comp.thermManu.number)

                                comp.thermInte.enabled = 0;
                                if(comp.thermInte.number)
                                    controlSystem.disableCfgState(comp.thermInte.number)

                                comp.condAuto.enabled = 0;
                                if(comp.condAuto.number)
                                    controlSystem.disableCfgState(comp.condAuto.number)
                                    
                                comp.thermAuto.enabled = 0;
                                if(comp.thermAuto.number)
                                    controlSystem.disableCfgState(comp.thermAuto.number)
                                    
                                comp.condInte.enabled = 0;
                                if(comp.condInte.number)
                                    controlSystem.disableCfgState(comp.condInte.number)
                                    

                                if(comp.condManu.number)
                                    controlSystem.enableCfgState(comp.condManu.number)
                                    
                            }
                            else
                            {
                                if(comp.condManu.number)
                                    controlSystem.disableCfgState(comp.condManu.number)
                            }
                        }break;
                        case 'cond_int':
                        {
                            if(value)
                            {
                                comp.thermManu.enabled = 0;
                                if(comp.thermManu.number)
                                    controlSystem.disableCfgState(comp.thermManu.number)

                                comp.thermInte.enabled = 0;
                                if(comp.thermInte.number)
                                    controlSystem.disableCfgState(comp.thermInte.number)

                                comp.condAuto.enabled = 0;
                                if(comp.condAuto.number)
                                    controlSystem.disableCfgState(comp.condAuto.number)
                                    
                                comp.condManu.enabled = 0;
                                if(comp.condManu.number)
                                    controlSystem.disableCfgState(comp.condManu.number)
                                    
                                comp.thermAuto.enabled = 0;
                                if(comp.thermAuto.number)
                                    controlSystem.disableCfgState(comp.thermAuto.number)
                                    

                                if(comp.condInte.number)
                                    controlSystem.enableCfgState(comp.condInte.number)
                                    
                            }
                            else
                            {
                                if(comp.condInte.number)
                                    controlSystem.disableCfgState(comp.condInte.number)
                            }
                        }break;
                    }
                };

                $scope.showAddBtnTherm = function(comp)
                {
                    switch(comp.thermType)
                    {
                        case "therm":
                        {
                            return comp.thermAuto.number != undefined || comp.thermInte.number != undefined || comp.thermManu.number != undefined;
                        }break;

                        case "cond":
                        {
                            return comp.condAuto.number != undefined || comp.condInte.number != undefined || comp.condManu.number != undefined;                            
                        }break;
                    }
                }

                $scope.SaveManualConfigTherm = function(comp)//Salvataggio della configurazione appena terminata
                {
                    var stringSetCfgState = "";
                    var useModeCode = "";
                    var currentComponent = {};
                    
                    if(comp.useMode == "therm_manual" || comp.useMode == "cond_manual")
                    {
                        switch(comp.useMode)
                        {
                            case 'therm_manual':
                            {
                                currentComponent = comp.thermManu;
                            }break;
                            case 'cond_manual':
                            {
                                currentComponent = comp.condManu;
                            }break;
                        }
                    

                        var stringSetCfgState = "";
                        var useModeCode = "cfgtmanu";
                        
                        stringSetCfgState = useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + comp.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

                        switch(currentComponent.assocOutputType)
                        {
                            case 'off':
                            {
                                stringSetCfgState += '0,0:4096,0:'
                            }break;
                            case 'on':
                            {
                                stringSetCfgState += '0,0:8192,0:'
                            }break;
                            case 'toggle':
                            {
                                stringSetCfgState += '0,0:16384,0:'
                            }break;
                        }

                        switch(currentComponent.assocInputType)
                        {
                            case 'and':
                            {
                                stringSetCfgState += '0,0:32769,0:'
                            }break;
                            case 'toggle':
                            {
                                stringSetCfgState += '0,0:32768,0:'
                            }break;
                        }

                        //Caricamento Timer
                        currentComponent.timer.forEach(function(timer){
                            
                            var valDisabledDays = 0;
                            if( timer.disabledDays )
                            {
                                for(var i=0; i < timer.disabledDays.length; i++)
                                {
                                    valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                                }
                            }

                            var valDisabledMonth = 0;
                            if( timer.disabledMonth )
                            {
                                for(var i=0; i < timer.disabledMonth.length; i++)
                                {
                                    valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                                }
                            }

                            stringSetCfgState += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                            switch(timer.type.toLowerCase())
                            {
                                case 'off':
                                {
                                    var index = 256 + parseInt(timer.index);
                                    stringSetCfgState += index.toString() + ',';
                                }break;
                                case 'on':
                                {
                                    var index = 512 + parseInt(timer.index);
                                    stringSetCfgState += index.toString() + ',';
                                }break;
                            }

                            stringSetCfgState += timer.minutesFromMidnight.toString();


                            stringSetCfgState += timer.side;

                            stringSetCfgState += timer.soglia.toString() + ":";
                        });

                        var inputList = currentComponent.inputList;
                        Object.keys(inputList).forEach(function (key) {
                            if(inputList[key] !== 'function'){
                                if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                                }
                            }
                        });
                        var sensorList = currentComponent.sensorList;
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + ",";
                                    var indexFirstNum = sensorList[key].value.indexOf(sensorList[key].value.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].value[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        var condizione = sensorList[key].value.substring(0,indexFirstNum);

                                        stringSetCfgState = stringSetCfgState + condizione + currentComponent.sogliaTemp.toFixed(1) + ':';

                                    }
                                }
                            }
                        });
                        var dimmerList = currentComponent.dimmerList;
                        Object.keys(dimmerList).forEach(function (key) {
                            if(dimmerList[key] !== 'function'){
                                if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                                }
                            }
                        });
                        var toggleList = currentComponent.toggleList;
                        Object.keys(toggleList).forEach(function (key) {
                            if(toggleList[key] !== 'function'){
                                if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                                }
                            }
                        });
                        
                        $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                        {"newIndex": controlSystem.getAssociationNumber(),
                         "oldIndex": currentComponent.number,
                         "message": stringSetCfgState,
                         "nextPage": 'app.service/room/detail/:room',
                         "nextPageData" : JSON.stringify({"room":$scope.room})
                        });
                        
                        //$state.go('app.service/detail/:service/:room', {"room":$scope.room, "service":$scope.service});
                    }
                };
                $scope.DeleteManualConfigTherm = function(comp)//Cancellazione della configurazione appena terminata
                {
                    var stringSetCfgState = "";
                    var currentComponent = {};
                    
                    if(comp.useMode == "therm_manual" || comp.useMode == "cond_manual")
                    {
                        switch(comp.useMode)
                        {
                            case 'therm_manual':
                            {
                                currentComponent = comp.thermManu;
                            }break;
                            case 'cond_manual':
                            {
                                currentComponent = comp.condManu;
                            }break;
                        }

                        $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                        {"newIndex": -1,
                        "oldIndex": currentComponent.number,
                        "message": stringSetCfgState,
                        "nextPage": 'app.service/room/detail/:room',
                        "nextPageData" : JSON.stringify({"room":$scope.room})
                        });
                        
                        //$state.go('app.service/detail/:service/:room', {"room":$scope.room, "service":$scope.service});
                    }
                };

                $scope.cloneConfigTherm = function(comp)//funzione per aggiungere una configurazione assente clonandone una dello stesso tipo
                {
                    var stringSetCfgState = "";
                    var useModeCode = "";
                    var currentComponent;
    
                    switch(comp.thermType)
                    {
                        case "therm":
                        {
                            if(comp.thermAuto.number != undefined)
                                currentComponent = comp.thermAuto;
                            else if (comp.thermInte.number != undefined)
                                currentComponent = comp.thermInte;
                            else if (comp.thermManu.number != undefined)
                                currentComponent = comp.thermManu;
                        }break;
    
                        case "cond":
                        {
                            if(comp.condAuto.number != undefined)
                                currentComponent = comp.condAuto;
                            else if (comp.condInte.number != undefined)
                                currentComponent = comp.condInte;
                            else if (comp.condManu.number != undefined)
                                currentComponent = comp.condManu;                           
                        }break;
                    }
                    
                    if(currentComponent != undefined)
                    {
                        switch(comp.useMode)
                        {
                            case "therm_manual":
                            case "cond_manual":
                            {
                                useModeCode = "cfgtmanu";
                            }break;
    
                            case "therm_auto":
                            case "cond_auto":
                            {
                                useModeCode = "cfgtauto";
                            }break;
    
                            case "therm_int":
                            case "cond_int":
                            {
                                useModeCode = "cfgtinte";
                            }break;
                        }
    
                        stringSetCfgState = useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + comp.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';
    
                        switch(currentComponent.assocOutputType)
                        {
                            case 'off':
                            {
                                stringSetCfgState += '0,0:4096,0:'
                            }break;
                            case 'on':
                            {
                                stringSetCfgState += '0,0:8192,0:'
                            }break;
                            case 'toggle':
                            {
                                stringSetCfgState += '0,0:16384,0:'
                            }break;
                        }
    
                        switch(currentComponent.assocInputType)
                        {
                            case 'and':
                            {
                                stringSetCfgState += '0,0:32769,0:'
                            }break;
                            case 'toggle':
                            {
                                stringSetCfgState += '0,0:32768,0:'
                            }break;
                        }
    
                        var inputList = currentComponent.inputList;
                        Object.keys(inputList).forEach(function (key) {
                            if(inputList[key] !== 'function'){
                                if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                                }
                            }
                        });
                        var sensorList = currentComponent.sensorList;
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                                }
                            }
                        });
                        var dimmerList = currentComponent.dimmerList;
                        Object.keys(dimmerList).forEach(function (key) {
                            if(dimmerList[key] !== 'function'){
                                if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                                }
                            }
                        });
                        var toggleList = currentComponent.toggleList;
                        Object.keys(toggleList).forEach(function (key) {
                            if(toggleList[key] !== 'function'){
                                if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    stringSetCfgState += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                                }
                            }
                        });
    
                        $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                        {"newIndex": controlSystem.getAssociationNumber(),
                         "oldIndex": -1,
                         "message": stringSetCfgState,
                         "nextPage": 'app.service/room/detail/:room',
                         "nextPageData" : JSON.stringify({"room":$scope.room})
                        });
                    }
                };
            }
            break;

            case "consume":
            {
                $scope.generalSwitchUpdate = function(comp){
                    comp.generalSwitchValue = comp.outputList.some(function(load){
                        //return (comp.toggle && $scope.element[comp.toggle].value !== 0) ||
                        //     (comp.dimmer && $scope.element[comp.dimmer].value !== 0);

                        return (load.element.value !== 0);
                    });
                };

                var asyncUpdateListenerConsume = $rootScope.$on('asyncUpdate', function (event, data){
                    $scope.roomComponents.filter(function(comp){return comp.service == "consume";}).forEach(function(comp){
                        $scope.generalSwitchUpdate(comp);
                    });
                    $scope.$digest();
                });

                if(roomComp.enabled == undefined)
                {
                    roomComp.enabled = 1;
                }
                $scope.generalSwitchUpdate(roomComp);
                

                $scope.$on('$destroy', asyncUpdateListenerConsume);

                $scope.changeModeCon = function(comp){
                    comp.enabled = (comp.enabled + 1)%2;
                    if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello("L1"))
                    {
                        if(comp.number != undefined)
                        {
                            if(comp.enabled == 0)
                            {
                                controlSystem.disableCfgState(comp.number)
                            }
                            else
                            {
                                controlSystem.enableCfgState(comp.number)
                            }
                        }
                    }
                };

                $scope.getSogliaCon = function(comp){
                    var soglia = "";
                    var sensorList = comp.sensorList;

                    if(sensorList != undefined)
                    {
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].condition != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    var indexFirstNum = sensorList[key].condition.indexOf(sensorList[key].condition.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].condition[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        soglia = sensorList[key].condition.substring(indexFirstNum) + ' ' + ( sensorList[key].element.unit ? sensorList[key].element.unit : '' );
                                        return;
                                    }
                                }
                            }
                        });
                    }
                    return (soglia == '') ? '--' : soglia ;
                };

                $scope.getCondizioneCon = function(comp){
                    var condizione = "";
                    var sensorList = comp.sensorList;

                    if(sensorList != undefined)
                    {
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].condition != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    var indexFirstNum = sensorList[key].condition.indexOf(sensorList[key].condition.match(/\d/));//estraggo il primo caratte numerico
                                    if(indexFirstNum != -1){
                                        if(indexFirstNum > 0 && sensorList[key].condition[indexFirstNum-1] == '-')
                                            indexFirstNum--;
                                        condizione = sensorList[key].condition.substring(0,indexFirstNum);
                                        return;
                                    }
                                }
                            }
                        });
                    }
                    return (condizione == '') ? '--' : condizione ;
                };

                $scope.getValueCon = function(comp){
                    var valore = -1000;
                    var unit = "";
                    var sensorList = comp.sensorList;
                    
                    if(sensorList != undefined)
                    {
                        Object.keys(sensorList).forEach(function (key) {
                            if(sensorList[key] !== 'function'){
                                if(sensorList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                                    if(sensorList[key].element.value > valore)
                                    {
                                        valore = sensorList[key].element.value;
                                        unit = ( sensorList[key].element.unit ? sensorList[key].element.unit : '' )
                                    }
                                }
                            }
                        });
                        return valore + ' ' + unit;
                    }

                    return'--';
                };
                
                $scope.hideSensorCon = function(comp){
                    return comp.sensorList == undefined || Object.keys(comp.sensorList).length == 0;
                };

                $scope.disableSwitchCon = function(comp){
                    if(this.hideSensorCon(comp))
                        return false;
                    else if (comp.enabled == 0)
                        return false;
                    else
                        return true;
                };

                $scope.updateCon = function(comp, id, value){
                    var assoc = id.split(';');
                    for(var i=0; i<assoc.length; i++)
                    {
                        controlSystem.setState(assoc[i],value).then(function(){
                            $scope.generalSwitchUpdate(comp);
                        }).catch(function(){
                            $cordovaDialogs.alert(
                                $translate('alert_noResponseFromCU'),
                                $translate('alert_serviceTitle'),
                                $translate('alert_button')
                            );
                        });
                    }
                };

                $scope.updateAllCon = function(comp, generalSwitchValue){
                    comp.outputList.forEach(function(load){
                        if(generalSwitchValue){
                            $scope.updateCon(comp, load.element.id, 1);
                        }else{
                            $scope.updateCon(comp, load.element.id, 0);
                        }
                    });
                };

                $scope.showEditCon = function(comp){
                    if(controlSystem.getEnableLevel() < controlSystem.getSogliaLivello("L1") || comp.outputList.length <= 1)
                        return false;
                    else
                        return true;
                };

            }break;
        }
    });
    

    // Defines scopes
    $scope.sceneAdd = function(){
        var $translate = $filter('translate');
        $cordovaDialogs.alert(
            $translate('alert_unavailableService'),
            $translate('scene_title'),
            $translate('alert_button')
        );
        /*$state.go('app.scene/add/:service/:room', {
            'room': $stateParams.room,
            'service': $stateParams.service
        });*/
    };

    $scope.goBack = function()
    {
        $scope.roomOpen($scope.room);
    }

    
    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.dashboard();
    },1000);

    $scope.checkEnabled = function(comp){
        if (comp.enabled == 0)
            return true;
        else
            return false;
    };

    $scope.getEnabledIcon = function(comp)
    {
        if(comp.enabled && comp.enabled == 1)
        {
            return "ion-ios-unlocked-outline";
        }
        else
        {
            return "ion-ios-locked-outline";
        }
    }

    $scope.editEnableTimer = function(comp, type)
    {
        $state.go('app.service/timer_enable/:service/:room/:element/:type',
        {"service": comp.service,
            "room": $scope.room,
            "element": comp.name,
            "type": type});
    };

    $scope.getEnabledIconRS = function(comp)
    {
        var upEnabled = (comp.rollUpComp && comp.rollUpComp.number && $scope.associations[comp.rollUpComp.number].enabled == 1)
        var downEnabled = (comp.rollDownComp && comp.rollDownComp.number && $scope.associations[comp.rollDownComp.number].enabled == 1)

        if(upEnabled || downEnabled)
        {
            return "ion-ios-unlocked-outline";
        }
        else
        {
            return "ion-ios-locked-outline";
        }
    }

    $scope.editEnableTimerRS = function(comp)
    {
        $state.go('app.service/rs_app_list/:service/:room/:element/:component',
        {"service": 'rollingShutter',
            "room": $scope.room,
            "element": comp.name,
            "component": JSON.stringify(comp)});
    };

}).controller('ServiceTimerController', function($scope, $rootScope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
		return comp.name === $stateParams.element && comp.service == $scope.service;
	})[0] || {};

    if($scope.roomComponent.dimmer){
    	$scope.roomComponent.preTimeId = $scope.roomComponent.dimmer.replace('LO','PR');
		$scope.roomComponent.postTimeId = $scope.roomComponent.dimmer.replace('LO','PS');
    }else if($scope.roomComponent.toggle){
    	$scope.roomComponent.preTimeId = $scope.roomComponent.toggle.replace('TO','PR');
		$scope.roomComponent.postTimeId = $scope.roomComponent.toggle.replace('TO','PS');
    }

    //TO_DO_PRE_POST
    $scope.preTime = -1;
	$scope.postTime = -1;
    if(Object.keys($scope.roomComponent.dimmerList).length > 0)
    {
        $scope.roomComponent.preTimeList = [];
        $scope.roomComponent.postTimeList = [];
        Object.keys($scope.roomComponent.dimmerList).forEach(function(key){
            var ElementPreId = key.replace('LO','PR');
            var ElementPostId = key.replace('LO','PS');
            $scope.roomComponent.preTimeList.push(key.replace('LO','PR'));
            $scope.roomComponent.postTimeList.push(key.replace('LO','PS'));

            if($scope.element[ElementPreId] && $scope.element[ElementPreId].configured)
            {
                if($scope.preTime == -1 || $scope.preTime > $scope.element[ElementPreId].configured)
                {
                    $scope.preTime = $scope.element[ElementPreId].configured;
                }
            }

            if($scope.element[ElementPostId] && $scope.element[ElementPostId].configured)
            {
                if($scope.postTime == -1 || $scope.postTime > $scope.element[ElementPostId].configured)
                {
                    $scope.postTime = $scope.element[ElementPostId].configured;
                }
            }
        });
    }else if(Object.keys($scope.roomComponent.toggleList).length > 0)
    {
        $scope.roomComponent.preTimeList = [];
        $scope.roomComponent.postTimeList = [];
        Object.keys($scope.roomComponent.toggleList).forEach(function(key){
            var ElementPreId = key.replace('TO','PR');
            var ElementPostId = key.replace('TO','PS');
            $scope.roomComponent.preTimeList.push(key.replace('TO','PR'));
            $scope.roomComponent.postTimeList.push(key.replace('TO','PS'));

            if($scope.element[ElementPreId] && $scope.element[ElementPreId].configured != undefined)
            {
                if($scope.preTime == -1 || $scope.preTime > $scope.element[ElementPreId].configured)
                {
                    $scope.preTime = $scope.element[ElementPreId].configured;
                }
            }

            if($scope.element[ElementPostId] && $scope.element[ElementPostId].configured != undefined)
            {
                if($scope.postTime == -1 || $scope.postTime > $scope.element[ElementPostId].configured)
                {
                    $scope.postTime = $scope.element[ElementPostId].configured;
                }
            }
        });
    }
    
	//$scope.preTime = $scope.element[$scope.roomComponent.preTimeId].configured;
	//$scope.postTime = $scope.element[$scope.roomComponent.postTimeId].configured;

    // Defines scopes
    //AC_20180720
    // $scope.timerChange = function(type){
    //     $cordovaDialogs.prompt(
    //     	$translate('alert_timerMessage'),
    //     	$translate('alert_timerTitle'), [
    //     		$translate('alert_buttonCancel'),
    //     		$translate('alert_buttonSave')
    //     	],
    //     	''
    //     ).then(function(result){
    //     	var newValue = result.input1;

    //         if(result.buttonIndex === 2 && newValue !== $scope[type + 'Time']){
    //         	$scope[type + 'Time'] = newValue;
    //         	controlSystem.setState($scope.roomComponent[type + 'TimeId'],newValue);
    //         }
    //     });
    // };

    $scope.timerChange = function(type){
        $cordovaDialogs.prompt(
        	$translate('alert_timerMessage'),
        	$translate('alert_timerTitle'), [
        		$translate('alert_buttonCancel'),
        		$translate('alert_buttonSave')
        	],
        	''
        ).then(function(result){
        	var newValue = result.input1;

            if(result.buttonIndex === 2 && newValue !== $scope[type + 'Time']){
                $scope[type + 'Time'] = newValue;
                
                switch(type.toLowerCase())
                {
                    case 'pre':
                    {
                        if($scope.roomComponent.preTimeList.length > 0)
                            $scope.roomComponent.preTimeList.forEach(function(item){
                                controlSystem.setState(item, $scope.preTime);
                                }
                            )
                    }break;

                    case 'post':
                    {
                        if($scope.roomComponent.postTimeList.length > 0)
                            $scope.roomComponent.postTimeList.forEach(function(item){
                                controlSystem.setState(item, $scope.postTime);
                                }
                            )
                    }break;
                }
            }
        });
    };

    var today = new Date();
    $scope.roomComponent.selectedDay = today.getDay();


    $scope.roomComponent.timer.forEach(function(timer){
        timer.showTimer = function(weekDay)
        {
            if(weekDay >=0 && weekDay <7)
                if(this.disabledDays[weekDay] == 1)
                    return false;
            return true;
        }; 

        timer.formatTime = function()
        {
            var minutes = this.minutesFromMidnight % 60;
            var hours = (this.minutesFromMidnight - minutes) / 60;

            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
        }
    });


    $scope.roomComponent.isSelected = function(dayNumber)
    {
        if($scope.roomComponent.selectedDay == dayNumber)
            return "selected";
        return "";
    }

    $scope.roomComponent.setSelectedDay = function(dayNumber)
    {
        $scope.roomComponent.selectedDay = dayNumber;
    }

    $scope.GoBack = function()
    {
        if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
        {
            $state.go('app.service/room/detail/:room',{"room":$scope.room});
        }
        else
        {
            $state.go('app.service/detail/:service/:room',{"service":$scope.service,"room":$scope.room});
        }
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.GoBack();
    },1000);

}).controller('ServiceTimerDetailController', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');

    $scope.edited = $stateParams.edited;

    $scope.useModeCode = 'cfgstate';

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element && comp.service == $scope.service; 
    })[0] || {};

    var today = new Date();

    $scope.roomComponent.timer.forEach(function(timer){
        timer.formatTime = function()
        {
            var minutes = this.minutesFromMidnight % 60;
            var hours = (this.minutesFromMidnight - minutes) / 60;

            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
        }
    });

    $scope.roomComponent.isEnabled = function(timer, dayNumber)
    {
        if(timer.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.SaveAndExit = function()//Salvataggio della configurazione appena terminata
    {
        var stringSetCfgState = "";
        if($scope.edited == '1' && $scope.useModeCode != "")
        {
            stringSetCfgState = $scope.useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + $scope.roomComponent.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

            switch($scope.roomComponent.assocOutputType)
            {
                case 'off':
                {
                    stringSetCfgState += '0,0:4096,0:'
                }break;
                case 'on':
                {
                    stringSetCfgState += '0,0:8192,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:16384,0:'
                }break;
            }

            switch($scope.roomComponent.assocInputType)
            {
                case 'and':
                {
                    stringSetCfgState += '0,0:32769,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:32768,0:'
                }break;
            }

            //Caricamento Timer
            $scope.roomComponent.timer.forEach(function(timer){
                
                var valDisabledDays = 0;
                if( timer.disabledDays )
                {
                    for(var i=0; i < timer.disabledDays.length; i++)
                    {
                        valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                    }
                }

                var valDisabledMonth = 0;
                if( timer.disabledMonth )
                {
                    for(var i=0; i < timer.disabledMonth.length; i++)
                    {
                        valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                    }
                }

                stringSetCfgState += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                switch(timer.type.toLowerCase())
                {
                    case 'off':
                    {
                        var index = 256 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                    case 'on':
                    {
                        var index = 512 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                }

                stringSetCfgState += timer.minutesFromMidnight.toString();


                //_AC_20180305
                // stringSetCfgState += timer.condition + ":";
                stringSetCfgState += timer.condition;

                if(timer.onlyOnce==1)
                {
                    stringSetCfgState += 'u:';                        
                }
                else
                {
                    stringSetCfgState += ':';
                }
            });

            var inputList = $scope.roomComponent.inputList;
            Object.keys(inputList).forEach(function (key) {
                if(inputList[key] !== 'function'){
                    if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                    }
                }
            });
            var sensorList = $scope.roomComponent.sensorList;
            Object.keys(sensorList).forEach(function (key) {
                if(sensorList[key] !== 'function'){
                    if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                    }
                }
            });
            var dimmerList = $scope.roomComponent.dimmerList;
            Object.keys(dimmerList).forEach(function (key) {
                if(dimmerList[key] !== 'function'){
                    if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                    }
                }
            });
            var toggleList = $scope.roomComponent.toggleList;
            Object.keys(toggleList).forEach(function (key) {
                if(toggleList[key] !== 'function'){
                    if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                    }
                }
            });
            
            $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                {"newIndex": controlSystem.getAssociationNumber(),
                 "oldIndex": $scope.roomComponent.number,
                 "message": stringSetCfgState,
                 "nextPage": "app.service/timer/:service/:room/:element",
                 "nextPageData" : JSON.stringify({"service":$scope.service, "room":$scope.room, "element":$stateParams.element})
                });
        }
        else
        {
            $state.go("app.service/timer/:service/:room/:element",{"service":$stateParams.service, "room":$stateParams.room, "element":$stateParams.element});
        }

    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.SaveAndExit();
    },1000)
}).controller('ServiceEditTimerController', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');
    $scope.padNumber = $filter('padNumber');

    $scope.edited = $stateParams.edited;
    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element  && comp.service == $scope.service;
    })[0] || {};

    var today = new Date();

    $scope.newTimer = false;
    var timer;

    if($stateParams.timer != undefined)
    {
        timer = JSON.parse($stateParams.timer)
    }
    else
    {
        $scope.newTimer = true;
        var hour = today.getHours();
        var minutes = today.getMinutes();
        var minutesFromMidnight = hour*60 + minutes;
        //AC_20180219
        // timer = {"disabledDays":[0,0,0,0,0,0,0],"disabledMonth":[0,0,0,0,0,0,0,0,0,0,0,0],"index":-1,"type":"on","minutesFromMidnight":minutesFromMidnight,"condition":"e"};
        timer = {"disabledDays":[0,0,0,0,0,0,0],"disabledMonth":[0,0,0,0,0,0,0,0,0,0,0,0],"index":-1,"type":"on","minutesFromMidnight":minutesFromMidnight,"condition":"",'onlyOnce':0};
    }

    $scope.minutes = timer.minutesFromMidnight%60;

    $scope.hours = (timer.minutesFromMidnight - $scope.minutes)/60;

    $scope.typeOn = (timer.type.toLowerCase() == 'on')? 1 : 0;

    $scope.disabledDays = timer.disabledDays;

    $scope.isEnabled = function(dayNumber)
    {
        if($scope.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.setEnabled = function(dayNumber)
    {
        $scope.disabledDays[dayNumber] = ($scope.disabledDays[dayNumber]+1) % 2 ;
        //AC_20180219
        if($scope.onlyOnce && $scope.disabledDays[dayNumber] == 0)
        {
            for(var i=0; i<$scope.disabledDays.length; i++)
            {
                if(i != dayNumber)
                    $scope.disabledDays[i] = 1;
            }
        }
    }

    $scope.upHour = function()
    {
        $scope.hours = ($scope.hours+1) % 24 ;
    }

    $scope.downHour = function()
    {
        $scope.hours = ($scope.hours-1) >= 0 ? $scope.hours-1 : 23;
    }

    $scope.upMinutes = function()
    {
        $scope.minutes = ($scope.minutes+1) % 60 ;
    }

    $scope.downMinutes = function()
    {
        $scope.minutes = ($scope.minutes-1) >= 0 ? $scope.minutes-1 : 59;
    }

    $scope.isOn = function(value)
    {
        return $scope.typeOn == value ? 'selected':'';
    }

    $scope.changeType = function(value)
    {
        $scope.typeOn = value;
    }

    $scope.SaveChanges = function()
    {
        var timerList;
        var currentTimer;

        timerList = ($scope.roomComponent.timer) ? $scope.roomComponent.timer : undefined;

        if(timerList != undefined)
        {
            if($scope.newTimer)
            {
                currentTimer = timer;
                currentTimer.index = timerList.length;
                timerList.push(currentTimer);
            }
            else
            {
                currentTimer = timerList.filter(function(comp){
                        return comp.index === timer.index;
                    })[0] || {}
            }

            
            //AC_20180305
            if($scope.timerMode == 0){
                currentTimer.disabledDays = $scope.disabledDays;
                currentTimer.minutesFromMidnight = $scope.hours * 60 + $scope.minutes;
                currentTimer.onlyOnce = $scope.onlyOnce?1:0;
            }
            else{
                currentTimer.disabledDays = [1,1,1,1,1,1,1];
                
                var today = new Date();
                var day = today.getDay();
                var hour = today.getHours() + $scope.hoursDelay;
                var minutes = today.getMinutes() + $scope.minutesDelay;
                
                if(minutes >= 60)
                {
                    minutes = minutes % 60;
                    hour = hour + 1;
                }

                if(hour >= 24)
                {
                    day += day%7;
                    hour = hour%24;
                }

                currentTimer.disabledDays[day]=0;

                currentTimer.minutesFromMidnight = hour*60 + minutes;
                currentTimer.onlyOnce = 1;
            }

            currentTimer.type = ($scope.typeOn == 1) ? 'On':'Off';
        }


        $state.go('app.service/timer_detail/:service/:room/:element/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": '1'
        });
    }

    $scope.DeleteTimer = function()
    {
        var timerList;
        var currentTimer;

        timerList = ($scope.roomComponent.timer) ? $scope.roomComponent.timer : undefined;

        if(timerList != undefined)
        {
            if(!$scope.newTimer)
            {
                var indexOf = -1
                for(var i=0; i<timerList.length;i++)
                {
                    if(timerList[i].index == timer.index)
                    {
                        indexOf = i;
                        break;
                    }
                }

                if(indexOf > -1)
                {
                    timerList.splice(indexOf,1);
                    timerList.forEach(function(Item,index){
                        Item.index = index;
                    });
                }
            }
        }


        $state.go('app.service/timer_detail/:service/:room/:element/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": '1'
        });
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $state.go('app.service/timer_detail/:service/:room/:element/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": $scope.edited
        });
    },1000);

    //AC_20180219
    $scope.onlyOnce = (timer.onlyOnce == 0)?false:true;
    $scope.changeOnlyOnce = function(){
        $scope.onlyOnce = !$scope.onlyOnce;
        if($scope.onlyOnce)
        {
            $scope.disabledDays=[1,1,1,1,1,1,1];
            var today = new Date();
            var day = today.getDay();
            $scope.disabledDays[day] = 0;
            //_AC_20180306
            // var bFound = false;
            // for(var i=0; i < $scope.disabledDays.length; i++)
            // {
            //     if($scope.disabledDays[i] == 0 && !bFound)
            //     {
            //         bFound = true;
            //     }
            //     else
            //         $scope.disabledDays[i] = 1
            // }
        }
    }

    //AC_20180220
    $scope.timerMode = 0;
    $scope.changeTimerMode = function(newMode)
    {
        $scope.timerMode = newMode;
    
        // Manages views
        $('#tabCalendar').removeClass('selected');
        $('#tabDelay').removeClass('selected');

        $('#panelCalendar').addClass('hide');
        $('#panelDelay').addClass('hide');

        $('#tab' + ($scope.timerMode==0?'Calendar':'Delay')).addClass('selected');
        $('#panel' + ($scope.timerMode==0?'Calendar':'Delay')).removeClass('hide');
    }
    $scope.isActive = function(mode)
    {
        if($scope.timerMode = mode)
            return "active";
        return "";
        $scope.$digest();
    }
    $scope.minutesDelay = 0;

    $scope.hoursDelay = 2;

    $scope.upHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay+1) % 24 ;
    }

    $scope.downHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay-1) >= 0 ? $scope.hoursDelay-1 : 23;
    }

    $scope.upMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay+1) % 60 ;
    }

    $scope.downMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay-1) >= 0 ? $scope.minutesDelay-1 : 59;
    }

}).controller('ServiceThermTimerDetailController', function($scope, $rootScope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');

    $scope.useMode = $stateParams.useMode;
    $scope.edited = $stateParams.edited;
    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element && comp.service == $scope.service;
    })[0] || {};

    $scope.useModeCode = "";


    if($scope.useMode!=undefined)
    {
        switch($scope.useMode)
        {
            case 'therm_auto':
            {
                $scope.timer = ($scope.roomComponent.thermAuto) ? $scope.roomComponent.thermAuto.timer : undefined;
                $scope.number = ($scope.roomComponent.thermAuto) ? $scope.roomComponent.thermAuto.number : undefined;
                $scope.useModeCode = "cfgtauto";
                $scope.activeComponent = $scope.roomComponent.thermAuto;
            }break;
            case 'therm_manual':
            {
                $scope.timer = ($scope.roomComponent.thermManu) ? $scope.roomComponent.thermManu.timer : undefined;
                $scope.number = ($scope.roomComponent.thermManu) ? $scope.roomComponent.thermManu.number : undefined;
                $scope.useModeCode = "cfgtmanu";
                $scope.activeComponent = $scope.roomComponent.thermManu;
            }break;
            case 'therm_int':
            {
                $scope.timer = ($scope.roomComponent.thermInte) ? $scope.roomComponent.thermInte.timer : undefined;
                $scope.number = ($scope.roomComponent.thermInte) ? $scope.roomComponent.thermInte.number : undefined;
                $scope.useModeCode = "cfgtinte";
                $scope.activeComponent = $scope.roomComponent.thermInte;
            }break;
            case 'cond_auto':
            {
                $scope.timer = ($scope.roomComponent.condAuto) ? $scope.roomComponent.condAuto.timer : undefined;
                $scope.number = ($scope.roomComponent.condAuto) ? $scope.roomComponent.condAuto.number : undefined;
                $scope.useModeCode = "cfgtauto";
                $scope.activeComponent = $scope.roomComponent.condAuto;

            }break;
            case 'cond_manual':
            {
                $scope.timer = ($scope.roomComponent.condManu) ? $scope.roomComponent.condManu.timer : undefined;
                $scope.number = ($scope.roomComponent.condManu) ? $scope.roomComponent.condManu.number : undefined;
                $scope.useModeCode = "cfgtmanu";
                $scope.activeComponent = $scope.roomComponent.condManu;
            }break;
            case 'cond_int':
            {
                $scope.timer = ($scope.roomComponent.condInte) ? $scope.roomComponent.condInte.timer : undefined;
                $scope.number = ($scope.roomComponent.condInte) ? $scope.roomComponent.condInte.number : undefined;
                $scope.useModeCode = "cfgtinte";
                $scope.activeComponent = $scope.roomComponent.condInte;
            }break;
        }
    }
    else
    {
        $scope.timer = $scope.roomComponent.timer;
        $scope.number = $scope.roomComponent.number;
    }
    var today = new Date();

    $scope.timer.forEach(function(timer){
        timer.formatTime = function()
        {
            var minutes = this.minutesFromMidnight % 60;
            var hours = (this.minutesFromMidnight - minutes) / 60;

            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
        }
    });

    $scope.isEnabled = function(timer, dayNumber)
    {
        if(timer.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.SaveAndExit = function()//Salvataggio della configurazione appena terminata
    {
        var stringSetCfgState = "";
        if($scope.edited == '1' && $scope.useMode!=undefined && $scope.useModeCode != "")
        {
            stringSetCfgState = $scope.useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + $scope.roomComponent.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

            switch($scope.activeComponent.assocOutputType)
            {
                case 'off':
                {
                    stringSetCfgState += '0,0:4096,0:'
                }break;
                case 'on':
                {
                    stringSetCfgState += '0,0:8192,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:16384,0:'
                }break;
            }

            switch($scope.activeComponent.assocInputType)
            {
                case 'and':
                {
                    stringSetCfgState += '0,0:32769,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:32768,0:'
                }break;
            }

            //Caricamento Timer
            $scope.timer.forEach(function(timer){
                
                var valDisabledDays = 0;
                if( timer.disabledDays )
                {
                    for(var i=0; i < timer.disabledDays.length; i++)
                    {
                        valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                    }
                }

                var valDisabledMonth = 0;
                if( timer.disabledMonth )
                {
                    for(var i=0; i < timer.disabledMonth.length; i++)
                    {
                        valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                    }
                }

                stringSetCfgState += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                switch(timer.type.toLowerCase())
                {
                    case 'off':
                    {
                        var index = 256 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                    case 'on':
                    {
                        var index = 512 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                }

                stringSetCfgState += timer.minutesFromMidnight.toString();

                if(timer.condition == 'e' || timer.condition == 'd' )//Timer di enable
                {
                    stringSetCfgState += timer.condition;
                }
                else
                {
                    stringSetCfgState += timer.side;

                    //_AC_20180305
                    // stringSetCfgState += timer.soglia.toString() + ":";
                    stringSetCfgState += timer.soglia.toString();
                }

                if(timer.onlyOnce==1)
                {
                    stringSetCfgState += 'u:';                        
                }
                else
                {
                    stringSetCfgState += ':';
                }

            });

            var inputList = $scope.activeComponent.inputList;
            Object.keys(inputList).forEach(function (key) {
                if(inputList[key] !== 'function'){
                    if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                    }
                }
            });
            var sensorList = $scope.activeComponent.sensorList;
            Object.keys(sensorList).forEach(function (key) {
                if(sensorList[key] !== 'function'){
                    if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                    }
                }
            });
            var dimmerList = $scope.activeComponent.dimmerList;
            Object.keys(dimmerList).forEach(function (key) {
                if(dimmerList[key] !== 'function'){
                    if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                    }
                }
            });
            var toggleList = $scope.activeComponent.toggleList;
            Object.keys(toggleList).forEach(function (key) {
                if(toggleList[key] !== 'function'){
                    if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                    }
                }
            });

            if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
            {
                $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                    {"newIndex": controlSystem.getAssociationNumber(),
                     "oldIndex": $scope.number,
                     "message": stringSetCfgState,
                     "nextPage": 'app.service/room/detail/:room',
                     "nextPageData" : JSON.stringify({"room":$scope.room})
                    });
            }
            else
            {
                $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                    {"newIndex": controlSystem.getAssociationNumber(),
                     "oldIndex": $scope.number,
                     "message": stringSetCfgState,
                     "nextPage": 'app.service/detail/:service/:room',
                     "nextPageData" : JSON.stringify({"service":$scope.service, "room":$scope.room})
                    });
            }
        }
        else
        {
            if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
            {
                $state.go('app.service/room/detail/:room',{"room":$scope.room});
            }
            else
            {
                $state.go('app.service/detail/:service/:room',{"service":$scope.service, "room":$scope.room});
            }
        }

    }
}).controller('ServiceThermEditTimerController', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');
    $scope.padNumber = $filter('padNumber');

    $scope.useMode = $stateParams.useMode;

    $scope.edited = $stateParams.edited;

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element && comp.service == $scope.service;
    })[0] || {};

    var today = new Date();

    $scope.newTimer = false;
    var timer;

    if($stateParams.timer != undefined)
    {
        timer = JSON.parse($stateParams.timer)
    }
    else
    {
        $scope.newTimer = true;
        var hour = today.getHours();
        var minutes = today.getMinutes();
        var minutesFromMidnight = hour*60 + minutes;
        var soglia = 25;
        var side;
        switch($scope.useMode)
        {
            case 'therm_auto':
            case 'therm_manual':
            case 'therm_int':
            {
                side = '=<'
            }break;
            case 'cond_auto':
            case 'cond_manual':
            case 'cond_int':
            {
               side = '=>'
            }break;
        }
        //AC_20180219
        //timer = {"disabledDays":[0,0,0,0,0,0,0],"disabledMonth":[0,0,0,0,0,0,0,0,0,0,0,0],"index":-1,"type":"on","minutesFromMidnight":minutesFromMidnight,"condition":side+soglia,"soglia":soglia,"side":side};
        timer = {"disabledDays":[0,0,0,0,0,0,0],"disabledMonth":[0,0,0,0,0,0,0,0,0,0,0,0],"index":-1,"type":"on","minutesFromMidnight":minutesFromMidnight,"condition":side+soglia,"soglia":soglia,"side":side,"onlyOnce":0};
    }

    $scope.minutes = timer.minutesFromMidnight%60;

    $scope.hours = (timer.minutesFromMidnight - $scope.minutes)/60;

    $scope.typeOn = (timer.type == 'on')? 1 : 0;

    $scope.disabledDays = timer.disabledDays;

    $scope.soglia = timer.soglia

    $scope.multiplier = 1

    $scope.isEnabled = function(dayNumber)
    {
        if($scope.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.setEnabled = function(dayNumber)
    {
        $scope.disabledDays[dayNumber] = ($scope.disabledDays[dayNumber]+1) % 2 ;
        //AC_20180219
        if($scope.onlyOnce && $scope.disabledDays[dayNumber] == 0)
        {
            for(var i=0; i<$scope.disabledDays.length; i++)
            {
                if(i != dayNumber)
                    $scope.disabledDays[i] = 1;
            }
        }
    }

    $scope.upHour = function()
    {
        $scope.hours = ($scope.hours+1) % 24 ;
    }

    $scope.downHour = function()
    {
        $scope.hours = ($scope.hours-1) >= 0 ? $scope.hours-1 : 23;
    }

    $scope.upMinutes = function()
    {
        $scope.minutes = ($scope.minutes+1) % 60 ;
    }

    $scope.downMinutes = function()
    {
        $scope.minutes = ($scope.minutes-1) >= 0 ? $scope.minutes-1 : 59;
    }

    $scope.upSoglia = function()
    {
        $scope.soglia = ($scope.soglia + parseFloat($scope.multiplier.toFixed(1))) < 100 ? $scope.soglia + parseFloat($scope.multiplier.toFixed(1)) : 100;
    }

    $scope.downSoglia = function()
    {
        $scope.soglia = ($scope.soglia - parseFloat($scope.multiplier.toFixed(1))) > -100 ? $scope.soglia - parseFloat($scope.multiplier.toFixed(1)) : -100;
    }

    $scope.currentMultiplier = function(value)
    {
        return ($scope.multiplier == value)?'selected':''
    }

    $scope.changeMultiplier = function(value)
    {
        $scope.multiplier = value;
    }

    $scope.isOn = function(value)
    {
        return $scope.typeOn == value ? 'selected':'';
    }

    $scope.changeType = function(value)
    {
        $scope.typeOn = value;
    }

    $scope.SaveChanges = function()
    {
        var timerList;
        var currentTimer;

        if($scope.useMode!=undefined)
        {
            switch($scope.useMode)
            {
                case 'therm_auto':
                {
                    timerList = ($scope.roomComponent.thermAuto) ? $scope.roomComponent.thermAuto.timer : undefined;
                }break;
                case 'therm_manual':
                {
                    timerList = ($scope.roomComponent.thermManu) ? $scope.roomComponent.thermManu.timer : undefined;
                }break;
                case 'therm_int':
                {
                    timerList = ($scope.roomComponent.thermInte) ? $scope.roomComponent.thermInte.timer : undefined;
                }break;
                case 'cond_auto':
                {
                    timerList = ($scope.roomComponent.condAuto) ? $scope.roomComponent.condAuto.timer : undefined;
                }break;
                case 'cond_manual':
                {
                    timerList = ($scope.roomComponent.condManu) ? $scope.roomComponent.condManu.timer : undefined;
                }break;
                case 'cond_int':
                {
                    timerList = ($scope.roomComponent.condInte) ? $scope.roomComponent.condInte.timer : undefined;
                }break;
            }
        }

        if(timerList != undefined)
        {
            if($scope.newTimer)
            {
                currentTimer = timer;
                currentTimer.index = timerList.length;
                timerList.push(currentTimer);
            }
            else
            {
                currentTimer = timerList.filter(function(comp){
                        return comp.index === timer.index;
                    })[0] || {}
            }

            //AC_20180305
            if($scope.timerMode == 0){
                currentTimer.disabledDays = $scope.disabledDays;
                currentTimer.minutesFromMidnight = $scope.hours * 60 + $scope.minutes;
                currentTimer.onlyOnce = $scope.onlyOnce?1:0;
            }
            else{
                currentTimer.disabledDays = [1,1,1,1,1,1,1];
                
                var today = new Date();
                var day = today.getDay();
                var hour = today.getHours() + $scope.hoursDelay;
                var minutes = today.getMinutes() + $scope.minutesDelay;
                
                if(minutes >= 60)
                {
                    minutes = minutes % 60;
                    hour = hour + 1;
                }

                if(hour >= 24)
                {
                    day += day%7;
                    hour = hour%24;
                }

                currentTimer.disabledDays[day]=0;

                currentTimer.minutesFromMidnight = hour*60 + minutes;
                currentTimer.onlyOnce = 1;
            }
            
            // currentTimer.disabledDays = $scope.disabledDays;
            // currentTimer.minutesFromMidnight = $scope.hours * 60 + $scope.minutes;

            currentTimer.soglia = parseFloat($scope.soglia.toFixed(1));
        }


        $state.go('app.service/timer_therm_detail/:service/:room/:element/:useMode/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "useMode": $stateParams.useMode,
            "edited": '1'
        });
    }

    $scope.DeleteTimer = function()
    {
        var timerList;

        if($scope.useMode!=undefined)
        {
            switch($scope.useMode)
            {
                case 'therm_auto':
                {
                    timerList = ($scope.roomComponent.thermAuto) ? $scope.roomComponent.thermAuto.timer : undefined;
                }break;
                case 'therm_manual':
                {
                    timerList = ($scope.roomComponent.thermManu) ? $scope.roomComponent.thermManu.timer : undefined;
                }break;
                case 'therm_int':
                {
                    timerList = ($scope.roomComponent.thermInte) ? $scope.roomComponent.thermInte.timer : undefined;
                }break;
                case 'cond_auto':
                {
                    timerList = ($scope.roomComponent.condAuto) ? $scope.roomComponent.condAuto.timer : undefined;
                }break;
                case 'cond_manual':
                {
                    timerList = ($scope.roomComponent.condManu) ? $scope.roomComponent.condManu.timer : undefined;
                }break;
                case 'cond_int':
                {
                    timerList = ($scope.roomComponent.condInte) ? $scope.roomComponent.condInte.timer : undefined;
                }break;
            }
        }

        if(timerList != undefined)
        {
            if(!$scope.newTimer)
            {
                var indexOf = -1
                for(var i=0; i<timerList.length;i++)
                {
                    if(timerList[i].index == timer.index)
                    {
                        indexOf = i;
                        break;
                    }
                }

                if(indexOf > -1)
                {
                    timerList.splice(indexOf,1);
                    timerList.forEach(function(Item,index){
                        Item.index = index;
                    });
                }
            }
        }


        $state.go('app.service/timer_therm_detail/:service/:room/:element/:useMode/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "useMode": $stateParams.useMode,
            "edited": '1'
        });
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $state.go('app.service/timer_therm_detail/:service/:room/:element/:useMode/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "useMode": $stateParams.useMode,
            "edited": $scope.edited
        });
    },1000)


    //AC_20180219
    $scope.onlyOnce = (timer.onlyOnce == 0)?false:true;
    $scope.changeOnlyOnce = function(){
        $scope.onlyOnce = !$scope.onlyOnce;
        if($scope.onlyOnce)
        {
            $scope.disabledDays=[1,1,1,1,1,1,1];
            var today = new Date();
            var day = today.getDay();
            $scope.disabledDays[day] = 0;
            // AC_20180306
            // var bFound = false;
            // for(var i=0; i < $scope.disabledDays.length; i++)
            // {
            //     if($scope.disabledDays[i] == 0 && !bFound)
            //     {
            //         bFound = true;
            //     }
            //     else
            //         $scope.disabledDays[i] = 1
            // }
        }
    }

    //AC_20180306
    $scope.timerMode = 0;
    $scope.changeTimerMode = function(newMode)
    {
        $scope.timerMode = newMode;
    
        // Manages views
        $('#tabCalendar').removeClass('selected');
        $('#tabDelay').removeClass('selected');

        $('#panelCalendar').addClass('hide');
        $('#panelDelay').addClass('hide');

        $('#tab' + ($scope.timerMode==0?'Calendar':'Delay')).addClass('selected');
        $('#panel' + ($scope.timerMode==0?'Calendar':'Delay')).removeClass('hide');
    }
    $scope.isActive = function(mode)
    {
        if($scope.timerMode = mode)
            return "active";
        return "";
        $scope.$digest();
    }
    $scope.minutesDelay = 0;

    $scope.hoursDelay = 2;

    $scope.upHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay+1) % 24 ;
    }

    $scope.downHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay-1) >= 0 ? $scope.hoursDelay-1 : 23;
    }

    $scope.upMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay+1) % 60 ;
    }

    $scope.downMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay-1) >= 0 ? $scope.minutesDelay-1 : 59;
    }
}).controller('TimerSpinner', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    
    //AC_20180123
    $scope.stopUpdateState();
    //AC_20180320
    $scope.stopUpdateEnable();
    $scope.message = $stateParams.message;
    $scope.nextPage = $stateParams.nextPage;
    var $translate = $filter('translate');
    $scope.nextPageData = JSON.parse($stateParams.nextPageData);

    $scope.newIndex = parseInt($stateParams.newIndex);
    $scope.oldIndex = parseInt($stateParams.oldIndex);

    $scope.association = controlSystem.associations();

    if($scope.newIndex != undefined && $scope.newIndex >= 0)
    {

        controlSystem.setCfgState($scope.newIndex,$scope.message).then(function(){
            if($scope.oldIndex != undefined && $scope.oldIndex >= 0){
                controlSystem.deleteCfgState($scope.oldIndex).then(function(){
                    setTimeout(function(){
                        if($scope.association[$scope.oldIndex])
                        {
                            if($scope.association[$scope.oldIndex].enabled != undefined && $scope.association[$scope.oldIndex].enabled == 1)
                                controlSystem.enableCfgState($scope.newIndex - 1);
                            else
                                controlSystem.disableCfgState($scope.newIndex - 1);
                        }
                        controlSystem.init().then(function(){
                            //AC_20180123
                            $scope.startUpdateState();
                            //AC_20180320
                            if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L1'))
                            {
                                $scope.startUpdateEnable();
                            };
                            $state.go($scope.nextPage,$scope.nextPageData);
                            //AC_20180508
                            if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L3'))
                            {
                                controlSystem.setFCMToken();
                            };
                        });
                    },500);
                }).catch(function(){
                    $cordovaDialogs.alert(
                        $translate('alert_cfgStateDeleteError'),
                        $translate('alert_cfgStateTitle'),
                        $translate('alert_button'));
                    
                    //AC_20180515
                    $rootScope.selectedNotification = undefined;
                    
                    $scope.configuration();

                });
            }
            else
            {
                controlSystem.disableCfgState($scope.newIndex,true);
                controlSystem.init().then(function(){
                    //AC_20180123
                    $scope.startUpdateState();
                    //AC_20180320
                    if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L1'))
                    {
                        $scope.startUpdateEnable();
                    };
                    $state.go($scope.nextPage,$scope.nextPageData);
                    //AC_20180508
                    if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L3'))
                    {
                        controlSystem.setFCMToken();
                    };
                });
            }
        }).catch(function(){
            $cordovaDialogs.alert(
                $translate('alert_cfgStateSetError'),
                $translate('alert_cfgStateTitle'),
                $translate('alert_button'));

            //AC_20180515
            $rootScope.selectedNotification = undefined

            $scope.configuration();
                
        });
    }
    else
    {
        if($scope.newIndex == -1)//Rimuovo associazione
        {
            controlSystem.deleteCfgState($scope.oldIndex).then(function(){
                setTimeout(function(){
                    
                    controlSystem.init().then(function(){
                        $scope.startUpdateState();
                        //AC_20180320
                        if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L1'))
                        {
                            $scope.startUpdateEnable();
                        };
                        $state.go($scope.nextPage,$scope.nextPageData);
                        //AC_20180508
                        if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L3'))
                        {
                            controlSystem.setFCMToken();
                        };
                    });
                },500);
            }).catch(function(){
                $cordovaDialogs.alert(
                    $translate('alert_cfgStateDeleteError'),
                    $translate('alert_cfgStateTitle'),
                    $translate('alert_button'));

                //AC_20180515
                $rootScope.selectedNotification = undefined
                
                $scope.configuration();
            });
        }
    }

}).controller('ServiceEditOrderConsumi', function($scope, $rootScope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');
    $scope.translate = $filter('translate');

    $scope.useModeCode = 'cfgprior';
    $scope.edited = $stateParams.edited;
    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element && comp.service == $scope.service;
    })[0] || {};

    $scope.outputList = $scope.roomComponent.outputList.slice(0)
    
    $scope.reordering = false;

    $scope.reorderItem=function(item,fromIndex,toIndex)
    {
        //Move the item in the array
        $scope.outputList.splice(fromIndex, 1);
        $scope.outputList.splice(toIndex, 0, item);
    }

    $scope.onReorderButtonTouch = function() {
        $scope.reordering = true
    }

    $scope.onReorderButtonRelease = function() {
        $scope.reordering = false
    }

    $scope.GoBack = function(){
        if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
        {
            $state.go('app.service/room/detail/:room',
                {"room":$scope.room});
        }else{
            $state.go('app.service/detail/:service/:room',
                {"service":$scope.service, "room":$scope.room});
        }
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.GoBack();
    },1000);
    

    $scope.SaveAndExit = function()//Salvataggio della configurazione appena terminata
    {
        var stringSetCfgState = "";
        
        stringSetCfgState = $scope.useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + $scope.roomComponent.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

            switch($scope.roomComponent.assocOutputType)
            {
                case 'off':
                {
                    stringSetCfgState += '0,0:4096,0:'
                }break;
                case 'on':
                {
                    stringSetCfgState += '0,0:8192,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:16384,0:'
                }break;
            }

            switch($scope.roomComponent.assocInputType)
            {
                case 'and':
                {
                    stringSetCfgState += '0,0:32769,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:32768,0:'
                }break;
            }

            //Caricamento Timer
            $scope.roomComponent.timer.forEach(function(timer){
                
                var valDisabledDays = 0;
                if( timer.disabledDays )
                {
                    for(var i=0; i < timer.disabledDays.length; i++)
                    {
                        valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                    }
                }

                var valDisabledMonth = 0;
                if( timer.disabledMonth )
                {
                    for(var i=0; i < timer.disabledMonth.length; i++)
                    {
                        valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                    }
                }

                stringSetCfgState += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                switch(timer.type.toLowerCase())
                {
                    case 'off':
                    {
                        var index = 256 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                    case 'on':
                    {
                        var index = 512 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                }

                stringSetCfgState += timer.minutesFromMidnight.toString();


                stringSetCfgState += timer.condition;

                if(timer.onlyOnce==1)
                {
                    stringSetCfgState += 'u:';                        
                }
                else
                {
                    stringSetCfgState += ':';
                }
            });

            var inputList = $scope.roomComponent.inputList;
            Object.keys(inputList).forEach(function (key) {
                if(inputList[key] !== 'function'){
                    if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                    }
                }
            });
            var sensorList = $scope.roomComponent.sensorList;
            Object.keys(sensorList).forEach(function (key) {
                if(sensorList[key] !== 'function'){
                    if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                    }
                }
            });
            var toggleList = $scope.outputList;
            toggleList.forEach(function (item) {
                if(item.value != undefined && item.element){//condizione del sensore [cond (<,>,etc...)][soglia]
                    stringSetCfgState += controlSystem.id2numId(item.element.id) + "," + item.value + ':';
                }
            });

            if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
            {
                $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                    {"newIndex": controlSystem.getAssociationNumber(),
                     "oldIndex": $scope.roomComponent.number,
                     "message": stringSetCfgState,
                     "nextPage": 'app.service/room/detail/:room',
                     "nextPageData" : JSON.stringify({"room":$scope.room})
                    });
            }else{
                $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                    {"newIndex": controlSystem.getAssociationNumber(),
                     "oldIndex": $scope.roomComponent.number,
                     "message": stringSetCfgState,
                     "nextPage": 'app.service/detail/:service/:room',
                     "nextPageData" : JSON.stringify({"service":$scope.service, "room":$scope.room})
                    });
            }

    }

}).controller('ConnectionListController', function($scope, $state, $rootScope, $filter, $stateParams, controlSystem, database, $ionicHistory,$ionicNavBarDelegate,$ionicPopup, SecuredPopups, $ionicPlatform){
    //AC_20180123
    $scope.stopUpdateState();
    //AC_20180320
    $scope.stopUpdateEnable();

    if(controlSystem.isOnline()){
        controlSystem.disconnect();
    }

    $scope.navigationHeader = $filter('capitalize')($filter('translate')("connection_list_header"));
    $ionicNavBarDelegate.align('center')
    $rootScope.selectedDevice = "";

    var $translate = $filter('translate');
    $scope.ConnectionList = [];
    // Defines scopes
    $scope.load = function(){
        
        // Loads data
        var query = 'SELECT DISTINCT name FROM configuration';
        
        database
        .execute(query)
        .then(function(result){
            if(result.rows.length > 0) {
                for(var i=0; i < result.rows.length ; i++)
                    $scope.ConnectionList.push(result.rows.item(i));

                if(result.rows.length == 1 && $rootScope.firstOpen && !$rootScope.blockAutoConnect)
                {
                    $rootScope.firstOpen = false;
                    $scope.ConnectToDevice(result.rows.item(0).name);
                }


            }
            $rootScope.firstOpen = false;

        }, function(error){
            $rootScope.firstOpen = false;

            // Error
            // $cordovaDialogs.alert(
            //     $translate('alert_configurationError'),
            //     $translate('alert_configurationTitle'),
            //     $translate('alert_button')
            // );
        });
        
        //AC_20180514
        $rootScope.handlePushNotification();


    };

    // PopUp per inserimento nuova centrale
    // Triggered on a button click, or some other target
    $scope.CreateNewCentral = function() {
        $scope.data = {};

        // An elaborate, custom popup
        //var newCenPopup = $ionicPopup.show({
        var newCenPopup = SecuredPopups.show('show',{
            template: '<input type="text" ng-model="data.centrale_name">',
            title: $translate('connection_list_new_title'),
            scope: $scope,
            buttons: [
            {   text: $translate('connection_list_new_cancel') },
            {
                text: '<b>' + $translate('connection_list_new_save') + '</b>',
                type: 'button-positive',
                onTap: function(e) {
                    if (!$scope.data.centrale_name) {
                        //don't allow the user to close unless he enters wifi password
                        e.preventDefault();
                    } else {
                        return $scope.data.centrale_name
                    }
                }
            }]
        });

        newCenPopup.then(function(res) {
            if(res != undefined){
                //Varifico se il nome della nuova centrale è già in uso per altre
                var query = "SELECT DISTINCT name FROM configuration WHERE name = '" + res + "'";

                database
                .execute(query)
                .then(function(result){
                    if(result.rows.length == 0) {
                        // Insert                
                        database
                        .execute('INSERT INTO configuration ( api, host, name, password, port, type ) VALUES  ( ?, ?, ?, ?, ?, ? )', [
                            '',
                            '',
                            res,
                            '',
                            '',
                            'lan']
                        ).then(function(result){
                            $rootScope.selectedDevice = res;
                            $state.go('app.configuration');
                        }, function(error){
                            //$ionicPopup.alert({
                            SecuredPopups.show('alert',{
                                title: 'Error',
                                template: $translate('connection_list_new_error'),
                            });
                        });
                    }
                    else
                    {
                        //$ionicPopup.alert({
                        SecuredPopups.show('alert',{
                            title: 'Error',
                            template: $translate('connection_list_new_error'),
                        });

                    }
                }, function(error){
                    // Error
                    // $cordovaDialogs.alert(
                    //     $translate('alert_configurationError'),
                    //     $translate('alert_configurationTitle'),
                    //     $translate('alert_button')
                    // );
                });
            }
        });
    };

    $scope.goToDeviceConfig = function(connectionName)
    {
        $rootScope.selectedDevice = connectionName;
        $state.go('app.configuration');
    };


    $scope.reorderItem=function(item,fromIndex,toIndex)
    {
        //Move the item in the array
        $scope.ConnectionList.splice(fromIndex, 1);
        $scope.ConnectionList.splice(toIndex, 0, item);
    }
    
    $scope.deleteEnabled=false;

    $scope.ToggleDelete = function()
    {
        $scope.deleteEnabled = !$scope.deleteEnabled;
    }

    $scope.DeleteDevice = function(connectionName)
    {
        // var confirmPopup = $ionicPopup.confirm({
        var confirmPopup = SecuredPopups.show('confirm',{
            title: $translate('connection_list_delete_title'),
            template: $translate('connection_list_delete_message'),
            cancelText: $translate('connection_list_delete_cancel'),
            okText: $translate('connection_list_delete_ok')
        });

        confirmPopup.then(function(res) {
            if(res) {
                var query = "SELECT * FROM configuration WHERE name = '" + connectionName + "'";

                database
                .execute(query)
                .then(function(result){
                    if(result.rows.length > 0)
                    {
                        controlSystem.clearPlantData(result.rows.item(0).api);
                    }
                    //Cancellazione delle configuratìzioni con il nome specificato
                    var query = "DELETE FROM configuration WHERE name = '" + connectionName + "'";

                    database
                    .execute(query)
                    .then(function(result){
                        //Cancellazione delle abilitazione dialog con il nome specificato
                        var query = "DELETE FROM configuration_alert WHERE name = '" + connectionName + "'";

                        database
                        .execute(query)
                        .then(function(result){
                            $scope.deleteEnabled = false;
                            $scope.ConnectionList = [];
                            $scope.load();
                        });

                    }, function(error){
                        // Error
                        // $cordovaDialogs.alert(
                        //     $translate('alert_configurationError'),
                        //     $translate('alert_configurationTitle'),
                        //     $translate('alert_button')
                        // );
                    });
                });
            } else {
            
            }
        });

    }

    $scope.$on('$ionicView.enter', function(event, data){
        $scope.load();
    });

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault(); 
        navigator.app.exitApp();
    },1000);    


}).controller('ServiceTimerRSController', function($scope, $rootScope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
		return comp.name === $stateParams.element && comp.service == $scope.service;
	})[0] || {};

    var today = new Date();
    $scope.roomComponent.selectedDay = today.getDay();


    $scope.roomComponent.timer.forEach(function(timer){
        timer.showTimer = function(weekDay)
        {
            if(weekDay >=0 && weekDay <7)
                if(this.disabledDays[weekDay] == 1)
                    return false;
            return true;
        }; 

        timer.formatTime = function()
        {
            var minutes = this.minutesFromMidnight % 60;
            var hours = (this.minutesFromMidnight - minutes) / 60;

            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
        }
    });


    $scope.roomComponent.isSelected = function(dayNumber)
    {
        if($scope.roomComponent.selectedDay == dayNumber)
            return "selected";
        return "";
    }

    $scope.roomComponent.setSelectedDay = function(dayNumber)
    {
        $scope.roomComponent.selectedDay = dayNumber;
    }

    $scope.GoBack = function()
    {
        if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
        {
            $state.go('app.service/room/detail/:room',{"room":$scope.room});
        }
        else
        {
            $state.go('app.service/detail/:service/:room',{"service":$scope.service,"room":$scope.room});
        }
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.GoBack();
    },1000);

}).controller('ServiceTimerDetailRSController', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');

    $scope.edited = $stateParams.edited;

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element && comp.service == $scope.service; 
    })[0] || {};

    var today = new Date();

    var tempUpIndex = -1;
    var tempDownIndex = -1;
    $scope.roomComponent.timer.forEach(function(timer,index){
        timer.formatTime = function()
        {
            var minutes = this.minutesFromMidnight % 60;
            var hours = (this.minutesFromMidnight - minutes) / 60;

            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
        }
        
        timer.arrayIndex = index;
        if(timer.direction == 'Up')
        {
            timer.index = ++tempUpIndex;
        }
        else if( timer.direction == 'Down' )
        {
            timer.index = ++tempDownIndex;
        }
    });

    $scope.roomComponent.timer.forEach(function(timer,index){
        
        timer.arrayIndex = index;
    });

    $scope.roomComponent.isEnabled = function(timer, dayNumber)
    {
        if(timer.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.SaveAndExit = function()//Salvataggio della configurazione appena terminata
    {
        var stringSetCfgShutU = "";
        var stringSetCfgShutD = "";

        var shutUpComponent = $scope.roomComponent.rollUpComp;
        var shutDownComponent = $scope.roomComponent.rollDownComp;

        if($scope.edited == '1')
        {
            if(shutUpComponent != undefined && shutUpComponent.number != undefined)
            {
                stringSetCfgShutU = 'cfgshutu;' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + $scope.roomComponent.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

                switch(shutUpComponent.assocOutputType)
                {
                    case 'off':
                    {
                        stringSetCfgShutU += '0,0:4096,0:'
                    }break;
                    case 'on':
                    {
                        stringSetCfgShutU += '0,0:8192,0:'
                    }break;
                    case 'toggle':
                    {
                        stringSetCfgShutU += '0,0:16384,0:'
                    }break;
                }

                switch(shutUpComponent.assocInputType)
                {
                    case 'and':
                    {
                        stringSetCfgShutU += '0,0:32769,0:'
                    }break;
                    case 'toggle':
                    {
                        stringSetCfgShutU += '0,0:32768,0:'
                    }break;
                }

                //Caricamento Timer
                $scope.roomComponent.timer.filter(function(item){
                    return item.direction=="Up";
                }).forEach(function(timer){
                    
                    var valDisabledDays = 0;
                    if( timer.disabledDays )
                    {
                        for(var i=0; i < timer.disabledDays.length; i++)
                        {
                            valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                        }
                    }

                    var valDisabledMonth = 0;
                    if( timer.disabledMonth )
                    {
                        for(var i=0; i < timer.disabledMonth.length; i++)
                        {
                            valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                        }
                    }

                    stringSetCfgShutU += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                    switch(timer.type.toLowerCase())
                    {
                        case 'off':
                        {
                            var index = 256 + parseInt(timer.index);
                            stringSetCfgShutU += index.toString() + ',';
                        }break;
                        case 'on':
                        {
                            var index = 512 + parseInt(timer.index);
                            stringSetCfgShutU += index.toString() + ',';
                        }break;
                    }

                    stringSetCfgShutU += timer.minutesFromMidnight.toString();

                    //_AC_20180305
                    //stringSetCfgShutU += timer.condition + ":";
                    stringSetCfgShutU += timer.condition;

                    if(timer.onlyOnce==1)
                    {
                        stringSetCfgShutU += 'u:';                        
                    }
                    else
                    {
                        stringSetCfgShutU += ':';
                    }
                });

                var inputList = shutUpComponent.inputList;
                Object.keys(inputList).forEach(function (key) {
                    if(inputList[key] !== 'function'){
                        if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutU += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                        }
                    }
                });
                var sensorList = shutUpComponent.sensorList;
                Object.keys(sensorList).forEach(function (key) {
                    if(sensorList[key] !== 'function'){
                        if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutU += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                        }
                    }
                });
                var dimmerList = shutUpComponent.dimmerList;
                Object.keys(dimmerList).forEach(function (key) {
                    if(dimmerList[key] !== 'function'){
                        if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutU += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                        }
                    }
                });
                var toggleList = shutUpComponent.toggleList;
                Object.keys(toggleList).forEach(function (key) {
                    if(toggleList[key] !== 'function'){
                        if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutU += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                        }
                    }
                });
            }
            if(shutDownComponent != undefined && shutDownComponent.number != undefined)
            {
                stringSetCfgShutD = 'cfgshutd;' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + $scope.roomComponent.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';
                
                switch(shutDownComponent.assocOutputType)
                {
                    case 'off':
                    {
                        stringSetCfgShutD += '0,0:4096,0:'
                    }break;
                    case 'on':
                    {
                        stringSetCfgShutD += '0,0:8192,0:'
                    }break;
                    case 'toggle':
                    {
                        stringSetCfgShutD += '0,0:16384,0:'
                    }break;
                }

                switch(shutDownComponent.assocInputType)
                {
                    case 'and':
                    {
                        stringSetCfgShutD += '0,0:32769,0:'
                    }break;
                    case 'toggle':
                    {
                        stringSetCfgShutD += '0,0:32768,0:'
                    }break;
                }

                //Caricamento Timer
                $scope.roomComponent.timer.filter(function(item){
                    return item.direction=="Down";
                }).forEach(function(timer){
                    
                    var valDisabledDays = 0;
                    if( timer.disabledDays )
                    {
                        for(var i=0; i < timer.disabledDays.length; i++)
                        {
                            valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                        }
                    }

                    var valDisabledMonth = 0;
                    if( timer.disabledMonth )
                    {
                        for(var i=0; i < timer.disabledMonth.length; i++)
                        {
                            valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                        }
                    }

                    stringSetCfgShutD += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                    switch(timer.type.toLowerCase())
                    {
                        case 'off':
                        {
                            var index = 256 + parseInt(timer.index);
                            stringSetCfgShutD += index.toString() + ',';
                        }break;
                        case 'on':
                        {
                            var index = 512 + parseInt(timer.index);
                            stringSetCfgShutD += index.toString() + ',';
                        }break;
                    }

                    stringSetCfgShutD += timer.minutesFromMidnight.toString();
                    
                    //_AC_20180305
                    //stringSetCfgShutD += timer.condition + ":";
                    stringSetCfgShutD += timer.condition;

                    if(timer.onlyOnce==1)
                    {
                        stringSetCfgShutD += 'u:';                        
                    }
                    else
                    {
                        stringSetCfgShutD += ':';
                    }
                });

                var inputList = shutDownComponent.inputList;
                Object.keys(inputList).forEach(function (key) {
                    if(inputList[key] !== 'function'){
                        if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutD += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                        }
                    }
                });
                var sensorList = shutDownComponent.sensorList;
                Object.keys(sensorList).forEach(function (key) {
                    if(sensorList[key] !== 'function'){
                        if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutD += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                        }
                    }
                });
                var dimmerList = shutDownComponent.dimmerList;
                Object.keys(dimmerList).forEach(function (key) {
                    if(dimmerList[key] !== 'function'){
                        if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutD += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                        }
                    }
                });
                var toggleList = shutDownComponent.toggleList;
                Object.keys(toggleList).forEach(function (key) {
                    if(toggleList[key] !== 'function'){
                        if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                            stringSetCfgShutD += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                        }
                    }
                });
            }

            $state.go('app.service/timer_spinner_rs/:newIndexU/:oldIndexU/:messageU/:newIndexD/:oldIndexD/:messageD/:nextPage/:nextPageData',
                {"newIndexU": controlSystem.getAssociationNumber(),
                "oldIndexU": $scope.roomComponent.rollUpComp.number,
                "messageU": stringSetCfgShutU,
                "newIndexD": controlSystem.getAssociationNumber(),
                "oldIndexD": $scope.roomComponent.rollDownComp.number,
                "messageD": stringSetCfgShutD,
                "nextPage": "app.service/timer_rs/:service/:room/:element",
                "nextPageData" : JSON.stringify({"service":$scope.service, "room":$scope.room, "element":$stateParams.element})
            });
        }
        else
        {
            $state.go("app.service/timer_rs/:service/:room/:element",{"service":$stateParams.service, "room":$stateParams.room, "element":$stateParams.element});
        }

    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.SaveAndExit();
    },1000)
}).controller('ServiceEditTimerRSController', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    var $translate = $filter('translate');
    $scope.padNumber = $filter('padNumber');

    $scope.edited = $stateParams.edited;
    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element  && comp.service == $scope.service;
    })[0] || {};

    var today = new Date();

    $scope.newTimer = false;
    var timer;

    if($stateParams.timer != undefined)
    {
        timer = JSON.parse($stateParams.timer)
    }
    else
    {
        $scope.newTimer = true;
        var hour = today.getHours();
        var minutes = today.getMinutes();
        var minutesFromMidnight = hour*60 + minutes;
        //20180219
        //timer = {"disabledDays":[0,0,0,0,0,0,0],"disabledMonth":[0,0,0,0,0,0,0,0,0,0,0,0],"index":-1,"type":"on","minutesFromMidnight":minutesFromMidnight,"condition":"e","direction":"Up"};
        timer = {"disabledDays":[0,0,0,0,0,0,0],"disabledMonth":[0,0,0,0,0,0,0,0,0,0,0,0],"index":-1,"type":"on","minutesFromMidnight":minutesFromMidnight,"condition":"","direction":"Up","onlyOnce":0};
    }

    $scope.minutes = timer.minutesFromMidnight%60;

    $scope.hours = (timer.minutesFromMidnight - $scope.minutes)/60;

    if($scope.roomComponent.rollingShutterUp && $scope.roomComponent.rollingShutterDown)
    {
        $scope.directionUp = (timer.direction.toLowerCase() == 'up')? 1 : 0;
        //AC_20180115
        $scope.showDirectionButtons = true;
    }
    else
    {
        //AC_20180115
        $scope.showDirectionButtons = false;        
        if($scope.roomComponent.rollingShutterDown)
        {
            $scope.directionUp = 0;
        }
        else if($scope.roomComponent.rollingShutterUp)
        {
            $scope.directionUp = 1;
        }
    }

    $scope.disabledDays = timer.disabledDays;

    $scope.isEnabled = function(dayNumber)
    {
        if($scope.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.setEnabled = function(dayNumber)
    {
        $scope.disabledDays[dayNumber] = ($scope.disabledDays[dayNumber]+1) % 2 ;
        //AC_20180219
        if($scope.onlyOnce && $scope.disabledDays[dayNumber] == 0)
        {
            for(var i=0; i<$scope.disabledDays.length; i++)
            {
                if(i != dayNumber)
                    $scope.disabledDays[i] = 1;
            }
        }
    }

    $scope.upHour = function()
    {
        $scope.hours = ($scope.hours+1) % 24 ;
    }

    $scope.downHour = function()
    {
        $scope.hours = ($scope.hours-1) >= 0 ? $scope.hours-1 : 23;
    }

    $scope.upMinutes = function()
    {
        $scope.minutes = ($scope.minutes+1) % 60 ;
    }

    $scope.downMinutes = function()
    {
        $scope.minutes = ($scope.minutes-1) >= 0 ? $scope.minutes-1 : 59;
    }

    $scope.isUp = function(value)
    {
        return $scope.directionUp == value ? 'selected':'';
    }

    $scope.changeDirection = function(value)
    {
        $scope.directionUp = value;
    }

    $scope.SaveChanges = function()
    {
        var timerList;
        var currentTimer;

        timerList = ($scope.roomComponent.timer) ? $scope.roomComponent.timer : undefined;

        if(timerList != undefined)
        {
            if($scope.newTimer)
            {
                currentTimer = timer;
                currentTimer.index = timerList.length;
                timerList.push(currentTimer);
            }
            else
            {
                currentTimer = timerList.filter(function(comp){
                        return comp.associationNumber === timer.associationNumber && comp.index === timer.index;
                    })[0] || {}
            }

            //AC_20180305
            if($scope.timerMode == 0){
                currentTimer.disabledDays = $scope.disabledDays;
                currentTimer.minutesFromMidnight = $scope.hours * 60 + $scope.minutes;
                currentTimer.onlyOnce = $scope.onlyOnce?1:0;
            }
            else{
                currentTimer.disabledDays = [1,1,1,1,1,1,1];
                
                var today = new Date();
                var day = today.getDay();
                var hour = today.getHours() + $scope.hoursDelay;
                var minutes = today.getMinutes() + $scope.minutesDelay;
                
                if(minutes >= 60)
                {
                    minutes = minutes % 60;
                    hour = hour + 1;
                }

                if(hour >= 24)
                {
                    day += day%7;
                    hour = hour%24;
                }

                currentTimer.disabledDays[day]=0;

                currentTimer.minutesFromMidnight = hour*60 + minutes;
                currentTimer.onlyOnce = 1;
            }
            currentTimer.type = 'On';
            currentTimer.direction = ($scope.directionUp == 1) ? 'Up':'Down';
        }


        $state.go('app.service/timer_detail_rs/:service/:room/:element/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": '1'
        });
    }

    $scope.DeleteTimer = function()
    {
        var timerList;
        var currentTimer;

        timerList = ($scope.roomComponent.timer) ? $scope.roomComponent.timer : undefined;

        if(timerList != undefined)
        {
            if(!$scope.newTimer)
            {
                var indexOf = -1
                for(var i=0; i<timerList.length;i++)
                {
                    if(timerList[i].arrayIndex == timer.arrayIndex)
                    {
                        indexOf = i;
                        break;
                    }
                }

                if(indexOf > -1)
                {
                    timerList.splice(indexOf,1);
                    timerList.forEach(function(Item,index){
                        Item.index = index;
                    });
                }
            }
        }


        $state.go('app.service/timer_detail_rs/:service/:room/:element/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": '1'
        });
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $state.go('app.service/timer_detail_rs/:service/:room/:element/:edited',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": $scope.edited
        });
    },1000)


    //AC_20180219
    $scope.onlyOnce = (timer.onlyOnce == 0)?false:true;
    $scope.changeOnlyOnce = function(){
        $scope.onlyOnce = !$scope.onlyOnce;
        if($scope.onlyOnce)
        {
            $scope.disabledDays=[1,1,1,1,1,1,1];
            var today = new Date();
            var day = today.getDay();
            $scope.disabledDays[day] = 0;
            //AC_20180306
            // var bFound = false;
            // for(var i=0; i < $scope.disabledDays.length; i++)
            // {
            //     if($scope.disabledDays[i] == 0 && !bFound)
            //     {
            //         bFound = true;
            //     }
            //     else
            //         $scope.disabledDays[i] = 1
            // }
        }
    }

    //AC_20180205
    $scope.timerMode = 0;
    $scope.changeTimerMode = function(newMode)
    {
        $scope.timerMode = newMode;
    
        // Manages views
        $('#tabCalendar').removeClass('selected');
        $('#tabDelay').removeClass('selected');

        $('#panelCalendar').addClass('hide');
        $('#panelDelay').addClass('hide');

        $('#tab' + ($scope.timerMode==0?'Calendar':'Delay')).addClass('selected');
        $('#panel' + ($scope.timerMode==0?'Calendar':'Delay')).removeClass('hide');
    }
    $scope.isActive = function(mode)
    {
        if($scope.timerMode = mode)
            return "active";
        return "";
        $scope.$digest();
    }
    $scope.minutesDelay = 0;

    $scope.hoursDelay = 2;

    $scope.upHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay+1) % 24 ;
    }

    $scope.downHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay-1) >= 0 ? $scope.hoursDelay-1 : 23;
    }

    $scope.upMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay+1) % 60 ;
    }

    $scope.downMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay-1) >= 0 ? $scope.minutesDelay-1 : 59;
    }

}).controller('TimerSpinnerRS', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    
    //AC_20180123
    $scope.stopUpdateState();
    //AC_20180320
    $scope.stopUpdateEnable();

    $scope.messageU = $stateParams.messageU;
    $scope.messageD = $stateParams.messageD;

    $scope.nextPage = $stateParams.nextPage;
    var $translate = $filter('translate');
    $scope.nextPageData = JSON.parse($stateParams.nextPageData);

    $scope.newIndexU = parseInt($stateParams.newIndexU);
    $scope.newIndexD = parseInt($stateParams.newIndexD);
    $scope.oldIndexU = parseInt($stateParams.oldIndexU);
    $scope.oldIndexD = parseInt($stateParams.oldIndexD);

    $scope.association = controlSystem.associations();

    if($scope.oldIndexU != undefined && $scope.oldIndexU >= 0 && $scope.newIndexU != undefined && $scope.newIndexU >= 0 && $scope.messageU != undefined && $scope.messageU != '')
    {
        if($scope.oldIndexD != undefined && $scope.oldIndexD >= 0 && $scope.newIndexD != undefined && $scope.newIndexD >= 0 && $scope.messageD != undefined && $scope.messageD != '')
        {
            controlSystem.setCfgState($scope.newIndexU,$scope.messageU).then(function(){
                controlSystem.deleteCfgState($scope.oldIndexU).then(function(){
                    controlSystem.setCfgState($scope.newIndexD,$scope.messageD).then(function(){   
                        controlSystem.deleteCfgState(($scope.oldIndexU < $scope.oldIndexD)?$scope.oldIndexD-1:$scope.oldIndexD).then(function(){
                            setTimeout(function(){
                                if($scope.association[$scope.oldIndexU]!= undefined && $scope.association[$scope.oldIndexD]!= undefined)
                                {
                                    var bEnableValue = false;
                                    if($scope.association[$scope.oldIndexU].enabled != undefined && $scope.association[$scope.oldIndexU].enabled == 1)
                                    {
                                        bEnableValue = true;
                                    }
                                    else
                                    {
                                        bEnableValue = false;
                                    }

                                    controlSystem.setCfgEnable($scope.newIndexU - 2,bEnableValue).then(function(){
                                        var bEnableValue = false;
                                        if($scope.association[$scope.oldIndexD].enabled != undefined && $scope.association[$scope.oldIndexD].enabled == 1)
                                        {
                                            bEnableValue = true;
                                        }
                                        else
                                        {
                                            bEnableValue = false;
                                        }
    
                                        controlSystem.setCfgEnable($scope.newIndexD - 1,bEnableValue).then(function(){
                                            controlSystem.init().then(function(){
                                                //AC_20180123
                                                $scope.startUpdateState();
                                                //AC_20180320
                                                if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L1'))
                                                {
                                                    $scope.startUpdateEnable();
                                                };
                                                $state.go($scope.nextPage,$scope.nextPageData);
                                                //AC_20180508
                                                if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L3'))
                                                {
                                                    controlSystem.setFCMToken();
                                                };
                                            });
                                        })
                                    });
                                }
                            },500);
                        })
                    })
                })
            }).catch(function(){
                $cordovaDialogs.alert(
                    $translate('alert_cfgStateDeleteError'),
                    $translate('alert_cfgStateTitle'),
                    $translate('alert_button'));

                //AC_20180515
                $rootScope.selectedNotification = undefined
                    
                $scope.configuration();
            });
        }
        else
        {
            controlSystem.setCfgState($scope.newIndexU,$scope.messageU).then(function(){
                controlSystem.deleteCfgState($scope.oldIndexU).then(function(){
                    setTimeout(function(){
                        if($scope.association[$scope.oldIndexU]!= undefined)
                        {
                            var bEnableValue = false;
                            if($scope.association[$scope.oldIndexU].enabled != undefined && $scope.association[$scope.oldIndexU].enabled == 1)
                            {
                                bEnableValue = true;
                            }
                            else
                            {
                                bEnableValue = false;
                            }

                            controlSystem.setCfgEnable($scope.newIndexU - 1,bEnableValue).then(function(){
                                controlSystem.init().then(function(){
                                    //AC_20180123
                                    $scope.startUpdateState();
                                    //AC_20180320
                                    if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L1'))
                                    {
                                        $scope.startUpdateEnable();
                                    };
                                    $state.go($scope.nextPage,$scope.nextPageData);
                                    //AC_20180508
                                    if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L3'))
                                    {
                                        controlSystem.setFCMToken();
                                    };
                                });
                            });
                        }
                    },500);
                })
            }).catch(function(){
                $cordovaDialogs.alert(
                    $translate('alert_cfgStateDeleteError'),
                    $translate('alert_cfgStateTitle'),
                    $translate('alert_button'));

                //AC_20180515
                $rootScope.selectedNotification = undefined
                    
                $scope.configuration();
            });
        }
    }
    else if($scope.oldIndexD != undefined && $scope.oldIndexD >= 0 && $scope.messageD != undefined && $scope.messageD != '')
    {
        //$scope.newIndexD--;
        controlSystem.setCfgState($scope.newIndexD,$scope.messageD).then(function(){
            controlSystem.deleteCfgState($scope.oldIndexD).then(function(){
                setTimeout(function(){
                    if($scope.association[$scope.oldIndexD]!= undefined)
                    {
                        var bEnableValue = false;
                        if($scope.association[$scope.oldIndexD].enabled != undefined && $scope.association[$scope.oldIndexD].enabled == 1)
                        {
                            bEnableValue = true;
                        }
                        else
                        {
                            bEnableValue = false;
                        }

                        controlSystem.setCfgEnable($scope.newIndexD - 1,bEnableValue).then(function(){
                            controlSystem.init().then(function(){
                                //AC_20180123
                                $scope.startUpdateState();
                                //AC_20180320
                                if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L1'))
                                {
                                    $scope.startUpdateEnable();
                                };
                                $state.go($scope.nextPage,$scope.nextPageData);
                                //AC_20180508
                                if(controlSystem.getEnableLevel() >= controlSystem.getSogliaLivello('L3'))
                                {
                                    controlSystem.setFCMToken();
                                };
                            });
                        });
                    }
                },500);
            })
        }).catch(function(){
            $cordovaDialogs.alert(
                $translate('alert_cfgStateDeleteError'),
                $translate('alert_cfgStateTitle'),
                $translate('alert_button'));

            //AC_20180515
            $rootScope.selectedNotification = undefined
                
            $scope.configuration();
        });
    }

}).controller('InfoController', function($scope, $cordovaDialogs, $cordovaNetwork, $ionicPlatform, $ionicHistory, $ionicConfig, $state, $rootScope, $filter, controlSystem, $interval){
    var $translate = $filter('translate');

    $scope.currentFW = controlSystem.CurrentFWVersion();
    $scope.requiredFW = controlSystem.getRequiredFWVersion();

    $scope.checkFWUpdate = function()
    {
        if($scope.currentFW < $scope.requiredFW)
            return "to-update";
        return "";
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.dashboard();
    },1000)

}).controller('ServiceEnableTimerController', function($scope, $rootScope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    $scope.type = $stateParams.type;

    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element  && comp.service == $scope.service;
    })[0] || {};

    $scope.enabled = ($scope.roomComponent) ? controlSystem.associations()[$scope.roomComponent.number].enabled : 0;

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    switch($scope.type)
    {
        case 'rsUp': 
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.rollUpComp.number) ? controlSystem.associations()[$scope.roomComponent.rollUpComp.number].enabled : 0;
        }break;
        case 'rsDown': 
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.rollDownComp.number) ? controlSystem.associations()[$scope.roomComponent.rollDownComp.number].enabled : 0;
        }break;

        case 'therm_auto':
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.thermAuto.number) ? controlSystem.associations()[$scope.roomComponent.thermAuto.number].enabled : 0;
        }break;
        case 'therm_int':
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.thermInte.number) ? controlSystem.associations()[$scope.roomComponent.thermInte.number].enabled : 0;
        }break;
        case 'therm_manual':
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.thermManu.number) ? controlSystem.associations()[$scope.roomComponent.thermManu.number].enabled : 0;
        }break;
        case 'cond_auto':
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.condAuto.number) ? controlSystem.associations()[$scope.roomComponent.condAuto.number].enabled : 0;
        }break;
        case 'cond_int':
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.condInte.number) ? controlSystem.associations()[$scope.roomComponent.condInte.number].enabled : 0;
        }break;
        case 'cond_manual':
        {
            $scope.enabled = ($scope.roomComponent && $scope.roomComponent.condManu.number) ? controlSystem.associations()[$scope.roomComponent.condManu.number].enabled : 0;
        }break;
    }

    var $translate = $filter('translate');

    

    var today = new Date();
    $scope.roomComponent.selectedDay = today.getDay();

    $scope.roomComponent.timer.forEach(function(timer){
        timer.showTimer = function(weekDay)
        {
            if(weekDay >=0 && weekDay <7)
                if(this.disabledDays[weekDay] == 1)
                    return false;
            return true;
        }; 

        timer.formatTime = function()
        {
            var minutes = this.minutesFromMidnight % 60;
            var hours = (this.minutesFromMidnight - minutes) / 60;

            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
        }
    });

    $scope.roomComponent.isSelected = function(dayNumber)
    {
        if($scope.roomComponent.selectedDay == dayNumber)
            return "selected";
        return "";
    }

    $scope.roomComponent.setSelectedDay = function(dayNumber)
    {
        $scope.roomComponent.selectedDay = dayNumber;
    }

    $scope.GoBack = function()
    {
        if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
        {
            $state.go('app.service/room/detail/:room',{"room":$scope.room});
        }
        else
        {
            $state.go('app.service/detail/:service/:room',{"service":$scope.service,"room":$scope.room});
        }
    }

    $scope.GoToDetail = function()
    {
        $state.go('app.service/timer_detail_en/:service/:room/:element/:edited/:type',
            {
                'service':$scope.service,
                'room':$scope.room,
                'element': $stateParams.element,
                'edited':'0',
                'type' : $scope.type
            }
        );
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.GoBack();
    },1000);


    $scope.getElencoTimer = function()
    {
        var comp = $scope.roomComponent;

        if($scope.type != undefined)
        {
            switch($scope.type)
            {
                case 'rsUp':
                {
                    comp = $scope.roomComponent.rollUpComp;
                }break;
                case 'rsDown':
                {
                    comp = $scope.roomComponent.rollDownComp;
                }break;

                case 'therm_auto':
                {
                    comp = $scope.roomComponent.thermAuto;
                }break;
                case 'therm_int':
                {
                    comp = $scope.roomComponent.thermInte;
                }break;
                case 'therm_manual':
                {
                    comp = $scope.roomComponent.thermManu;
                }break;
                case 'cond_auto':
                {
                    comp = $scope.roomComponent.condAuto;
                }break;
                case 'cond_int':
                {
                    comp = $scope.roomComponent.condInte;
                }break;
                case 'cond_manual':
                {
                    comp = $scope.roomComponent.condManu;
                }break;
            }            
        }

        var timer = comp.timer.filter(function(item){return item.condition == 'e' || item.condition == 'd'})
        return timer;
    }

    $scope.getTypeIcon = function(timer)
    {
        if(timer.condition == 'e')
        {
            return 'ion-ios-circle-filled balanced';
        }
        else if(timer.condition == 'd')
        {
            return 'ion-ios-circle-filled assertive';            
        }

        return '';
    }

    $scope.toggleEnable = function(currentComp,value)
    {
        var comp = currentComp;
        if($scope.type != undefined)
        {
            switch($scope.type)
            {
                case 'rsUp':
                {
                    comp = currentComp.rollUpComp;
                }break;
                case 'rsDown':
                {
                    comp = currentComp.rollDownComp;
                }break;

                case 'therm_auto':
                {
                    comp = currentComp.thermAuto;
                }break;
                case 'therm_int':
                {
                    comp = currentComp.thermInte;
                }break;
                case 'therm_manual':
                {
                    comp = currentComp.thermManu;
                }break;
                case 'cond_auto':
                {
                    comp = currentComp.condAuto;
                }break;
                case 'cond_int':
                {
                    comp = currentComp.condInte;
                }break;
                case 'cond_manual':
                {
                    comp = currentComp.condManu;
                }break;

            }            
        }

        if(value)
        {
            controlSystem.enableCfgState(comp.number);                     
        }
        else
        {
            controlSystem.disableCfgState(comp.number);
        }
    };

    $scope.HideEnableButton = function()
    {
        if($scope.type != undefined)
        {
            switch($scope.type)
            {
                case 'therm_auto':
                case 'therm_int':
                case 'therm_manual':
                case 'cond_auto':
                case 'cond_int':
                case 'cond_manual':
                case 'prior':
                {
                    return true;
                }break;
                default:
                {
                    return false;
                }break;
            }
        }
        return false;
    };

    var enUpdatedListener = $rootScope.$on('enUpdated', function (event, data){
        $scope.enabled = ($scope.roomComponent) ? controlSystem.associations()[$scope.roomComponent.number].enabled : 0;

        switch($scope.type)
        {
            case 'rsUp': 
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.rollUpComp.number) ? controlSystem.associations()[$scope.roomComponent.rollUpComp.number].enabled : 0;
            }break;
            case 'rsDown': 
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.rollDownComp.number) ? controlSystem.associations()[$scope.roomComponent.rollDownComp.number].enabled : 0;
            }break;

            case 'therm_auto':
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.thermAuto.number) ? controlSystem.associations()[$scope.roomComponent.thermAuto.number].enabled : 0;
            }break;
            case 'therm_int':
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.thermInte.number) ? controlSystem.associations()[$scope.roomComponent.thermInte.number].enabled : 0;
            }break;
            case 'therm_manual':
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.thermManu.number) ? controlSystem.associations()[$scope.roomComponent.thermManu.number].enabled : 0;
            }break;
            case 'cond_auto':
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.condAuto.number) ? controlSystem.associations()[$scope.roomComponent.condAuto.number].enabled : 0;
            }break;
            case 'cond_int':
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.condInte.number) ? controlSystem.associations()[$scope.roomComponent.condInte.number].enabled : 0;
            }break;
            case 'cond_manual':
            {
                $scope.enabled = ($scope.roomComponent && $scope.roomComponent.condManu.number) ? controlSystem.associations()[$scope.roomComponent.condManu.number].enabled : 0;
            }break;
        }
    });
    $scope.$on('$destroy', enUpdatedListener);

    $scope.isRS = function()
    {
        switch($scope.type)
        {
            case 'rsUp':
            case 'rsDown':
                return true;
                break;
            default:
                return false;
                break;
        }
    }

    $scope.getRSType = function()
    {
        switch($scope.type)
        {
            case 'rsUp':
                {
                    if($scope.roomComponent.rollingShutterUp && $scope.roomComponent.rollingShutterDown == null)
                        return 2;
                    else
                        return 0;
                }break;
            case 'rsDown':
            {
                if($scope.roomComponent.rollingShutterUp == null && $scope.roomComponent.rollingShutterDown)
                    return 2;
                else
                    return 1;
            }break;
        }
    }

}).controller('ServiceEnableTimerDetailController', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    $scope.type = $stateParams.type;

    var $translate = $filter('translate');

    $scope.edited = $stateParams.edited;

    $scope.useModeCode = '';
    
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element  && comp.service == $scope.service;
    })[0] || {};

    $scope.activeComponent;

    switch($scope.type)
    {
        case 'rsUp':
        {
            $scope.useModeCode = "cfgshutu";
            $scope.activeComponent = $scope.roomComponent.rollUpComp;
        }break;

        case 'rsDown':
        {
            $scope.useModeCode = "cfgshutd";
            $scope.activeComponent = $scope.roomComponent.rollDownComp;
        }break;

        case 'therm_auto':
        {
            $scope.useModeCode = "cfgtauto";
            $scope.activeComponent = $scope.roomComponent.thermAuto;
        }break;

        case 'therm_manual':
        {
            $scope.useModeCode = "cfgtmanu";
            $scope.activeComponent = $scope.roomComponent.thermManu;
        }break;

        case 'therm_int':
        {
            $scope.useModeCode = "cfgtinte";
            $scope.activeComponent = $scope.roomComponent.thermInte;
        }break;

        case 'cond_auto':
        {
            $scope.useModeCode = "cfgtauto";
            $scope.activeComponent = $scope.roomComponent.condAuto;
        }break;

        case 'cond_manual':
        {
            $scope.useModeCode = "cfgtmanu";
            $scope.activeComponent = $scope.roomComponent.condManu;
        }break;

        case 'cond_int':
        {
            $scope.useModeCode = "cfgtinte";
            $scope.activeComponent = $scope.roomComponent.condInte;
        }break;

        case 'light':
        {
            $scope.useModeCode = "cfgstate";
            $scope.activeComponent = $scope.roomComponent;
        }break;
        case 'prior':
        {
            $scope.useModeCode = "cfgprior";
            $scope.activeComponent = $scope.roomComponent;
        }break;
    }

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();

    var today = new Date();

    $scope.activeComponent.timer.forEach(function(timer){
        timer.formatTime = function()
        {
            var minutes = this.minutesFromMidnight % 60;
            var hours = (this.minutesFromMidnight - minutes) / 60;

            return ( hours<10 ? "0" + hours : hours) + " : " + ( minutes < 10 ? "0" + minutes : minutes )
        }
    });

    $scope.getElencoTimer = function()
    {
        var comp = $scope.roomComponent;

        if($scope.type != undefined)
        {
            switch($scope.type)
            {
                case 'rsUp':
                {
                    comp = $scope.roomComponent.rollUpComp;
                }break;
                case 'rsDown':
                {
                    comp = $scope.roomComponent.rollDownComp;
                }break;

                case 'therm_auto':
                {
                    comp = $scope.roomComponent.thermAuto;
                }break;
                case 'therm_int':
                {
                    comp = $scope.roomComponent.thermInte;
                }break;
                case 'therm_manual':
                {
                    comp = $scope.roomComponent.thermManu;
                }break;
                case 'cond_auto':
                {
                    comp = $scope.roomComponent.condAuto;
                }break;
                case 'cond_int':
                {
                    comp = $scope.roomComponent.condInte;
                }break;
                case 'cond_manual':
                {
                    comp = $scope.roomComponent.condManu;
                }break;
            }            
        }

        var timer = comp.timer.filter(function(item){return item.condition == 'e' || item.condition == 'd'})
        return timer;
    }

    $scope.roomComponent.isEnabled = function(timer, dayNumber)
    {
        if(timer.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.SaveAndExit = function()//Salvataggio della configurazione appena terminata
    {
        var stringSetCfgState = "";

        if($scope.edited == '1' && $scope.useModeCode != "" && $scope.activeComponent)
        {
            stringSetCfgState = $scope.useModeCode + ';' + $scope.room.replace(/_252F/g,"/").replace(/_2523/g,"#") + ',' + $scope.roomComponent.name.replace(/_252F/g,"/").replace(/_2523/g,"#") + ':';

            switch($scope.activeComponent.assocOutputType)
            {
                case 'off':
                {
                    stringSetCfgState += '0,0:4096,0:'
                }break;
                case 'on':
                {
                    stringSetCfgState += '0,0:8192,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:16384,0:'
                }break;
            }

            switch($scope.activeComponent.assocInputType)
            {
                case 'and':
                {
                    stringSetCfgState += '0,0:32769,0:'
                }break;
                case 'toggle':
                {
                    stringSetCfgState += '0,0:32768,0:'
                }break;
            }

            //Caricamento Timer
            $scope.activeComponent.timer.forEach(function(timer){
                
                var valDisabledDays = 0;
                if( timer.disabledDays )
                {
                    for(var i=0; i < timer.disabledDays.length; i++)
                    {
                        valDisabledDays += timer.disabledDays[i] * Math.pow(2,i);
                    }
                }

                var valDisabledMonth = 0;
                if( timer.disabledMonth )
                {
                    for(var i=0; i < timer.disabledMonth.length; i++)
                    {
                        valDisabledMonth += timer.disabledMonth[i] * Math.pow(2,i);
                    }
                }

                stringSetCfgState += valDisabledDays.toString() + ',' + valDisabledMonth.toString() + ':';


                switch(timer.type.toLowerCase())
                {
                    case 'off':
                    {
                        var index = 256 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                    case 'on':
                    {
                        var index = 512 + parseInt(timer.index);
                        stringSetCfgState += index.toString() + ',';
                    }break;
                }

                stringSetCfgState += timer.minutesFromMidnight.toString();


                //_AC_20180305
                // stringSetCfgState += timer.condition + ":";
                stringSetCfgState += timer.condition;

                if(timer.onlyOnce==1)
                {
                    stringSetCfgState += 'u:';                        
                }
                else
                {
                    stringSetCfgState += ':';
                }
            });

            var inputList = $scope.activeComponent.inputList;
            Object.keys(inputList).forEach(function (key) {
                if(inputList[key] !== 'function'){
                    if(inputList[key].value != undefined && inputList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + inputList[key].value + ':';
                    }
                }
            });
            var sensorList = $scope.activeComponent.sensorList;
            Object.keys(sensorList).forEach(function (key) {
                if(sensorList[key] !== 'function'){
                    if(sensorList[key].value != undefined && sensorList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + sensorList[key].value + ':';
                    }
                }
            });
            var dimmerList = $scope.activeComponent.dimmerList;
            Object.keys(dimmerList).forEach(function (key) {
                if(dimmerList[key] !== 'function'){
                    if(dimmerList[key].value != undefined && dimmerList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + dimmerList[key].value + ':';
                    }
                }
            });
            var toggleList = $scope.activeComponent.toggleList;
            Object.keys(toggleList).forEach(function (key) {
                if(toggleList[key] !== 'function'){
                    if(toggleList[key].value != undefined && toggleList[key].element){//condizione del sensore [cond (<,>,etc...)][soglia]
                        stringSetCfgState += controlSystem.id2numId(key) + "," + toggleList[key].value + ':';
                    }
                }
            });
            
            $state.go('app.service/timer_spinner/:newIndex/:oldIndex/:message/:nextPage/:nextPageData',
                {"newIndex": controlSystem.getAssociationNumber(),
                 "oldIndex": $scope.activeComponent.number,
                 "message": stringSetCfgState,
                 "nextPage": "app.service/timer_enable/:service/:room/:element/:type",
                 "nextPageData" : JSON.stringify({"service":$stateParams.service, "room":$stateParams.room, "element":$stateParams.element,"type":$scope.type})
                });
        }
        else
        {
            $state.go("app.service/timer_enable/:service/:room/:element/:type",{"service":$stateParams.service, "room":$stateParams.room, "element":$stateParams.element,"type":$scope.type});
        }

    }

    $scope.getTypeIcon = function(timer)
    {
        if(timer.condition == 'e')
        {
            return 'ion-ios-circle-filled balanced';
        }
        else if(timer.condition == 'd')
        {
            return 'ion-ios-circle-filled assertive';            
        }

        return '';
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.SaveAndExit();
    },1000)

}).controller('ServiceEditEnableTimerController', function($scope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){

    $scope.room = $stateParams.room;
    $scope.service = $stateParams.service;
    $scope.type = $stateParams.type;
    var $translate = $filter('translate');
    $scope.padNumber = $filter('padNumber');

    $scope.edited = $stateParams.edited;
    // Defines titles
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    $scope.element = controlSystem.element();
    $scope.roomComponent = controlSystem.roomComponents($stateParams.room).filter(function(comp){
        return comp.name === $stateParams.element  && comp.service == $scope.service;
    })[0] || {};

    var today = new Date();

    $scope.newTimer = false;
    var timer;

    if($stateParams.timer != undefined)
    {
        timer = JSON.parse($stateParams.timer)
    }
    else
    {
        $scope.newTimer = true;
        var hour = today.getHours();
        var minutes = today.getMinutes();
        var minutesFromMidnight = hour*60 + minutes;
        timer = {"disabledDays":[0,0,0,0,0,0,0],"disabledMonth":[0,0,0,0,0,0,0,0,0,0,0,0],"index":-1,"type":"on","minutesFromMidnight":minutesFromMidnight,"condition":"e",'onlyOnce':0};
    }

    $scope.minutes = timer.minutesFromMidnight%60;

    $scope.hours = (timer.minutesFromMidnight - $scope.minutes)/60;

    $scope.typeEn = (timer.condition.toLowerCase() == 'e')? 1 : 0;

    $scope.typeOn = (timer.type.toLowerCase() == 'on')? 1 : 0;

    $scope.disabledDays = timer.disabledDays;

    $scope.isEnabled = function(dayNumber)
    {
        if($scope.disabledDays[dayNumber] == 0)
            return "selected";
        return "";
    }

    $scope.setEnabled = function(dayNumber)
    {
        $scope.disabledDays[dayNumber] = ($scope.disabledDays[dayNumber]+1) % 2 ;
        if($scope.onlyOnce && $scope.disabledDays[dayNumber] == 0)
        {
            for(var i=0; i<$scope.disabledDays.length; i++)
            {
                if(i != dayNumber)
                    $scope.disabledDays[i] = 1;
            }
        }
    }

    $scope.upHour = function()
    {
        $scope.hours = ($scope.hours+1) % 24 ;
    }

    $scope.downHour = function()
    {
        $scope.hours = ($scope.hours-1) >= 0 ? $scope.hours-1 : 23;
    }

    $scope.upMinutes = function()
    {
        $scope.minutes = ($scope.minutes+1) % 60 ;
    }

    $scope.downMinutes = function()
    {
        $scope.minutes = ($scope.minutes-1) >= 0 ? $scope.minutes-1 : 59;
    }

    $scope.isOn = function(value)
    {
        return $scope.typeOn == value ? 'selected':'';
    }

    $scope.changeType = function(value)
    {
        $scope.typeOn = value;
    }

    $scope.changeTypeEn = function(value)
    {
        $scope.typeEn = value;
    }

    $scope.SaveChanges = function()
    {
        var timerList;
        var currentTimer;

        switch($scope.type)
        {
            case 'rsUp':
            {
                timerList = ($scope.roomComponent.rollUpComp && $scope.roomComponent.rollUpComp.timer) ? $scope.roomComponent.rollUpComp.timer : undefined;
            }break;

            case 'rsDown':
            {
                timerList = ($scope.roomComponent.rollDownComp && $scope.roomComponent.rollDownComp.timer) ? $scope.roomComponent.rollDownComp.timer : undefined;
            }break;

            case 'therm_auto':
            {
                timerList = ($scope.roomComponent.thermAuto && $scope.roomComponent.thermAuto.timer) ? $scope.roomComponent.thermAuto.timer : undefined;
            }break;

            case 'therm_int':
            {
                timerList = ($scope.roomComponent.thermInte && $scope.roomComponent.thermInte.timer) ? $scope.roomComponent.thermInte.timer : undefined;
            }break;

            case 'therm_manual':
            {
                timerList = ($scope.roomComponent.thermManu && $scope.roomComponent.thermManu.timer) ? $scope.roomComponent.thermManu.timer : undefined;
            }break;

            case 'cond_auto':
            {
                timerList = ($scope.roomComponent.condAuto && $scope.roomComponent.condAuto.timer) ? $scope.roomComponent.condAuto.timer : undefined;                
            }break;

            case 'cond_int':
            {
                timerList = ($scope.roomComponent.condInte && $scope.roomComponent.condInte.timer) ? $scope.roomComponent.condInte.timer : undefined;                
            }break;

            case 'cond_manual':
            {
                timerList = ($scope.roomComponent.condManu && $scope.roomComponent.condManu.timer) ? $scope.roomComponent.condManu.timer : undefined;                
            }break;

            case 'light':
            case 'prior':
            {
                timerList = ($scope.roomComponent.timer) ? $scope.roomComponent.timer : undefined;
            }break;
        }

        if(timerList != undefined)
        {
            if($scope.newTimer)
            {
                currentTimer = timer;
                currentTimer.index = timerList.length;
                timerList.push(currentTimer);
            }
            else
            {
                currentTimer = timerList.filter(function(comp){
                        return comp.index === timer.index;
                    })[0] || {}
            }

            if($scope.timerMode == 0){
                currentTimer.disabledDays = $scope.disabledDays;
                currentTimer.minutesFromMidnight = $scope.hours * 60 + $scope.minutes;
                currentTimer.onlyOnce = $scope.onlyOnce?1:0;
            }
            else{
                currentTimer.disabledDays = [1,1,1,1,1,1,1];
                
                var today = new Date();
                var day = today.getDay();
                var hour = today.getHours() + $scope.hoursDelay;
                var minutes = today.getMinutes() + $scope.minutesDelay;
                
                if(minutes >= 60)
                {
                    minutes = minutes % 60;
                    hour = hour + 1;
                }

                if(hour >= 24)
                {
                    day += day%7;
                    hour = hour%24;
                }

                currentTimer.disabledDays[day]=0;

                currentTimer.minutesFromMidnight = hour*60 + minutes;
                currentTimer.onlyOnce = 1;
            }

            currentTimer.type = ($scope.typeOn == 1) ? 'On':'Off';

            currentTimer.condition = ($scope.typeEn == 1) ? 'e':'d';
        }


        $state.go('app.service/timer_detail_en/:service/:room/:element/:edited/:type',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": '1',
            "type": $stateParams.type
        });
    }

    $scope.DeleteTimer = function()
    {
        var timerList;
        var currentTimer;

        switch($scope.type)
        {
            case 'rsUp':
            {
                timerList = ($scope.roomComponent.rollUpComp && $scope.roomComponent.rollUpComp.timer) ? $scope.roomComponent.rollUpComp.timer : undefined;
            }break;

            case 'rsDown':
            {
                timerList = ($scope.roomComponent.rollDownComp && $scope.roomComponent.rollDownComp.timer) ? $scope.roomComponent.rollDownComp.timer : undefined;
            }break;

            case 'therm_auto':
            {
                timerList = ($scope.roomComponent.thermAuto && $scope.roomComponent.thermAuto.timer) ? $scope.roomComponent.thermAuto.timer : undefined;
            }break;

            case 'therm_int':
            {
                timerList = ($scope.roomComponent.thermInte && $scope.roomComponent.thermInte.timer) ? $scope.roomComponent.thermInte.timer : undefined;
            }break;

            case 'therm_manual':
            {
                timerList = ($scope.roomComponent.thermManu && $scope.roomComponent.thermManu.timer) ? $scope.roomComponent.thermManu.timer : undefined;
            }break;

            case 'cond_auto':
            {
                timerList = ($scope.roomComponent.condAuto && $scope.roomComponent.condAuto.timer) ? $scope.roomComponent.condAuto.timer : undefined;                
            }break;

            case 'cond_int':
            {
                timerList = ($scope.roomComponent.condInte && $scope.roomComponent.condInte.timer) ? $scope.roomComponent.condInte.timer : undefined;                
            }break;

            case 'cond_manual':
            {
                timerList = ($scope.roomComponent.condManu && $scope.roomComponent.condManu.timer) ? $scope.roomComponent.condManu.timer : undefined;                
            }break;

            case 'light':
            case 'prior':
            {
                timerList = ($scope.roomComponent.timer) ? $scope.roomComponent.timer : undefined;
            }break;
        }
        
        if(timerList != undefined)
        {
            if(!$scope.newTimer)
            {
                var indexOf = -1
                for(var i=0; i<timerList.length;i++)
                {
                    if(timerList[i].index == timer.index)
                    {
                        indexOf = i;
                        break;
                    }
                }

                if(indexOf > -1)
                {
                    timerList.splice(indexOf,1);
                    timerList.forEach(function(Item,index){
                        Item.index = index;
                    });
                }
            }
        }
        
        $state.go('app.service/timer_detail_en/:service/:room/:element/:edited/:type',
        {
            "service":$stateParams.service,
            "room": $stateParams.room,
            "element": $stateParams.element,
            "edited": '1',
            "type": $stateParams.type
        });
    }
    
    $scope.onlyOnce = (timer.onlyOnce == 0)?false:true;
    $scope.changeOnlyOnce = function(){
        $scope.onlyOnce = !$scope.onlyOnce;
        if($scope.onlyOnce)
        {
            $scope.disabledDays=[1,1,1,1,1,1,1];
            var today = new Date();
            var day = today.getDay();
            $scope.disabledDays[day] = 0;
        }
    }

    $scope.timerMode = 0;
    $scope.changeTimerMode = function(newMode)
    {
        $scope.timerMode = newMode;
    
        // Manages views
        $('#tabCalendar').removeClass('selected');
        $('#tabDelay').removeClass('selected');

        $('#panelCalendar').addClass('hide');
        $('#panelDelay').addClass('hide');

        $('#tab' + ($scope.timerMode==0?'Calendar':'Delay')).addClass('selected');
        $('#panel' + ($scope.timerMode==0?'Calendar':'Delay')).removeClass('hide');
    }
    $scope.isActive = function(mode)
    {
        if($scope.timerMode = mode)
            return "active";
        return "";
        $scope.$digest();
    }
    $scope.minutesDelay = 0;

    $scope.hoursDelay = 2;

    $scope.upHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay+1) % 24 ;
    }

    $scope.downHourDelay = function()
    {
        $scope.hoursDelay = ($scope.hoursDelay-1) >= 0 ? $scope.hoursDelay-1 : 23;
    }

    $scope.upMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay+1) % 60 ;
    }

    $scope.downMinutesDelay = function()
    {
        $scope.minutesDelay = ($scope.minutesDelay-1) >= 0 ? $scope.minutesDelay-1 : 59;
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $state.go('app.service/timer_detail_en/:service/:room/:element/:edited/:type',{
            'service':$scope.service,
            'room':$scope.room,
            'element':$scope.roomComponent.name,
            'edited':$scope.edited,
            'type':$scope.type});
    },200);

}).controller('RsAssociationList', function($scope, $rootScope, $state, $cordovaDialogs, $filter, $stateParams, controlSystem, $ionicHistory,$ionicPlatform){
    $scope.room = $stateParams.room;
    $scope.element = $stateParams.element;
    $scope.service = $stateParams.service;
    $scope.roomComponent = JSON.parse($stateParams.component);
    $scope.associations = controlSystem.associations();

    var $translate = $filter('translate');

    // Defines titles
    // $scope.navigationHeader = $filter('capitalize')($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));
    $scope.navigationHeader = ($stateParams.element.toString().replace(/_252F/g, "/").replace(/_2523/g,"#"));

    var today = new Date();

    $scope.roomComponent.isSelected = function(dayNumber)
    {
        if($scope.roomComponent.selectedDay == dayNumber)
            return "selected";
        return "";
    }

    $scope.roomComponent.setSelectedDay = function(dayNumber)
    {
        $scope.roomComponent.selectedDay = dayNumber;
    }

    $scope.GoBack = function()
    {
        if($rootScope.navigateByRoom || $rootScope.navigateByCustomService)
        {
            $state.go('app.service/room/detail/:room',{"room":$scope.room});
        }
        else
        {
            $state.go('app.service/detail/:service/:room',{"service":$scope.service,"room":$scope.room});
        }
    }

    $ionicPlatform.registerBackButtonAction(function(event){
        event.preventDefault();
        $scope.GoBack();
    },200);

    $scope.editEnableTimer = function(type)
    {
        var comp = (type == 'rsUp') ? $scope.roomComponent.rollUpComp : $scope.roomComponent.rollDownComp;
        $state.go('app.service/timer_enable/:service/:room/:element/:type',
        {"service": 'rollingShutter',
            "room": $scope.room,
            "element": $scope.element,
            "type":type});
    };

}).controller('UtilityController', function($scope, $cordovaDialogs, $cordovaNetwork, $ionicPlatform, $ionicHistory, $ionicConfig, $state, $rootScope, $filter, controlSystem, $interval, database, fcm, $ionicPopup, SecuredPopups){
    // Network status

    $ionicConfig.views.swipeBackEnabled(false);
    

    $rootScope.isOnline = true;
    $scope.softDisconnect = false;
    $rootScope.navigateByRoom = false;//Flag per navigazione per Stanze
    $rootScope.firstOpen = true;//Flag prima apertura
    $rootScope.navigateByCustomService = false; //Flag di navigazione tramite servizio custom
    $rootScope.getStateInterval = null;
    $rootScope.getEnableInterval = null;
    $rootScope.hideDialog = false;
    $rootScope.connectionErrorDialogOpen = false;

    $scope.appVersion = controlSystem.appVersion;

    var $translate = $filter('translate');

    // listen for Online event
    var gotOnlineListener = $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
        //console.log("got online");
        $rootScope.isOnline = true;
    });
    $scope.$on('$destroy', gotOnlineListener);
    // listen for Offline event
    var gotOfflineListener = $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
        console.log("got offline");
        $rootScope.isOnline = false;

        //AC_20180515
        $rootScope.selectedNotification = undefined
        
        $scope.configuration();
    });
    $scope.$on('$destroy', gotOfflineListener);

    var socketDisconnectedListener = $rootScope.$on('socketDisconnected', function (event, data){
        if($scope.softDisconnect)
        {
            $scope.softDisconnect = false;
        }
        else
        {
    		$scope.configuration();
        }
	});
  	$scope.$on('$destroy', socketDisconnectedListener);

    $scope.previousPage;
    $scope.$on('$ionicView.beforeLeave', function(event, data){      
        $scope.previousPage = data.stateId;
    });

    // Defines scopes
    $scope.back = function(){
        $ionicHistory.goBack();
    };
    $scope.dashboard = function(){
        $state.go('app.dashboard');
    };
    $scope.configuration = function(){
        if(controlSystem.isOnline()){
            controlSystem.disconnect();
        }
        $state.go('app.configuration');
    };

    $scope.notification = function(){
        $state.go('app.notfication');
    };

    //AC_20180514
    $scope.connectionList = function(){
        if(controlSystem.isOnline()){
            controlSystem.disconnect();
        }
        $state.go('app.connection_list');
    };

    var _Initialization = function()
    {
        // Loads configuration_alert flag
        var query = 'SELECT * FROM configuration_alert WHERE name = ?';
            
        database
        .execute(query, [$rootScope.selectedDevice])
        .then(function(result){
            if(result.rows.length > 0) {
                // Gets data
                $rootScope.hideDialog = (result.rows.item(0).show_fw_alert == 1)?false:true;
            }
            else
            {
                $rootScope.hideDialog = false;
            }
            $state.go('app.initialization');
        });
    };
    //_AC_20170730
    $scope.initialization = function(){
        //AC_20180529 - Leggo da DB, se presente, il lastUpdate in modo da sapere se l'impianto salvato è aggiornato o da aggiornare
        var query = 'SELECT * FROM configuration WHERE type = "cloud" AND name = ?';
        
        database.
        execute(query, [$rootScope.selectedDevice]).
        then(function(result){
            if(result.rows.length > 0){
                controlSystem.setCurrentCentralID(result.rows.item(0).api);
                database.execute("SELECT * FROM plant_info WHERE plant_id = ?",[result.rows.item(0).api]).
                then(function(result){
                    if(result.rows.length > 0){
                        controlSystem.setDBLastUpdate(result.rows.item(0).last_update);
                    }
                    else
                    {
                        controlSystem.setDBLastUpdate("");
                    }
                    _Initialization();   
                },
                function(error){
                    controlSystem.setDBLastUpdate("");
                    _Initialization();
                })
            }
            else{
                controlSystem.setCurrentCentralID("");
                controlSystem.setDBLastUpdate("");
            }
            _Initialization();
        },
        function(error){
            controlSystem.setCurrentCentralID("");
            controlSystem.setDBLastUpdate("");
            _Initialization();
        })
    };

    //AC_20180605
    $scope.refresh = function(){
        controlSystem.clearPlantData().then(function(result){
            $scope.initialization();
        });

    }

    //_AC_20180129
    $scope.info = function(){
        
        $state.go('app.info');
        
    };

    //_AC_20180109
    $scope.stopAsync = function(){
        
        controlSystem.stopAsync();
    };

    $scope.connection_list = function(){
        $state.go('app.connection_list');
    };

    // $ionicPlatform.registerBackButtonAction(function(event){
    //     switch($ionicHistory.currentStateName())
    //     {
    //         case 'app.configuration':
    //         case 'app.dashboard':
    //         case 'app.initialization':
    //         case 'app.connection_list':
    //         {
    //             navigator.app.exitApp()
    //         }break;

    //         default:
    //         {
    //             event.preventDefault();
    //         }break;
    //     }
    // }, 100);

    $scope.serviceOpen = function(name){
        if(controlSystem.serviceExist(name)){
            if(name == 'thermostatus' && controlSystem.getEnableLevel() < controlSystem.getSogliaLivello("L1"))
            {
                $cordovaDialogs.alert(
                    $translate('alert_serviceError'),
                    $translate('alert_serviceTitle'), 
                    $translate('alert_button')
                );
            }
            else
            {
                $state.go('app.service/:service', {
                    'service': name
                });
            }
        }else if (name == 'sensor')
        {
            $state.go('app.sensor', {});
        }else{
            $cordovaDialogs.alert(
                $translate('alert_serviceError'),
                $translate('alert_serviceTitle'), 
                $translate('alert_button')
            );
        }
    };

    $scope.roomOpen = function(roomName,componentName){
        /*if(controlSystem.roomExist(name)){
            $state.go('app.service/room/:room', {
                'room': name
            });
        }*/

        if(controlSystem.roomExist(roomName)){
            if(componentName)
            {
                var component = controlSystem.roomComponents(roomName).filter(function(comp){
                    return comp.name != undefined && comp.name === componentName;
                })
                if(component && component[0] && component[0].service != "sensor")
                {
                    $state.go('app.service/room/detail/:room/:component', {
                        'room': roomName,
                        'component': componentName
                    });
                }
            }
            else
            {
                $state.go('app.service/room/detail/:room', {
                    'room': roomName
                });
            }
        }
        
    };

    //AC_20180514
    $scope.sensorOpen = function(roomName, componentName){
        if(controlSystem.roomExist(roomName)){
            if(componentName)
            {
                var component = controlSystem.roomComponents(roomName).filter(function(comp){
                    return comp.name != undefined && comp.name === componentName;
                })
                if(component && component[0] && component[0].service === "sensor")
                {
                    $state.go('app.sensor/detail/:room/:component', {
                        'room': roomName,
                        'component': componentName
                    });
                }
            }
            else
            {
                if(controlSystem.roomServices(roomName).filter(function(item){return item === "sensor";}).length > 0){
                    $state.go('app.sensor/detail/:room', {
                        'room': roomName
                    });
                }
            }
        }
        
    };

    $scope.showTimer = function(){
        if(controlSystem.getEnableLevel() < controlSystem.getSogliaLivello("L1"))
            return false;
        else
            return true;
    };

    $scope.getDirectionIcon = function(direction)
    {
        if(direction == "Up")
            return "ion-arrow-up-a";
        else if(direction == "Down")
            return "ion-arrow-down-a";
    }

    $rootScope.$on('login_failed',function(){
        if(!$rootScope.connectionErrorDialogOpen)
        {
            $rootScope.connectionErrorDialogOpen = true;
            $cordovaDialogs.alert(
                $translate('alert_loginError'),
                $translate('alert_loginErrorTitle'),
                $translate('alert_button'));
        }
    });

    $rootScope.$on('central_disconnected',function(){
        if(!$rootScope.connectionErrorDialogOpen)
        {
            $rootScope.connectionErrorDialogOpen = true;
            $cordovaDialogs.alert(
                $translate('alert_centralDisconnected'),
                $translate('alert_centralDisconnectedTitle'),
                $translate('alert_button'));
        }
    })


    //AC_20180123
    $scope.stopUpdateState = function()
    {
        $interval.cancel($rootScope.getStateInterval);
        $rootScope.getStateInterval = undefined;
    }

    $scope.startUpdateState = function()
    {
        if($rootScope.getStateInterval != undefined){
            $scope.stopUpdateState();
        }
        controlSystem.updateState();
        $rootScope.getStateInterval = $interval(controlSystem.updateState, _STATE_UPDATE_TIMEOUT);
    }

    //AC_20180123
    $scope.stopUpdateEnable = function()
    {
        $interval.cancel($rootScope.getEnableInterval);
        $rootScope.getEnableInterval = undefined;
    }

    $scope.startUpdateEnable = function()
    {
        if($rootScope.getEnableInterval != undefined){
            $scope.stopUpdateEnable();
        }
        controlSystem.getAllCfgEnables().then(function(){
            $rootScope.$emit('enUpdated');
        })
        $rootScope.getEnableInterval = $interval(function(){
            controlSystem.getAllCfgEnables().then(function(){
                $rootScope.$emit('enUpdated');
            })
        }
        ,_ENABLE_UPDATE_TIMEOUT);
    }

    //AC_20180320
    $scope.FilterTimers = function(timers)
    {
        return (timers)?timers.filter(function(item){return !(item.condition == 'e' || item.condition == 'd')}):[]
    }

    //AC_20180326
    $scope.getMUTherm = function(comp){               
        var unit = "";
        var sensorList;
        switch(comp.useMode)
        {
            case 'therm_auto':
            {
                sensorList = (comp.thermAuto) ? comp.thermAuto.sensorList : undefined;
            }break;
            case 'therm_manual':
            {
                sensorList = (comp.thermManu) ? comp.thermManu.sensorList : undefined;
            }break;
            case 'therm_int':
            {
                sensorList = (comp.thermInte) ? comp.thermInte.sensorList : undefined;
            }break;
            case 'cond_auto':
            {
                sensorList = (comp.condAuto) ? comp.condAuto.sensorList : undefined;
            }break;
            case 'cond_manual':
            {
                sensorList = (comp.condManu) ? comp.condManu.sensorList : undefined;
            }break;
            case 'cond_int':
            {
                sensorList = (comp.condInte) ? comp.condInte.sensorList : undefined;
            }break;
        }
        if(sensorList != undefined)
        {
            Object.keys(sensorList).forEach(function (key) {
                if(sensorList[key] !== 'function'){
                    if(sensorList[key].element != undefined){//condizione del sensore [cond (<,>,etc...)][soglia]
                        unit = ( sensorList[key].element.unit ? sensorList[key].element.unit : '' )
                    }
                }
            });
            return unit;
        }

        return 'n.p.' ;
    };

    //AC_20180514
    $rootScope.selectedNotification = undefined;
    $rootScope.notificationReceived = false;
    $rootScope.blockAutoConnect = false;

    $scope.ConnectToDevice = function(connectionName)
    {
        $rootScope.selectedDevice = connectionName;
        $scope.initialization();
        //$state.go('app.initialization');
    };

    $rootScope.handlePushNotification = function()
    {
        if($rootScope.notificationReceived && $rootScope.selectedNotification != undefined)
        {
            var query = 'SELECT DISTINCT name FROM configuration where api=?';
            database
            .execute(query, [$rootScope.selectedNotification.central_id])
            .then(function(result){
                if(result.rows.length > 0) {
                    $rootScope.notificationReceived = false;
                    $scope.ConnectToDevice(result.rows.item(0).name);
                }
                else
                {
                    $rootScope.notificationReceived = false;
                    $rootScope.selectedNotification = undefined
                }

            }, function(error){
                $rootScope.notificationReceived = false;
                $rootScope.selectedNotification = undefined
            });
        }
    }

    $rootScope.$on('pushNotification',function(event)
    {
        $rootScope.blockAutoConnect = true;        
        var lastNotification = fcm.getLastNotification();
        if(lastNotification){
            var confirmPopup = SecuredPopups.show('confirm',{
                title: ((lastNotification.values.title)? lastNotification.values.title : $translate('push_notification_alternative_title')),
                template:   '<div>' +
                            $translate('push_notification_central_id') + ':' +
                            '<br>' + lastNotification.central_id + 
                            '<br><br>' +
                            $translate('push_notification_body_msg') + ':' +
                            '<br>' +
                            ((lastNotification.values.body) ? lastNotification.values.body : $translate('push_notification_alternative_msg')) +
                            '</div>',
                cssClass:'pushPopup',
                cancelText: $translate('push_notification_hide'),
                okText: $translate('push_notification_show')
            });

            confirmPopup.then(function(res) {
                $rootScope.blockAutoConnect = false;
                
                if(res) {
                    $rootScope.notificationReceived = true;
                    $rootScope.selectedNotification = lastNotification;
                    switch($ionicHistory.currentStateName())
                    {
                        case 'app.connection_list':
                        {
                            $rootScope.handlePushNotification();
                        }break;
                        default:
                        {
                            $scope.connectionList();
                        }
                    }
                } else {
                    $rootScope.notificationReceived = false;
                }
            });
        }
    });
});